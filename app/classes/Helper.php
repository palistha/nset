<?php

class Helper {

    public static function GetHours($appendSelect, $selectText, $selectValue) {
        $values = array();
        $init = 0;
        if ($appendSelect) {
            $values[$selectValue] = $selectText;
            $init++;
        }
        for ($i = $init, $j = 1; $i <= (11 + $init), $j <= 12; $i++, $j++) {
            $values[$j] = Helper::GetPaddedNumber($j, 2, '0');
        }
        return $values;
    }


    public static function buildTree(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = \Helper::buildTree($elements, $element['id']);
            if ($children) {
              $element['is_leaf'] = '0';
              $element['children'] = array($children);

            }
            else
              $element['is_leaf'] = '1';
            $index = $element['content_heading'];
            unset($element['content_heading']);
            $branch[$index] = array($element);
        }
    }

    return $branch;
  }

    
    
    public static function GetSubHeader($parentId, $counter = "1") {
      $menuList = "";
      $newCount = $counter+1;
      if($parentId!=0) {
        $listLevHeader = ContentModel::headerContentMenus($parentId);
//        echo count($listLevHeader)."<br />";
        if(count($listLevHeader) > 0) {

          $menuList .= '<ul class="dropdown-menu" role="menu">';

          $subCount = 1;
          foreach($listLevHeader as $lev2Content) {
            $listLev2Header = ContentModel::headerContentMenus($lev2Content->id);
            if(count($listLev2Header)>0) {

              $li2Class = "";

            }
            else
              $li2Class = "";
            
            $exClass = ($subCount=="1") ? "first" : (($subCount==count($listLevHeader)) ? "last" : "");
            $menuList .= '<li class="'.$li2Class.' '.$exClass.'"><a href="'. URL($lev2Content->content_url).'">'. strtoupper($lev2Content->content_heading).'</a>';
            $menuList .= Helper::GetSubHeader($lev2Content->id, $newCount);
            $menuList .= '</li>';     
            $subCount++;
          }
          
          $menuList .='</ul>';
        }
        
      }
      return $menuList;
    }
    // get subheader in content
     public static function GetSubContent($parenturl,$parentId, $counter = "1") {
      $menuList = "";
      $newCount = $counter+1;
      if($parentId!=0) {
        $listLevHeader = ContentModel::headerContentMenus($parentId);
//        echo count($listLevHeader)."<br />";
        if(count($listLevHeader) > 0) {

          $menuList .= '<ul class="dropdown-menu" role="menu">';

          $subCount = 1;
          foreach($listLevHeader as $lev2Content) {
            $listLev2Header = ContentModel::headerContentMenus($lev2Content->id);
            if(count($listLev2Header)>0) {
              $li2Class = "parent-item haschild";
            }
            else
              $li2Class = "";
            
            $exClass = ($subCount=="1") ? "first" : (($subCount==count($listLevHeader)) ? "last" : "");
            $menuList .= '<li class="'.$li2Class.' '.$exClass.'"><a href="'. URL($parenturl.'/'.$lev2Content->content_url).'">'. $lev2Content->content_heading.'</a>';
            $menuList .= Helper::GetSubHeader($lev2Content->id, $newCount);
            $menuList .= '</li>';     
            $subCount++;
          }
          
          $menuList .='</ul>';
        }
        
      }
      return $menuList;
    }
    
    public static function GetProgramSubHeader($parentId) {
      $menuList = "";
//      $newCount = $counter+1;
      if($parentId!=0) {
        $listLevHeader = ContentModel::headerContentMenus($parentId);
//        echo count($listLevHeader)."<br />";
        if(count($listLevHeader) > 0) {
          $menuList .= '<ul class="dropdown-menu">';
          $subCount = 1;
          foreach($listLevHeader as $lev2Content) {
            $menuList .= '<li >
                             <a href="'. URL($lev2Content->content_url).'" class="ln-tr link">'. $lev2Content->content_heading.'</a>';
            $menuList .= '</li>';     
            $subCount++;
          }
          
          $menuList .='</ul>';
        }
        
      }
      return $menuList;
    }

}
