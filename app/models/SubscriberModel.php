<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SubscriberModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'subscriber';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	public static function getAllSubscriber()
	{
		return $subscribers = DB::table('subscriber')
							->where('is_active', 1)
							->get();	
	}
	public static function createSubsciber($data)
	{
		return $result = DB::table('subscriber')->insert($data);							
	}

}
