<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class VideoModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'video';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public $limit;
	
	public static function getAllVideos()
	{
		return $videos = DB::table('video')
                    ->where('deleted', '=', 0)
                    ->get();		
	}
  
  public static function getActiveVideos()
	{
		return $videos = DB::table('video')
                    ->leftJoin('media', 'video.id', '=', 'media.video_id')
                    ->where('video.deleted', '=', 0)
                    ->where('video.is_active', '=', 1)
                    ->orderby('video.created_at', 'desc')
                    ->groupby('video.id')
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $videos = DB::table('video')
                    ->where('deleted', '=', 0)
					->where('video_url', '=', $url)
                    ->first();		
	}
		
	public static function getAllVideo()
	{
		$videos = DB::table('video')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->get();
		return $videos;
	}

	public static function getAllVideoFrontend()
	{
		$videos = DB::table('video')
			->where('deleted', 0)
			->where('is_active', 1)
			->orderby('created_at', 'desc')
			->paginate(6);
		return $videos;
	}
	
	public static function getHomePageVideos()
	{
		$limit = 8;
		$videos = DB::table('video')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $videos;
	}
  
  public static function getLatestVideo()
	{
		return $result = DB::table('video')
						->where('deleted', '0')
						->where('is_active', '1')
						->orderBy('id','DESC')
						->first();
	}
	public static function getDetailByUrl($url)
	{
		return $result = DB::table('video')
						->where('video_url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
}
