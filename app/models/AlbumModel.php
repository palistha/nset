<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AlbumModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'album';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public $limit;

	public function galleries(){
		 return $this->hasMany('MediaModel', 'album_id');
	}
	
	public static function getAllAlbums()
	{
		return $albums = DB::table('album')
                    ->where('deleted', '=', 0)
                    ->get();		
	}
  
  public static function getActiveAlbums()
	{
		return $albums =DB::table('album')
//                    ->leftJoin('media', 'album.id', '=', 'media.album_id')
                    ->where('album.deleted', '=', 0)
                    ->where('album.is_active', '=', 1)
                    ->orderby('album.created_at', 'desc')
                    // ->groupby('album.id')
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $albums = DB::table('album')
                    ->where('deleted', '=', 0)
					           ->where('album_url', '=', $url)
                    ->first();		
	}
		
	public static function getAllAlbum()
	{
		$albums = DB::table('album')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->get();
		return $albums;
	}
	
	public static function getHomePageAlbums()
	{
		$limit = 8;
		$albums = DB::table('album')
							->leftJoin('media', 'album.id', '=', 'media.album_id')
							->where('album.deleted', '=', 0)
							->where('album.is_active', '=', 1)
							->where('media.deleted', '=', 0)
							->where('media.is_active', '=', 1)
//							->orderby('album.created_at', 'desc')
							->orderby(DB::raw('RAND()'))
							->take($limit)
							->get();
		return $albums;
	}
  
  public static function getDetailByUrl($url)
	{
		return $result = DB::table('album')
						->where('album_url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}

	public static function getSingleImageByAlbumId($albumId)
	{
		$image = DB::table('media')->where('album_id',$albumId)->where('media_attachment','!=','NULL')->where('is_active','1')->where('deleted','0')->first();
		return $image;

	}
}
