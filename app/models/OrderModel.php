<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class OrderModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	public static function getAllOrders()
	{
		$query = "select t1.id,t1.created_at,t1.order_total_price,t1.order_status,t2.user_first_name,t2.user_last_name,t2.username
					from tbl_orders t1 join tbl_users t2 on
					t1.order_customer_id = t2.id where t1.deleted = '0'
					order by t1.created_at desc";
		return DB::select($query);
	}
	
	public static function getOrderDetail($id)
	{
		$query = "select t1.id,t1.order_total_price,t2.product_quantity,t3.product_heading,t3.product_price,t3.product_url
					from tbl_orders t1 join tbl_ordered_product t2 on t1.id = t2.order_id
						join tbl_product t3 on t2.product_id = t3.id 
					where t1.id = $id";
		return DB::select($query);
	}
}
