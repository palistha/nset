<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class EventsModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'events';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public $limit;
	 
	public static function getAllEvents()
	{
		return $Events = DB::table('events')
                    ->where('deleted', '0')
                    ->get();		
	}
	
	public static function getEventsbyDate($date)
	{
		$Events = DB::table('events')
                    ->where('events_sdate', $date)
                    ->where('deleted', '0')
                    ->get();	
     $body = "<strong>Events List:</strong> <br>";
    foreach ($Events as $event) {
    $body .= '<strong>'.$event->events_heading.'</strong>  --   ';
    $body .= $event->events_short_description.'<a href="cevents/'.$event->events_url.'" >    Read More..</a><br>';
   
     	 	}	
  	return $body;
	}
	
	public static function checkExist($url)
	{	 
		return $Eventss = DB::table('events')
                    ->where('deleted', '0')
					->where('events_url', $url)
                    ->first();		
	}
	
	public static function getEventsDetailByUrl($url)
	{
		return $result = DB::table('events')
						->where('events_url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
	
	public static function getAllActiveEvents()
	{
		$limit = ConstantModel::getDetailByName('per_page_news')->constant_value;
		return $Events = DB::table('events')
                    ->where('deleted', '0')
                    ->where('is_active', '1')
					->orderby('created_at', 'desc')
                    ->paginate($limit);	
	}
  
  public static function getAllActiveEventsForHome()
	{
    $limit = '2';
//		$limit = ConstantModel::getDetailByName('per_page_Events')->constant_value;
		return $Events = DB::table('events')
                    ->where('deleted', '0')
                    ->where('is_active', '1')
                    ->orderby('created_at', 'desc')
                    ->paginate($limit);	
	}  

	public static function latestEventsSidebar()
	{
		return $Events = DB::table('events')
						->where('deleted', '0')
						->where('is_active', '1')
						->orderby('events_sdate', 'desc')
						->take(4)
						->get();
	}
	
	
	
	public static function HomePagePhotoEvents()
	{
		return $Events = DB::table('events')
						->where('deleted', '0')
						->where('is_active', '1')
						->where('events_type', 'P')
						->orderby('events_date', 'desc')
						->take(10)
						->get();
	}
	
	

}
