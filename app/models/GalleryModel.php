<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class GalleryModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gallery';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllGalleries()
	{
		return DB::table('gallery')
					->where('deleted', 0)
                    ->get();		
	}
	
	public static function getActiveGalleries()
	{
		$limit = ConstantModel::getDetailByName('per_page_images')->constant_value;
		return DB::table('gallery')
					->where('is_active', 1)
					->where('deleted', 0)
					->paginate($limit);
	}
	
}
