<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AchievementTypeModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'achievement_type';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public $limit;
	
	public static function getAllAchievementTypes()
	{
		return $albums = DB::table('achievement_type')
                    ->where('deleted', '=', 0)
                    ->get();		
	}
  
  public static function getActiveAchievementTypes()
	{
		return $albums = DB::table('achievement_type')
                    ->leftJoin('achievement', 'achievement_type.id', '=', 'achievement.type_id')
                    ->where('achievement_type.deleted', '=', 0)
                    ->where('achievement_type.is_active', '=', 1)
                    ->orderby('achievement_type.created_at', 'desc')
                    ->groupby('achievement_type.id')
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $albums = DB::table('achievement_type')
                    ->where('deleted', '=', 0)
					->where('achievement_url', '=', $url)
                    ->first();		
	}
		
	public static function getAllAchievementType()
	{
		$albums = DB::table('achievement_type')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->get();
		return $albums;
	}
	
	public static function getHomePageAchievementTypes()
	{
		$limit = 8;
		$albums = DB::table('achievement_type')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $albums;
	}
  
  public static function getDetailByUrl($url)
	{
		return $result = DB::table('achievement_type')
						->where('achievement_url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
	 public static function getDetailByID($id)
	{
		return $result = DB::table('achievement_type')
						->where('id', $id)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
}
