<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Gregwar\Captcha\CaptchaBuilder;

class FunctionModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'countries';

	/*
	| fetch all country list
	*/
	public static function getAllCountries()
	{
		$query = "Select * from tbl_countries";	
		return DB::select($query);
	}
	
	public static function sendEmail($receiverEmail, $subject, $content, $replyTo = "", $ccBCC = array())
	{	
		$senderEmail	= ConstantModel::getDetailByName('no_reply_email')->constant_value;
		$senderName		= ConstantModel::getDetailByName('from_name')->constant_value;
		$sitePath		= ConstantModel::getDetailByName('site_path')->constant_value;
		$siteName		= ConstantModel::getDetailByName('site_name')->constant_value;
		
		$data = array(
					'logopath' => 'includes/client/images/logo.png',
					'content' => $content,
					'footer' => ' Copyright '.date('Y').' ',
					'sitepath' => $sitePath,
					'sitename' => $siteName,
					);
		
		try{
			Mail::send('emails.email', $data, function($message) use ($senderEmail, $senderName, $receiverEmail, $subject)
			{
				$message->from($senderEmail, $senderName);
				$message->to($receiverEmail)
				->subject($subject);
			});
			return count(Mail::failures()) > 0 ? false : true;
		
		} catch(Swift_RfcComplianceException $e) {
			return false;
		}
		
	}
	
	public static function charcut($text, $max_length = 450, $tail = ' ...')
	{
		$tail_len = strlen($tail);
		if(strlen($text) > $max_length) {
			$tmp_text = substr($text, 0, $max_length - $tail_len);
			if(substr($text, $max_length - $tail_len, 1) == ' ') {
				$text = $tmp_text;
		}
		else {
			$pos = strrpos($tmp_text, ' ');
			$text = substr($text, 0, $pos);
		}
		$text = $text . $tail;
		}
		return $text;
	}
	
	/*
	| get last executable query
	|
	| return string
	*/
	public static function get_last_query()
	{
		$queries = DB::getQueryLog();
		$sql = end($queries);
			
		if( ! empty($sql['bindings']))
		{
			$pdo = DB::getPdo();
			foreach($sql['bindings'] as $binding)
			{
				$sql['query'] =
				preg_replace('/\?/', $pdo->quote($binding), $sql['query'], 1);
			}
		}
		return $sql['query'];	
	}
	


	/*
	| create captcha image dynamically
	*/
	public static function create_captcha()
	{
		$builder = new CaptchaBuilder;
		$builder->build();		
		$captcha = $builder->inline();
		Session::put('phrase', $builder->getPhrase());
		return $captcha;
	}
	
}
