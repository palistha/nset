<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class BrandModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'brands';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	public static function getAllBrands()
	{
		return $categories = BrandModel::where('deleted', 0)
                          ->get();	
	}
	
	/**
	* check whether url exist or not
	*/
	public static function checkExist($url)
	{	 
		return $categories = BrandModel::where('deleted', 0)
                          ->where('url', $url)
                          ->first();		
	}
	
	public static function getAllActiveBrands()
	{
		return $usertypes = BrandModel::where('is_active', 1)
                        ->where('deleted', 0)
                        ->get();	
	}
	public static function getBrandName($id)
	{
		return $brand = BrandModel::where('id', $id)
                        ->pluck('name');
	}
	
	/*
	|--------------------------------------------------------------------------------
	| Below functions are used on front end 
	|---------------------------------------------------------------------------------
	*/
	
	/*
	* fetch categories at header
	* @return array
	*/
	public static function getBrandsHeader()
	{
		return $categories = BrandModel::where('deleted', 0)
                          ->where('is_active', 1)
                          ->take(8)
                          ->get();		
	}
	
	public static function getDetailByUrl($url)
	{
		$brand = BrandModel::where('url', $url)
                  ->where('deleted', 0)
                  ->where('is_active', 1)
                  ->first();
		return ($brand != null) ? $brand : false;
	}
	
	/*
	* fetch categories at home page
	* @return array
	*/
	public static function getHomePageBrands()
	{
		return $categories = BrandModel::where('deleted', 0)
                          ->where('is_active', 1)
                          ->take(8)
                          ->get();
	}

}
