<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ConstantModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;	
    

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'constant';
 
	public static function getAllConstants()
	{
		return $constants = DB::table('constant')
                    ->where('deleted', 0)
                    ->get();
	}
	
	public static function getDetailByName($constantName)
	{
		return $result = DB::table('constant')
						->where('constant_heading', $constantName)
						->first();
	}
}
