<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class RetailerModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;	
    

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'retailer';
 
	public static function getAllRetailers()
	{
		$query = "select t1.city_heading,t2.*
					from tbl_city t1 join tbl_retailer t2
					on t1.id = t2.city_id
					where t1.deleted = '0' and t2.deleted = '0'";
		return DB::select($query);
	}
	
	public static function getDetailByName($retailerName)
	{
		return $result = DB::table('retailer')
						->where('retailer_heading', $retailerName)
						->first();
	}
}
