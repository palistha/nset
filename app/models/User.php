<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	
	/**
	* get all users except deleted
	*
	* @return array
	*/
	public static function getAllUsers()
	{
		$query = "select t1.id,t1.user_first_name,t1.user_last_name,t1.username,t1.is_active,t1.updated_at,t2.user_type_name
					from tbl_users t1 join tbl_user_type t2 on
					t1.user_type = t2.id where t1.deleted = '0' and t1.id !='1' ";
		return DB::select($query);
		
	}
	
	/**
	* check whether user exist or not
	*
	* @return array
	*/
	public static function checkExist($username)
	{	 
		return $result = DB::table('users')
                    ->where('deleted', 0)
					->where('username', $username)
                    ->first();		
	}
	
	public static function createUser($data)
	{
		return $result = DB::table('users')->insert($data);
	}
	
	public static function updateUser($data, $id)
	{
		return $result = DB::table('users')
						->where('id', $id)
						->update($data);
	}
	
	/**
	* publish user on client side
	*/
	public static function publishUser($id)
	{
		return DB::table('user')
					->where('user_id', $id)
					->update(array('is_active' => 1));
	}
	
	/**
	* unpublish user on client side
	*/
	public static function unpublishUser($id)
	{
		return DB::table('user')
					->where('user_id', $id)
					->update(array('is_active' => 0));
	}
	
	/**
	* user is not deleted from database but is invisible on both admin and client side
	*/
	public static function deleteUser($id)
	{
		return DB::table('user')
					->where('user_id', $id)
					->update(array('is_active' => 0,'deleted' => 1));
	}
	
	public static function getdetail($id)
	{
		return $result = DB::table('users')
						->where('id', $id)
						->first();
	}
	public static function changePassword($newPassword)
	{
		return $result = DB::table('users')
								->where('id',Auth::user()->id)
								->update(
									array('password' => Hash::make($newPassword),
											'password_changed_on' => date("Y-m-d H:i:s")
								));
	}
}
