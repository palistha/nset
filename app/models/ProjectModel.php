<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ProjectModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllProjects()
	{
		return ProjectModel::where('deleted', 0)->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $testimonials = ProjectModel::where('deleted', 0)
                            ->where('url', $url)
                            ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = ProjectModel::where('testimonial_id', $id)
                        ->first();
	}
	
	public static function getTestimonials()
	{
		$testimonials = ProjectModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->get();
		return $testimonials;
	} 
	
	public static function getActiveProjects($type='')
	{
		$limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
		$testimonials = ProjectModel::where('deleted', 0)
                    ->where('is_active', 1);
    if($type && $type!='all') {
      $testimonials->where('type', $type);
    }                
    $testimonials->orderby('updated_at', 'desc')
                    ->take($limit);
    $result = $testimonials->get();
		return $result;
	}
  
  public static function getProjectDetailByUrl($url)
	{
		return $result = ProjectModel::where('url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
	
}
