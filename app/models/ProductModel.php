<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ProductModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public $limit;
	 
	public static function getAllProducts()
	{
		return $products = DB::table('product')
                    ->where('deleted', 0)
                    ->get();		
	}
		
	/**
	* check whether url exist or not
	*/
	public static function checkExist($url)
	{	 
		return $products = DB::table('product')
                    ->where('deleted', 0)
					->where('product_url', $url)
                    ->first();		
	}
		
	/*--------------------- Below functions are used on front end -----------------------------------------------*/
		
	/**
	* fetch popular product with highest views home page
	* @return array
	*/
	public static function homePagePopularProducts()
	{
		$limit = ConstantModel::getDetailByName('home_page_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('product_views', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/**
	* fetch featured products
	* @return array
	*/
	public static function homePageFeaturedProducts()
	{
		$limit = ConstantModel::getDetailByName('home_page_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('is_featured', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/**
	* fetch special products
	* @return array
	*/
	public static function homePageSpecialProducts()
	{
		$limit = ConstantModel::getDetailByName('home_page_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('is_special', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/**
	* fetch latest product
	* @return array
	*/
	public static function homePageLatestProducts()
	{
		$limit = ConstantModel::getDetailByName('home_page_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/**
	* fetch latest product
	* @return array
	*/
	public static function getRelatedProducts($catId)
	{
		$limit = ConstantModel::getDetailByName('per_page_related_product')->constant_value;
		$products = DB::table('product')
						->where('category_id', $catId)
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/*
	* fetch product detail using url
	*/
	public static function getDetailByUrl($url)
	{
		$productData = DB::table('product')
						->where('product_url', $url)
						->where('deleted', 0)
						->where('is_active', 1)
						->first();
		return $productData;
	}
	
	/*
	* fetch product list category wise
	*/
	public static function getProductsCategoryWise($catId)
	{
		$limit = ConstantModel::getDetailByName('per_page_product')->constant_value;
		$products = DB::table('product')
						->where('category_id', $catId)
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('created_at', 'desc')
						->paginate($limit);
		return $products;
	}
  
  /*
	* fetch product list category wise
	*/
	public static function listProducts($brandId = '', $catId = '')
	{
		$limit = ConstantModel::getDetailByName('per_page_product')->constant_value;
		$result = ProductModel::where('deleted', 0)
                ->where('is_active', 1);
    
    if($brandId) {
      $result->where('brand_id', $brandId);
    }
       
    if($catId) {
      $result->where('category_id', $catId);
    }
		$y = $result->orderBy('created_at', 'desc')->paginate($limit);
    return $y;

	}
	
	/**
	* fetch popular product with highest views collection page sidebar
	* @return array
	*/
	public static function sidebarPopularProducts()
	{
		$limit = ConstantModel::getDetailByName('sidebar_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('product_views', 'desc')
						->take($limit)
						->get();
		return $products;
	}
	
	/**
	* fetch all active product
	* @return array
	*/
	public static function getActiveProducts()
	{
		$limit = ConstantModel::getDetailByName('per_page_product')->constant_value;
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('product_views', 'desc')
						->paginate($limit);
		return $products;
	}
	
	/**
	* fetch latest one product category wise
	* @return array
	*/
	public static function getLatestProductHomePage($id)
	{
		return $products = DB::table('product')
							->where('deleted', 0)
							->where('is_active', 1)
							->where('category_id', $id)
							->orderby('created_at', 'desc')
							->take(1)
							->get();
	}
	
	/**
	* increase view counter of product
	*/
	public static function CounterIncrement($id)
	{
		return $product = DB::table('product')
							->where('id', $id)
							->increment('product_views');
	}
	
	/**
	* fetch footer active product
	* @return array
	*/
	public static function getFooterProducts()
	{
		$products = DB::table('product')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('product_views', 'desc')
						->take(2)
						->get();
		return $products;
	}
}
