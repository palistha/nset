<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ProfileModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'client_profile';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllProfiles()
	{
		return ProfileModel::where('deleted', 0)->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $testimonials = ProfileModel::where('deleted', 0)
                            ->where('url', $url)
                            ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = ProfileModel::where('testimonial_id', $id)
                        ->first();
	}
	
	public static function getTestimonials()
	{
		$testimonials = ProfileModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->get();
		return $testimonials;
	} 
	
	public static function getActiveProfiles()
	{
		$limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
		$testimonials = ProfileModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->take($limit)
                    ->get();
		return $testimonials;
	}
  
  public static function getProfileDetailByUrl($url)
	{
		return $result = ProfileModel::where('url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
	
}
