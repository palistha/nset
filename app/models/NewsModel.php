<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class NewsModel extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public $limit;

    public static function getAllNews()
    {
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->get();
    }

    public static function checkExist($url)
    {
        return $newss = DB::table('news')
            ->where('deleted', '0')
            ->where('news_url', $url)
            ->first();
    }

    public static function getNewsDetailByUrl($url)
    {
        return $result = DB::table('news')
            ->where('news_url', $url)
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->first();
    }

    public static function getAllActiveNews($type = 'N')
    {
        $limit = ConstantModel::getDetailByName('per_page_news')->constant_value;
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', $type)
            ->orderby('created_at', 'desc')
            ->paginate($limit);
    }

    public static function getAllActiveNewsForHome($type = 'N')
    {
        $limit = '6';
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', $type)
            ->orderby('news_date', 'DESC')
            ->paginate($limit);
    }

    public static function getLatestAnnouncementForHome($type = 'N', $take = 3)
    {
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', $type)
            ->orderby('news_date', 'DESC')
            ->take($take)
            ->get();
    }

    public static function getAllActiveAnnouncementForHome($type = 'A')
    {
        $limit = '10';
//		$limit = ConstantModel::getDetailByName('per_page_news')->constant_value;
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', $type)
            ->orderby('created_at', 'desc')
            ->paginate($limit);
    }

    public static function getAllActiveEventsForHome($type = 'A')
    {
        $limit = '6';
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', $type)
            ->where('show_in_home', '1')
            ->orderby('news_date', 'desc')
            ->paginate($limit);
    }


    public static function latestNewsSidebar()
    {
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', 'N')
            ->orderby('news_date', 'desc')
            ->take(4)
            ->get();
    }

    public static function getLatestNewsHomePage()
    {
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', 'N')
            ->orderby('news_date', 'desc')
            ->take(1)
            ->get();
    }

    public static function HomePagePhotoNews()
    {
        return $news = DB::table('news')
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->where('news_type', 'P')
            ->orderby('news_date', 'desc')
            ->take(10)
            ->get();
    }


    public static function getEventsbyDate($date)
    {
        $Events = DB::table('news')
            ->where('news_date', $date)
            ->get();
        $body = "<strong>Events List:</strong> <br>";
        foreach ($Events as $event) {
            $body .= '<strong>' . $event->news_heading . '</strong>  --   ';
            $body .= substr($event->news_short_description, 0, 80) . '<a href="events/' . $event->news_url . '" >    Read More..</a><br>';

        }
        return $body;
    }


}
