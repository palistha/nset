<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PublicationModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'publication';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllPublications()
	{
		return $publications = DB::table('publication')
                    ->where('deleted', '=', 0)
                    ->orderby('id', 'desc')
                    ->get();		
	}
  
  public static function getActivePublications($type='')
	{
		$limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
		$testimonials = PublicationModel::where('deleted', 0)
						->where('is_active', 1)
						->where('publication_type', $type)
						->orderby('updated_at', 'desc')
						->take($limit)
						->get();
		return $testimonials;
	}
	
	public static function checkExist($url)
	{	 
		return $publications = DB::table('publication')
                    ->where('deleted', '=', 0)
					->where('publication_url', '=', $url)
                    ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = DB::table('publication')
						->where('publication_id', $id)
						->orderby('id', 'desc')
						->first();
	}
	public static function getFirstNotice($type)
	{
		return $result = DB::table('publication')
						->where('is_active', 1)
						->where('publication_type', $type)
						->orderby('id', 'desc')
						->first();
	}
	
	/*
	* home page publications
	* @return array
	*/
	public static function getHomePagePublications($type)
	{
		$publications = DB::table('publication')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('publication_type', $type)
						->orderby('created_at', 'desc')
						->take(3)
						->get();
		return $publications;
	}
	
}
