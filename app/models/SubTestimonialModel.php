<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SubTestimonialModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'subtestimonials';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function getAllSubTestimonial()
	{
		return $subtestimonials = DB::table('subtestimonials')
		->where('deleted', 0)
		->where('is_active', '1')
		->orderby('updated_at', 'DESC')
		->get();		
	}
	
	public static function getTestimonialDetailByUrl($testimonial_company)
    { 
        return $result = DB::table('subtestimonials')
            ->where('testimonial_company', $testimonial_company)
            ->where('deleted', '0')
            ->where('is_active', '1')
            ->first();
    }

}
