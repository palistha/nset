<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class PortfolioModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'portfolio';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllPortfolios()
	{
		return PortfolioModel::where('deleted', 0)->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $testimonials = PortfolioModel::where('deleted', 0)
                            ->where('url', $url)
                            ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = PortfolioModel::where('testimonial_id', $id)
                        ->first();
	}
	
	public static function getTestimonials()
	{
		$testimonials = PortfolioModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->get();
		return $testimonials;
	} 
	
	public static function getActivePortfolios()
	{
		$limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
		$testimonials = PortfolioModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->take($limit)
                    ->get();
		return $testimonials;
	}
  
  public static function getPortfolioDetailByUrl($url)
	{
		return $result = PortfolioModel::where('url', $url)
						->where('deleted', '0')
						->where('is_active', '1')
						->first();
	}
	
}
