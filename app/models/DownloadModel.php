<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class DownloadModel extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'download';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function getAllDownloads()
    {
        return $downloads = DB::table('download')
            ->where('deleted', '=', 0)
            ->get();
    }

    public static function getActiveDownloads($type = '')
    {
        $limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
        $testimonials = DownloadModel::where('deleted', 0)
            ->where('is_active', 1)
            ->where('download_type', $type)
            ->orderby('updated_at', 'desc')
            ->take($limit)
            ->get();
        return $testimonials;
    }


    public static function getActiveDownloadsByCategory($category)
    {
        $downloads = DownloadModel::where('deleted', 0)
            ->where('is_active', 1)
            ->where('category_id', $category)
            ->orderby('created_at', 'desc')
            ->get();
        return $downloads;
    }

    public static function checkExist($url)
    {
        return $downloads = DB::table('download')
            ->where('deleted', '=', 0)
            ->where('download_url', '=', $url)
            ->first();
    }

    public static function getdetail($id)
    {
        return $result = DB::table('download')
            ->where('download_id', $id)
            ->first();
    }

    /*
    * home page downloads
    * @return array
    */
    public static function getHomePageDownloads($type,$take)
    {
        $downloads = DB::table('download')
            ->where('deleted', 0)
            ->where('is_active', 1)
            ->where('download_type', $type)
            ->orderby('created_at', 'desc')
            ->take($take)
            ->get();
        return $downloads;
    }

}
