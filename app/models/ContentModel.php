<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class ContentModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'content';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public function parent()
    {
        return $this->belongsTo('ContentModel', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('ContentModel', 'parent_id', 'id');
    }

	public static function getAllContents()
	{
		return $contents = DB::table('content')
                    ->where('deleted', '=', 0)
                    ->orderBy('created_at', 'desc')
                    ->get();		
	}
	/*create home page contetn*/
	public static function getWelcomeContent()
	{
		return $contents = DB::table('content')
                    ->where('show_in_welcome', '=', 1)               
                    ->where('deleted', '=', 0)               
                    ->orderby('id','desc')
                    ->take(4)
                    ->get();		
	}
		
	/**
	* check whether url exist or not
	*/
	public static function checkExist($url)
	{	 
		return $contents = DB::table('content')
                    ->where('deleted', 0)
					->where('content_url', $url)
                    ->first();		
	}
		
	public static function content_list_for_contentEntry($parentId, $level, $selected)
	{
		$contents = DB::table('content')
					->where('parent_id', $parentId)
					->where('is_active', 1)
					->orderby('id','asc')
					->get();		
		
		$returnList = '<select class="m-wrap span12" name="parent_id" id="parent_id">';
		$returnList .= '<option value="0" selected>Parent Itself</option>';
		foreach($contents as $content) {
			if($selected > 0 && $selected == $content->id) {
				$selected_option = 'selected="selected"';
            }
            else {
				$selected_option = "";
            }
				  
			$returnList .= '<option value="' . $content->id . '" ' . $selected_option . '>' . 
							str_repeat('&nbsp;>>&nbsp;',$level) . $content->content_heading . '</option>';
			$returnList .= self::subcontent_list_for_contentEntry($content->id, $level+1, $selected);
			
		}
		$returnList .= '</select>';
		
		return $returnList;
	}
	
	public static function subcontent_list_for_contentEntry($parentId, $level, $selected = 0)
	{
		$subcontents = DB::table('content')
					->where('parent_id', $parentId)
					->where('is_active', 1)
					->orderby('id','desc')
					->get();		
		$returnList = '';
		foreach($subcontents as $subcontent) {
			if($selected > 0 && $selected == $subcontent->id) {
				$selected_option = 'selected="selected"';
            }
            else {
				$selected_option = "";
            }
			$returnList .= '<option value="' . $subcontent->id . '" ' . $selected_option . '>' . 
							str_repeat('&nbsp;>>&nbsp;',$level) . $subcontent->content_heading . '</option>';
			$returnList .= self::subcontent_list_for_contentEntry($subcontent->id, $level+1, $selected);
		}
		return $returnList;
	}
	
	/*--------------------- Below functions are used on front end -----------------------------------------------*/
	
	/**
	* fetch all detail of content
	*/
	public static function getDetailByUrl($url)
	{
		$contentData = DB::table('content')
							->where('content_url', $url)
							->where('deleted', 0)
							->where('is_active', 1)
							->first();
		return $contentData;
	}
  
  /**
	* fetch all detail of content
	*/
	public static function getDetailById($id)
	{
		$contentData = DB::table('content')
							->where('id', $id)
							->where('deleted', 0)
							->where('is_active', 1)
							->first();
		return $contentData;
	}
	
	/**
	* fetch contentlist at footer
	*/
	public static function footerContents()
	{
		$contents = DB::table('content')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('show_in', 'LIKE', '%2%')
						->take(18)
						->get();
		return $contents;
	}
	
	/**
	* fetch contentlist at header
	*/
	public static function headerContentMenus($parentId = 0, $singleMenu = 0, $singleMenuId = '')
	{
		$contents = DB::table('content')
						->where('deleted', 0)
            ->where('parent_id', $parentId)
						->where('is_active', 1)
						->where('show_in', 'LIKE', '%1%')
						->orderby('show_order','ASC');
    if($singleMenu && $singleMenuId) {
      $contents->where('id', $singleMenuId);
      

    }
    else{
      $programMenuId = ConstantModel::getDetailByName('menu_program_id')->constant_value;
      if($programMenuId)
        $contents->where('id', '!=', $programMenuId);
    }
		$contents->take(10);
		$result = $contents->get();
		return $result;
	}

	/**
	 * fetch contentlist at header
	 */

	public static function footerContentMenus($parentId = 0, $take=5)
	{
		$contents = DB::table('content')
			->where('deleted', 0)
			->where('parent_id', $parentId)
			->where('is_active', 1)
			->where('show_in', 'LIKE', '%2%')
			->orderby('show_order','ASC');

		$contents->take($take);
		$result = $contents->get();
		return $result;
	}



	public static function getSidebarContent()
	{
            $sudebarContent = ContentModel::with('children')->where('parent_id',0)->where('is_active',1)->where('deleted', 0)->orderBy('id','asc')->take(3)->get();
            return $sudebarContent;

    }


	
}
