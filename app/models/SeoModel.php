<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SeoModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;	
    

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'seo';
 
	public static function getAllSeos()
	{
		return $seos = DB::table('seo')
                    ->where('deleted', 0)
                    ->get();
	}
	
	public static function getDetailBypage($page)
	{
		return $result = DB::table('seo')
						->where('seo_heading', $page)
						->first();
	}
}
