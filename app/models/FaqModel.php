<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class FaqModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;	
    

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'faqs';
 
	public static function getAllFaqs()
	{
		return $faqs = FaqModel::where('deleted', 0)
                            ->get();
	}
	
	public static function checkExist($heading)
	{	 
		return $faqs = FaqModel::where('deleted', '=', 0)
                    ->where('faq_heading', '=', $heading)
                    ->first();		
	}
	public static function createFaq($data)
	{
		return $result = FaqModel::insert($data);
	}
	
	public static function updateFaq($data, $id)
	{
		return $result = FaqModel::where('faq_id', $id)
                      ->update($data);
	}
	
	public static function getdetail($id)
	{
		return $result = FaqModel::where('faq_id', $id)
                      ->first();
	}
	
	public static function getFaqs()
	{
		$faqs = FaqModel::where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
						->get();
		return $faqs;
	} 
	
	public static function getActiveFaqs()
	{
    // $limit = ConstantModel::getDetailByName('home_page_why_uc')->constant_value;
		$faqs = FaqModel::where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
            // ->limit($limit)
						->get();
		return $faqs;
	}
	
}
