<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class WhyUsModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'why_us';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAll()
	{
		return WhyUsModel::where('deleted', 0)->get();		
	}
	
	public static function checkExist($heading)
	{	 
		return $testimonials = DB::table('testimonial')
                    ->where('deleted', 0)
					->where('testimonial_heading', $heading)
                    ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = DB::table('testimonial')
						->where('testimonial_id', $id)
						->first();
	}
	
	public static function getTestimonials()
	{
		$testimonials = DB::table('testimonial')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
						->get();
		return $testimonials;
	} 
	
	public static function getActiveTestimonials()
	{
		$limit = ConstantModel::getDetailByName('home_page_testimonials')->constant_value;
		$testimonials = DB::table('testimonial')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
						->take($limit)
						->get();
		return $testimonials;
	}
	
}
