<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class AchievementModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'achievement';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllAchievements($type)
	{
		return $achievements = DB::table('achievement')
                    ->where('deleted', 0)
                    ->where('type', $type)
                    ->get();		
	}
  
  public static function getActiveAchievements($type)
	{
		return $achievements = DB::table('achievement')
                    ->where('is_active', 1)
                    ->where('deleted', 0)
                    ->where('type', $type)
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $achievements = DB::table('achievement')
                    ->where('deleted', '=', 0)
					->where('achievement_url', '=', $url)
                    ->first();		
	}
		
	public static function getImageListFooter()
	{
		$achievements = DB::table('achievement')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby(DB::raw('RAND()'))
						->take(4)
						->get();
		return $achievements;
	}
	
	public static function getAllAchievement($id)
	{
		$achievements = DB::table('achievement')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('album_id', $id)
						->orderby('updated_on', 'desc')
						->get();
		return $achievements;
	}
	
	public static function getHomeAchievementsAlbumWise($id)
	{
		$query = "select t1.album_url,t2.*
					from tbl_album t1 join tbl_achievement t2
					on t1.id = t2.album_id
					where t1.id = '$id'
					limit 0,4";
		return DB::select($query);
		
	}
		public static function getAllTypeAchievements()
	{
		$query = "select * from tbl_achievement_type";
		return DB::select($query);
		
	}
	
}
