<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class CityModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;	
    

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'city';
 
	public static function getAllCities()
	{
		return $cities = DB::table('city')
                    ->where('deleted', 0)
                    ->get();
	}
	
	public static function getAllActiveCities()
	{
		return $cities = DB::table('city')
                    ->where('deleted', 0)
                    ->where('is_active', 1)
                    ->get();
	}
	
	public static function getDetailByName($cityName)
	{
		return $result = DB::table('city')
						->where('city_heading', $cityName)
						->first();
	}
}
