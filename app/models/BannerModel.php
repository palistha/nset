<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class BannerModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'banner';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllBanners()
	{
		return $banners = DB::table('banner')
                    ->where('deleted', '=', 0)
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $banners = DB::table('banner')
                    ->where('deleted', '=', 0)
					->where('banner_url', '=', $url)
                    ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = DB::table('banner')
						->where('banner_id', $id)
						->first();
	}
	
	/*
	* home page banners
	* @return array
	*/
	public static function getHomePageBanners($type,$take=5)
	{
		$banners = DB::table('banner')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('banner_type', $type)
						->orderby('created_at', 'desc')
						->take($take)
						->get();
		return $banners;
	}
	
}
