<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class MediaModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'media';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllMedias($aid)
	{
		return $medias = DB::table('media')
                    ->where('deleted', 0)
                    ->where('album_id', $aid)
                    ->get();		
	}
  
  public static function getActiveMedias($aid)
	{
		return $medias = DB::table('media')
                    ->where('is_active', 1)
                    ->where('deleted', 0)
                    ->where('album_id', $aid)
                    ->get();		
	}
	
	public static function checkExist($url)
	{	 
		return $medias = DB::table('media')
                    ->where('deleted', '=', 0)
					->where('media_url', '=', $url)
                    ->first();		
	}
		
	public static function getImageListFooter()
	{
		$medias = DB::table('media')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby(DB::raw('RAND()'))
						->take(4)
						->get();
		return $medias;
	}
	
	public static function getAllMedia($id)
	{
		$medias = DB::table('media')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('album_id', $id)
						->orderby('updated_on', 'desc')
						->get();
		return $medias;
	}
	
	public static function getHomeMediasAlbumWise($id)
	{
		$query = "select t1.album_url,t2.*
					from tbl_album t1 join tbl_media t2
					on t1.id = t2.album_id
					where t1.id = '$id'
					limit 0,4";
		return DB::select($query);
		
	}
	
}
