<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ManagementTeamModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'managementteams';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public static function getAllManagementTeam()
	{
		return $testimonials = DB::table('managementteams')
		->where('deleted', 0)
		->where('is_active', '1')
		->orderby('updated_at', 'DESC')
		->get();		
	}
	
	
}
