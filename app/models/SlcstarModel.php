<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class SlcstarModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'slc_star';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllSlcstars()
	{
		return $slcstars = DB::table('slc_star')
                    ->where('deleted', 0)
                    ->get();		
	}
	
	public static function checkExist($heading)
	{	 
		return $slcstars = DB::table('slc_star')
                    ->where('deleted', 0)
					->where('slc_star_heading', $heading)
                    ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = DB::table('slc_star')
						->where('slc_star_id', $id)
						->first();
	}
	
	public static function getSlcstars()
	{
		$slcstars = DB::table('slc_star')
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
						->get();
		return $slcstars;
	} 
	
	public static function getActiveSlcstars()
	{
//		$limit = ConstantModel::getDetailByName('home_page_slcstars')->constant_value;
		$slcstars = DB::table('slc_star')
//            ->where('type', $type)
						->where('deleted', 0)
						->where('is_active', 1)
						->orderby('updated_at', 'desc')
//						->take($limit)
						->get();
		return $slcstars;
	}
	
}
