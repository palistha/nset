<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ServiceModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'services';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	 
	public static function getAllServices()
	{
		return ServiceModel::where('deleted', 0)->get();		
	}
	
	public static function checkExist($heading)
	{	 
		return $testimonials = ServiceModel::where('deleted', 0)
                            ->where('testimonial_heading', $heading)
                            ->first();		
	}
		
	public static function getdetail($id)
	{
		return $result = ServiceModel::where('testimonial_id', $id)
                        ->first();
	}
	
	public static function getTestimonials()
	{
		$testimonials = ServiceModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->get();
		return $testimonials;
	} 
	
	public static function getActiveServices()
	{
		$limit = ConstantModel::getDetailByName('per_page_news')->constant_value;
		$testimonials = ServiceModel::where('deleted', 0)
                    ->where('is_active', 1)
                    ->orderby('updated_at', 'desc')
                    ->take($limit)
                    ->get();
		return $testimonials;
	}
	
}
