<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class CategoryModel extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	
	public static function getAllCategories()
	{
		return $categories = DB::table('category')
							->where('deleted', 0)
							->get();	
	}
	
	/**
	* check whether url exist or not
	*/
	public static function checkExist($url)
	{	 
		return $categories = DB::table('category')
                    ->where('deleted', 0)
					->where('category_url', $url)
                    ->first();		
	}
	
	public static function getAllActiveCategories()
	{
		return $usertypes = DB::table('category')
							->where('is_active', 1)
							->where('deleted', 0)
							->get();	
	}
	public static function getCategoryName($id)
	{
		return $category = DB::table('category')
								->where('category_id', $id)
								->pluck('category_heading');
	}
  
  public static function getMenuCategories()
  {
    return $category = DB::table('category')
            ->where('is_active', '1')
            ->where('deleted', '0')
            ->where('show_in_menu', '1')->get();
  }
	
	/*
	|--------------------------------------------------------------------------------
	| Below functions are used on front end 
	|---------------------------------------------------------------------------------
	*/
	
	/*
	* fetch categories at header
	* @return array
	*/
	public static function getCategoriesHeader()
	{
		return $categories = DB::table('category')
								->where('deleted', 0)
								->where('is_active', 1)
								->take(8)
								->get();		
	}
	
	public static function getDetailByUrl($url)
	{
		$category = DB::table('category')
						->where('category_url', $url)
						->where('deleted', 0)
						->where('is_active', 1)
						->first();
		return ($category != null) ? $category : false;
	}
	
	/*
	* fetch categories at home page
	* @return array
	*/
	public static function getHomePageCategories()
	{
		return $categories = DB::table('category')
								->where('deleted', 0)
								->where('is_active', 1)
								->take(8)
								->get();
	}


	public static function category_list_for_CategoryEntry($parentId, $level, $selected)
	{
		$contents = DB::table('category')
			->where('parent_id', $parentId)
			->where('is_active', 1)
			->orderby('id','asc')
			->get();

		$returnList = '<select class="m-wrap span12" name="parent_id" id="parent_id">';
		$returnList .= '<option value="0" selected>Parent Itself</option>';
		foreach($contents as $content) {
			if($selected > 0 && $selected == $content->id) {
				$selected_option = 'selected="selected"';
			}
			else {
				$selected_option = "";
			}

			$returnList .= '<option value="' . $content->id . '" ' . $selected_option . '>' .
				str_repeat('&nbsp;>>&nbsp;',$level) . $content->category_heading . '</option>';

		}
		$returnList .= '</select>';

		return $returnList;
	}

    public static function category_list_for_DownloadEntry($parentId, $level, $selected)
    {
        $contents = DB::table('category')
            ->where('parent_id', $parentId)
            ->where('is_active', 1)
            ->orderby('id','asc')
            ->get();

        $returnList = '<select class="m-wrap span12" name="parent_id" id="parent_id">';
        foreach($contents as $content) {
            if($selected > 0 && $selected == $content->id) {
                $selected_option = 'selected="selected"';
            }
            else {
                $selected_option = "";
            }

            $returnList .= '<option value="' . $content->id . '" ' . $selected_option . '>' .
                str_repeat('&nbsp;>>&nbsp;',$level) . $content->category_heading . '</option>';
            $returnList .= self::subcategory_list_for_DownloadEntry($content->id, $level+1, $selected);

        }
        $returnList .= '</select>';

        return $returnList;
    }

    public static function subcategory_list_for_DownloadEntry($parentId, $level, $selected = 0)
    {
        $subcontents = DB::table('category')
            ->where('parent_id', $parentId)
            ->where('is_active', 1)
            ->orderby('id','desc')
            ->get();
        $returnList = '';
        foreach($subcontents as $subcontent) {
            if($selected > 0 && $selected == $subcontent->id) {
                $selected_option = 'selected="selected"';
            }
            else {
                $selected_option = "";
            }
            $returnList .= '<option value="' . $subcontent->id . '" ' . $selected_option . '>' .
                str_repeat('&nbsp;>>&nbsp;',$level) . $subcontent->category_heading . '</option>';
            $returnList .= self::subcategory_list_for_DownloadEntry($subcontent->id, $level+1, $selected);
        }
        return $returnList;
    }

    public function parent()
    {
        return $this->belongsTo('CategoryModel', 'parent_id');
    }


}
