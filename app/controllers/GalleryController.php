<?php

class GalleryController extends BaseController {
	
	public function getGalleries()
    {
        $galleryList = GalleryModel::getAllGalleries();
		
		//var_dump($galleryList); die();
		$i = 1;
		$gallerys = '';
		foreach($galleryList as $gallery) {
			$gallerys .= '<tr class="odd gradeX">';
			$gallerys .= '<td>'.$i++.'</td>';
			$gallerys .= '<td>'.$gallery->gallery_title.'</td>';
			
			if(file_exists("uploads/gallery/".$gallery->gallery_attachment) && $gallery->gallery_attachment!=''){
				$gallerys .= '<td><img src="'.'../uploads/gallery/'.$gallery->gallery_attachment.'" style="width: 200px; height: 120px;"></td>';
			} else {
				$gallerys .= '<td><img src="'.'../uploads/noimage.jpg'.'" style="width: 200px; height: 120px;"></td>';
			}
			if($gallery->is_active=='1') { 
				$gallerys .= '<td>'.'<a href="javascript:void(0)" class="unpublish-gallery" id="'.$gallery->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$gallerys .= '<td>'.'<a href="javascript:void(0)" class="publish-gallery" id="'.$gallery->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$gallerys .= '<td>'.$gallery->updated_at.'</td>';
            $gallerys .= '<td>'.
						'<a href="gallery/edit/'.$gallery->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-gallery" id="'.$gallery->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$gallerys .= '</tr>';
		}

        return View::make('admin.gallery.list', array('gallerys' => $gallerys));
    }
	
	public function getAddGallery()
	{
		return View::make('admin.gallery.add');
	}
	
	public function addGallery()
	{
		$files = Input::file('userfile');
		$count = 0;
		$valid = true;
		
		if(count($files) > 10 || count($files) == 0) {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'There is no image to upload or maximum numbers of images exceeds 10.');
			return View::make('admin.gallery.add');
		} else {
			for($i=0; $i<count($files); $i++) {
				$imageFile = $files[$i];
				$destinationPath = 'uploads/gallery/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();
					
					if (preg_match($rEFileTypes, $extension) == false) {
						$valid = false;
					} else if ($size > $maximum_filesize) {
						$valid = false;
					} else {
						$new_image_name = "gallery_" . time() . "_" . rand(0,9999);
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);
						$valid = true;
					}			
				}
				if($valid) {
					$logo = isset($attachment) ? $new_image_name . $extension : NULL;
					$objectGallery = new GalleryModel();
					$objectGallery->is_active = Input::get('is_active');
					$objectGallery->updated_by = Auth::user()->id;
					
					if($logo != '') {
						$objectGallery->gallery_attachment = $logo;
					}				
					$objectGallery->save();
					$count++;
				}
			}
			if($count > 0) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Gallery successfully added');
				return View::make('admin.gallery.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.gallery.add');				
			}
		}
	}
	
	public function getEditGallery($id)
	{
		$galleryDetail = GalleryModel::find($id);		
		try {
			if($galleryDetail->deleted == 0) {
				return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editGallery()
	{	
		$galleryId = Input::get('gallery_id');
		$galleryDetail = GalleryModel::find($galleryId);
		$rules = array('gallery_title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
		} 
		else {
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/gallery/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "gallery_" . time() . "_" . rand(0,9999);
					
				if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
				} else  {
					if (is_file('uploads/gallery/'.$galleryDetail->gallery_attachment)) { 
						unlink('uploads/gallery/'.$galleryDetail->gallery_attachment);
					}
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} 				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			
			$objectGallery = GalleryModel::find($galleryId);
			$objectGallery->gallery_title = Input::get('gallery_title');
			$objectGallery->gallery_caption = Input::get('gallery_caption');
			$objectGallery->is_active = Input::get('is_active');
			$objectGallery->updated_by = Auth::user()->id;
			
			if($logo != '') {
				if(file_exists('uploads/gallery/'.$objectGallery->gallery_attachment) && $objectGallery->gallery_attachment != '') {
					unlink('uploads/gallery/'.$objectGallery->gallery_attachment);
				}
				$objectGallery->gallery_attachment = $logo;
			}				
			$objectGallery->save();
			
			if($objectGallery->id) {
				$galleryDetail = GalleryModel::find($galleryId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Gallery successfully updated');
				return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.gallery.edit')->with('galleryDetail',$galleryDetail);
			}		
		}
	}
	
	public function publishGallery()
	{ 
		$galleryId = Input::get('gallery_id');
		$objectGallery = GalleryModel::find($galleryId);
		$objectGallery->updated_by = Auth::user()->id;		
		$objectGallery->is_active = 1;
		$objectGallery->save();
		
		if($objectGallery->id) {
			$array = array('message' => 'Gallery is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishGallery()
	{ 
		$galleryId = Input::get('gallery_id');
		$objectGallery = GalleryModel::find($galleryId);
		$objectGallery->updated_by = Auth::user()->id;		
		$objectGallery->is_active = 0;
		$objectGallery->save();
		
		if($objectGallery->id) {
			$array = array('message' => 'Gallery is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteGallery()
	{ 
		$galleryId = Input::get('gallery_id');
		$objectGallery = GalleryModel::find($galleryId);
		$objectGallery->updated_by = Auth::user()->id;		
		$objectGallery->is_active = 0;
		$objectGallery->deleted = 1;
		$objectGallery->save();
		
		if($objectGallery->id) {
			$array = array('message' => 'Gallery is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

}