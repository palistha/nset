<?php

class CityController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getCities()
    {
        $cityList = CityModel::getAllCities();
		$i = 1;
		$cities = '';
		foreach($cityList as $city) {
			$cities .= '<tr class="odd gradeX">';
			$cities .= '<td>'.$i++.'</td>';
			$cities .= '<td>'.$city->city_heading.'</td>';
			if($city->is_active=='1') { 
				$cities .= '<td>'.'<a href="javascript:void(0)" class="unpublish-city" id="'.$city->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$cities .= '<td>'.'<a href="javascript:void(0)" class="publish-city" id="'.$city->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$cities .= '<td>'.
						'<a href="city/edit/'.$city->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-city" id="'.$city->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$cities .= '</tr>';
		}

        return View::make('admin.city.list', array('cities' => $cities));
    }
	
	public function getAddCity()
	{
		return View::make('admin.city.add');
	}
	
	public function addCity()
	{
		$rules = array('city_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.city.add');
		} 
		else {
			$objectCity = new CityModel();
			$objectCity->city_heading = preg_replace('/[^A-Za-z0-9\-]/', '_', strtolower(Input::get('city_heading')));
			$objectCity->is_active = 1;
			$objectCity->updated_by = Auth::user()->id;			
			$objectCity->save();
			
			if($objectCity->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'city successfully added');
				return View::make('admin.city.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.city.add');
			}						
		}
	}
	
	public function getEditCity($id)
	{
		$cityDetail = CityModel::find($id);		
		try {
			if($cityDetail->deleted == 0) {
				return View::make('admin.city.edit')->with('cityDetail',$cityDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editCity()
	{	
		$cityId = Input::get('city_id');
		$cityDetail = CityModel::find($cityId);
		$rules = array('city_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.city.edit')->with('cityDetail',$cityDetail);
		} 
		else {
			$objectCity = CityModel::find($cityId);
			$objectCity->city_heading = preg_replace('/[^A-Za-z0-9\-]/', '_', strtolower(Input::get('city_heading')));
			$objectCity->is_active = 1;
			$objectCity->updated_by = Auth::user()->id;			
			$objectCity->save();
				
			if($objectCity->id) {
				$cityDetail = CityModel::find($cityId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'City successfully updated');
				return View::make('admin.city.edit')->with('cityDetail',$cityDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.city.edit')->with('cityDetail',$cityDetail);
			}					
		}
	}
	
	public function publishCity()
	{ 
		$cityId = Input::get('city_id');
		$objectCity = CityModel::find($cityId);
		$objectCity->is_active = 1;
		$objectCity->save();
		
		if($objectCity->id) {
			$array = array('message' => 'City is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishCity()
	{ 
		$cityId = Input::get('city_id');
		$objectCity = CityModel::find($cityId);
		$objectCity->is_active = 0;
		$objectCity->save();
		
		if($objectCity->id) {
			$array = array('message' => 'City is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteCity()
	{ 
		$cityId = Input::get('city_id');
		$objectCity = CityModel::find($cityId);
		$objectCity->is_active = 0;
		$objectCity->deleted = 1;
		$objectCity->save();
		
		if($objectCity->id) {
			$array = array('message' => 'City is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

}
