<?php

class ServiceController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getServices()
    {
        $serviceList = ServiceModel::getAllServices();
		
		//var_dump($serviceList); die();
		$i = 1;
		$services = '';
		foreach($serviceList as $service) {
			$services .= '<tr class="odd gradeX">';
			$services .= '<td>'.$i++.'</td>';
			$services .= '<td>'.$service->title.'</td>';
			if($service->is_active=='1') { 
				$services .= '<td>'.'<a href="javascript:void(0)" class="unpublish-service" id="'.$service->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$services .= '<td>'.'<a href="javascript:void(0)" class="publish-service" id="'.$service->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$services .= '<td>'.$service->updated_at.'</td>';
            $services .= '<td>'.
						'<a href="service/edit/'.$service->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-service" id="'.$service->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$services .= '</tr>';
		}

        return View::make('admin.service.list', array('services' => $services));
    }
	
	public function getAddService()
	{
		return View::make('admin.service.add');
	}
	
	public function addService()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.service.add');
		} 
		else {
			$serviceTitle = Input::get('title');
			$serviceUrl = Input::get('service_url');
			$serviceUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $serviceUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/services/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
			
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "services" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.service.add');
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.service.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectService = new ServiceModel();
			$objectService->title = Input::get('title');
      $objectService->type = Input::get('type');
			$objectService->short_desc = Input::get('short_desc');
			$objectService->desc = Input::get('desc');
			$objectService->is_active = Input::get('is_active');
			$objectService->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectService->attachment = $logo;
			}
			$objectService->save();
			
			if($objectService->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Service successfully added');
				return View::make('admin.service.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.service.add');
			}
						
		}
	}
	
	public function getEditService($id)
	{
		$serviceDetail = ServiceModel::find($id);		
		try {
			if($serviceDetail->deleted == 0) {
				return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editService()
	{	
		$serviceId = Input::get('service_id');
		$serviceDetail = ServiceModel::find($serviceId);
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
		} 
		else {
			$serviceTitle = Input::get('title');
			$serviceUrl = Input::get('service_url');
			$serviceUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $serviceUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/services/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "services" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
				
				$objectService = ServiceModel::find($serviceId);
				$objectService->title = Input::get('title');
        $objectService->type = Input::get('type');
				$objectService->short_desc = Input::get('short_desc');
				$objectService->desc = Input::get('desc');
				$objectService->is_active = Input::get('is_active');
				$objectService->updated_by = Auth::user()->id;
				
				if($logo != '') {
					$objectService->attachment = $logo;
				}
				$objectService->save();
				
				if($objectService->id) {
					$serviceDetail = ServiceModel::find($serviceId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Service successfully updated');
					return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.service.edit')->with('serviceDetail',$serviceDetail);
				}
					
		}
	}
	
	public function publishService()
	{ 
		$serviceId = Input::get('service_id');
		$objectService = ServiceModel::find($serviceId);
		$objectService->is_active = 1;
		$objectService->save();
		
		if($objectService->id) {
			$array = array('message' => 'Service is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishService()
	{ 
		$serviceId = Input::get('service_id');
		$objectService = ServiceModel::find($serviceId);
		$objectService->is_active = 0;
		$objectService->save();
		
		if($objectService->id) {
			$array = array('message' => 'Service is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteService()
	{ 
		$serviceId = Input::get('service_id');
		$objectService = ServiceModel::find($serviceId);
		$objectService->is_active = 0;
		$objectService->deleted = 1;
		$objectService->save();
		
		if($objectService->id) {
			$array = array('message' => 'Service is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
