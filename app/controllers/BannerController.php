<?php

class BannerController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getBanners()
    {
        $bannerList = BannerModel::getAllBanners();
		
		//var_dump($bannerList); die();
		$i = 1;
		$banners = '';
		foreach($bannerList as $banner) {
			$banners .= '<tr class="odd gradeX">';
			$banners .= '<td>'.$i++.'</td>';
			$banners .= '<td>'.$banner->banner_caption.'</td>';
			
			if(file_exists("uploads/banners/".$banner->banner_attachment) && $banner->banner_attachment!=''){
				$banners .= '<td><img src="'.'../uploads/banners/'.$banner->banner_attachment.'" style="width: 200px; height: 120px;"></td>';
			} else {
				$banners .= '<td><img src="'.'../uploads/noimage.jpg'.'" style="width: 200px; height: 120px;"></td>';
			}
			if($banner->is_active=='1') { 
				$banners .= '<td>'.'<a href="javascript:void(0)" class="unpublish-banner" id="'.$banner->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$banners .= '<td>'.'<a href="javascript:void(0)" class="publish-banner" id="'.$banner->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$banners .= '<td>'.$banner->updated_at.'</td>';
            $banners .= '<td>'.
						'<a href="banner/edit/'.$banner->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-banner" id="'.$banner->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$banners .= '</tr>';
		}

        return View::make('admin.banner.list', array('banners' => $banners));
    }
	
	public function getAddBanner()
	{
		return View::make('admin.banner.add');
	}
	
	public function addBanner()
	{
		$rules = array('banner_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.banner.add');
		} 
		else {						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/banners/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 5 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "banners" . "_" . time();
					
			if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
				$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
			} else if (preg_match($rEFileTypes, $extension) == false) {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Warning : Invalid Image File!');
				return View::make('admin.banner.add');
			} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 5MB!");
					return View::make('admin.banner.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectBanner = new BannerModel();
			$objectBanner->banner_caption = Input::get('banner_caption');
			$objectBanner->banner_description = Input::get('banner_description');
			$objectBanner->banner_url = Input::get('banner_url');
			$objectBanner->banner_type = Input::get('banner_type');
			$objectBanner->is_active = Input::get('is_active');
			$objectBanner->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectBanner->banner_attachment = $logo;
			}				
			$objectBanner->save();
			
			if($objectBanner->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Banner successfully added');
				return View::make('admin.banner.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.banner.add');				
			}			
		}
	}
	
	public function getEditBanner($id)
	{
		$bannerDetail = BannerModel::find($id);		
		try {
			if($bannerDetail->deleted == 0) {
				return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editBanner()
	{	
		$bannerId = Input::get('banner_id');
		$bannerDetail = BannerModel::find($bannerId);
		$rules = array('banner_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
		} 
		else {
						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/banners/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 5 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "banners" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 5MB!");
					return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			
			$objectBanner = BannerModel::find($bannerId);
			$objectBanner->banner_caption = Input::get('banner_caption');
			$objectBanner->banner_description = Input::get('banner_description');
			$objectBanner->banner_url = Input::get('banner_url');
			$objectBanner->banner_type = Input::get('banner_type');
			$objectBanner->is_active = Input::get('is_active');
			$objectBanner->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectBanner->banner_attachment = $logo;
			}				
			$objectBanner->save();
			
			if($objectBanner->id) {
				$bannerDetail = BannerModel::find($bannerId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Banner successfully updated');
				return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.banner.edit')->with('bannerDetail',$bannerDetail);
			}		
		}
	}
	
	public function publishBanner()
	{ 
		$bannerId = Input::get('banner_id');
		$objectBanner = BannerModel::find($bannerId);
		$objectBanner->updated_by = Auth::user()->id;		
		$objectBanner->is_active = 1;
		$objectBanner->save();
		
		if($objectBanner->id) {
			$array = array('message' => 'Banner is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishBanner()
	{ 
		$bannerId = Input::get('banner_id');
		$objectBanner = BannerModel::find($bannerId);
		$objectBanner->updated_by = Auth::user()->id;		
		$objectBanner->is_active = 0;
		$objectBanner->save();
		
		if($objectBanner->id) {
			$array = array('message' => 'Banner is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteBanner()
	{ 
		$bannerId = Input::get('banner_id');
		$objectBanner = BannerModel::find($bannerId);
		$objectBanner->updated_by = Auth::user()->id;		
		$objectBanner->is_active = 0;
		$objectBanner->deleted = 1;
		$objectBanner->save();
		
		if($objectBanner->id) {
			$array = array('message' => 'Banner is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

}
