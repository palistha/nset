<?php

class OrderController extends BaseController {
	
	public function getOrders()
    {
        $orderList = OrderModel::getAllOrders();		
		
		$i = 1;
		$orders = '';
		foreach($orderList as $order) {
			$selected = $disabled = '';
			$orders .= '<tr class="odd gradeX">';
			$orders .= '<td>'.$i++.'</td>';
			$orders .= '<td>'.$order->created_at.'</td>';			
			$orders .= '<td>'.$order->user_first_name .' '. $order->user_last_name . '(' . $order->username . ')' .'</td>';         
			$orders .= '<td>'.$order->order_total_price .'</td>';   
			$selected = ($order->order_status == '1') ? 'selected="selected"' : '';
			$disabled = ($order->order_status == '1') ? 'disabled="disabled"' : '';
			$orders .= '<td><div id="changestatus_'. $order->id .'">'.
							'<select name="order_status[]" class="order_status" id="order-'. $order->id .'" '. $disabled .'>'.
								'<option value="0" '. $selected .' >To be dispatch </option>'.
								'<option value="1" '. $selected .' >Dispatched</option>'.
							'</select>'.
						'</td>'; 
			$orders .= '<td><a href="order-detail/'. $order->id .'" title="View" ><i class="icon-eye-open"></i> View </a>&nbsp;&nbsp;</td>';
		} 

        return View::make('admin.order.list', array('orders' => $orders));
    }
	
	public function getOrderDetail($id)
	{
		$orderList = OrderModel::getOrderDetail($id);		
		$i = 1;
		$totalPrice = 0;
		$totalQuantity = 0;
		$orders = '';
		foreach($orderList as $order) {
			$orders .= '<tr class="odd gradeX">';
			$orders .= '<td>'.$i++.'</td>';
			$orders .= '<td><a href="'. Config::get('constants.SITE_PATH') . 'product/'. $order->product_url .'" target="_blank">'.$order->product_heading.'</a></td>';			
			$orders .= '<td>'.$order->product_quantity .'</td>';         
			$orders .= '<td>'.$order->product_price.'</td>';
			$orders .= '<td>'.$order->product_price * $order->product_quantity .'</td>';
			$totalPrice += $order->product_price * $order->product_quantity;
			$totalQuantity += $order->product_quantity;
		}
		$orders .= '<tr>';
		$orders .= '<td>'. $i .'</td>';
		$orders .= '<td>Total</td>';
		$orders .= '<td>' . $totalQuantity. '</td>';
		$orders .= '<td></td>';
		$orders .= '<td>'. $totalPrice .'</td>';
		$orders .= '</tr>';
		return View::make('admin.order.detail', array('orders' => $orders));
	}
	
	public function orderChangeStatus()
	{
		$orderId = Input::get('order_id');
		$objectOrder = OrderModel::find($orderId);
		$objectOrder->order_status = Input::get('order_status');
		$objectOrder->save();
		
		if($objectOrder->id) {
			$returnHtml = '<select name="order_status[]" class="order_status" id="order-'. $orderId .'" disabled >'.
								'<option value="0"  >To be dispatch </option>'.
								'<option value="1" selected >Dispatched</option>'.
							'</select>';
			$array = array(
					'message' => 'Order status is changed successfully.',
					'returnHtml' => $returnHtml,
					'flag' => true
				);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}