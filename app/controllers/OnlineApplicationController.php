<?php

class OnlineApplicationController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getApplications()
    {
        $applicationLists = OnlineApplicationModel::orderBy('user_id','DESC')->get();
		$i = 1;
		$applications = '';
		foreach($applicationLists as $application) {
			$applications .= '<tr class="odd gradeX">';
			$applications .= '<td>'.$i++.'</td>';
			$applications .= '<td>'.$application->full_name.'</td>';
			$applications .= '<td>'.$application->email.'</td>';
			$applications .= '<td>'.$application->contact_no.'</td>';
			$applications .= '<td>'.$application->choose.'</td>';
			$applications .= '<td>'.$application->created_at.'</td>';
			$applications .= '<td>'.
						'<a href="'. route('admin.application.detail',$application->user_id).'"><i class="icon-eye-open"></i>View</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="deleteOnlineApplication" id="'.$application->user_id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$applications .= '</tr>';
		}

        return View::make('admin.application.list', array('applications' => $applications));
    }

	

	
	public function deleteApplication()
	{
		$applicationId = Input::get('application_id');
		$objectApplication = OnlineApplicationModel::find($applicationId);
		$objectApplication->deleted_at = date('Y-m-d H:i:s');
		$objectApplication->save();

		if($objectApplication->user_id) {
			$array = array('message' => 'Application is deleted successfully.', 'flag' => true);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

	public function getApplicationDetail($applicationId)
	{
		$applicantInfo = OnlineApplicationModel::find($applicationId);
		return View::make('admin.application.detail', array('applicantInfo' => $applicantInfo));
	}

}
