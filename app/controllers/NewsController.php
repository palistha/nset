<?php

class NewsController extends BaseController
{
    protected $thumb_w;
    protected $thumb_h;
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function __construct(NewsModel $albums){
        $this->albums = $albums;
        $this->destinationpath = './uploads/news/';
        $this->thumbpath = $this->destinationpath.'thumbs/';
        $this->imagePrefix = 'album_';
        $this->thumb_w = 360;
        $this->thumb_h = 270;
        $this->thumb_sec_w =  1920;
        $this->thumb_sec_h = 608;
    }
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function getNews()
    {
        $newsList = NewsModel::getAllNews();

        //var_dump($newsList); die();
        //
        $i = 1;
        $newss = '';

        foreach ($newsList as $news) {
            $type = '';
            if ($news->news_type == 'E') {
                $type = 'News & Events';
            } elseif ($news->news_type == 'N') {
                $type = 'Latest Notice';
            } elseif ($news->news_type == 'A') {
                $type = 'Events';
            }


            $newss .= '<tr class="odd gradeX">';
            $newss .= '<td>' . $i++ . '</td>';
            $newss .= '<td>' . $news->news_heading . '</td>';
            $newss .= '<td>' . $type . '</td>';
            if ($news->is_active == '1') {
                $newss .= '<td>' . '<a href="javascript:void(0)" class="unpublish-news" id="' . $news->id . '" >' .
                    '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp' . '</td>';
            } else {
                $newss .= '<td>' . '<a href="javascript:void(0)" class="publish-news" id="' . $news->id . '" >' .
                    '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp' . '</td>';
            }
            $newss .= '<td>' . $news->updated_at . '</td>';
            $newss .= '<td>' .
                '<a href="news/edit/' . $news->id . '"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;' .
                '<a href="javascript:void(0)" class="delete-news" id="' . $news->id . '" >' .
                '<i class="icon-trash" ></i> Delete</a>' . '</td>';
            $newss .= '</tr>';
        }

        return View::make('admin.news.list', array('newss' => $newss));
    }

    public function getAddNews()
    {
        return View::make('admin.news.add');
    }

    public function addNews()
    {
        $rules = array('news_heading' => 'required',
            'news_url' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('admin.news.add');
        } else {
            $newsHeading = Input::get('news_heading');
            $newsUrl = Input::get('news_url');
            $newsUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $newsUrl);

            $newsExist = NewsModel::checkExist($newsUrl);
            if (count($newsExist) != 0) {
                $message = 'news <b>' . $newsHeading . '</b> with url <b>' . $newsUrl . '</b> is already exist';
                Session::flash('class', 'alert alert-error');
                Session::flash('message', $message);
                return View::make('admin.news.add');
            } else {
                $imageFile = Input::file('userfile');
                $destinationPath = 'uploads/news/';
                $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
                $maximum_filesize = 1 * 1024 * 1024;

                if($imageFile) {
                $extension = strrchr($imageFile->getClientOriginalName(), '.');
                $new_file_name = $this->imagePrefix . time();
                $fullFilename = $new_file_name . $extension;

                $attachment = $imageFile->move($this->destinationpath, $fullFilename);
                $imagePath = $this->destinationpath.$fullFilename;
                $album_attachment= isset($attachment) ? $fullFilename : NULL;

                $img = Image::make($imagePath);

                $img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
                    $constraint->upsize();
                });

                if(!file_exists($this->thumbpath)) {
                    @mkdir($this->thumbpath);
                }

            // finally we save the image as a new file
                $img->save($this->thumbpath.$fullFilename);
            }
         
                $logo = isset($attachment) ? $new_file_name . $extension : NULL;
                //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

                $objectNews = new NewsModel();
                $objectNews->news_heading = Input::get('news_heading');
                $objectNews->news_url = $newsUrl;
                $objectNews->news_date = Input::get('news_date');
                $objectNews->news_page_title = Input::get('news_page_title');
                $objectNews->news_meta_tags = Input::get('news_meta_tags');
                $objectNews->news_meta_description = Input::get('news_meta_description');
                $objectNews->news_short_description = Input::get('news_short_description');
                $objectNews->news_description = Input::get('news_description');
                $objectNews->news_type = Input::get('news_type');
                $objectNews->is_active = Input::get('is_active');
                $objectNews->show_in_home = Input::get('show_in_home');
                $objectNews->updated_by = Auth::user()->id;

                if ($logo != '') {
                    $objectNews->news_attachment = $logo;
                }
                $objectNews->save();

                if ($objectNews->id) {
                    Session::flash('class', 'alert alert-success');
                    Session::flash('message', 'news successfully added');
                    return View::make('admin.news.add');
                } else {
                    Session::flash('class', 'alert alert-error');
                    Session::flash('message', 'something error');
                    return View::make('admin.news.add');
                }

            }
        }
    }

    public function getEditNews($id)
    {
        $newsDetail = NewsModel::find($id);
        try {
            if ($newsDetail->deleted == 0) {
                return View::make('admin.news.edit')->with('newsDetail', $newsDetail);
            } else {
                return View::make('admin.error');
            }
        } catch (ErrorException $e) {
            return View::make('admin.error');
        }
    }

    public function editNews()
    {
        $newsId = Input::get('news_id');
        $newsDetail = NewsModel::find($newsId);
        $rules = array('news_heading' => 'required',
            'news_url' => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('admin.news.edit')->with('newsDetail', $newsDetail);
        } else {
            $newsHeading = Input::get('news_heading');
            $newsUrl = Input::get('news_url');
            $newsUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $newsUrl);
            $newsExist = NewsModel::checkExist($newsUrl);
            if (count($newsExist) != 0 && $newsExist->id != $newsId) {
                $message = 'news <b>' . $newsHeading . '</b> with url <b>' . $newsUrl . '</b> is already exist';
                Session::flash('class', 'alert alert-error');
                Session::flash('message', $message);
                return View::make('admin.news.edit')->with('newsDetail', $newsDetail);
            } else {
                $imageFile = Input::file('userfile');
               if($imageFile) {
                $oldimage = $this->albums->find($newsId)->images;
                $extension = strrchr($imageFile->getClientOriginalName(), '.');
                $new_file_name = $this->imagePrefix . time();
                $fullFilename = $new_file_name . $extension;

                if(!file_exists($this->destinationpath)) {
                    @mkdir($this->destinationpath);
                }

                $attachment = $imageFile->move($this->destinationpath, $fullFilename);
                $imagePath = $this->destinationpath.$fullFilename;
                $images = isset($attachment) ? $fullFilename : NULL;

                $img = Image::make($imagePath);


            // add callback functionality to retain maximal original image size
                $img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
                    $constraint->upsize();
                });

                if(!file_exists($this->thumbpath)) {
                    @mkdir($this->thumbpath);
                }

            // finally we save the image as a new file
                $img->save($this->thumbpath . $fullFilename);
                if($oldimage) {
                    if (file_exists($this->destinationpath . $oldimage)) {
                        @unlink($this->destinationpath . $oldimage);
                    }
                    if (file_exists($this->thumbpath . $oldimage)) {
                        @unlink($this->thumbpath . $oldimage);
                    }
                }
            }

            $albums = $this->albums->update();
            $logo = isset($attachment) ? $new_file_name . $extension : NULL;
                $objectNews = NewsModel::find($newsId);
                $objectNews->news_heading = Input::get('news_heading');
                $objectNews->news_url = $newsUrl;
                $objectNews->news_date = Input::get('news_date');
                $objectNews->news_page_title = Input::get('news_page_title');
                $objectNews->news_meta_tags = Input::get('news_meta_tags');
                $objectNews->news_meta_description = Input::get('news_meta_description');
                $objectNews->news_short_description = Input::get('news_short_description');
                $objectNews->news_description = Input::get('news_description');
                $objectNews->news_type = Input::get('news_type');
                $objectNews->is_active = Input::get('is_active');
                $objectNews->show_in_home = Input::get('show_in_home');
                $objectNews->updated_by = Auth::user()->id;

                if ($logo != '') {
                    $objectNews->news_attachment = $logo;
                }
                //var_dump($objectNews);
                $objectNews->save();

                if ($objectNews->id) {
                    $newsDetail = NewsModel::find($newsId);
                    Session::flash('class', 'alert alert-success');
                    Session::flash('message', 'News successfully updated');
                    return View::make('admin.news.edit')->with('newsDetail', $newsDetail);
                } else {
                    Session::flash('class', 'alert alert-error');
                    Session::flash('message', 'something error');
                    return View::make('admin.news.edit')->with('newsDetail', $newsDetail);
                }

            }
        }
    }

    public function publishNews()
    {
        $newsId = Input::get('news_id');
        $objectNews = NewsModel::find($newsId);
        $objectNews->is_active = 1;
        $objectNews->save();

        if ($objectNews->id) {
            $array = array('message' => 'News is published successfully.', 'flag' => true);
            $returnValue = json_encode($array);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
            $returnValue = json_encode($array);
        }
        return $returnValue;
    }

    public function unpublishNews()
    {
        $newsId = Input::get('news_id');
        $objectNews = NewsModel::find($newsId);
        $objectNews->is_active = 0;
        $objectNews->save();

        if ($objectNews->id) {
            $array = array('message' => 'News is unpublished successfully.', 'flag' => true);
            $returnValue = json_encode($array);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
            $returnValue = json_encode($array);
        }
        return $returnValue;
    }

    public function deleteNews()
    {
        $newsId = Input::get('news_id');
        $objectNews = NewsModel::find($newsId);
        $objectNews->is_active = 0;
        $objectNews->deleted = 1;
        $objectNews->save();

        if ($objectNews->id) {
            $array = array('message' => 'News is deleted successfully.', 'flag' => true);
            $returnValue = json_encode($array);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
            $returnValue = json_encode($array);
        }
        return $returnValue;
    }

}
