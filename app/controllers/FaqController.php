<?php

class FaqController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getFaqs()
    {
        $faqList = FaqModel::getAllFaqs();
		$i = 1;
		$faqs = '';
		foreach($faqList as $faq) {
			$faqs .= '<tr class="odd gradeX">';
			$faqs .= '<td>'.$i++.'</td>';
			$faqs .= '<td>'.$faq->faq_heading.'</td>';
			if($faq->is_active=='1') { 
				$faqs .= '<td>'.'<a href="javascript:void(0)" class="unpublish-faq" id="'.$faq->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$faqs .= '<td>'.'<a href="javascript:void(0)" class="publish-faq" id="'.$faq->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$faqs .= '<td>'.$faq->updated_at.'</td>';
            $faqs .= '<td>'.
						'<a href="faq/edit/'.$faq->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-faq" id="'.$faq->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$faqs .= '</tr>';
		}

        return View::make('admin.faq.list', array('faqs' => $faqs));
    }
	
	public function getAddFaq()
	{
		return View::make('admin.faq.add');
	}
	
	public function addFaq()
	{
		$rules = array('faq_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.faq.add');
		} 
		else {
			$faqHeading = Input::get('faq_heading');
			$faqUrl = Input::get('faq_url');
			$faqUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $faqUrl);
			$imageFile = Input::file('userfile');
			$destinationPath = 'images/faqs/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
			
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "faqs" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.faq.add');
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.faq.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			
			$objectFaq = new FaqModel();
			$objectFaq->faq_heading = Input::get('faq_heading');
			$objectFaq->faq_description = Input::get('faq_description');
			$objectFaq->is_active = Input::get('is_active');
			$objectFaq->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectFaq->faq_attachment = $logo;
			}
			$objectFaq->save();
			
			if($objectFaq->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Why UC successfully added');
				return View::make('admin.faq.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.faq.add');
			}						
		}
	}
	
	public function getEditFaq($id)
	{
		$faqDetail = FaqModel::find($id);		
		try {
			if($faqDetail->deleted == 0) {
				return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editFaq()
	{	
		$faqId = Input::get('faq_id');
		$faqDetail = FaqModel::find($faqId);
		$rules = array('faq_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
		} 
		else {
			$faqHeading = Input::get('faq_heading');
			$faqUrl = Input::get('faq_url');
			$faqUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $faqUrl);
			$imageFile = Input::file('userfile');
			$destinationPath = 'images/faqs/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "faqs" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				
			$objectFaq = FaqModel::find($faqId);
			$objectFaq->faq_heading = Input::get('faq_heading');
			$objectFaq->faq_description = Input::get('faq_description');
			$objectFaq->is_active = Input::get('is_active');
			$objectFaq->updated_by = Auth::user()->id;				
			if($logo != '') {
				$objectFaq->faq_attachment = $logo;
			}
			$objectFaq->save();
				
			if($objectFaq->id) {
				$faqDetail = FaqModel::find($faqId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Why UC successfully updated');
				return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.faq.edit')->with('faqDetail',$faqDetail);
			}					
		}
	}
	
	public function publishFaq()
	{ 
		$faqId = Input::get('faq_id');
		$objectFaq = FaqModel::find($faqId);
		$objectFaq->is_active = 1;
		$objectFaq->save();
		
		if($objectFaq->id) {
			$array = array('message' => 'Why UC is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishFaq()
	{ 
		$faqId = Input::get('faq_id');
		$objectFaq = FaqModel::find($faqId);
		$objectFaq->is_active = 0;
		$objectFaq->save();
		
		if($objectFaq->id) {
			$array = array('message' => 'Why UC is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteFaq()
	{ 
		$faqId = Input::get('faq_id');
		$objectFaq = FaqModel::find($faqId);
		$objectFaq->is_active = 0;
		$objectFaq->deleted = 1;
		$objectFaq->save();
		
		if($objectFaq->id) {
			$array = array('message' => 'Why UC is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function getActiveFaqs()
	{
		$faqs = FaqModel::getActiveFaqs();
		return View::make('client.faqs', 
						array('pageTitle' => 'Faqs',
								'metaDescription' => 'Luxury Holidays',
								'metaKeywords' => 'Luxury Holidays',
							)
						)->with('faqs',$faqs);
	}
}
