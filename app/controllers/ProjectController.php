<?php

class ProjectController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getProjects()
    {
        $projectList = ProjectModel::getAllProjects();
		
		//var_dump($projectList); die();
		$i = 1;
		$projects = '';
		foreach($projectList as $project) {
			$projects .= '<tr class="odd gradeX">';
			$projects .= '<td>'.$i++.'</td>';
			$projects .= '<td>'.$project->title.'</td>';
			if($project->is_active=='1') { 
				$projects .= '<td>'.'<a href="javascript:void(0)" class="unpublish-project" id="'.$project->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$projects .= '<td>'.'<a href="javascript:void(0)" class="publish-project" id="'.$project->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$projects .= '<td>'.$project->updated_at.'</td>';
            $projects .= '<td>'.
						'<a href="project/edit/'.$project->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-project" id="'.$project->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$projects .= '</tr>';
		}

        return View::make('admin.project.list', array('projects' => $projects));
    }
	
	public function getAddProject()
	{
		return View::make('admin.project.add');
	}
	
	public function addProject()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.project.add');
		} 
		else {
			$projectTitle = Input::get('title');
			$projectUrl = Input::get('url');
			$projectUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $projectUrl);
      
      $projectExist = ProjectModel::checkExist($projectUrl);
			if( count($projectExist)!=0) {
				$message = 'projects <b>'.$projectTitle.'</b> with url <b>'.$projectUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.project.add');
			}
			else {
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/projects/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "projects" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.project.add');
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.project.add');
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

        $objectProject = new ProjectModel();
        $objectProject->title = Input::get('title');
        $objectProject->url = $projectUrl;
        $objectProject->short_desc = Input::get('short_desc');
        $objectProject->page_title = Input::get('page_title');
        $objectProject->type = Input::get('type');
        $objectProject->meta_description = Input::get('meta_description');
        $objectProject->meta_tags = Input::get('meta_tags');
        $objectProject->desc = Input::get('desc');        
        $objectProject->is_active = Input::get('is_active');
        $objectProject->updated_by = Auth::user()->id;

        if($logo != '') {
          $objectProject->attachment = $logo;
        }
        $objectProject->save();

        if($objectProject->id) {
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'Project successfully added');
          return View::make('admin.project.add');
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.project.add');
        }
      }
						
		}
	}
	
	public function getEditProject($id)
	{
		$projectDetail = ProjectModel::find($id);		
		try {
			if($projectDetail->deleted == 0) {
				return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editProject()
	{	
		$projectId = Input::get('project_id');
		$projectDetail = ProjectModel::find($projectId);
		$rules = array('title' => 'required',
					   'url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
		} 
		else {
			$projectTitle = Input::get('title');
			$projectUrl = Input::get('url');
			$projectUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $projectUrl);
			
      $projectExist = ProjectModel::checkExist($projectUrl);
			if( count($projectExist)!=0 && $projectExist->id != $projectId) {
				$message = 'projects <b>'.$projectTitle.'</b> with url <b>'.$projectUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
			}
			else {		
      
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/projects/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "projects" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

          $objectProject = ProjectModel::find($projectId);
          $objectProject->title = Input::get('title');
          $objectProject->url = $projectUrl;
//          $objectProject->short_desc = Input::get('short_desc');
          $objectProject->short_desc = Input::get('short_desc');
          $objectProject->page_title = Input::get('page_title');
          $objectProject->type = Input::get('type');
          
          $objectProject->meta_description = Input::get('meta_description');
          $objectProject->meta_tags = Input::get('meta_tags');
          
          $objectProject->desc = Input::get('desc');
          $objectProject->is_active = Input::get('is_active');
          $objectProject->updated_by = Auth::user()->id;

          if($logo != '') {
            $objectProject->attachment = $logo;
          }
          $objectProject->save();

          if($objectProject->id) {
            $projectDetail = ProjectModel::find($projectId);		
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'Project successfully updated');
            return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.project.edit')->with('projectDetail',$projectDetail);
          }
      }
					
		}
	}
	
	public function publishProject()
	{ 
		$projectId = Input::get('project_id');
		$objectProject = ProjectModel::find($projectId);
		$objectProject->is_active = 1;
		$objectProject->save();
		
		if($objectProject->id) {
			$array = array('message' => 'Project is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishProject()
	{ 
		$projectId = Input::get('project_id');
		$objectProject = ProjectModel::find($projectId);
		$objectProject->is_active = 0;
		$objectProject->save();
		
		if($objectProject->id) {
			$array = array('message' => 'Project is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteProject()
	{ 
		$projectId = Input::get('project_id');
		$objectProject = ProjectModel::find($projectId);
		$objectProject->is_active = 0;
		$objectProject->deleted = 1;
		$objectProject->save();
		
		if($objectProject->id) {
			$array = array('message' => 'Project is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
