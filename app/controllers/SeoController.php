<?php

class SeoController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getSeos()
    {
        $seoList = SeoModel::getAllSeos();
		$i = 1;
		$seos = '';
		foreach($seoList as $seo) {
			$seos .= '<tr class="odd gradeX">';
			$seos .= '<td>'.$i++.'</td>';
			$seos .= '<td>'.$seo->seo_heading.'</td>';
			$seos .= '<td>'.$seo->page_title.'</td>';
			$seos .= '<td>'.$seo->page_meta_tags.'</td>';
			$seos .= '<td>'.$seo->page_meta_description.'</td>';
			$seos .= '<td><a href="seo/edit/'.$seo->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;</td>';
			$seos .= '</tr>';
		}

        return View::make('admin.seo.list', array('seos' => $seos));
    }
	
	public function getAddSeo()
	{
		return View::make('admin.seo.add');
	}
	
	public function addSeo()
	{
		$rules = array('seo_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.seo.add');
		} 
		else {
			$seoHeading = Input::get('seo_heading');			
			$objectSeo = new SeoModel();
			$objectSeo->seo_heading = strtolower(Input::get('seo_heading'));
      $objectSeo->desc = Input::get('desc');
			$objectSeo->page_title = Input::get('page_title');
			$objectSeo->page_meta_tags = Input::get('page_meta_tags');
			$objectSeo->page_meta_description = Input::get('page_meta_description');
			$objectSeo->is_active = 1;
			$objectSeo->updated_by = Auth::user()->id;			
			$objectSeo->save();
			
			if($objectSeo->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'seo successfully added');
				return View::make('admin.seo.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.seo.add');
			}						
		}
	}
	
	public function getEditSeo($id)
	{
		$seoDetail = SeoModel::find($id);		
		try {
			if($seoDetail->deleted == 0) {
				return View::make('admin.seo.edit')->with('seoDetail',$seoDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editSeo()
	{	
		$seoId = Input::get('seo_id');
		$seoDetail = SeoModel::find($seoId);
		$rules = array('seo_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.seo.edit')->with('seoDetail',$seoDetail);
		} 
		else {
			$objectSeo = SeoModel::find($seoId);
			$objectSeo->seo_heading = strtolower(Input::get('seo_heading'));
			$objectSeo->page_title = Input::get('page_title');
      $objectSeo->desc = Input::get('desc');
			$objectSeo->page_meta_tags = Input::get('page_meta_tags');
			$objectSeo->page_meta_description = Input::get('page_meta_description');
			$objectSeo->is_active = 1;
			$objectSeo->updated_by = Auth::user()->id;			
			$objectSeo->save();
				
			if($objectSeo->id) {
				$seoDetail = SeoModel::find($seoId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Seo successfully updated');
				return View::make('admin.seo.edit')->with('seoDetail',$seoDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.seo.edit')->with('seoDetail',$seoDetail);
			}					
		}
	}

}
