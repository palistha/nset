<?php

class AchievementController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	 public function getTypeAchievements()
    {
        $achievementList = AchievementTypeModel::getAllAchievementTypes();
    
    //var_dump($achievementList); die();
    $i = 1;
    $achievements = '';
    foreach($achievementList as $achievement) {
      $achievements .= '<tr class="odd gradeX">';
      $achievements .= '<td>'.$i++.'</td>';
      $achievements .= '<td>'.$achievement->achievement_type_title.'</td>';
            
      if($achievement->is_active=='1') { 
        $achievements .= '<td>'.'<a href="javascript:void(0)" class="unpublish-achievementtype" id="'.$achievement->id.'" >'.
             '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
      } else {
        $achievements .= '<td>'.'<a href="javascript:void(0)" class="publish-achievementtype" id="'.$achievement->id.'" >'.
             '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
      }
      $achievements .= '<td>'.$achievement->updated_at.'</td>';
            $achievements .= '<td>'.
            '<a href="achievementtype/edit/'.$achievement->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
                      '<a href="javascript:void(0)" class="delete-achievementtype" id="'.$achievement->id.'" >'.
            '<i class="icon-trash" ></i> Delete</a>'.'</td>';
      $achievements .= '<td>'.
            '<a href="achievements/'.$achievement->id.'" style="text-decoration:none;">View</a> | '.
            '<a href="achievement/add/'.$achievement->id.'" style="text-decoration:none;">Add New</a>'.
            '</td>';
      $achievements .= '</tr>';
    }
    
        return View::make('admin.achievement.mainlist', array('achievements' => $achievements));
    }

  public function getAddAchievementType()
  {
    return View::make('admin.achievement.addtype');
  }
  
  public function addAchievementType()
  {
   
    $count = 0;
    $valid = true;
    $rules = array('achievement_type_title' => 'required');
    $validator = Validator::make(Input::all(), $rules);
    if($validator->fails()){
      Session::flash('class', 'alert alert-error');
      Session::flash('message', 'Some fields are missing');
      return View::make('admin.achievement.addtype');
    } 
    else {
      $albumHeading = Input::get('achievement_type_title');
      $achievementurl = Input::get('achievement_url');
      $achievementurl = preg_replace('/[^A-Za-z0-9\-]/', '', $achievementurl);
      
      $achievementurlExist = AchievementTypeModel::checkExist($achievementurl);
      if( count($achievementurlExist)!=0) {
        $message = 'Achievement <b>'.$albumHeading.'</b> with url <b>'.$achievementurl.'</b> is already exist';
        Session::flash('class', 'alert alert-error');
        Session::flash('message', $message);  
        return View::make('admin.achievement.add');
      }
      else {  
       
          $objectAchievementType = new AchievementTypeModel();
          $objectAchievementType->achievement_type_title = Input::get('achievement_type_title');
          $objectAchievementType->achievement_url = $achievementurl;
          $objectAchievementType->achievement_description = Input::get('achievement_description');
          $objectAchievementType->is_active = Input::get('is_active');
          $objectAchievementType->updated_by = Auth::user()->id;
          $objectAchievementType->save();
          $achievementId = $objectAchievementType->id;
         
          if($achievementId) {
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'AchievementType successfully added');
            return Redirect::route('admin.achievement.mainlist' );
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.achievement.addtype');       
          }
        
      }
    }
  }
  
  public function getEditAchievementType($id)
  {
 $achievementDetail = AchievementTypeModel::find($id); 
    // print_r($achievementDetail);exit;  
    try {
      if($achievementDetail->deleted == 0) {
        return View::make('admin.achievement.edittype')->with('achievementDetail',$achievementDetail);
      } else {
        return View::make('admin.error');
      }     
    } catch(ErrorException $e) {
      return View::make('admin.error');
    }
  }
  
  public function editAchievementType()
  { 
    $achievementId = Input::get('achievement_id');
    $achievementDetail = AchievementTypeModel::find($achievementId);
    $rules = array('achievement_type_title' => 'required');
    $validator = Validator::make(Input::all(), $rules);
    
    if($validator->fails()){
      Session::flash('class', 'alert alert-error');
      Session::flash('message', 'Some fields are missing');
      return View::make('admin.achievement.edittype')->with('achievementDetail',$achievementDetail);
    } 
    else {
      $albumHeading = Input::get('achievement_type_title');
      $achievementurl = Input::get('achievement_url');
      $achievementurl = preg_replace('/[^A-Za-z0-9\-]/', '', $achievementurl);
      
      $achievementurlExist = AchievementTypeModel::checkExist($achievementurl);
      if( count($achievementurlExist)!=0 && $achievementurlExist->id != $achievementId) {
        $message = 'album <b>'.$albumHeading.'</b> with url <b>'.$achievementurl.'</b> is already exist';
        Session::flash('class', 'alert alert-error');
        Session::flash('message', $message);  
        return View::make('admin.achievement.edittype')->with('achievementDetail',$achievementDetail);
      }
      else {  
      
       
        $objectAchievementType = AchievementTypeModel::find($achievementId);
        $objectAchievementType->achievement_type_title = Input::get('achievement_type_title');
        $objectAchievementType->achievement_url = $achievementurl;
        $objectAchievementType->achievement_description = Input::get('achievement_description');
        $objectAchievementType->is_active = Input::get('is_active');
        $objectAchievementType->updated_by = Auth::user()->id;
        $objectAchievementType->save();

        if($objectAchievementType->id) {
          $achievementDetail = AchievementTypeModel::find($achievementId);    
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'AchievementType successfully updated');
          return View::make('admin.achievement.edittype')->with('achievementDetail',$achievementDetail);
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.achievement.edittype')->with('achievementDetail',$achievementDetail);
        }
      }
    }
  }
  
  public function publishAchievementType()
  { 
    $achievementId = Input::get('achievement_id');
    $objectAchievementType = AchievementTypeModel::find($achievementId);
    $objectAchievementType->is_active = 1;
    $objectAchievementType->updated_by = Auth::user()->id;
    $objectAchievementType->save();
    
    if($objectAchievementType->id) {
      $array = array('message' => 'AchievementType is published successfully.', 'flag' => true);      
    } else {
      $array = array('message' => 'server error.', 'flag' => false);      
    }
    return $returnValue = json_encode($array);
  }
  
  public function unpublishAchievementType()
  { 
    $achievementId = Input::get('achievement_id');
    $objectAchievementType = AchievementTypeModel::find($achievementId);
    $objectAchievementType->is_active = 0;
    $objectAchievementType->updated_by = Auth::user()->id;
    $objectAchievementType->save();
    
    if($objectAchievementType->id) {
      $array = array('message' => 'AchievementType is unpublished successfully.', 'flag' => true);      
    } else {
      $array = array('message' => 'server error.', 'flag' => false);      
    }
    return $returnValue = json_encode($array);
  }
  
  public function deleteAchievementType()
  { 
    $achievementId = Input::get('achievement_id');
    $objectAchievementType = AchievementTypeModel::find($achievementId);
    $objectAchievementType->updated_by = Auth::user()->id;
    $objectAchievementType->is_active = 0;
    $objectAchievementType->deleted = 1;    
    $objectAchievementType->save();
    
    if($objectAchievementType->id) {
      $array = array('message' => 'AchievementType is deleted successfully.', 'flag' => true);      
    } else {
      $array = array('message' => 'server error.', 'flag' => false);      
    }
    return $returnValue = json_encode($array);
  }
// 
// 
// 
// 
// Achievement list start
// 
// 
// 
// 
// 
	public function getAchievements($type)
    {
      $achievementList = AchievementModel::getAllAchievements($type);
      $achievementType = AchievementTypeModel::getDetailByID($type);
      
        $typeName = $achievementType->achievement_type_title;
        $divId = $achievementType->id;
     
      //var_dump($achievementList); die();
      $i = 1;
      $achievements = '';
      foreach($achievementList as $achievement) {
        $achievements .= '<tr class="odd gradeX">';
        $achievements .= '<td>'.$i++.'</td>';
        $achievements .= '<td>'.$achievement->title.'</td>';

        // if(file_exists("uploads/achievements/".$achievement->attachment) && $achievement->attachment!=''){
        //   $achievements .= '<td><img src="'.'../../uploads/achievements/'.$achievement->attachment.'" /></td>';
        // } else {
        //   $achievements .= '<td><img src="'.'../../uploads/noimage.jpg'.'" /></td>';
        // }
        if($achievement->is_active=='1') { 
          $achievements .= '<td>'.'<a href="javascript:void(0)" class="unpublish-achievement" id="'.$achievement->id.'" >'.
               '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
        } else {
          $achievements .= '<td>'.'<a href="javascript:void(0)" class="publish-achievement" id="'.$achievement->id.'" >'.
               '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
        }
        $achievements .= '<td>'.$achievement->updated_at.'</td>';
              $achievements .= '<td>'.
              '<a href="../achievement/edit/'.$achievement->id.'/'.$type.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
                        '<a href="javascript:void(0)" class="delete-achievement" id="'.$achievement->id.'" >'.
              '<i class="icon-trash" ></i> Delete</a>'.'</td>';
        $achievements .= '</tr>';
      }

        return View::make('admin.achievement.list', array('achievements' => $achievements,
                                                          'typeName'=>$typeName,
                                                          'divId'=>$divId,
                                                          'type'=>$type ));
    }
	
	public function getAddAchievement($type)
	{
     $achievementType = AchievementTypeModel::getDetailByID($type);
      
        $typeName = $achievementType->achievement_type_title;
        $divId = $achievementType->id;
		return View::make('admin.achievement.add', array(
                                                          'typeName'=>$typeName,
                                                          'divId'=>$divId,
                                                          'type'=>$type ));
	}
	
	public function addAchievement($type)
	{
     $achievementType = AchievementTypeModel::getDetailByID($type);
      
        $typeName = $achievementType->achievement_type_title;
        $divId = $achievementType->id;
		$files = Input::file('userfile');
		$count = 0;
		$valid = true;
			
        if(count($files) > 10 || count($files) == 0) {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'There is no image to upload or maximum numbers of images exceeds 10.');
          return View::make('admin.achievement.add', array(
                                                          'typeName'=>$typeName,
                                                          'divId'=>$divId,
                                                          'type'=>$type ));
        } else {
          $objectAchievement = new AchievementModel();
          
          
          for($i=0; $i<count($files); $i++) {
            $objectMedia = new AchievementModel();
            $imageFile = $files[$i];
            $destinationPath = 'uploads/achievements/';
            $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
            $maximum_filesize = 1 * 1024 * 1024;
            if($imageFile) {
              $filename = $imageFile->getClientOriginalName();
              $extension = strrchr($filename, '.');
              $size = $imageFile->getSize();

              if (preg_match($rEFileTypes, $extension) == false) {
                $valid = false;
              } else if ($size > $maximum_filesize) {
                $valid = false;
              } else {
                $new_image_name = "achievement_" . time() . "_" . rand(0,9999);
                $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);
                $valid = true;
              }			
            }
            if($valid) {
              $logo = isset($attachment) ? $new_image_name . $extension : NULL;

              if($logo != '') {
                $objectMedia->type = $type;
                $objectMedia->title = Input::get('title');
                $objectMedia->note = Input::get('note');
                $objectMedia->is_active = Input::get('is_active');
                $objectMedia->updated_by = Auth::user()->id;
                $objectMedia->attachment = $logo;
                $objectMedia->save();
                $count++;
              }				

            }
          }
          if($count > 0) {
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'Achievement successfully added');
            return Redirect::route('admin.achievement.list', $type );
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.achievement.add', array(
                                                          'typeName'=>$typeName,
                                                          'divId'=>$divId,
                                                          'type'=>$type ));				
          }
        }
      
    
	}
	
	public function getEditAchievement($id, $type)
	{
    $achievementType = AchievementTypeModel::getDetailByID($type);
      
        $typeName = $achievementType->achievement_type_title;
        $divId = $achievementType->id;
		$achievementDetail = AchievementModel::find($id);		
		try {
			if($achievementDetail->deleted == 0) {
				return View::make('admin.achievement.edit')
                ->with('achievementDetail',$achievementDetail)
                ->with('typeName', $typeName)
                ->with('divId',$divId)
                ->with('type', $type);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editAchievement()
	{	
		$achievementId = Input::get('achievement_id');
    $type = Input::get('achievement_type');
       $achievementType = AchievementTypeModel::getDetailByID($type);
      
        $typeName = $achievementType->achievement_type_title;
        $divId = $achievementType->id;
    
		$achievementDetail = AchievementModel::find($achievementId);
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.achievement.edit')->with('achievementDetail',$achievementDetail)
            ->with('typeName', $typeName)
                ->with('divId',$divId)
                ->with('type', $type);
		} 
		else {
      $achievementHeading = Input::get('title');
			
			
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/achievements/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "achievements" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.achievement.edit')->with('achievementDetail',$achievementDetail)
                  ->with('typeName', $typeName)
                ->with('divId',$divId)
                ->with('type', $type);
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.achievement.edit')->with('achievementDetail',$achievementDetail)
                  ->with('typeName', $typeName)
                ->with('divId',$divId)
                ->with('type', $type);
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;

        $objectAchievement = AchievementModel::find($achievementId);
        $objectAchievement->title = Input::get('title');
        $objectAchievement->second_title = Input::get('second_title');
        $objectAchievement->note = Input::get('note');
        $objectAchievement->is_active = Input::get('is_active');
        $objectAchievement->updated_by = Auth::user()->id;

        if($logo != '') {
          if(file_exists('uploads/achievements/'.$objectAchievement->attachment) && $objectAchievement->attachment != '') {
            unlink('uploads/achievements/'.$objectAchievement->attachment);
          }
          $objectAchievement->attachment = $logo;
        }
        $objectAchievement->save();

        if($objectAchievement->id) {
          $achievementDetail = AchievementModel::find($achievementId);		
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'Achievement successfully updated');
          return Redirect::route('admin.achievement.list', $type );
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.achievement.edit')->with('achievementDetail',$achievementDetail)
                ->with('typeName', $typeName)
                ->with('divId',$divId)
                ->with('type', $type);
        }
      
		}
	}
	
	public function publishAchievement()
	{ 
		$achievementId = Input::get('achievement_id');
		$objectAchievement = AchievementModel::find($achievementId);
		$objectAchievement->is_active = 1;
		$objectAchievement->updated_by = Auth::user()->id;
		$objectAchievement->save();
		
		if($objectAchievement->id) {
			$array = array('message' => 'Achievement is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishAchievement()
	{ 
		$achievementId = Input::get('achievement_id');
		$objectAchievement = AchievementModel::find($achievementId);
		$objectAchievement->is_active = 0;
		$objectAchievement->updated_by = Auth::user()->id;
		$objectAchievement->save();
		
		if($objectAchievement->id) {
			$array = array('message' => 'Achievement is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteAchievement()
	{ 
		$achievementId = Input::get('achievement_id');
		$objectAchievement = AchievementModel::find($achievementId);
		$objectAchievement->updated_by = Auth::user()->id;
		$objectAchievement->is_active = 0;
		$objectAchievement->deleted = 1;		
		$objectAchievement->save();
		
		if($objectAchievement->id) {
			$array = array('message' => 'Achievement is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
}
