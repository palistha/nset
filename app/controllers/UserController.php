<?php

class UserController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function showProfile($id)
    {
        $user = User::find($id);

        return View::make('user.profile', array('user' => $user));
    }
	public function getUsers()
    {
        $userList = User::getAllUsers();
		
		#var_dump($userList); die();
		$i = 1;
		$users = '';
		foreach($userList as $user) {
			$users .= '<tr class="odd gradeX">';
			$users .= '<td>'.$i++.'</td>';
			$users .= '<td>'.$user->user_first_name.' '.$user->user_last_name.'</td>';
			$users .= '<td class="hidden-480">'.$user->username.'</td>';
			$users .= '<td class="hidden-480">'.$user->user_type_name.'</td>';
			if($user->is_active==1) { 
				$users .= '<td>'.'<a href="javascript:void(0)" class="unpublish-user" id="'.$user->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$users .= '<td>'.'<a href="javascript:void(0)" class="publish-user" id="'.$user->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$users .= '<td>'.$user->updated_at.'</td>';
            $users .= '<td>'.
						'<a href="user/edit/'.$user->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
						'<a href="user/change-password/'.$user->id.'"><i class="icon-key"></i> Change Password</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-user" id="'.$user->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$users .= '</tr>';
		}

        return View::make('admin.user.list', array('users' => $users));
    }
	
	public function getAddUser()
	{
		return View::make('admin.user.add');
	}
	
	public function addUser()
	{
		$rules = array('user_first_name' => 'required',
					   'user_last_name' => 'required',
					   'username' => 'required',
					   'password' => 'required',
					   'confirm_password' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails())
		{
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.user.add');
		} 
		else {
			$username = Input::get('username');
			$userExist = User::checkExist($username);
			if( count($userExist)!=0) {
				$message = 'Username <b>'.$username.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.user.add');
			}
			else {			
				$imageFile = Input::file('userfile');
				$destinationPath = 'images/users/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "user" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.user.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.user.add');
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				
				$objectUser = new User();
				$objectUser->user_first_name	= Input::get('user_first_name');
				$objectUser->user_last_name		= Input::get('user_last_name');
				$objectUser->user_type			= Input::get('user_type');
				$objectUser->username			= Input::get('username');
				$objectUser->password			= Hash::make(Input::get('password'));
				$objectUser->user_company		= Input::get('user_company');
				$objectUser->gender				= Input::get('gender');
				$objectUser->user_phone_number	= Input::get('user_phone_number');
				$objectUser->user_mobile_number	= Input::get('user_mobile_number');
				$objectUser->user_address_line_1	= Input::get('user_address_line_1');
				$objectUser->user_address_line_2	= Input::get('user_address_line_2');
				$objectUser->user_city			= Input::get('user_city');
				$objectUser->user_state			= Input::get('user_state');
				$objectUser->user_zip_code		= Input::get('user_zip_code');
				$objectUser->country_id			= Input::get('country_id');
				$objectUser->is_active			= Input::get('is_active');
				$objectUser->updated_by			= Auth::user()->id;
				
				if($logo != '') {
					$objectUser->user_attachment	= $logo;
				}				
				$objectUser->save();
				
				if($objectUser->id) {
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'User successfully added');
					return View::make('admin.user.add');
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Something error');
					return View::make('admin.user.add');
				}				
			}			
		}
	}
	
	public function getEditUser($id)
	{
		$userDetail = User::find($id);
		try {
			if($userDetail->deleted == 0) {
				return View::make('admin.user.edit')->with('userDetail',$userDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editUser()
	{	
		$userId = Input::get('user_id');
		$userDetail = User::find($userId);
			
		$rules = array('user_first_name' => 'required',
					   'user_last_name' => 'required',
					   'username' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails())
		{
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.user.edit')->with('userDetail',$userDetail);
		} 
		else {
			$username = Input::get('username');
			$userExist = User::checkExist($username);
			
			if( count($userExist)!=0 && $userExist->id != $userId) {
				$message = 'Username <b>'.$username.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.user.edit')->with('userDetail',$userDetail);
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/users/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "user" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.user.edit')->with('userDetail',$userDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.user.edit')->with('userDetail',$userDetail);
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				$objectUser = User::find($userId);
				$objectUser->user_first_name	= Input::get('user_first_name');
				$objectUser->user_last_name		= Input::get('user_last_name');
				$objectUser->user_type			= Input::get('user_type');
				$objectUser->username			= Input::get('username');
				$objectUser->password			= Hash::make(Input::get('password'));
				$objectUser->user_company		= Input::get('user_company');
				$objectUser->gender				= Input::get('gender');
				$objectUser->user_phone_number	= Input::get('user_phone_number');
				$objectUser->user_mobile_number	= Input::get('user_mobile_number');
				$objectUser->user_address_line_1	= Input::get('user_address_line_1');
				$objectUser->user_address_line_2	= Input::get('user_address_line_2');
				$objectUser->user_city			= Input::get('user_city');
				$objectUser->user_state			= Input::get('user_state');
				$objectUser->user_zip_code		= Input::get('user_zip_code');
				$objectUser->country_id			= Input::get('country_id');
				$objectUser->is_active			= Input::get('is_active');
				$objectUser->updated_by			= Auth::user()->id;
				
				if($logo != '') {
					$objectUser->user_attachment	= $logo;
				}				
				$objectUser->save();
				
				if($objectUser->id) {
					$userDetail = User::find($userId);
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'User successfully updated.');
					return View::make('admin.user.edit')->with('userDetail',$userDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Something error');
					return View::make('admin.user.edit')->with('userDetail',$userDetail);
				}
				
			}			
		}
	}	
	
	public function publishUser()
	{ 
		$userId = Input::get('user_id');
		$objectUser = User::find($userId);
		$objectUser->updated_by = Auth::user()->id;		
		$objectUser->is_active = 1;
		$objectUser->save();
		
		if($objectUser->id) {
			$array = array('message' => 'User is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishUser()
	{ 
		$userId = Input::get('user_id');
		$objectUser = User::find($userId);
		$objectUser->updated_by = Auth::user()->id;		
		$objectUser->is_active = 0;
		$objectUser->save();
		
		if($objectUser->id) {
			$array = array('message' => 'User is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteUser()
	{ 
		$userId = Input::get('user_id');
		$objectUser = User::find($userId);
		$objectUser->updated_by = Auth::user()->id;		
		$objectUser->is_active = 0;
		$objectUser->deleted = 1;
		$objectUser->save();
		
		if($objectUser->id) {
			$array = array('message' => 'User is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function getUserChangePassword($id)
    {
        $user = User::find($id);		
		return View::make('admin.user.change-password')->with('user',$user);
    }
	
	public function passwordChangeUser()
	{ 
		$userId = Input::get('id');
		$objectUser = User::find($userId);
		$rules = array('password' => 'required',
					   'confirm_password' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails())
		{
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.user.change-password')->with('user', $objectUser);
		} 
		else {
			
			if(Input::get('password') != Input::get('confirm_password')) {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Password and confirm password do not match.');
				return View::make('admin.user.change-password')->with('user', $objectUser);			
			}
			
			$objectUser->updated_by = Auth::user()->id;
			$objectUser->password	= Hash::make(Input::get('password'));
			$objectUser->password_changed_on = date("Y-m-d H:i:s");
			$objectUser->save();
			if($objectUser->id) {
				$receiverEmail	= $objectUser->username;
				$subject		= "Password is changed.";
				$content = '<br>Your password with username <strong>'. $objectUser->username .
						'</strong> is changed by <strong>'. Session::get('adminUserName') .'</strong> as per your request.<br>'.
						'And your new password is : '.Input::get('password') .' .'; 
				
				$email = FunctionModel::sendEmail($receiverEmail, $subject, $content);
			
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'User password is updated successfully.');
				return View::make('admin.user.change-password')->with('user', $objectUser);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Something error');
				return View::make('admin.user.edit')->with('user',$objectUser);
			}
			
		}
	}
	

}
