<?php

class FrontendController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Frontend Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'FrontendController@getHome');
    |
    */

    /*
    * home page
    */
    public function getHome()

    {  
            $bannerList = BannerModel::getAllBanners();
     $aboutUs = ContentModel::getDetailByUrl('about-us');
        $president_message = ContentModel::getDetailByUrl('message-from-president-of-npfca');
		$ambassador_message = ContentModel::getDetailByUrl('message-from-he-ambassador-of-pakistan');
        $seoDetail = SeoModel::getDetailBypage('home');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.home',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
                'pageDesc' => $seoDetail->desc,
                'homeNotice' => '1',
                'aboutUs'=>$aboutUs,
                'president_message'=>$president_message,
				'ambassador_message' => $ambassador_message

            )
        )->with('sitePath', $sitePath);
    }
    function getManagementTeamDetails(Request $request)
        {
            $slug = $request->segment(2);
            try{
                $data['page'] = ManagementTeamModel::where('is_active', '1')->where('slug', $slug)->firstOrFail();
                return view('client.managementteamdetails')->with($data);
            } catch(Exception $e) {
                return view('layout.404');
            }
        }
    /*
* dynamic home detail page
* 03-01-2015
* author:vinod b handari
*/
    public function getHomeDetail()
    {
        $seoDetail = SeoModel::getDetailBypage('home');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.homedetail',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
                'pageDesc' => $seoDetail->desc,
            )
        )->with('sitePath', $sitePath);
    }

    /*
    * gallery page
    */
 public function getGallery()
    {
        // echo "fdf";die;
        $seoDetail = SeoModel::getDetailBypage('gallery');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $galleries = GalleryModel::getActiveGalleries();

        return View::make('client.gallery',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('galleries', $galleries);
    }


    /*
    * gallery page
    */
    public function getVideos()
    {
        $seoDetail = SeoModel::getDetailBypage('Home');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        // print_r($sitePath	);exit;
        return View::make('client.videos',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath);
    }

    /*
    * contact us page
    */
    public function getContact()
    {
         
        $seoDetail  = SeoModel::getDetailBypage('contact');
        $captcha    = FunctionModel::create_captcha();
        $sitePath   = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.contact',
                            array('pageTitle' => $seoDetail->page_title,
                                'metaDescription' => $seoDetail->page_meta_tags,
                                'metaKeywords' => $seoDetail->page_meta_description,                            
                            )
                        )->with('captcha', $captcha)
                        ->with('sitePath', $sitePath);
    }
    
    /*
    * contact us page submit form
    */
    public function postContact()
    {

        $seoDetail  = SeoModel::getDetailBypage('contact');
        $sitePath   = ConstantModel::getDetailByName('site_path')->constant_value;
         // print_r(Input::all()); die();

        $rules = array(
                    'fullname' => 'required',
                    'address'=>'required',
                    'phone'=>'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})/',
                    'subject' => 'required',
                    'email'=>'required|email',
                    'mobile' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                    'inquiry'=>'required'
        );     
 
        $validator = Validator::make(Input::all(), $rules);
     // dd(1);
        
            if($validator->fails()) {
                // dd(1);
      $captcha  = FunctionModel::create_captcha();
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('client.contact',
                            array('pageTitle' => $seoDetail->page_title,
                                'metaDescription' => $seoDetail->page_meta_tags,
                                'metaKeywords' => $seoDetail->page_meta_description,                            
                            )
                        )->withErrors($validator)
                         ->withInput(Input::flash())
                         ->with('captcha', $captcha)
                         ->with('sitePath', $sitePath);
        }
        else {
      if(Session::get('phrase') != Input::get('captcha')) {
            // dd(2);
        $captcha    = FunctionModel::create_captcha();
                Session::flash('class', 'alert alert-error');
                Session::flash('message', 'Invalid Image verification.');
                return View::make('client.contact',
                            array('pageTitle' => $seoDetail->page_title,
                                'metaDescription' => $seoDetail->page_meta_tags,
                                'metaKeywords' => $seoDetail->page_meta_description,                            
                            )
                        )->withInput(Input::flash())
                         ->with('captcha', $captcha)
                         ->with('sitePath', $sitePath);
            }
        
      else {
                $captcha    = FunctionModel::create_captcha();
                $subject    = "Contact form submitted.";
                $siteName   = ConstantModel::getDetailByName('site_name')->constant_value;
                
                $content     = '<table cellspacing="0" cellpadding="0" width="100%">';
                $content    .= '<tr><td><p>Dear <strong>admin,</strong> <br />';
                $content    .= '<br />A visitor <strong>' . Input::get('full_name') . '</strong> has given feedback with following details.</td>';
                $content    .= '</table>';          
                $content    .= '<table cellspacing="0" cellpadding="0" width="100%" border="0" style="border-collapse:collapse;margin-top: 20px;">';                
                $content    .= '<tr><td width="150">Full Name :</td><td>' . Input::get('fullname')  .'</td></tr>';
                $content    .= '<tr><td width="150">Address :</td><td>' . Input::get('address')  .'</td></tr>';
                $content    .= '<tr><td width="150">Phone :</td><td>' . Input::get('phone')  .'</td></tr>';
                $content    .= '<tr style="margin-top: 10px;"><td width="150">Email Address :</td><td>' . Input::get('email')  .'</td></tr>';
                $content    .= '<tr style="margin-top: 10px;"><td width="150">Message:</td><td>' . Input::get('inquiry') .'</td></tr>';
                $content    .= '<tr style="margin-top: 10px;"><td width="150">Subject :</td><td>' . Input::get('subject')  .'</td></tr>';
                $content    .= '<tr style="margin-top: 10px;"><td width="150">Mobile :</td><td>' . Input::get('mobile')  .'</td></tr>';         
                $content    .= '</table>';          
                $content    .= '<table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;margin-top: 20px;">';
                $content    .= '<tr><td><p>Kind Regards,</strong> <br />';
                $content    .= $siteName.'</td>';
                $content    .= '</table>';
                
                $adminEmail = ConstantModel::getDetailByName('admin_email')->constant_value;
                 
       
                $email = FunctionModel::sendEmail($adminEmail, $subject, $content);
                // dd(2);
                Session::flash('class', 'alert alert-success');
                Session::flash('message', 'Your message was sent successfully.');
                // dd(2);
                return View::make('client.contact',
                                array('pageTitle' => $seoDetail->page_title,
                                    'metaDescription' => $seoDetail->page_meta_tags,
                                    'metaKeywords' => $seoDetail->page_meta_description,                            
                                )
                            )->with('captcha', $captcha)
                             ->with('sitePath', $sitePath);
                            
            }
        }
    
    }
    /*
    * dynamic content page
    */
    public function getContentDetail($url)
    {
        $contentData = ContentModel::getDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;

        try {
            if (isset($contentData) && $contentData != '') {
                return View::make('client.content',
                    array(
                        'breadcum' => $contentData->content_page_title,
                        'pageTitle' => $contentData->content_page_title,
                        'metaDescription' => $contentData->content_meta_tags,
                        'metaKeywords' => $contentData->content_meta_description,
                    )
                )
                    ->with('contentData', $contentData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }

    }

    /*
* dynamic content page
*/
    public function getContentParentDetail($purl, $url)
    {

        $contentData = ContentModel::getDetailByUrl($url);
        $pcontentData = ContentModel::getDetailByUrl($purl);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        //print_r($contentData);exit;
        try {
            if (isset($contentData) && $contentData != '') {
                return View::make('client.content',
                    array(
                        'pageTitle' => $contentData->content_page_title,
                        'breadcum' => '<a href="' . $sitePath . $pcontentData->content_url . '">' . $pcontentData->content_page_title . '</a>' . ' / ' . $contentData->content_page_title,
                        'metaDescription' => $contentData->content_meta_tags,
                        'metaKeywords' => $contentData->content_meta_description,
                    )
                )
                    ->with('contentData', $contentData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }

    }

    /*
    * news list page
    */
    public function getNewsList()
    {
        $seoDetail = SeoModel::getDetailBypage('news');
        $news = NewsModel::where('deleted', '0')->where('is_active', '1')->orderBy('news_date', 'DESC')->paginate(12);

        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.news',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('newsList', $news)
            ->with('sitePath', $sitePath);
    }

     public function getTestimonialList()
    {
        $seoDetail = SeoModel::getDetailBypage('news');
        $subtestimonials = SubTestimonialModel::where('deleted', '0')->where('is_active', '1')->get();

        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.testimonials')
            
        ->with('subtestimonials', $subtestimonials)
            ->with('sitePath', $sitePath);
    }
    public function getTestimonialDetail($testimonial_company)
    {
          $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
      $subtestimonial = SubTestimonialModel::getTestimonialDetailByUrl($testimonial_company);

      
                return View::make('client.testimonialdetail')
                  
                    ->with('subtestimonial', $subtestimonial)
                    ->with('sitePath', $sitePath);
          
    }


    /*
    * dynamic news detail page
    */
    public function getNewsDetail($url)
    {
        $newsData = NewsModel::getNewsDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        try {
            if (isset($newsData) && $newsData != '') {
                return View::make('client.news-detail',
                    array('pageTitle' => $newsData->news_page_title,
                        'metaDescription' => $newsData->news_meta_tags,
                        'metaKeywords' => $newsData->news_meta_description,
                    )
                )
                    ->with('newsData', $newsData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }


    /**
     * fetch change password
     */
    public function getChangePassword()
    {
        if (Auth::id()) {
            $seoDetail = SeoModel::getDetailBypage('change-password');
            $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
            return View::make('client.change-password',
                array('pageTitle' => $seoDetail->page_title,
                    'metaDescription' => $seoDetail->page_meta_tags,
                    'metaKeywords' => $seoDetail->page_meta_description,
                )
            )->with('sitePath', $sitePath);
        } else {
            return Redirect::to('login');
        }
    }

    /**
     * change password
     */
    public function ChangePassword()
    {
        $seoDetail = SeoModel::getDetailBypage('change-password');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $rules = array('old_password' => 'required',
            'new_password1' => 'required',
            'new_password2' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('client.change-password',
                array('pageTitle' => $seoDetail->page_title,
                    'metaDescription' => $seoDetail->page_meta_tags,
                    'metaKeywords' => $seoDetail->page_meta_description,
                )
            )->with('sitePath', $sitePath);
        } else {
            $auth = Auth::attempt(array(
                'id' => Auth::user()->id,
                'password' => Input::get('old_password'),
                'is_active' => 1,
                'deleted' => 0));
            if ($auth) {
                if (Input::get('new_password1') != (Input::get('new_password2'))) {
                    Session::flash('class', 'alert alert-error');
                    Session::flash('message', 'New Passwords do not match');
                    return View::make('client.change-password',
                        array('pageTitle' => $seoDetail->page_title,
                            'metaDescription' => $seoDetail->page_meta_tags,
                            'metaKeywords' => $seoDetail->page_meta_description,
                        )
                    )->with('sitePath', $sitePath);
                } else {
                    $user = User::changePassword(Input::get('new_password1'));
                    Session::flash('class', 'alert alert-success');
                    Session::flash('message', 'Password successfully updated.');
                    return View::make('client.change-password',
                        array('pageTitle' => $seoDetail->page_title,
                            'metaDescription' => $seoDetail->page_meta_tags,
                            'metaKeywords' => $seoDetail->page_meta_description,
                        )
                    )->with('sitePath', $sitePath);
                }
            } else {
                Session::flash('class', 'alert alert-error');
                Session::flash('message', 'Old Password do not match');
                return View::make('client.change-password',
                    array('pageTitle' => $seoDetail->page_title,
                        'metaDescription' => $seoDetail->page_meta_tags,
                        'metaKeywords' => $seoDetail->page_meta_description,
                    )
                )->with('sitePath', $sitePath);
            }
        }
    }

    /**
     * fetch success page
     */
    public function getSuccess()
    {
        if (Auth::id()) {
            $seoDetail = SeoModel::getDetailBypage('success');
            $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
            return View::make('client.success',
                array('pageTitle' => $seoDetail->page_title,
                    'metaDescription' => $seoDetail->page_meta_tags,
                    'metaKeywords' => $seoDetail->page_meta_description,
                )
            )->with('sitePath', $sitePath);
        } else {
            return Redirect::to('login');
        }
    }

    /**
     * fetch login page for wholesalers
     */
    public function getClientLogin()
    {
        $seoDetail = SeoModel::getDetailBypage('login');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.login',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath);
    }

    /**
     * fetch login page for wholesalers
     */
    public function postClientLogin()
    {
        $seoDetail = SeoModel::getDetailBypage('login');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $rules = array(
            'username' => 'required',
            'password' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('class', 'alert alert-warning');
            Session::flash('message', 'Username or password is missing.');
            return View::make('client.login',
                array('pageTitle' => $seoDetail->page_title,
                    'metaDescription' => $seoDetail->page_meta_tags,
                    'metaKeywords' => $seoDetail->page_meta_description,
                )
            )->with('sitePath', $sitePath);
        } else {
            $auth = Auth::attempt(array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'is_active' => 1,
                'user_type' => 3,
                'deleted' => 0));
            //var_dump($auth); exit();
            if ($auth) {
                return Redirect::to('home');
            } else {
                Session::flash('class', 'alert alert-warning');
                Session::flash('message', 'Username and password do not match.');
                return View::make('client.login',
                    array('pageTitle' => $seoDetail->page_title,
                        'metaDescription' => $seoDetail->page_meta_tags,
                        'metaKeywords' => $seoDetail->page_meta_description,
                    )
                )->with('sitePath', $sitePath);
            }
        }
    }

    /**
     * fetch google search page
     */
    public function getGoogleSearch()
    {
        $seoDetail = SeoModel::getDetailBypage('services');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.search',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath);
    }

    /**
     * get logout of wholesalers
     */
    public function getClientLogout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('home');
    }

    /**
    * dynamically reload captcha
    */
    public function reloadCaptcha()
    {
        $captcha = FunctionModel::create_captcha();
        $returnValue = '<img src="'. $captcha .'">';
        return $returnValue;    
    }
  


    /*
     * get Services
     */
    public function getServices()
    {
        $seoDetail = SeoModel::getDetailBypage('services');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $services = ServiceModel::getActiveServices();

        return View::make('client.services',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('services', $services);
    }

    /*
     * get Profiles
     */
    public function getProfiles()
    {
        $seoDetail = SeoModel::getDetailBypage('profiles');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $profiles = ProfileModel::getActiveProfiles();

        return View::make('client.profiles',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('profiles', $profiles);
    }

    /*
      * dynamic news detail page
      */
    public function getProfileDetail($url)
    {
        $profileData = ProfileModel::getProfileDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        try {
            if (isset($profileData) && $profileData != '') {
                return View::make('client.profile-detail',
                    array(
                        'pageTitle' => $profileData->title,
//								'metaDescription' => $profileData->news_meta_tags,
//								'metaKeywords' => $profileData->news_meta_description,
                    )
                )
                    ->with('profile', $profileData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }

    /*
     * get Brands
     */
    public function getBrands()
    {
        $seoDetail = SeoModel::getDetailBypage('brands');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $brands = BrandModel::getAllActiveBrands();

        return View::make('client.brands',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('brands', $brands);
    }

    public function getbrandDetail($url)
    {
        $brandData = BrandModel::getDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $productData = ProductModel::listProducts($brandData->id);
        $categories = CategoryModel::getAllActiveCategories();
        $brands = BrandModel::getAllActiveBrands();

        if (isset($brandData) && $brandData != '') {
            return View::make('client.brand-detail',
                array('pageTitle' => $brandData->name,
                    'metaDescription' => $brandData->meta_tags,
                    'metaKeywords' => $brandData->meta_desc,
                )
            )
                ->with('sitePath', $sitePath)
                ->with('brand', $brandData)
                ->with('products', $productData)
                ->with('brandPage', true)
                ->with('categories', $categories)
                ->with('brands', $brands);
        } else {
            return Redirect::to(404);
        }
    }

    /*
     * get Brands
     */
    public function getCategories()
    {
        $seoDetail = SeoModel::getDetailBypage('categories');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $categories = CategoryModel::getAllActiveCategories();

        return View::make('client.categories',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('categories', $categories);
    }

    /*
      * dynamic collection page category wise
      */
    public function getCategoryDetail($url)
    {
        $categoryData = CategoryModel::getDetailByUrl($url);
        $categories = CategoryModel::getAllActiveCategories();
        $brands = BrandModel::getAllActiveBrands();
        try {
            if ($categoryData) {
                $products = ProductModel::getProductsCategoryWise($categoryData->id);
                $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
                return View::make('client.category-detail',
                    array('pageTitle' => $categoryData->category_page_title,
                        'metaDescription' => $categoryData->category_meta_description,
                        'metaKeywords' => $categoryData->category_meta_tags,
                    )
                )
                    ->with('products', $products)
                    ->with('category', $categoryData)
                    ->with('sitePath', $sitePath)
                    ->with('categories', $categories)
                    ->with('brands', $brands);

            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }

    /*
      * fetch all products collection page
      */
    public function getProducts()
    {
        $categories = CategoryModel::getAllActiveCategories();
        $brands = BrandModel::getAllActiveBrands();
        $seoDetail = SeoModel::getDetailBypage('collection');
        $products = ProductModel::getActiveProducts();
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.products',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )
            ->with('products', $products)
            ->with('sitePath', $sitePath)
            ->with('categories', $categories)
            ->with('brands', $brands);
    }

    /*
      * fetch all products collection page
      */
    public function getFAQ()
    {

        $seoDetail = SeoModel::getDetailBypage('faqs');
        $faqs = FaqModel::getActiveFaqs();
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.faqs',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )
            ->with('sitePath', $sitePath)
            ->with('faqs', $faqs);
    }

    /*
     * get Portfolios
     */
    public function getPortfolios()
    {
        $seoDetail = SeoModel::getDetailBypage('portfolios');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $portfolios = PortfolioModel::getActivePortfolios();

        return View::make('client.portfolios',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('portfolios', $portfolios);
    }

    /*
      * projects list page
      */
    public function getProjects($type = '')
    {

        $seoDetail = SeoModel::getDetailBypage('projects');
        $projects = ProjectModel::getActiveProjects($type);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
//    die(count($projects));
        return View::make('client.projects',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('projects', $projects)
            ->with('sitePath', $sitePath)
            ->with('type', $type);
    }

    /*
    * dynamic project detail page
    */
    public function getProjectDetail($url)
    {
        $project = ProjectModel::getProjectDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        try {
            if (isset($project) && $project != '') {
                return View::make('client.project-detail',
                    array('pageTitle' => $project->page_title,
                        'metaDescription' => $project->meta_tags,
                        'metaKeywords' => $project->meta_description,
                    )
                )
                    ->with('project', $project)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }

    public function getEventsList()
    {
        $seoDetail = SeoModel::getDetailBypage('news');
        $news = NewsModel::getAllActiveNews('A');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.events',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('newsList', $news)
            ->with('sitePath', $sitePath);
    }

    /*
    * dynamic news detail page
    */
    public function getEventsDetail($url)
    {
        $newsData = NewsModel::getNewsDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        try {
            if (isset($newsData) && $newsData != '') {
                return View::make('client.events-detail',
                    array('pageTitle' => $newsData->news_page_title,
                        'metaDescription' => $newsData->news_meta_tags,
                        'metaKeywords' => $newsData->news_meta_description,
                    )
                )
                    ->with('newsData', $newsData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }

// list of calendar events
    public function getCalEventsList()
    {
        $seoDetail = SeoModel::getDetailBypage('events');
        $events = EventsModel::getAllActiveEvents();
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.calevents',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('eventsList', $events)
            ->with('sitePath', $sitePath);
    }

    /*
    * dynamic news detail page
    */
    public function getCalEventsDetail($url)
    {
        $eventsData = EventsModel::getEventsDetailByUrl($url);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        try {
            if (isset($eventsData) && $eventsData != '') {
                return View::make('client.calevents-detail',
                    array('pageTitle' => $eventsData->events_page_title,
                        'metaDescription' => $eventsData->events_meta_tags,
                        'metaKeywords' => $eventsData->events_meta_description,
                    )
                )
                    ->with('eventsData', $eventsData)
                    ->with('sitePath', $sitePath);
            } else {
                return Redirect::to(404);
            }
        } catch (ErrorException $e) {
            return Redirect::to(404);
        }
    }

    /*
     * get Student Opinions
     */
    public function getPrincipalOpinions()
    {
        $seoDetail = SeoModel::getDetailBypage('principal');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $principal = TestimonialModel::getPrincipaldetail();

        return View::make('client.testimonials',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('principal', $principal);
    }

    /*
     * get Student Opinions
     */
    public function getStudentOpinions()
    {
        $seoDetail = SeoModel::getDetailBypage('student-opinions');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $testimonials = TestimonialModel::getActiveTestimonials('SO');

        return View::make('client.testimonials',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('testimonials', $testimonials);
    }

    /*
     * get UC Geniuses
     */
    public function getGeniuses()
    {
        $seoDetail = SeoModel::getDetailBypage('geniuses');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $testimonials = TestimonialModel::getActiveTestimonials('G');

        return View::make('client.testimonials',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('testimonials', $testimonials);
    }

    /*
     * get Downloads
     */
    public function getDownloadlist()
    {
        $seoDetail = SeoModel::getDetailBypage('downloads');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        // $downloads	= DownloadModel::getActiveDownloads();

        return View::make('client.downloadlist',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath);
        // ->with('downloads', $downloads);
    }


    public function getDownloadSubCategory($slug, $id)
    {
        $seoDetail = SeoModel::getDetailBypage('downloads');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $category = CategoryModel::find($id);
        $subcategory = CategoryModel::where('parent_id', $id)->where('is_active', 1)->get();

        return View::make('client.downloadListSubCategory',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('subcategories', $subcategory)
            ->with('category', $category);

    }

    /*
    * get Downloads
    */
    public function getDownloads($slug, $id)
    {
        $seoDetail = SeoModel::getDetailBypage('downloads');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $category = CategoryModel::where('category_url', $slug)->where('id', $id)->first();
        $downloads = DownloadModel::getActiveDownloadsByCategory($category->id);

        return View::make('client.downloads',
            array(
                'pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('downloads', $downloads)
            ->with('category', $category);
    }

    public function forceDownload($id)
    {
        $objectDownload = DownloadModel::find($id);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        return View::make('client.force-download')
            ->with('download', $objectDownload)
            ->with('sitePath', $sitePath);
    }


    /*
     * get Publications
     */
    public function getPublicationlist()
    {
        $seoDetail = SeoModel::getDetailBypage('publications');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        // $publications	= PublicationModel::getActivePublications();

        return View::make('client.publicationlist',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath);
        // ->with('publications', $publications);
    }

    /*
    * get Publications
    */
    public function getPublications($url)
    {
        if ($url == 'Prospectus') {
            $type = 'P';
            $name = 'Prospectus';
        } else {
            $type = 'J';
            $name = 'Journal';
        }
        $seoDetail = SeoModel::getDetailBypage('publications');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $publications = PublicationModel::getActivePublications($type);

        return View::make('client.publications',
            array('pageTitle' => $seoDetail->page_title . ' - ' . $name,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('publications', $publications);
    }

    /*
     * Get albums
     */
    public function getAlbums()
    {
        $seoDetail = SeoModel::getDetailBypage('gallery');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        // $albums = AlbumModel::getActiveAlbums();
$albums=$users = DB::table('album')
//                    ->leftJoin('media', 'album.id', '=', 'media.album_id')
                    ->where('album.deleted', '=', 0)
                    ->where('album.is_active', '=', 1)
                    ->orderby('album.created_at', 'desc')
                    // ->groupby('album.id')
                    ->get();
                  
        return View::make('client.albums',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('albums', $albums);
    }

    public function getAlbumImages($url)
    {
        $album = AlbumModel::getDetailByUrl($url);
        $medias = MediaModel::getActiveMedias($album->id);
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;

        return View::make('client.album-images',
            array('pageTitle' => $album->album_title
            )
        )->with('sitePath', $sitePath)
            ->with('images', $medias)
            ->with('album', $album);
    }

    /*
     * get UC Geniuses
     */
    public function getSlcStars()
    {
        $seoDetail = SeoModel::getDetailBypage('geniuses');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $slcstars = SlcstarModel::getActiveSlcstars();

        return View::make('client.slcstars',
            array('pageTitle' => $seoDetail->page_title,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('slcstars', $slcstars);
    }

    /*
     * All achievements MBBS
     */
    public function getMbbsAchievements()
    {
        $seoDetail = SeoModel::getDetailBypage('mbbs');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('M');

        return View::make('client.achievements',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments);
    }

    /*
     * All achievements Engg
     */

    public function getEnggAchievements()
    {
        $seoDetail = SeoModel::getDetailBypage('engineering');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('E');

        return View::make('client.achievements',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments);
    }

    /*
     * All achievements Outstanding Performance
     */
    public function getOutstandingPerformances()
    {
        $seoDetail = SeoModel::getDetailBypage('outstanding-performance');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('OP');

        return View::make('client.achievements',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments);
    }

    /*
     * All achievements HSEB
     */
    public function getHsebAchievements()
    {
        $seoDetail = SeoModel::getDetailBypage('achievements-in-hseb');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('H');

        return View::make('client.achievements',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments);
    }

    /*
     * All achievements +2 Toppers
     */

    public function getPlusTwoToppers()
    {
        $seoDetail = SeoModel::getDetailBypage('uc-plus-2-toppers');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('P2');

        return View::make('client.achievements',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments);
    }

    public function calendarEvents()
    {
        $today = date('Y-m-d');
        $eventDate = NewsModel::select('news_date')->where('news_type', 'A')->where('deleted', 0)->where('is_active', 1)->distinct()->get();
        $dates = array();
        $samedate = '';
        $body = 'Events :';
        $count = 0;
        foreach ($eventDate as $i => $date) {
            $body = NewsModel::getEventsbyDate($date->news_date);
            $dates[$i] = array(
                'date' => $date->news_date,
                'badge' => true,
                'title' => 'Events For ' . date('M d, Y', strtotime($date->news_date)),
                'body' => $body,
                'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>',
            );

        }
//print_r($dates);
        echo json_encode($dates);

    }

    /*
    * All achievements +2 Toppers
    */

    public function getOnlineApplication()
    {
        $seoDetail = SeoModel::getDetailBypage('onlineform');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;
        $achievments = AchievementModel::getActiveAchievements('P2');

        return View::make('client.application',
            array('pageTitle' => $seoDetail->page_title,
                'pageDesc' => $seoDetail->desc,
                'metaDescription' => $seoDetail->page_meta_tags,
                'metaKeywords' => $seoDetail->page_meta_description,
            )
        )->with('sitePath', $sitePath)
            ->with('achievements', $achievments)
            ->with('captcha', $captcha = FunctionModel::create_captcha());
    }


    public function postOnlineApplication()
    {

        $seoDetail = SeoModel::getDetailBypage('onlineform');
        $sitePath = ConstantModel::getDetailByName('site_path')->constant_value;

        if (Session::get('phrase') != Input::get('captcha')) {
            $captcha = FunctionModel::create_captcha();
            Session::flash('class', 'alert alert-error');
            Session::flash('message', '<span style="color:red;">Invalid Image verification.');
            return Redirect::back()->withInput()->with(array('pageTitle' => $seoDetail->page_title, 'metaDescription' => $seoDetail->page_meta_tags, 'metaKeywords' => $seoDetail->page_meta_description, 'captcha' => $captcha, 'sitePath' => $sitePath));

        } else {

            $userEmail = trim(Input::get('email'));

            $arrayField = array('full_name', 'email', 'contact_no', 'previous_college', 'known_from', 'choose');

            $itemData = array();

            foreach ($arrayField as $singleValue) {
                $itemData[$singleValue] = trim($_POST[$singleValue]);
            }

            $id = DB::table('form')->insertGetId($itemData);


            if ($id) {
                // send user email
                $subject = "Online Application submitted.";
                $siteName = ConstantModel::getDetailByName('site_name')->constant_value;
                $contentUser = '<table cellspacing="0" cellpadding="0" width="100%">';
                $contentUser .= '<tr><td><p>Dear <strong>' . Input::get('full_name') . ',</strong> <br />';
                $contentUser .= '<br />Your Online Application From has been submitted for <strong>' . Input::get('choose') . '.</strong> If you have any query , feel free to contact us.<br/>You can also contact us at:<br><p>
					<strong>Orchid International College</strong><br>Address: Sinamangal 9, Kathmandu <br> Phone: 01-4110672 <br>Email: info@oic.edu.np<br> Url: www.oic.edu.np
					</p></td>';

                $contentUser .= '<tr><td><p>Kind Regards,</strong> <br />';
                $contentUser .= $siteName . '</td>';
                $contentUser .= '</table>';

                $email = FunctionModel::sendEmail($userEmail, $subject, $contentUser);

                Session::flash('class', 'alert alert-success');
                Session::flash('message', '<span style="color:green;">Application successfully Submitted');

                return View::make('client.application',
                    array('pageTitle' => $seoDetail->page_title,
                        'pageDesc' => $seoDetail->desc,
                        'metaDescription' => $seoDetail->page_meta_tags,
                        'metaKeywords' => $seoDetail->page_meta_description,
                    )
                )->with('sitePath', $sitePath)
                    ->with('achievements', '')
                    ->with('captcha', $captcha = FunctionModel::create_captcha());
            } else {

                Session::flash('class', 'alert alert-error');
                Session::flash('message', 'Some fields are missing');
                return View::make('client.application',
                    array('pageTitle' => $seoDetail->page_title,
                        'pageDesc' => $seoDetail->desc,
                        'metaDescription' => $seoDetail->page_meta_tags,
                        'metaKeywords' => $seoDetail->page_meta_description,
                    )
                )->with('sitePath', $sitePath)
                    ->with('achievements', '')
                    ->with('captcha', $captcha = FunctionModel::create_captcha());
            }

        }


    }
}
