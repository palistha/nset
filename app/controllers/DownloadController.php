<?php

class DownloadController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getDownloads()
    {
        $downloadList = DownloadModel::getAllDownloads();
		$i = 1;
		$downloads = '';
		foreach($downloadList as $download) {
			$category = CategoryModel::find($download->category_id);
            $category_name=$category ? $category->category_heading:"N/A";
			$downloads .= '<tr class="odd gradeX">';
			$downloads .= '<td>'.$i++.'</td>';
			$downloads .= '<td>'.$download->download_caption.'</td>';
			$downloads .= '<td>'.$category_name.'</td>';

			if(file_exists("uploads/downloads/".$download->download_attachment) && $download->download_attachment!=''){
				$downloads .= '<td><a target="_blank" href="'.'../uploads/downloads/'.$download->download_attachment.'">'.$download->download_caption.'</a></td>';
			} else {
				$downloads .= '<td></td>';
			}
			if($download->is_active=='1') { 
				$downloads .= '<td>'.'<a href="javascript:void(0)" class="unpublish-download" id="'.$download->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$downloads .= '<td>'.'<a href="javascript:void(0)" class="publish-download" id="'.$download->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$downloads .= '<td>'.$download->updated_at.'</td>';
            $downloads .= '<td>'.
						'<a href="download/edit/'.$download->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-download" id="'.$download->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$downloads .= '</tr>';
		}

        return View::make('admin.download.list', array('downloads' => $downloads));
    }
	
	public function getAddDownload()
	{
		return View::make('admin.download.add');
	}
	
	public function addDownload()
	{
		$rules = array('download_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.download.add');
		} 
		else {						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/downloads/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png|doc|docx|xls|xlsx|pdf|txt){1}$/i";
			$maximum_filesize = 10 * 1024 * 1024;
//      print_r($imageFile);
      $doc_type = $imageFile->getClientMimeType();
      	
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "downloads" . "_" . time();
					
			if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
				$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
			} else if (preg_match($rEFileTypes, $extension) == false) {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Warning : Invalid Image File!');
				return View::make('admin.download.add');
			} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 10MB!");
					return View::make('admin.download.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectDownload = new DownloadModel();
			$objectDownload->download_caption = Input::get('download_caption');
			$objectDownload->category_id = Input::get('parent_id');

			$objectDownload->is_active = Input::get('is_active');
			$objectDownload->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectDownload->download_attachment = $logo;
        $objectDownload->document_type = $doc_type;
			}				
			$objectDownload->save();
			
			if($objectDownload->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Download successfully added');
				return View::make('admin.download.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.download.add');				
			}			
		}
	}
	
	public function getEditDownload($id)
	{
		$downloadDetail = DownloadModel::find($id);		
		try {
			if($downloadDetail->deleted == 0) {
				return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editDownload()
	{	
		$downloadId = Input::get('download_id');
		$downloadDetail = DownloadModel::find($downloadId);
		$rules = array('download_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
		} 
		else {
						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/downloads/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png|doc|docx|xls|xlsx|pdf|txt){1}$/i";
			$maximum_filesize = 10 * 1024 * 1024;
//      print_r($imageFile); die();
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "downloads" . "_" . time();
        $doc_type = $imageFile->getClientMimeType();

					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 10MB!");
					return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			
			$objectDownload = DownloadModel::find($downloadId);
			$objectDownload->download_caption = Input::get('download_caption');
			$objectDownload->category_id = Input::get('parent_id');
			$objectDownload->is_active = Input::get('is_active');
			$objectDownload->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectDownload->download_attachment = $logo;
        $objectDownload->document_type = $doc_type;
			}				
			$objectDownload->save();
			
			if($objectDownload->id) {
				$downloadDetail = DownloadModel::find($downloadId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Download successfully updated');
				return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.download.edit')->with('downloadDetail',$downloadDetail);
			}		
		}
	}
	
	public function publishDownload()
	{ 
		$downloadId = Input::get('download_id');
		$objectDownload = DownloadModel::find($downloadId);
		$objectDownload->updated_by = Auth::user()->id;		
		$objectDownload->is_active = 1;
		$objectDownload->save();
		
		if($objectDownload->id) {
			$array = array('message' => 'Download is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishDownload()
	{ 
		$downloadId = Input::get('download_id');
		$objectDownload = DownloadModel::find($downloadId);
		$objectDownload->updated_by = Auth::user()->id;		
		$objectDownload->is_active = 0;
		$objectDownload->save();
		
		if($objectDownload->id) {
			$array = array('message' => 'Download is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteDownload()
	{ 
		$downloadId = Input::get('download_id');
		$objectDownload = DownloadModel::find($downloadId);
		$objectDownload->updated_by = Auth::user()->id;		
		$objectDownload->is_active = 0;
		$objectDownload->deleted = 1;
		$objectDownload->save();
		
		if($objectDownload->id) {
			$array = array('message' => 'Download is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
}
