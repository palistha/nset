<?php

class BrandController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getBrands()
    {
        $brandList = BrandModel::getAllBrands();
		
		//var_dump($brandList); die();
		$i = 1;
		$brands = '';
		foreach($brandList as $brand) {
			$brands .= '<tr class="odd gradeX">';
			$brands .= '<td>'.$i++.'</td>';
			$brands .= '<td>'.$brand->name.'</td>';
			if($brand->is_active=='1') { 
				$brands .= '<td>'.'<a href="javascript:void(0)" class="unpublish-brand" id="'.$brand->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$brands .= '<td>'.'<a href="javascript:void(0)" class="publish-brand" id="'.$brand->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$brands .= '<td>'.$brand->updated_at.'</td>';
            $brands .= '<td>'.
						'<a href="brand/edit/'.$brand->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-brand" id="'.$brand->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$brands .= '</tr>';
		}

        return View::make('admin.brand.list', array('brands' => $brands));
    }
	
	public function getAddBrand()
	{
		return View::make('admin.brand.add');
	}
	
	public function addBrand()
	{
		$rules = array('name' => 'required',
					   'url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.brand.add');
		} 
		else {
			$brandHeading = Input::get('name');
			$brandUrl = Input::get('url');
			$brandUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $brandUrl);
			$brandExist = BrandModel::checkExist($brandUrl);
			if( count($brandExist)!=0) {
				$message = 'brand <b>'.$brandHeading.'</b> with url <b>'.$brandUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.brand.add');
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/brands/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
        
        $thumbFile = Input::file('thumb_image');
        
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "brand" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.brand.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.brand.add');
					}				
				}
        
        if($thumbFile) {
					$filename = $thumbFile->getClientOriginalName();
					$extension1 = strrchr($filename, '.');
					$size = $thumbFile->getSize();					
					$new_thumb_name = "brand_thumb" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension1)) {
						$attachment_thumb = $thumbFile->move($destinationPath, $new_thumb_name.$extension1);						
					} else if (preg_match($rEFileTypes, $extension1) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.brand.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.brand.add');
					}				
				}
        
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
        $logo_thumb = isset($attachment_thumb) ? $new_thumb_name . $extension1 : NULL;
				
				$objectBrand = new BrandModel;
				$objectBrand->name		= Input::get('name');
				$objectBrand->url			= $brandUrl;
				$objectBrand->meta_tags	= Input::get('meta_tags');
				$objectBrand->meta_desc	= Input::get('meta_desc');
				$objectBrand->short_desc	= Input::get('short_desc');
				$objectBrand->desc	= Input::get('desc');
				$objectBrand->is_active	= Input::get('is_active');
				$objectBrand->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectBrand->attachment = $logo; 
				}
        if($logo_thumb != '')
				{
					$objectBrand->thumb_attachment = $logo_thumb; 
				}
				$objectBrand->save();
				
				if($objectBrand->id) {
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Brand successfully added');
					return View::make('admin.brand.add');
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Something error');
					return View::make('admin.brand.add');
				}
				
			}			
		}
	}
	
	public function getEditBrand($id)
	{
		$brandDetail = BrandModel::find($id);		
		try {
			if($brandDetail->deleted == 0) {
				return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editBrand()
	{	
		$brandId = Input::get('brand_id');
		$brandDetail = BrandModel::find($brandId);
		
		$rules = array('name' => 'required',
					   'url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
		} 
		else {
			$brandHeading = Input::get('name');
			$brandUrl = Input::get('url');
			$brandUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $brandUrl);
			$brandExist = BrandModel::checkExist($brandUrl);
			if( count($brandExist)!=0 && $brandExist->id != $brandId) {
				$message = 'brand <b>'.$brandHeading.'</b> with url <b>'.$brandUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/brands/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
        $thumbFile = Input::file('thumb_image');
        
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "brand" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
					}				
				}
        
        if($thumbFile) {
					$filename = $thumbFile->getClientOriginalName();
					$extension1 = strrchr($filename, '.');
					$size = $thumbFile->getSize();					
					$new_thumb_name = "brand_thumb" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension1)) {
						$attachment_thumb = $thumbFile->move($destinationPath, $new_thumb_name.$extension1);						
					} else if (preg_match($rEFileTypes, $extension1) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
        $logo_thumb = isset($attachment_thumb) ? $new_thumb_name . $extension1 : NULL;
				//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;			
				
				$objectBrand = BrandModel::find($brandId);
				$objectBrand->name		= Input::get('name');
				$objectBrand->url			= $brandUrl;
				$objectBrand->meta_tags	= Input::get('meta_tags');
				$objectBrand->meta_desc	= Input::get('meta_desc');
				$objectBrand->short_desc	= Input::get('short_desc');
				$objectBrand->desc	= Input::get('desc');
				$objectBrand->is_active	= Input::get('is_active');
				$objectBrand->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectBrand->attachment = $logo;
				}
        
        if($logo_thumb != '')
				{
					$objectBrand->thumb_attachment = $logo_thumb; 
				}
        
				if(Input::get('show_in') != '')
				{
					$objectBrand->show_in = join(',',Input::get('show_in'));
				}
				$objectBrand->save();
				
				if($objectBrand->id) {
					$brandDetail = BrandModel::find($brandId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Brand successfully updated');
					return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.brand.edit')->with('brandDetail',$brandDetail);
				}
				
			}			
		}
	}
	
	public function publishBrand()
	{ 
		$brandId = Input::get('brand_id');
		$objectBrand = BrandModel::find($brandId);
		$objectBrand->is_active = 1;
		$objectBrand->save();
		
		if($objectBrand->id) {
			$array = array('message' => 'Brand is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishBrand()
	{ 
		$brandId = Input::get('brand_id');
		$objectBrand = BrandModel::find($brandId);
		$objectBrand->is_active = 0;
		$objectBrand->save();
		
		if($objectBrand->id) {
			$array = array('message' => 'Brand is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteBrand()
	{ 
		$brandId = Input::get('brand_id');
		$objectBrand = BrandModel::find($brandId);
		$objectBrand->is_active = 0;
		$objectBrand->deleted = 1;
		$objectBrand->save();
		
		if($objectBrand->id) {
			$array = array('message' => 'Brand is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
//	public function brandDetail($url)
//	{
//		$brandData = BrandModel::getDetailByUrl($url);
//		if(isset($brandData) && $brandData != '') {
//			return View::make('client.about', 
//						array('pageTitle' => $brandData->name,
//								'metaDescription' => $brandData->meta_tags,
//								'metaKeywords' => $brandData->meta_desc,
//							)
//						)			
//						->with('brandData',$brandData);
//		}
//		else {
//			return Redirect::to(404);
//		}
//	}
	
}
