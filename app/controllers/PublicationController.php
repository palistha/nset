<?php

class PublicationController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getPublications()
    {
        $publicationList = PublicationModel::getAllPublications();
		
		//var_dump($publicationList); die();
		$i = 1;
		$publications = '';
		foreach($publicationList as $publication) {
			$publications .= '<tr class="odd gradeX">';
			$publications .= '<td>'.$i++.'</td>';
			$publications .= '<td>'.$publication->publication_caption.'</td>';
			
			if(file_exists("uploads/publications/".$publication->publication_attachment) && $publication->publication_attachment!=''){
				$publications .= '<td><a target="_blank" href="'.'../uploads/publications/'.$publication->publication_attachment.'">'.$publication->publication_caption.'</a></td>';
			} else {
				$publications .= '<td></td>';
			}
			if($publication->is_active=='1') { 
				$publications .= '<td>'.'<a href="javascript:void(0)" class="unpublish-publication" id="'.$publication->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$publications .= '<td>'.'<a href="javascript:void(0)" class="publish-publication" id="'.$publication->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$publications .= '<td>'.$publication->updated_at.'</td>';
            $publications .= '<td>'.
						'<a href="publication/edit/'.$publication->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-publication" id="'.$publication->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$publications .= '</tr>';
		}

        return View::make('admin.publication.list', array('publications' => $publications));
    }
	
	public function getAddPublication()
	{
		return View::make('admin.publication.add');
	}
	
	public function addPublication()
	{
		$rules = array('publication_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.publication.add');
		} 
		else {						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/publications/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png|doc|docx|xls|xlsx|pdf|txt){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
//      print_r($imageFile);
      $doc_type = $imageFile->getClientMimeType();
      	
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "publications" . "_" . time();
					
			if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
				$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
			} else if (preg_match($rEFileTypes, $extension) == false) {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Warning : Invalid Image File!');
				return View::make('admin.publication.add');
			} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.publication.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectPublication = new PublicationModel();
			$objectPublication->publication_caption = Input::get('publication_caption');
			$objectPublication->publication_type = Input::get('publication_type');
			
			$objectPublication->is_active = Input::get('is_active');
			$objectPublication->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectPublication->publication_attachment = $logo;
        $objectPublication->document_type = $doc_type;
			}				
			$objectPublication->save();
			
			if($objectPublication->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Publication successfully added');
				return View::make('admin.publication.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.publication.add');				
			}			
		}
	}
	
	public function getEditPublication($id)
	{
		$publicationDetail = PublicationModel::find($id);		
		try {
			if($publicationDetail->deleted == 0) {
				return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editPublication()
	{	
		$publicationId = Input::get('publication_id');
		$publicationDetail = PublicationModel::find($publicationId);
		$rules = array('publication_caption' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
		} 
		else {
						
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/publications/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png|doc|docx|xls|xlsx|pdf|txt){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
//      print_r($imageFile); die();
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "publications" . "_" . time();
        $doc_type = $imageFile->getClientMimeType();

					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			
			$objectPublication = PublicationModel::find($publicationId);
			$objectPublication->publication_caption = Input::get('publication_caption');
			$objectPublication->publication_type = Input::get('publication_type');
			
			$objectPublication->is_active = Input::get('is_active');
			$objectPublication->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectPublication->publication_attachment = $logo;
        $objectPublication->document_type = $doc_type;
			}				
			$objectPublication->save();
			
			if($objectPublication->id) {
				$publicationDetail = PublicationModel::find($publicationId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Publication successfully updated');
				return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.publication.edit')->with('publicationDetail',$publicationDetail);
			}		
		}
	}
	
	public function publishPublication()
	{ 
		$publicationId = Input::get('publication_id');
		$objectPublication = PublicationModel::find($publicationId);
		$objectPublication->updated_by = Auth::user()->id;		
		$objectPublication->is_active = 1;
		$objectPublication->save();
		
		if($objectPublication->id) {
			$array = array('message' => 'Publication is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishPublication()
	{ 
		$publicationId = Input::get('publication_id');
		$objectPublication = PublicationModel::find($publicationId);
		$objectPublication->updated_by = Auth::user()->id;		
		$objectPublication->is_active = 0;
		$objectPublication->save();
		
		if($objectPublication->id) {
			$array = array('message' => 'Publication is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deletePublication()
	{ 
		$publicationId = Input::get('publication_id');
		$objectPublication = PublicationModel::find($publicationId);
		$objectPublication->updated_by = Auth::user()->id;		
		$objectPublication->is_active = 0;
		$objectPublication->deleted = 1;
		$objectPublication->save();
		
		if($objectPublication->id) {
			$array = array('message' => 'Publication is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
}
