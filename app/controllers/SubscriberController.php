<?php

class SubscriberController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	
	public function addSubscriber($email)
	{
		$data = array('email_address'=>$email,'registered_on'=>date("Y-m-d"),'is_active'=>1,);
		return $subscriberAdd = SubscriberModel::createSubsciber($data);
	}	
}
