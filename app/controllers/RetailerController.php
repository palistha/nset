<?php

class RetailerController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getRetailers()
    {
        $retailerList = RetailerModel::getAllRetailers();
		$i = 1;
		$retailers = '';
		foreach($retailerList as $retailer) {
			$retailers .= '<tr class="odd gradeX">';
			$retailers .= '<td>'.$i++.'</td>';
			$retailers .= '<td>'.$retailer->retailer_heading.'</td>';
			$retailers .= '<td>'.$retailer->city_heading.'</td>';
			if($retailer->is_active=='1') { 
				$retailers .= '<td>'.'<a href="javascript:void(0)" class="unpublish-retailer" id="'.$retailer->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$retailers .= '<td>'.'<a href="javascript:void(0)" class="publish-retailer" id="'.$retailer->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$retailers .= '<td>'.
						'<a href="retailer/edit/'.$retailer->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-retailer" id="'.$retailer->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$retailers .= '</tr>';
		}

        return View::make('admin.retailer.list', array('retailers' => $retailers));
    }
	
	public function getAddRetailer()
	{
		return View::make('admin.retailer.add');
	}
	
	public function addRetailer()
	{
		$rules = array('retailer_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.retailer.add');
		} 
		else {
			$objectRetailer = new RetailerModel();
			$objectRetailer->city_id = Input::get('city_id');
			$objectRetailer->retailer_heading = Input::get('retailer_heading');
			$objectRetailer->retailer_short_description = Input::get('retailer_short_description');
			$objectRetailer->is_active = Input::get('is_active');
			$objectRetailer->updated_by = Auth::user()->id;			
			$objectRetailer->save();
			
			if($objectRetailer->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'retailer successfully added');
				return View::make('admin.retailer.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.retailer.add');
			}						
		}
	}
	
	public function getEditRetailer($id)
	{
		$retailerDetail = RetailerModel::find($id);		
		try {
			if($retailerDetail->deleted == 0) {
				return View::make('admin.retailer.edit')->with('retailerDetail',$retailerDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editRetailer()
	{	
		$retailerId = Input::get('retailer_id');
		$retailerDetail = RetailerModel::find($retailerId);
		$rules = array('retailer_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.retailer.edit')->with('retailerDetail',$retailerDetail);
		} 
		else {
			$objectRetailer = RetailerModel::find($retailerId);
			$objectRetailer->city_id = Input::get('city_id');
			$objectRetailer->retailer_heading = Input::get('retailer_heading');
			$objectRetailer->retailer_short_description = Input::get('retailer_short_description');
			$objectRetailer->is_active = Input::get('is_active');
			$objectRetailer->updated_by = Auth::user()->id;			
			$objectRetailer->save();
				
			if($objectRetailer->id) {
				$retailerDetail = RetailerModel::find($retailerId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Retailer successfully updated');
				return View::make('admin.retailer.edit')->with('retailerDetail',$retailerDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.retailer.edit')->with('retailerDetail',$retailerDetail);
			}					
		}
	}
	
	public function publishRetailer()
	{ 
		$retailerId = Input::get('retailer_id');
		$objectRetailer = RetailerModel::find($retailerId);
		$objectRetailer->is_active = 1;
		$objectRetailer->save();
		
		if($objectRetailer->id) {
			$array = array('message' => 'Retailer is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishRetailer()
	{ 
		$retailerId = Input::get('retailer_id');
		$objectRetailer = RetailerModel::find($retailerId);
		$objectRetailer->is_active = 0;
		$objectRetailer->save();
		
		if($objectRetailer->id) {
			$array = array('message' => 'Retailer is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteRetailer()
	{ 
		$retailerId = Input::get('retailer_id');
		$objectRetailer = RetailerModel::find($retailerId);
		$objectRetailer->is_active = 0;
		$objectRetailer->deleted = 1;
		$objectRetailer->save();
		
		if($objectRetailer->id) {
			$array = array('message' => 'Retailer is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

}
