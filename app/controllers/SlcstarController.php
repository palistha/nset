<?php

class SlcstarController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getSlcstars()
    {
        $slcstarList = SlcstarModel::getAllSlcstars();
		
		//var_dump($slcstarList); die();
		$i = 1;
		$slcstars = '';
		foreach($slcstarList as $slcstar) {
			$slcstars .= '<tr class="odd gradeX">';
			$slcstars .= '<td>'.$i++.'</td>';
			$slcstars .= '<td>'.$slcstar->slcstar_auther.'</td>';
			if($slcstar->is_active=='1') { 
				$slcstars .= '<td>'.'<a href="javascript:void(0)" class="unpublish-slcstar" id="'.$slcstar->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$slcstars .= '<td>'.'<a href="javascript:void(0)" class="publish-slcstar" id="'.$slcstar->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$slcstars .= '<td>'.$slcstar->updated_at.'</td>';
            $slcstars .= '<td>'.
						'<a href="slcstar/edit/'.$slcstar->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-slcstar" id="'.$slcstar->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$slcstars .= '</tr>';
		}

        return View::make('admin.slcstar.list', array('slcstars' => $slcstars));
    }
	
	public function getAddSlcstar()
	{
		return View::make('admin.slcstar.add');
	}
	
	public function addSlcstar()
	{
		$rules = array('slcstar_auther' => 'required');
    $rules = array('slcstar_per' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.slcstar.add');
		} 
		else {
			$slcstarAuther = Input::get('slcstar_auther');
			$slcstarUrl = Input::get('slcstar_url');
			$slcstarUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $slcstarUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/slcstars/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
			
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "slcstars" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.slcstar.add');
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.slcstar.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectSlcstar = new SlcstarModel();
			$objectSlcstar->slcstar_auther = Input::get('slcstar_auther');
      $objectSlcstar->slcstar_per = Input::get('slcstar_per');
      $objectSlcstar->slcstar_from = Input::get('slcstar_from');
      $objectSlcstar->slcstar_remark = Input::get('slcstar_remark');
//			$objectSlcstar->slcstar_company = Input::get('slcstar_company');
			$objectSlcstar->slcstar_description = Input::get('slcstar_description');
			$objectSlcstar->is_active = Input::get('is_active');
			$objectSlcstar->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectSlcstar->slcstar_attachment = $logo;
			}
			$objectSlcstar->save();
			
			if($objectSlcstar->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Slc Star successfully added');
				return View::make('admin.slcstar.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.slcstar.add');
			}
						
		}
	}
	
	public function getEditSlcstar($id)
	{
		$slcstarDetail = SlcstarModel::find($id);		
		try {
			if($slcstarDetail->deleted == 0) {
				return View::make('admin.slcstar.edit')->with('slcstarDetail',$slcstarDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editSlcstar()
	{	
		$slcstarId = Input::get('slcstar_id');
		$slcstarDetail = SlcstarModel::find($slcstarId);
		$rules = array('slcstar_auther' => 'required');
    $rules = array('slcstar_per' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.slcstar.edit')->with('slcstarDetail',$slcstarDetail);
		} 
		else {
			$slcstarHeading = Input::get('slcstar_auther');
			$slcstarUrl = Input::get('slcstar_url');
			$slcstarUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $slcstarUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/slcstars/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "slcstars" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.slcstar.edit')->with('slcstarDetail',$slcstarDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.slcstar.edit')->with('slcstarDetail',$slcstarDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
				
				$objectSlcstar = SlcstarModel::find($slcstarId);
				$objectSlcstar->slcstar_auther = Input::get('slcstar_auther');
        $objectSlcstar->slcstar_per = Input::get('slcstar_per');
        $objectSlcstar->slcstar_from = Input::get('slcstar_from');
        $objectSlcstar->slcstar_remark = Input::get('slcstar_remark');
//				$objectSlcstar->slcstar_company = Input::get('slcstar_company');
				$objectSlcstar->slcstar_description = Input::get('slcstar_description');
				$objectSlcstar->is_active = Input::get('is_active');
				$objectSlcstar->updated_by = Auth::user()->id;
				
				if($logo != '') {
					$objectSlcstar->slcstar_attachment = $logo;
				}
				$objectSlcstar->save();
				
				if($objectSlcstar->id) {
					$slcstarDetail = SlcstarModel::find($slcstarId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Slc Star successfully updated');
					return Redirect::route('admin.slcstar.list' );
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.slcstar.edit')->with('slcstarDetail',$slcstarDetail);
				}
					
		}
	}
	
	public function publishSlcstar()
	{ 
		$slcstarId = Input::get('slcstar_id');
		$objectSlcstar = SlcstarModel::find($slcstarId);
		$objectSlcstar->is_active = 1;
		$objectSlcstar->save();
		
		if($objectSlcstar->id) {
			$array = array('message' => 'Slc Star is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishSlcstar()
	{ 
		$slcstarId = Input::get('slcstar_id');
		$objectSlcstar = SlcstarModel::find($slcstarId);
		$objectSlcstar->is_active = 0;
		$objectSlcstar->save();
		
		if($objectSlcstar->id) {
			$array = array('message' => 'Slc Star is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteSlcstar()
	{ 
		$slcstarId = Input::get('slcstar_id');
		$objectSlcstar = SlcstarModel::find($slcstarId);
		$objectSlcstar->is_active = 0;
		$objectSlcstar->deleted = 1;
		$objectSlcstar->save();
		
		if($objectSlcstar->id) {
			$array = array('message' => 'Slc Star is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
