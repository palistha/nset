<?php

class ConstantController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getConstants()
    {
        $constantList = ConstantModel::getAllConstants();
		$i = 1;
		$constants = '';
		foreach($constantList as $constant) {
			$constants .= '<tr class="odd gradeX">';
			$constants .= '<td>'.$i++.'</td>';
			$constants .= '<td>'.$constant->constant_heading.'</td>';
			$constants .= '<td>'.$constant->constant_value.'</td>';
			$constants .= '<td>'.$constant->constant_description.'</td>';
			$constants .= '<td><a href="constant/edit/'.$constant->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;</td>';
			$constants .= '</tr>';
		}

        return View::make('admin.constant.list', array('constants' => $constants));
    }
	
	public function getAddConstant()
	{
		return View::make('admin.constant.add');
	}
	
	public function addConstant()
	{
		$rules = array('constant_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.constant.add');
		} 
		else {
			$constantHeading = Input::get('constant_heading');			
			$objectConstant = new ConstantModel();
			$objectConstant->constant_heading = preg_replace('/[^A-Za-z0-9\-]/', '_', strtolower(Input::get('constant_heading')));
			$objectConstant->constant_value = Input::get('constant_value');
			$objectConstant->constant_description = Input::get('constant_description');
			$objectConstant->is_active = 1;
			$objectConstant->updated_by = Auth::user()->id;			
			$objectConstant->save();
			
			if($objectConstant->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'constant successfully added');
				return View::make('admin.constant.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.constant.add');
			}						
		}
	}
	
	public function getEditConstant($id)
	{
		$constantDetail = ConstantModel::find($id);		
		try {
			if($constantDetail->deleted == 0) {
				return View::make('admin.constant.edit')->with('constantDetail',$constantDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editConstant()
	{	
		$constantId = Input::get('constant_id');
		$constantDetail = ConstantModel::find($constantId);
		$rules = array('constant_heading' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.constant.edit')->with('constantDetail',$constantDetail);
		} 
		else {
			$objectConstant = ConstantModel::find($constantId);
			$objectConstant->constant_heading = preg_replace('/[^A-Za-z0-9\-]/', '_', strtolower(Input::get('constant_heading')));
			$objectConstant->constant_value = Input::get('constant_value');
			$objectConstant->constant_description = Input::get('constant_description');
			$objectConstant->is_active = 1;
			$objectConstant->updated_by = Auth::user()->id;			
			$objectConstant->save();
				
			if($objectConstant->id) {
				$constantDetail = ConstantModel::find($constantId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Constant successfully updated');
				return View::make('admin.constant.edit')->with('constantDetail',$constantDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.constant.edit')->with('constantDetail',$constantDetail);
			}					
		}
	}

}
