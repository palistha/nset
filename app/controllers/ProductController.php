<?php

class ProductController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getProducts()
    {
        $productList = ProductModel::getAllProducts();
		
		//var_dump($productList); die();
		$i = 1;
		$products = '';
		foreach($productList as $product) {
			$products .= '<tr class="odd gradeX">';
			$products .= '<td>'.$i++.'</td>';
			$products .= '<td>'.$product->product_heading.'</td>';
			if($product->is_active=='1') { 
				$products .= '<td>'.'<a href="javascript:void(0)" class="unpublish-product" id="'.$product->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$products .= '<td>'.'<a href="javascript:void(0)" class="publish-product" id="'.$product->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$products .= '<td>'.$product->updated_at.'</td>';
            $products .= '<td>'.
						'<a href="product/edit/'.$product->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-product" id="'.$product->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$products .= '</tr>';
		}

        return View::make('admin.product.list', array('products' => $products));
    }
	
	public function getAddProduct()
	{
		return View::make('admin.product.add');
	}
	
	public function addProduct()
	{
		$rules = array('product_heading' => 'required',
					   'product_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.product.add');
		} 
		else {
			$productHeading = Input::get('product_heading');
			$productUrl = Input::get('product_url');
			$productUrl	= preg_replace('/[^A-Za-z0-9\-]/', '', $productUrl);
			$productExist = ProductModel::checkExist($productUrl);
			if( count($productExist)!=0) {
				$message = 'product <b>'.$productHeading.'</b> with url <b>'.$productUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.product.add');
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/products/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "products" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.product.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.product.add');
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				
				$objectProduct = new ProductModel;
				$objectProduct->product_heading		= Input::get('product_heading');
				$objectProduct->product_url			= $productUrl;
				$objectProduct->product_code		= Input::get('product_code');
        $objectProduct->brand_id			= Input::get('brand_id');
				$objectProduct->category_id			= Input::get('category_id');
				$objectProduct->product_price		= Input::get('product_price');
				$objectProduct->product_page_title	= Input::get('product_page_title');
				$objectProduct->product_meta_tags	= Input::get('product_meta_tags');
				$objectProduct->product_meta_description	= Input::get('product_meta_description');
				$objectProduct->product_short_description	= Input::get('product_short_description');
				$objectProduct->product_description	= Input::get('product_description');
				$objectProduct->is_featured	= Input::get('is_featured');
				$objectProduct->is_special	= Input::get('is_special');
				$objectProduct->is_active	= Input::get('is_active');
				$objectProduct->created_by	= Auth::user()->id;
				$objectProduct->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectProduct->product_attachment = $logo; 
				}
				
				$objectProduct->save();
				if($objectProduct->id) {
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Product successfully added');
					return View::make('admin.product.add');
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Something error');
					return View::make('admin.product.add');
				}				
			}			
		}
	}
	
	public function getEditProduct($id)
	{
		$productDetail = ProductModel::find($id);		
		try {
			if($productDetail->deleted == 0) {
				return View::make('admin.product.edit')->with('productDetail',$productDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editProduct()
	{	
		$productId = Input::get('product_id');
		$productDetail = ProductModel::find($productId);
		
		$rules = array('product_heading' => 'required',
					   'product_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.product.edit')->with('productDetail',$productDetail);
		} 
		else {
			$productHeading = Input::get('product_heading');
			$productUrl = Input::get('product_url');
			$productUrl	= preg_replace('/[^A-Za-z0-9\-]/', '', $productUrl);
			$productExist = ProductModel::checkExist($productUrl);
			if( count($productExist)!=0 && $productExist->id != $productId) {
				$message = 'product <b>'.$productHeading.'</b> with url <b>'.$productUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.product.edit')->with('productDetail',$productDetail);
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/products/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "products" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.product.edit')->with('productDetail',$productDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.product.edit')->with('productDetail',$productDetail);
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;			
				
				$objectProduct = ProductModel::find($productId);
				$objectProduct->product_heading		= Input::get('product_heading');
				$objectProduct->product_url			= $productUrl;
				$objectProduct->product_code		= Input::get('product_code');
        $objectProduct->brand_id			= Input::get('brand_id');
				$objectProduct->category_id			= Input::get('category_id');
				$objectProduct->product_price		= Input::get('product_price');
				$objectProduct->product_page_title	= Input::get('product_page_title');
				$objectProduct->product_meta_tags	= Input::get('product_meta_tags');
				$objectProduct->product_meta_description	= Input::get('product_meta_description');
				$objectProduct->product_short_description	= Input::get('product_short_description');
				$objectProduct->product_description	= Input::get('product_description');
				$objectProduct->is_featured	= Input::get('is_featured');
				$objectProduct->is_special	= Input::get('is_special');
				$objectProduct->is_active	= Input::get('is_active');
				$objectProduct->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectProduct->product_attachment = $logo;
				}
				
				$objectProduct->save();
				
				if($objectProduct->id) {
					$productDetail = ProductModel::find($productId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Product successfully updated');
					return View::make('admin.product.edit')->with('productDetail',$productDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.product.edit')->with('productDetail',$productDetail);
				}				
			}			
		}
	}
	
	public function publishProduct()
	{ 
		$productId = Input::get('product_id');
		$objectProduct = ProductModel::find($productId);
		$objectProduct->updated_by	= Auth::user()->id;
		$objectProduct->is_active = 1;
		$objectProduct->save();
		
		if($objectProduct->id) {
			$array = array('message' => 'Product is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishProduct()
	{ 
		$productId = Input::get('product_id');
		$objectProduct = ProductModel::find($productId);
		$objectProduct->updated_by	= Auth::user()->id;
		$objectProduct->is_active = 0;
		$objectProduct->save();
		
		if($objectProduct->id) {
			$array = array('message' => 'Product is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteProduct()
	{ 
		$productId = Input::get('product_id');
		$objectProduct = ProductModel::find($productId);
		$objectProduct->updated_by	= Auth::user()->id;
		$objectProduct->is_active = 0;
		$objectProduct->deleted = 1;
		$objectProduct->save();
		
		if($objectProduct->id) {
			$array = array('message' => 'Product is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}	
	
}
