<?php

class ProfileController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getProfiles()
    {
        $profileList = ProfileModel::getAllProfiles();
		
		//var_dump($profileList); die();
		$i = 1;
		$profiles = '';
		foreach($profileList as $profile) {
			$profiles .= '<tr class="odd gradeX">';
			$profiles .= '<td>'.$i++.'</td>';
			$profiles .= '<td>'.$profile->title.'</td>';
			if($profile->is_active=='1') { 
				$profiles .= '<td>'.'<a href="javascript:void(0)" class="unpublish-profile" id="'.$profile->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$profiles .= '<td>'.'<a href="javascript:void(0)" class="publish-profile" id="'.$profile->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$profiles .= '<td>'.$profile->updated_at.'</td>';
            $profiles .= '<td>'.
						'<a href="profile/edit/'.$profile->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-profile" id="'.$profile->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$profiles .= '</tr>';
		}

        return View::make('admin.profile.list', array('profiles' => $profiles));
    }
	
	public function getAddProfile()
	{
		return View::make('admin.profile.add');
	}
	
	public function addProfile()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.profile.add');
		} 
		else {
			$profileTitle = Input::get('title');
			$profileUrl = Input::get('profile_url');
			$profileUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $profileUrl);
      
      $profileExist = ProfileModel::checkExist($profileUrl);
			if( count($profileExist)!=0) {
				$message = 'profiles <b>'.$profileTitle.'</b> with url <b>'.$profileUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.profile.add');
			}
			else {
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/profiles/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "profiles" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.profile.add');
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.profile.add');
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

        $objectProfile = new ProfileModel();
        $objectProfile->title = Input::get('title');
        $objectProfile->url = $profileUrl;
        $objectProfile->short_desc = Input::get('short_desc');
        $objectProfile->desc = Input::get('desc');
        $objectProfile->is_active = Input::get('is_active');
        $objectProfile->updated_by = Auth::user()->id;

        if($logo != '') {
          $objectProfile->attachment = $logo;
        }
        $objectProfile->save();

        if($objectProfile->id) {
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'Profile successfully added');
          return View::make('admin.profile.add');
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.profile.add');
        }
      }
						
		}
	}
	
	public function getEditProfile($id)
	{
		$profileDetail = ProfileModel::find($id);		
		try {
			if($profileDetail->deleted == 0) {
				return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editProfile()
	{	
		$profileId = Input::get('profile_id');
		$profileDetail = ProfileModel::find($profileId);
		$rules = array('title' => 'required',
					   'url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
		} 
		else {
			$profileTitle = Input::get('title');
			$profileUrl = Input::get('url');
			$profileUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $profileUrl);
			
      $profileExist = ProfileModel::checkExist($profileUrl);
			if( count($profileExist)!=0 && $profileExist->id != $profileId) {
				$message = 'profiles <b>'.$profileTitle.'</b> with url <b>'.$profileUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
			}
			else {		
      
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/profiles/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "profiles" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

          $objectProfile = ProfileModel::find($profileId);
          $objectProfile->title = Input::get('title');
          $objectProfile->url = $profileUrl;
          $objectProfile->short_desc = Input::get('short_desc');
          $objectProfile->desc = Input::get('desc');
          $objectProfile->is_active = Input::get('is_active');
          $objectProfile->updated_by = Auth::user()->id;

          if($logo != '') {
            $objectProfile->attachment = $logo;
          }
          $objectProfile->save();

          if($objectProfile->id) {
            $profileDetail = ProfileModel::find($profileId);		
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'Profile successfully updated');
            return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.profile.edit')->with('profileDetail',$profileDetail);
          }
      }
					
		}
	}
	
	public function publishProfile()
	{ 
		$profileId = Input::get('profile_id');
		$objectProfile = ProfileModel::find($profileId);
		$objectProfile->is_active = 1;
		$objectProfile->save();
		
		if($objectProfile->id) {
			$array = array('message' => 'Profile is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishProfile()
	{ 
		$profileId = Input::get('profile_id');
		$objectProfile = ProfileModel::find($profileId);
		$objectProfile->is_active = 0;
		$objectProfile->save();
		
		if($objectProfile->id) {
			$array = array('message' => 'Profile is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteProfile()
	{ 
		$profileId = Input::get('profile_id');
		$objectProfile = ProfileModel::find($profileId);
		$objectProfile->is_active = 0;
		$objectProfile->deleted = 1;
		$objectProfile->save();
		
		if($objectProfile->id) {
			$array = array('message' => 'Profile is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
