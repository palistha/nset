<?php

class MediaController extends BaseController {
	protected $thumb_w;
	protected $thumb_h;
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct(AlbumModel $albums, MediaModel $photos){
		$this->albums = $albums;
		        $this->photos = $photos;
		$this->destinationpath = './uploads/albums/';
		$this->thumbpath = $this->destinationpath.'thumbs/';
		$this->imagePrefix = 'album_photo_';
		$this->thumb_w = 360;
		$this->thumb_h = 270;
		$this->thumb_sec_w =  1920;
		$this->thumb_sec_h = 608;
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	/*
	* get all images of album
	* $aid is album_id in tbl_media 
	*/
	public function getMedias($aid)
	{	
		$mediaList = MediaModel::getAllMedias($aid);
		$album_title = AlbumModel::find($aid)->album_title;
		
		$i = 1;
		$medias = '';
		foreach($mediaList as $media) {
			$medias .= '<tr class="odd gradeX">';
			$medias .= '<td>'.$i++.'</td>';
			$medias .= '<td>'.$media->media_caption.'</td>';
			
			if(file_exists("uploads/albums/".$media->media_attachment) && $media->media_attachment!=''){
				$medias .= '<td><img src="'.'../../../uploads/albums/'.$media->media_attachment.'" style="width: 200px; height: 120px;"></td>';
			} else {
				$medias .= '<td><img src="'.'../../../uploads/noimage.jpg'.'" style="width: 200px; height: 120px;"></td>';
			}
			if($media->is_active=='1') { 
				$medias .= '<td>'.'<a href="javascript:void(0)" class="unpublish-media" id="'.$media->id.'" >'.
				'<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$medias .= '<td>'.'<a href="javascript:void(0)" class="publish-media" id="'.$media->id.'" >'.
				'<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$medias .= '<td>'.$media->updated_at.'</td>';
			$medias .= '<td>'.
			'<a href="../../media/edit/'.$media->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
			'<a href="javascript:void(0)" class="delete-media" id="'.$media->id.'" >'.
			'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$medias .= '</tr>';
		}
		
		return View::make('admin.media.list', array('medias' => $medias))
		->with('album_id', $aid)
		->with('album_title', $album_title);
	}
	
	public function getAddMedia($aid)
	{
		$album_title = AlbumModel::find($aid)->album_title;
		return View::make('admin.media.add')
		->with('album_id', $aid)
		->with('album_title', $album_title);
	}
	
	public function addMedia()
	{
		$files	= Input::file('userfile');
		$count	= 0;
		    $i = 0;
		$valid	= true;
		$aid	= Input::get('album_id');
		$album_title = AlbumModel::find($aid)->album_title;
		
		if(count($files) > 10 || count($files) == 0) {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'There is no image to upload or maximum numbers of images exceeds 10.');
			return View::make('admin.media.add')
			->with('album_id', $aid)
			->with('album_title', $album_title);
		} else {
			foreach ($files as $index=>$imageFile) {
			
				$data = Input::get('album_id', 'is_active');
				if($imageFile) {
					$extension = strrchr($imageFile->getClientOriginalName(), '.');
					$new_file_name = $this->imagePrefix . $index. time() . $i;
					$fullFilename = $new_file_name . $extension;

					$attachment = $imageFile->move($this->destinationpath, $fullFilename);
					$media_attachment = isset($attachment) ? $fullFilename : NULL;

					$imagePath = $this->destinationpath.$fullFilename;

					$img = Image::make($imagePath);

                // add callback functionality to retain maximal original image size
					$img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
						$constraint->upsize();
					});

					if(!file_exists($this->thumbpath)) {
						@mkdir($this->thumbpath);
					}

                // finally we save the image as a new file
					$img->save($this->thumbpath.$fullFilename);
				
					$logo = isset($attachment) ? $new_file_name . $extension : NULL;
					$objectMedia = new MediaModel();
					$objectMedia->album_id = Input::get('album_id');
					$objectMedia->is_active = Input::get('is_active');
					$objectMedia->updated_by = Auth::user()->id;
					
					if($logo != '') {
						$objectMedia->media_attachment = $logo;
					}				
					$objectMedia->save();
					$count++;
				}
			}
			if($count > 0) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Media successfully added');
				return View::make('admin.media.add')
				->with('album_id', $aid)
				->with('album_title', $album_title);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.media.add')
				->with('album_id', $aid)
				->with('album_title', $album_title);				
			}
		}
	}

	public function getEditMedia($id)
	{
		$mediaDetail = MediaModel::find($id);		
		try {
			if($mediaDetail->deleted == 0) {
				$album_title = AlbumModel::find($mediaDetail->album_id)->album_title;
				return View::make('admin.media.edit')
				->with('mediaDetail',$mediaDetail)
				->with('album_title',$album_title);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}

	public function editMedia()
	{	
		$mediaId = Input::get('media_id');
		$mediaDetail = MediaModel::find($mediaId);
		$album_title = AlbumModel::find($mediaDetail->album_id)->album_title;
					
		$imageFile = Input::file('userfile');
	   

        if($imageFile) {
            $oldimage = $this->photos->find($mediaId)->media_attachment;
            $extension = strrchr($imageFile->getClientOriginalName(), '.');
            $new_file_name = $this->imagePrefix . time();
            $fullFilename = $new_file_name . $extension;

            $attachment = $imageFile->move($this->destinationpath, $fullFilename);
            $media_attachment = isset($attachment) ? $fullFilename : NULL;

            $imagePath = $this->destinationpath.$fullFilename;

            $img = Image::make($imagePath);

            // add callback functionality to retain maximal original image size
            $img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
                $constraint->upsize();
            });
            
            if(!file_exists($this->thumbpath)) {
                @mkdir($this->thumbpath);
            }

            // finally we save the image as a new file
            $img->save($this->thumbpath.$fullFilename);

            if($oldimage) {
                if(file_exists($this->destinationpath.$oldimage)) {
                    @unlink($this->destinationpath.$oldimage);
                }

                if(file_exists($this->thumbpath.$oldimage)) {
                    @unlink($this->thumbpath.$oldimage);
                }
            }

          
        }
        
        $photo = $this->photos->update();
		$logo = isset($attachment) ? $new_file_name. $extension : NULL;

		$objectMedia = MediaModel::find($mediaId);
//			$objectMedia->media_title = Input::get('media_title');
		$objectMedia->media_caption = Input::get('media_caption');
		$objectMedia->is_active = Input::get('is_active');
		$objectMedia->updated_by = Auth::user()->id;

		if($logo != '')	{
			if(file_exists('uploads/albums/thumbs/'.$objectMedia->media_attachment) && $objectMedia->media_attachment != '') {
				unlink('uploads/albums/thumbs/'.$objectMedia->media_attachment);
			}
			$objectMedia->media_attachment = $logo;
		}
		$objectMedia->save();

		if($objectMedia->id) {
			$mediaDetail = MediaModel::find($mediaId);		
			Session::flash('class', 'alert alert-success');
			Session::flash('message', 'Media successfully updated');
			return View::make('admin.media.edit')
			->with('mediaDetail',$mediaDetail)
			->with('album_title',$album_title);
		} else {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'something error');
			return View::make('admin.media.edit')
			->with('mediaDetail',$mediaDetail)
			->with('album_title',$album_title);
		}		
//		}
	}

	public function publishMedia()
	{ 
		$mediaId = Input::get('media_id');
		$objectMedia = MediaModel::find($mediaId);
		$objectMedia->updated_by = Auth::user()->id;		
		$objectMedia->is_active = 1;
		$objectMedia->save();

		if($objectMedia->id) {
			$array = array('message' => 'Media is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

	public function unpublishMedia()
	{ 
		$mediaId = Input::get('media_id');
		$objectMedia = MediaModel::find($mediaId);
		$objectMedia->updated_by = Auth::user()->id;		
		$objectMedia->is_active = 0;
		$objectMedia->save();

		if($objectMedia->id) {
			$array = array('message' => 'Media is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

	public function deleteMedia()
	{ 
		$mediaId = Input::get('media_id');
		$objectMedia = MediaModel::find($mediaId);
		$objectMedia->updated_by = Auth::user()->id;		
		$objectMedia->is_active = 0;
		$objectMedia->deleted = 1;
		$objectMedia->save();

		if($objectMedia->id) {
			$array = array('message' => 'Media is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}

	public function getAllMedia($id)
	{
		$medias = MediaModel::getAllMedia($id);
		$album = AlbumModel::getdetail($id);	
		return View::make('client.photogallery', 
			array('pageTitle' => $album->album_caption,
				'metaDescription' => $album->album_caption . ' Luxury Holidays Pvt. Ltd.',
				'metaKeywords' => $album->album_caption . ' Luxury Holidays Pvt. Ltd.',
				)
			)			
		->with('medias',$medias)
		->with('album',$album);
	}

}
