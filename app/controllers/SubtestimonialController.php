<?php

class SubtestimonialController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getSubTestimonial()
	{
		$subtestimonialList = SubTestimonialModel::getAllSubTestimonial();
		
		//var_dump($testimonialList); die();
		$i = 1;
		$subtestimonials = '';
		foreach($subtestimonialList as $subtestimonial) {

			$subtestimonials .= '<tr class="odd gradeX">';
			$subtestimonials .= '<td>'.$i++.'</td>';
			$subtestimonials .= '<td>'.$subtestimonial->testimonial_auther.'</td>';
			$subtestimonials .= '<td>'.$subtestimonial->testimonial_remark.'</td>';
			$subtestimonials .= '<td>'.$subtestimonial->testimonial_description.'</td>';
			
			if($subtestimonial->is_active=='1') { 
				$subtestimonials .= '<td>'.'<a href="javascript:void(0)" class="unpublish-subtestimonial" id="'.$subtestimonial->id.'" >'.
				'<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$subtestimonials .= '<td>'.'<a href="javascript:void(0)" class="publish-subtestimonial" id="'.$subtestimonial->id.'" >'.
				'<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$subtestimonials .= '<td>'.$subtestimonial->updated_at.'</td>';
			$subtestimonials .= '<td>'.
			'<a href="subtestimonial/edit/'.$subtestimonial->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
			'<a href="javascript:void(0)" class="delete-subtestimonial" id="'.$subtestimonial->id.'" >'.
			'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$subtestimonials .= '</tr>';
		}

		return View::make('admin.subtestimonial.list', array('subtestimonials' => $subtestimonials));
	}
	
	public function getAddSubTestimonial()
	{
		return View::make('admin.subtestimonial.add');
	}
	
	public function addSubTestimonial()
	{
		$rules = array('testimonial_auther' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.subtestimonial.add');
		} 
		else {
			// $testimonialAuther = Input::get('testimonial_auther');
			$Url = Input::get('testimonial_company');
			$testimonialUrl = str_replace(" ", "_", $Url);
			$testimonialUrls = strtolower($testimonialUrl );
			
			// $testimonialExist = SubTestimonialModel::checkExist($testimonialUrl);
			// if (count($testimonialExist) != 0) {
			// 	$message = 'Testimonial <b>' . $testimonialAuther . '</b> with url <b>' . $newsUrl . '</b> is already exist';
			// 	Session::flash('class', 'alert alert-error');
			// 	Session::flash('message', $message);
			// 	return View::make('admin.subtestimonial.add');
			// } else {



			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/subtestimonials/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;

			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "subtestimonials" . "_" . time();

				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.subtestimonial.add');
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.subtestimonial.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

			$objectSubTestimonial = new SubTestimonialModel();
			$objectSubTestimonial->testimonial_auther = Input::get('testimonial_auther');
			$objectSubTestimonial->testimonial_remark = Input::get('testimonial_remark');

			$objectSubTestimonial->testimonial_company = $testimonialUrls ;
			
			$objectSubTestimonial->testimonial_description = Input::get('testimonial_description');
			$objectSubTestimonial->is_active = Input::get('is_active');
			$objectSubTestimonial->updated_by = Auth::user()->id;

			if($logo != '') {
				$objectSubTestimonial->testimonial_attachment = $logo;
			}
			$objectSubTestimonial->save();

			if($objectSubTestimonial->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Opinion successfully added');
				return View::make('admin.subtestimonial.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.subtestimonial.add');
				// }

			}
		}
	}
	
	public function getEditSubTestimonial($id)
	{
		$subtestimonialDetail = SubTestimonialModel::find($id);		
		try {
			if($subtestimonialDetail->deleted == 0) {
				return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editSubTestimonial()
	{	
		$subtestimonialId = Input::get('subtestimonial_id');
		$subtestimonialDetail = SubTestimonialModel::find($subtestimonialId);
		$rules = array('testimonial_auther' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail);
		} 
		else {
			$subtestimonialHeading = Input::get('testimonial_auther');

			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/subtestimonials/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
			
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "subtestimonials" . "_" . time();
				
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectSubTestimonial = SubTestimonialModel::find($subtestimonialId);
			$objectSubTestimonial->testimonial_auther = Input::get('testimonial_auther');
			$objectSubTestimonial->testimonial_remark = Input::get('testimonial_remark');
			$objectSubTestimonial->testimonial_company = Input::get('testimonial_company');
//				$objectTestimonial->testimonial_company = Input::get('testimonial_company');
			$objectSubTestimonial->testimonial_description = Input::get('testimonial_description');
			$objectSubTestimonial->is_active = Input::get('is_active');
			$objectSubTestimonial->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectSubTestimonial->testimonial_attachment = $logo;
			}
			$objectSubTestimonial->update();
			
			if($objectSubTestimonial->id) {
				$subtestimonialDetail = SubTestimonialModel::find($subtestimonialId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Testimonial successfully updated');
				return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.subtestimonial.edit')->with('subtestimonialDetail',$subtestimonialDetail );
			}
			
		}
	}
	
	public function publishSubTestimonial()
	{ 
		$subtestimonialId = Input::get('subtestimonial_id');
		$objectSubTestimonial = SubTestimonialModel::find($subtestimonialId);
		$objectSubTestimonial->is_active = 1;
		$objectSubTestimonial->save();
		
		if($objectSubTestimonial->id) {
			$array = array('message' => 'Testimonial is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishSubTestimonial()
	{ 
		$subtestimonialId = Input::get('subtestimonial_id');
		$objectSubTestimonial = SubTestimonialModel::find($subtestimonialId);
		$objectSubTestimonial->is_active = 0;
		$objectSubTestimonial->save();
		
		if($objectSubTestimonial->id) {
			$array = array('message' => 'Testimonial is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteSubTestimonial()
	{ 
		$subtestimonialId = Input::get('subtestimonial_id');
		$objectSubTestimonial = SubTestimonialModel::find($subtestimonialId);
		$objectSubTestimonial->is_active = 0;
		$objectSubTestimonial->deleted = 1;
		$objectSubTestimonial->save();
		
		// if($objectSubTestimonial->id) {
		// 	$array = array('message' => 'Testimonial is deleted successfully.', 'flag' => true);
		// 	$returnValue = json_encode($array);
		// } else {
		// 	$array = array('message' => 'Something error.', 'flag' => false);
		// 	$returnValue = json_encode($array);
		// }
		// return $returnValue;
		if($objectSubTestimonial->id) {
			print_r(json_encode(['flag' => true, 'message' => 'Management Team is deleted succesfully.']));
		} else {
			print_r(json_encode(['flag' => false, 'message' => 'Something error.']));

		}


		
	}
	
}
