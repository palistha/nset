<?php

class TestimonialController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getTestimonials()
    {
        $testimonialList = TestimonialModel::getAllTestimonials();
		
		//var_dump($testimonialList); die();
		$i = 1;
		$testimonials = '';
		foreach($testimonialList as $testimonial) {
			 $type = '';
			if ($testimonial->type == 'SO') {
			$type = 'Student';
			}
			elseif ($testimonial->type == 'G') {
			$type = 'Genius';
			}
			elseif ($testimonial->type == 'P') {
			$type = 'Principal';
			}
			$testimonials .= '<tr class="odd gradeX">';
			$testimonials .= '<td>'.$i++.'</td>';
			$testimonials .= '<td>'.$testimonial->testimonial_auther.'</td>';
			$testimonials .= '<td>'.$type.'</td>';
			if($testimonial->is_active=='1') { 
				$testimonials .= '<td>'.'<a href="javascript:void(0)" class="unpublish-testimonial" id="'.$testimonial->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$testimonials .= '<td>'.'<a href="javascript:void(0)" class="publish-testimonial" id="'.$testimonial->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$testimonials .= '<td>'.$testimonial->updated_at.'</td>';
            $testimonials .= '<td>'.
						'<a href="testimonial/edit/'.$testimonial->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-testimonial" id="'.$testimonial->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$testimonials .= '</tr>';
		}

        return View::make('admin.testimonial.list', array('testimonials' => $testimonials));
    }
	
	public function getAddTestimonial()
	{
		return View::make('admin.testimonial.add');
	}
	
	public function addTestimonial()
	{
		$rules = array('testimonial_auther' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.testimonial.add');
		} 
		else {
			$testimonialAuther = Input::get('testimonial_auther');
			$testimonialUrl = Input::get('testimonial_url');
			$testimonialUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $testimonialUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/testimonials/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
			
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "testimonials" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.testimonial.add');
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.testimonial.add');
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectTestimonial = new TestimonialModel();
			$objectTestimonial->testimonial_auther = Input::get('testimonial_auther');
      $objectTestimonial->type = Input::get('type');
      $objectTestimonial->testimonial_remark = Input::get('testimonial_remark');
//			$objectTestimonial->testimonial_company = Input::get('testimonial_company');
			$objectTestimonial->testimonial_description = Input::get('testimonial_description');
			$objectTestimonial->is_active = Input::get('is_active');
			$objectTestimonial->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectTestimonial->testimonial_attachment = $logo;
			}
			$objectTestimonial->save();
			
			if($objectTestimonial->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Opinion successfully added');
				return View::make('admin.testimonial.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.testimonial.add');
			}
						
		}
	}
	
	public function getEditTestimonial($id)
	{
		$testimonialDetail = TestimonialModel::find($id);		
		try {
			if($testimonialDetail->deleted == 0) {
				return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editTestimonial()
	{	
		$testimonialId = Input::get('testimonial_id');
		$testimonialDetail = TestimonialModel::find($testimonialId);
		$rules = array('testimonial_auther' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
		} 
		else {
			$testimonialHeading = Input::get('testimonial_auther');
			$testimonialUrl = Input::get('testimonial_url');
			$testimonialUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $testimonialUrl);
			
			$imageFile = Input::file('userfile');
			$destinationPath = 'uploads/testimonials/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;
				
			if($imageFile) {
				$filename = $imageFile->getClientOriginalName();
				$extension = strrchr($filename, '.');
				$size = $imageFile->getSize();					
				$new_image_name = "testimonials" . "_" . time();
					
				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
				} else if (preg_match($rEFileTypes, $extension) == false) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Warning : Invalid Image File!');
					return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
				} else if ($size > $maximum_filesize) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
					return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
				}				
			}
			$logo = isset($attachment) ? $new_image_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
				
				$objectTestimonial = TestimonialModel::find($testimonialId);
				$objectTestimonial->testimonial_auther = Input::get('testimonial_auther');
        $objectTestimonial->type = Input::get('type');
        $objectTestimonial->testimonial_remark = Input::get('testimonial_remark');
//				$objectTestimonial->testimonial_company = Input::get('testimonial_company');
				$objectTestimonial->testimonial_description = Input::get('testimonial_description');
				$objectTestimonial->is_active = Input::get('is_active');
				$objectTestimonial->updated_by = Auth::user()->id;
				
				if($logo != '') {
					$objectTestimonial->testimonial_attachment = $logo;
				}
				$objectTestimonial->save();
				
				if($objectTestimonial->id) {
					$testimonialDetail = TestimonialModel::find($testimonialId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Opinion successfully updated');
					return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.testimonial.edit')->with('testimonialDetail',$testimonialDetail);
				}
					
		}
	}
	
	public function publishTestimonial()
	{ 
		$testimonialId = Input::get('testimonial_id');
		$objectTestimonial = TestimonialModel::find($testimonialId);
		$objectTestimonial->is_active = 1;
		$objectTestimonial->save();
		
		if($objectTestimonial->id) {
			$array = array('message' => 'Opinion is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishTestimonial()
	{ 
		$testimonialId = Input::get('testimonial_id');
		$objectTestimonial = TestimonialModel::find($testimonialId);
		$objectTestimonial->is_active = 0;
		$objectTestimonial->save();
		
		if($objectTestimonial->id) {
			$array = array('message' => 'Opinion is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteTestimonial()
	{ 
		$testimonialId = Input::get('testimonial_id');
		$objectTestimonial = TestimonialModel::find($testimonialId);
		$objectTestimonial->is_active = 0;
		$objectTestimonial->deleted = 1;
		$objectTestimonial->save();
		
		if($objectTestimonial->id) {
			$array = array('message' => 'Opinion is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
