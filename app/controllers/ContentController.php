<?php
class ContentController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getContents()
    {
    
        $contentList = ContentModel::getAllContents();
		
		//var_dump($contentList); die();
		$i = 1;
		$contents = '';
		foreach($contentList as $content) {
			$contents .= '<tr class="odd gradeX">';
			$contents .= '<td>'.$i++.'</td>';
			$contents .= '<td>'.$content->content_heading.'</td>';
			if($content->is_active=='1') { 
				$contents .= '<td>'.'<a href="javascript:void(0)" class="unpublish-content" id="'.$content->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$contents .= '<td>'.'<a href="javascript:void(0)" class="publish-content" id="'.$content->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$contents .= '<td>'.$content->updated_at.'</td>';
            $contents .= '<td>'.
						'<a href="content/edit/'.$content->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-content" id="'.$content->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$contents .= '</tr>';
		}

        return View::make('admin.content.list', array('contents' => $contents));
    }
	
	public function getAddContent()
	{
		return View::make('admin.content.add');
	}
	
	public function addContent()
	{
		$rules = array('content_heading' => 'required',
					   'content_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.content.add');
		} 
		else {
			$contentHeading = Input::get('content_heading');
			$contentUrl = Input::get('content_url');
			$contentUrl	= strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-', $contentUrl));
			
			$contentExist = ContentModel::checkExist($contentUrl);
			if( count($contentExist)!=0) {
				$message = 'content <b>'.$contentHeading.'</b> with url <b>'.$contentUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.content.add');
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/pages/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "pages" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.content.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.content.add');
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				
				$objectContent = new ContentModel;
				$objectContent->content_heading		= Input::get('content_heading');
				$objectContent->content_url			= $contentUrl;
				$objectContent->content_page_title	= Input::get('content_page_title');
				$objectContent->content_meta_tags	= Input::get('content_meta_tags');
				$objectContent->content_meta_description	= Input::get('content_meta_description');
				$objectContent->content_short_description	= Input::get('content_short_description');
				$objectContent->content_description	= Input::get('content_description');
				$objectContent->is_active	= Input::get('is_active');
				$objectContent->show_order	= Input::get('show_order');
				$objectContent->parent_id	= Input::get('parent_id');
				$objectContent->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectContent->content_attachment = $logo; 
				}
				if(Input::get('show_in') != '')
				{
					$objectContent->show_in = join(',',Input::get('show_in'));
				}
				$objectContent->save();
				if($objectContent->id) {
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Content successfully added');
					return View::make('admin.content.add');
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'Something error');
					return View::make('admin.content.add');
				}
				
			}			
		}
	}
	
	public function getEditContent($id)
	{
		$contentDetail = ContentModel::find($id);		
		try {
			if($contentDetail->deleted == 0) {
				return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editContent()
	{	
		$contentId = Input::get('content_id');
		$contentDetail = ContentModel::find($contentId);
		
		$rules = array('content_heading' => 'required',
					   'content_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
		} 
		else {
			$contentHeading = Input::get('content_heading');
			$contentUrl = Input::get('content_url');
			if(empty($contentUrl)) {
				$contentUrl = str_slug($contentHeading, '-').date('Y-m-d_H-i-s');
				}
			$contentUrl	= strtolower(preg_replace('/[^A-Za-z0-9\-]/', '-', $contentUrl));
			
			$contentExist = ContentModel::checkExist($contentUrl);
			if( count($contentExist)!=0 && $contentExist->id != $contentId) {
				$message = 'content <b>'.$contentHeading.'</b> with url <b>'.$contentUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
			}
			else {
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/pages/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "pages" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;			
				
				$objectContent = ContentModel::find($contentId);
				$objectContent->content_heading		= Input::get('content_heading');
				$objectContent->content_url			= $contentUrl;
				$objectContent->content_page_title	= Input::get('content_page_title');
				$objectContent->content_meta_tags	= Input::get('content_meta_tags');
				$objectContent->content_meta_description	= Input::get('content_meta_description');
				$objectContent->content_short_description	= Input::get('content_short_description');
				$objectContent->content_description	= Input::get('content_description');
				$objectContent->is_active	= Input::get('is_active');
				$objectContent->show_order	= Input::get('show_order');
				$objectContent->parent_id	= Input::get('parent_id');
				$objectContent->updated_by	= Auth::user()->id;
				
				if($logo != '')
				{
					$objectContent->content_attachment = $logo;
				}
				if(Input::get('show_in') != '')
				{
					$objectContent->show_in = join(',',Input::get('show_in'));
				}
				$objectContent->save();
				
				if($objectContent->id) {
					$contentDetail = ContentModel::find($contentId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Content successfully updated');
					return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.content.edit')->with('contentDetail',$contentDetail);
				}
				
			}			
		}
	}
	
	public function publishContent()
	{ 
		$contentId = Input::get('content_id');
		$objectContent = ContentModel::find($contentId);
		$objectContent->is_active = 1;
		$objectContent->save();
		
		if($objectContent->id) {
			$array = array('message' => 'Content is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishContent()
	{ 
		$contentId = Input::get('content_id');
		$objectContent = ContentModel::find($contentId);
		$objectContent->is_active = 0;
		$objectContent->save();
		
		if($objectContent->id) {
			$array = array('message' => 'Content is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteContent()
	{ 
		$contentId = Input::get('content_id');
		$objectContent = ContentModel::find($contentId);
		$objectContent->is_active = 0;
		$objectContent->deleted = 1;
		$objectContent->save();
		
		if($objectContent->id) {
			$array = array('message' => 'Content is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}


	public function removeContentImage()
	{
		$contentId = Input::get('content_id');
		$objectContent = ContentModel::find($contentId);
        File::delete(public_path().'uploads/pages/'.$objectContent->content_attachment);
        $objectContent->content_attachment = NULL;
        $objectContent->save();
        if($objectContent->id) {
            $array = array('message' => 'Content Image deleted successfully.', 'flag' => true);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
        }
        return $returnValue = json_encode($array);
		
	}
	
}
