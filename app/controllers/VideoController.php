<?php

class VideoController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getVideos()
    {
        $videoList = VideoModel::getAllVideos();
		
		//var_dump($videoList); die();
		$i = 1;
		$videos = '';
		foreach($videoList as $video) {
			$videos .= '<tr class="odd gradeX">';
			$videos .= '<td>'.$i++.'</td>';
      $videos .= '<td>'.$video->video_title.'</td>';
			$videos .= '<td><img src="http://img.youtube.com/vi/'.$video->video_url.'/hqdefault.jpg"></td>';
						
			if($video->is_active=='1') { 
				$videos .= '<td>'.'<a href="javascript:void(0)" class="unpublish-video" id="'.$video->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$videos .= '<td>'.'<a href="javascript:void(0)" class="publish-video" id="'.$video->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$videos .= '<td>'.$video->updated_at.'</td>';
            $videos .= '<td>'.
						'<a href="video/edit/'.$video->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-video" id="'.$video->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			
			$videos .= '</tr>';
		}
		
        return View::make('admin.video.list', array('videos' => $videos));
    }
	
	public function getAddVideo()
	{
		return View::make('admin.video.add');
	}
	
	public function addVideo()
	{
		$files = Input::file('userfile');
		$count = 0;
		$valid = true;
		$rules = array('video_title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
    if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.video.add');
		} 
		else {
      $videoHeading = Input::get('video_title');
      $videoUrl = Input::get('video_url');
			$videoUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $videoUrl);
			
			$videoExist = VideoModel::checkExist($videoUrl);
			if( count($videoExist)!=0) {
				$message = 'video <b>'.$videoHeading.'</b> with url <b>'.$videoUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.video.add');
			}
			else {	
        
          $objectVideo = new VideoModel();
          $objectVideo->video_title = Input::get('video_title');
          $objectVideo->video_url = $videoUrl;
          $objectVideo->video_description = Input::get('video_description');
          $objectVideo->is_active = Input::get('is_active');
          $objectVideo->updated_by = Auth::user()->id;
          $objectVideo->save();
          $videoId = $objectVideo->id;
       
          if($videoId > 0) {
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'Video successfully added');
            return Redirect::route('admin.videos.list' );
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.video.add');				
          }
        
      }
    }
	}
	
	public function getEditVideo($id)
	{
		$videoDetail = VideoModel::find($id);		
		try {
			if($videoDetail->deleted == 0) {
				return View::make('admin.video.edit')->with('videoDetail',$videoDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editVideo()
	{	
		$videoId = Input::get('video_id');
		$videoDetail = VideoModel::find($videoId);
		$rules = array('video_title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.video.edit')->with('videoDetail',$videoDetail);
		} 
		else {
      $videoHeading = Input::get('video_title');
      $videoUrl = Input::get('video_url');
			$videoUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $videoUrl);
			
			$videoExist = VideoModel::checkExist($videoUrl);
			if( count($videoExist)!=0 && $videoExist->id != $videoId) {
				$message = 'video <b>'.$videoHeading.'</b> with url <b>'.$videoUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.video.edit')->with('videoDetail',$videoDetail);
			}
			else {
      
        $objectVideo = VideoModel::find($videoId);
        $objectVideo->video_title = Input::get('video_title');
        $objectVideo->video_url = $videoUrl;
        $objectVideo->video_description = Input::get('video_description');
        $objectVideo->is_active = Input::get('is_active');
        $objectVideo->updated_by = Auth::user()->id;

//        if($logo != '') {
//          if(file_exists('uploads/videos/'.$objectVideo->video_attachment) && $objectVideo->video_attachment != '') {
//            unlink('uploads/videos/'.$objectVideo->video_attachment);
//          }
//          $objectVideo->video_attachment = $logo;
//        }
        $objectVideo->save();

        if($objectVideo->id) {
          $videoDetail = VideoModel::find($videoId);		
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'Video successfully updated');
          return View::make('admin.video.edit')->with('videoDetail',$videoDetail);
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.video.edit')->with('videoDetail',$videoDetail);
        }
      }
		}
	}
	
	public function publishVideo()
	{ 
		$videoId = Input::get('video_id');
		$objectVideo = VideoModel::find($videoId);
		$objectVideo->is_active = 1;
		$objectVideo->updated_by = Auth::user()->id;
		$objectVideo->save();
		
		if($objectVideo->id) {
			$array = array('message' => 'Video is published successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function unpublishVideo()
	{ 
		$videoId = Input::get('video_id');
		$objectVideo = VideoModel::find($videoId);
		$objectVideo->is_active = 0;
		$objectVideo->updated_by = Auth::user()->id;
		$objectVideo->save();
		
		if($objectVideo->id) {
			$array = array('message' => 'Video is unpublished successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
	public function deleteVideo()
	{ 
		$videoId = Input::get('video_id');
		$objectVideo = VideoModel::find($videoId);
		$objectVideo->updated_by = Auth::user()->id;
		$objectVideo->is_active = 0;
		$objectVideo->deleted = 1;		
		$objectVideo->save();
		
		if($objectVideo->id) {
			$array = array('message' => 'Video is deleted successfully.', 'flag' => true);			
		} else {
			$array = array('message' => 'server error.', 'flag' => false);			
		}
		return $returnValue = json_encode($array);
	}
	
}
