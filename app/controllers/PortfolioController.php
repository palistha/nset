<?php

class PortfolioController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getPortfolios()
    {
        $portfolioList = PortfolioModel::getAllPortfolios();
		
		//var_dump($portfolioList); die();
		$i = 1;
		$portfolios = '';
		foreach($portfolioList as $portfolio) {
			$portfolios .= '<tr class="odd gradeX">';
			$portfolios .= '<td>'.$i++.'</td>';
			$portfolios .= '<td>'.$portfolio->title.'</td>';
			if($portfolio->is_active=='1') { 
				$portfolios .= '<td>'.'<a href="javascript:void(0)" class="unpublish-portfolio" id="'.$portfolio->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$portfolios .= '<td>'.'<a href="javascript:void(0)" class="publish-portfolio" id="'.$portfolio->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$portfolios .= '<td>'.$portfolio->updated_at.'</td>';
            $portfolios .= '<td>'.
						'<a href="portfolio/edit/'.$portfolio->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-portfolio" id="'.$portfolio->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$portfolios .= '</tr>';
		}

        return View::make('admin.portfolio.list', array('portfolios' => $portfolios));
    }
	
	public function getAddPortfolio()
	{
		return View::make('admin.portfolio.add');
	}
	
	public function addPortfolio()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.portfolio.add');
		} 
		else {
			$portfolioTitle = Input::get('title');
			$portfolioUrl = Input::get('portfolio_url');
			$portfolioUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $portfolioUrl);
      
      $portfolioExist = PortfolioModel::checkExist($portfolioUrl);
			if( count($portfolioExist)!=0) {
				$message = 'portfolios <b>'.$portfolioTitle.'</b> with url <b>'.$portfolioUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.portfolio.add');
			}
			else {
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/portfolios/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "portfolios" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.portfolio.add');
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.portfolio.add');
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

        $objectPortfolio = new PortfolioModel();
        $objectPortfolio->title = Input::get('title');
        $objectPortfolio->url = $portfolioUrl;
//        $objectPortfolio->short_desc = Input::get('short_desc');
        $objectPortfolio->desc = Input::get('desc');
        $objectPortfolio->is_active = Input::get('is_active');
        $objectPortfolio->updated_by = Auth::user()->id;

        if($logo != '') {
          $objectPortfolio->attachment = $logo;
        }
        $objectPortfolio->save();

        if($objectPortfolio->id) {
          Session::flash('class', 'alert alert-success');
          Session::flash('message', 'Portfolio successfully added');
          return View::make('admin.portfolio.add');
        } else {
          Session::flash('class', 'alert alert-error');
          Session::flash('message', 'something error');
          return View::make('admin.portfolio.add');
        }
      }
						
		}
	}
	
	public function getEditPortfolio($id)
	{
		$portfolioDetail = PortfolioModel::find($id);		
		try {
			if($portfolioDetail->deleted == 0) {
				return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editPortfolio()
	{	
		$portfolioId = Input::get('portfolio_id');
		$portfolioDetail = PortfolioModel::find($portfolioId);
		$rules = array('title' => 'required',
					   'url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
		} 
		else {
			$portfolioTitle = Input::get('title');
			$portfolioUrl = Input::get('url');
			$portfolioUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $portfolioUrl);
			
      $portfolioExist = PortfolioModel::checkExist($portfolioUrl);
			if( count($portfolioExist)!=0 && $portfolioExist->id != $portfolioId) {
				$message = 'portfolios <b>'.$portfolioTitle.'</b> with url <b>'.$portfolioUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
			}
			else {		
      
        $imageFile = Input::file('userfile');
        $destinationPath = 'uploads/portfolios/';
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
        $maximum_filesize = 1 * 1024 * 1024;

        if($imageFile) {
          $filename = $imageFile->getClientOriginalName();
          $extension = strrchr($filename, '.');
          $size = $imageFile->getSize();					
          $new_image_name = "portfolios" . "_" . time();

          if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
            $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
          } else if (preg_match($rEFileTypes, $extension) == false) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Warning : Invalid Image File!');
            return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
          } else if ($size > $maximum_filesize) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
            return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
          }				
        }
        $logo = isset($attachment) ? $new_image_name . $extension : NULL;
        //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

          $objectPortfolio = PortfolioModel::find($portfolioId);
          $objectPortfolio->title = Input::get('title');
          $objectPortfolio->url = $portfolioUrl;
//          $objectPortfolio->short_desc = Input::get('short_desc');
          $objectPortfolio->desc = Input::get('desc');
          $objectPortfolio->is_active = Input::get('is_active');
          $objectPortfolio->updated_by = Auth::user()->id;

          if($logo != '') {
            $objectPortfolio->attachment = $logo;
          }
          $objectPortfolio->save();

          if($objectPortfolio->id) {
            $portfolioDetail = PortfolioModel::find($portfolioId);		
            Session::flash('class', 'alert alert-success');
            Session::flash('message', 'Portfolio successfully updated');
            return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
          } else {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'something error');
            return View::make('admin.portfolio.edit')->with('portfolioDetail',$portfolioDetail);
          }
      }
					
		}
	}
	
	public function publishPortfolio()
	{ 
		$portfolioId = Input::get('portfolio_id');
		$objectPortfolio = PortfolioModel::find($portfolioId);
		$objectPortfolio->is_active = 1;
		$objectPortfolio->save();
		
		if($objectPortfolio->id) {
			$array = array('message' => 'Portfolio is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishPortfolio()
	{ 
		$portfolioId = Input::get('portfolio_id');
		$objectPortfolio = PortfolioModel::find($portfolioId);
		$objectPortfolio->is_active = 0;
		$objectPortfolio->save();
		
		if($objectPortfolio->id) {
			$array = array('message' => 'Portfolio is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deletePortfolio()
	{ 
		$portfolioId = Input::get('portfolio_id');
		$objectPortfolio = PortfolioModel::find($portfolioId);
		$objectPortfolio->is_active = 0;
		$objectPortfolio->deleted = 1;
		$objectPortfolio->save();
		
		if($objectPortfolio->id) {
			$array = array('message' => 'Portfolio is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
