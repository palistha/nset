<?php

class CategoryController extends BaseController
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function getCategories()
    {
        $categoryList = CategoryModel::getAllCategories();
        $i = 1;
        $categories = '';
        foreach ($categoryList as $category) {
            $categories .= '<tr class="odd gradeX">';
            $categories .= '<td>' . $i++ . '</td>';
            $categories .= '<td>' . $category->category_heading . '</td>';
            if($category->parent_id >0 ){

                $subcategory = CategoryModel::find($category->parent_id);

                $categories .= '<td>'.$subcategory->category_heading.'</td>';
            }else {
                $categories .= '<td>&nbsp;</td>';
            }

            if ($category->is_active == '1') {
                $categories .= '<td>' . '<a href="javascript:void(0)" class="unpublish-category" id="' . $category->id . '" >' .
                    '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp' . '</td>';
            } else {
                $categories .= '<td>' . '<a href="javascript:void(0)" class="publish-category" id="' . $category->id . '" >' .
                    '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp' . '</td>';
            }
            $categories .= '<td>' . $category->updated_at . '</td>';
            $categories .= '<td>' .
                '<a href="category/edit/' . $category->id . '"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;' .
                '<a href="javascript:void(0)" class="delete-category" id="' . $category->id . '" >' .
                '<i class="icon-trash" ></i> Delete</a>' . '</td>';
            $categories .= '</tr>';
        }

        return View::make('admin.category.list', array('categories' => $categories));
    }

    public function getAddCategory()
    {
        return View::make('admin.category.add');
    }

    public function addCategory()
    {
        $rules = array('category_heading' => 'required',
            'category_url' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('admin.category.add');
        } else {
            $categoryHeading = Input::get('category_heading');
            $categoryUrl = Input::get('category_url');
            $categoryUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $categoryUrl);
            $categoryExist = CategoryModel::checkExist($categoryUrl);
            if (count($categoryExist) != 0) {
                $message = 'category <b>' . $categoryHeading . '</b> with url <b>' . $categoryUrl . '</b> is already exist';
                Session::flash('class', 'alert alert-error');
                Session::flash('message', $message);
                return View::make('admin.category.add');
            } else {
                $imageFile = Input::file('userfile');
                $destinationPath = 'uploads/categories/';
                $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
                $maximum_filesize = 1 * 1024 * 1024;

                $thumbFile = Input::file('thumb_image');

                if ($imageFile) {
                    $filename = $imageFile->getClientOriginalName();
                    $extension = strrchr($filename, '.');
                    $size = $imageFile->getSize();
                    $new_image_name = "category" . "_" . time();

                    if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
                        $attachment = $imageFile->move($destinationPath, $new_image_name . $extension);
                    } else if (preg_match($rEFileTypes, $extension) == false) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', 'Warning : Invalid Image File!');
                        return View::make('admin.category.add');
                    } else if ($size > $maximum_filesize) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
                        return View::make('admin.category.add');
                    }
                }

                if ($thumbFile) {
                    $filename = $thumbFile->getClientOriginalName();
                    $extension1 = strrchr($filename, '.');
                    $size = $thumbFile->getSize();
                    $new_thumb_name = "category_thumb" . "_" . time();

                    if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension1)) {
                        $attachment_thumb = $thumbFile->move($destinationPath, $new_thumb_name . $extension1);
                    } else if (preg_match($rEFileTypes, $extension1) == false) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', 'Warning : Invalid Image File!');
                        return View::make('admin.category.add');
                    } else if ($size > $maximum_filesize) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
                        return View::make('admin.category.add');
                    }
                }

                $logo = isset($attachment) ? $new_image_name . $extension : NULL;
                $logo_thumb = isset($attachment_thumb) ? $new_thumb_name . $extension1 : NULL;

                $objectCategory = new CategoryModel;
                $objectCategory->category_heading = Input::get('category_heading');
                $objectCategory->category_url = $categoryUrl;
                $objectCategory->category_page_title = Input::get('category_page_title');
                $objectCategory->category_meta_tags = Input::get('category_meta_tags');
                $objectCategory->category_meta_description = Input::get('category_meta_description');
                $objectCategory->category_short_description = Input::get('category_short_description');
                $objectCategory->category_description = Input::get('category_description');
                $objectCategory->is_active = Input::get('is_active');
                $objectCategory->parent_id = Input::get('parent_id');
                $objectCategory->updated_by = Auth::user()->id;

                if ($logo != '') {
                    $objectCategory->category_attachment = $logo;
                }

                if ($logo_thumb != '') {
                    $objectCategory->thumb_attachment = $logo_thumb;
                }
                $objectCategory->save();

                if ($objectCategory->id) {
                    Session::flash('class', 'alert alert-success');
                    Session::flash('message', 'Category successfully added');
                    return View::make('admin.category.add');
                } else {
                    Session::flash('class', 'alert alert-error');
                    Session::flash('message', 'Something error');
                    return View::make('admin.category.add');
                }

            }
        }
    }

    public function getEditCategory($id)
    {
        $categoryDetail = CategoryModel::find($id);


        try {
            if ($categoryDetail->deleted == 0) {
                return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
            } else {
                return View::make('admin.error');
            }
        } catch (ErrorException $e) {
            return View::make('admin.error');
        }
    }

    public function editCategory()
    {
        $categoryId = Input::get('category_id');
        $categoryDetail = CategoryModel::find($categoryId);

        $rules = array('category_heading' => 'required',
            'category_url' => 'required');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            Session::flash('class', 'alert alert-error');
            Session::flash('message', 'Some fields are missing');
            return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
        } else {
            $categoryHeading = Input::get('category_heading');
            $categoryUrl = Input::get('category_url');
            $categoryUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $categoryUrl);
            $categoryExist = CategoryModel::checkExist($categoryUrl);
            if (count($categoryExist) != 0 && $categoryExist->id != $categoryId) {
                $message = 'category <b>' . $categoryHeading . '</b> with url <b>' . $categoryUrl . '</b> is already exist';
                Session::flash('class', 'alert alert-error');
                Session::flash('message', $message);
                return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
            } else {
                $imageFile = Input::file('userfile');
                $destinationPath = 'uploads/categories/';
                $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
                $maximum_filesize = 1 * 1024 * 1024;

                $thumbFile = Input::file('thumb_image');

                if ($imageFile) {
                    $filename = $imageFile->getClientOriginalName();
                    $extension = strrchr($filename, '.');
                    $size = $imageFile->getSize();
                    $new_image_name = "category" . "_" . time();

                    if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
                        $attachment = $imageFile->move($destinationPath, $new_image_name . $extension);
                    } else if (preg_match($rEFileTypes, $extension) == false) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', 'Warning : Invalid Image File!');
                        return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                    } else if ($size > $maximum_filesize) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
                        return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                    }
                }

                if ($thumbFile) {
                    $filename = $thumbFile->getClientOriginalName();
                    $extension1 = strrchr($filename, '.');
                    $size = $thumbFile->getSize();
                    $new_thumb_name = "category_thumb" . "_" . time();

                    if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension1)) {
                        $attachment_thumb = $thumbFile->move($destinationPath, $new_thumb_name . $extension1);
                    } else if (preg_match($rEFileTypes, $extension1) == false) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', 'Warning : Invalid Image File!');
                        return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                    } else if ($size > $maximum_filesize) {
                        Session::flash('class', 'alert alert-error');
                        Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
                        return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                    }
                }
                $logo = isset($attachment) ? $new_image_name . $extension : NULL;
                $logo_thumb = isset($attachment_thumb) ? $new_thumb_name . $extension1 : NULL;
                //echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

                $objectCategory = CategoryModel::find($categoryId);
                $objectCategory->category_heading = Input::get('category_heading');
                $objectCategory->category_url = $categoryUrl;
                $objectCategory->category_page_title = Input::get('category_page_title');
                $objectCategory->category_meta_tags = Input::get('category_meta_tags');
                $objectCategory->category_meta_description = Input::get('category_meta_description');
                $objectCategory->category_short_description = Input::get('category_short_description');
                $objectCategory->category_description = Input::get('category_description');
                $objectCategory->parent_id = Input::get('parent_id');
                $objectCategory->is_active = Input::get('is_active');
                $objectCategory->updated_by = Auth::user()->id;

                if ($logo != '') {
                    $objectCategory->category_attachment = $logo;
                }
                if ($logo_thumb != '') {
                    $objectCategory->thumb_attachment = $logo_thumb;
                }
                if (Input::get('show_in') != '') {
                    $objectCategory->show_in = join(',', Input::get('show_in'));
                }
                $objectCategory->save();

                if ($objectCategory->id) {
                    $categoryDetail = CategoryModel::find($categoryId);
                    Session::flash('class', 'alert alert-success');
                    Session::flash('message', 'Category successfully updated');
                    return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                } else {
                    Session::flash('class', 'alert alert-error');
                    Session::flash('message', 'something error');
                    return View::make('admin.category.edit')->with('categoryDetail', $categoryDetail);
                }

            }
        }
    }

    public function publishCategory()
    {
        $categoryId = Input::get('category_id');
        $objectCategory = CategoryModel::find($categoryId);
        $objectCategory->is_active = 1;
        $objectCategory->save();

        if ($objectCategory->id) {
            $array = array('message' => 'Category is published successfully.', 'flag' => true);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
        }
        return $returnValue = json_encode($array);
    }

    public function unpublishCategory()
    {
        $categoryId = Input::get('category_id');
        $objectCategory = CategoryModel::find($categoryId);
        $objectCategory->is_active = 0;
        $objectCategory->save();

        if ($objectCategory->id) {
            $array = array('message' => 'Category is unpublished successfully.', 'flag' => true);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
        }
        return $returnValue = json_encode($array);
    }

    public function deleteCategory()
    {
        $categoryId = Input::get('category_id');
        $objectCategory = CategoryModel::find($categoryId);
        $objectCategory->is_active = 0;
        $objectCategory->deleted = 1;
        $objectCategory->save();

        if ($objectCategory->id) {
            $array = array('message' => 'Category is deleted successfully.', 'flag' => true);
        } else {
            $array = array('message' => 'server error.', 'flag' => false);
        }
        return $returnValue = json_encode($array);
    }

    public function categoryDetail($url)
    {
        $categoryData = CategoryModel::getDetailByUrl($url);
        if (isset($categoryData) && $categoryData != '') {
            return View::make('client.about',
                array('pageTitle' => $categoryData->category_page_title,
                    'metaDescription' => $categoryData->category_meta_tags,
                    'metaKeywords' => $categoryData->category_meta_description,
                )
            )
                ->with('categoryData', $categoryData);
        } else {
            return Redirect::to(404);
        }
    }

}
