<?php

class WhyusController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getWhyUs()
    {
        $whyusList = WhyUsModel::getAll();
		
		//var_dump($whyusList); die();
		$i = 1;
		$whyuss = '';
		foreach($whyusList as $whyus) {
			$whyuss .= '<tr class="odd gradeX">';
			$whyuss .= '<td>'.$i++.'</td>';
			$whyuss .= '<td>'.$whyus->title.'</td>';
			if($whyus->is_active=='1') { 
				$whyuss .= '<td>'.'<a href="javascript:void(0)" class="unpublish-whyus" id="'.$whyus->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$whyuss .= '<td>'.'<a href="javascript:void(0)" class="publish-whyus" id="'.$whyus->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$whyuss .= '<td>'.$whyus->updated_at.'</td>';
            $whyuss .= '<td>'.
						'<a href="whyus/edit/'.$whyus->id.'"><i class="icon-pencil"></i> Edit</a></td>';
			$whyuss .= '</tr>';
		}

        return View::make('admin.whyus.list', array('whyuss' => $whyuss));
    }
	
	public function getAddWhyUs()
	{
		return View::make('admin.whyus.add');
	}
	
	public function addWhyUs()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.whyus.add');
		} 
		else {
			$whyusTitle = Input::get('title');
			
			
			$objectWhyUs = new WhyUsModel();
			$objectWhyUs->title = $whyusTitle;
			$objectWhyUs->content = Input::get('content');
			$objectWhyUs->is_active = Input::get('is_active');
			$objectWhyUs->updated_by = Auth::user()->id;
			
			$objectWhyUs->save();
			
			if($objectWhyUs->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Home Content successfully added');
				return View::make('admin.whyus.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.whyus.add');
			}
						
		}
	}
	
	public function getEditWhyUs($id)
	{
		$whyusDetail = WhyUsModel::find($id);		
		try {
			if($whyusDetail->deleted == 0) {
				return View::make('admin.whyus.edit')->with('whyusDetail',$whyusDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editWhyUs()
	{	
		$whyusId = Input::get('whyus_id');
		$whyusDetail = WhyUsModel::find($whyusId);
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.whyus.edit')->with('whyusDetail',$whyusDetail);
		} 
		else {
			$whyusTitle = Input::get('title');
			
				
				$objectWhyUs = WhyUsModel::find($whyusId);
				$objectWhyUs->title = Input::get('title');
				$objectWhyUs->content = Input::get('content');
				$objectWhyUs->is_active = Input::get('is_active');
				$objectWhyUs->updated_by = Auth::user()->id;
				
				$objectWhyUs->save();
				
				if($objectWhyUs->id) {
					$whyusDetail = WhyUsModel::find($whyusId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Home content successfully updated');
					return View::make('admin.whyus.edit')->with('whyusDetail',$whyusDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.whyus.edit')->with('whyusDetail',$whyusDetail);
				}
					
		}
	}
	
	public function publishWhyUs()
	{ 
		$whyusId = Input::get('whyus_id');
		$objectWhyUs = WhyUsModel::find($whyusId);
		$objectWhyUs->is_active = 1;
		$objectWhyUs->save();
		
		if($objectWhyUs->id) {
			$array = array('message' => 'Home content is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishWhyUs()
	{ 
		$whyusId = Input::get('whyus_id');
		$objectWhyUs = WhyUsModel::find($whyusId);
		$objectWhyUs->is_active = 0;
		$objectWhyUs->save();
		
		if($objectWhyUs->id) {
			$array = array('message' => 'Home content is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteWhyUs()
	{ 
		$whyusId = Input::get('whyus_id');
		$objectWhyUs = WhyUsModel::find($whyusId);
		$objectWhyUs->is_active = 0;
		$objectWhyUs->deleted = 1;
		$objectWhyUs->save();
		
		if($objectWhyUs->id) {
			$array = array('message' => 'Home content is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
}
