<?php

class LoginController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	public function getAdminBase()
	{
		return Redirect::to('admin/login');
	}
	
	public function getLogin()
	{
		return View::make('admin.adminlogin');
	}
	public function postLogin()
	{
		$rules = array(
						'username' => 'required',
						'password' => 'required',
					);
		$validator = Validator::make(Input::all(),$rules);
		
		if($validator->fails()) {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Username or password is missing');
			return View::make('adminlogin');
		} else {
			$auth = Auth::attempt(array(
					'username' => Input::get('username'),
					'password' => Input::get('password'),
					'is_active' => 1,
					'user_type' => 1,
					'deleted' => 0));
			//var_dump(); exit();
			if($auth)
			{ 
				return Redirect::to('admin/dashboard');
			}
			else {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Username or password do not match');
				return View::make('admin.adminlogin');
			}				
		}
	}
	// default dash borad -03-01-2016
	// vinod
	public function postDefault()
	{
		$default = Input::get('default');
			
			//var_dump(); exit();
			// if($default)
			// { 
			// 	$filename = '../../index.php'; // the file to change
			// 	$search = '/school/'; // the content after which you want to insert new stuff
			// 	$insert = '/college/'; // your new stuff
			// 	$replace =  $insert;
			// 	file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));
			// 	Session::flash('class', 'alert alert-success');
			// 	Session::flash('message', 'Change to default site');
			// 	return Redirect::to('admin/dashboard');
			// }
			// else {
				Session::flash('class', 'alert alert-alert');
				Session::flash('message', 'Error');
				return Redirect::to('admin/dashboard');
			// }				
	
	}
	
	public function dashboard()
	{	

		// $filename = '../../index.php'; // the file to change
		// $search = 'college';
		// $content = file_get_contents($filename);
		// preg_match("/^.*$search.*\$/m",$content,$line);
		// $default =array_filter($line);
		// print_r($default);exit;
		// return View::make('admin.dashboard')->with('default',$default);
		 return View::make('admin.dashboard');
	}
	public function logout()
	{	
		Auth::logout();
		return Redirect::to('admin/login');
	}
	public function getChangePassword()
	{
		return View::make('admin.change-password');
	}
	public function changePassword()
	{
		$rules = array('old_password' => 'required',
						'new_password1' => 'required',
						'new_password2' => 'required'
					);
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()) {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.change-password');
		}
		else {
			$auth = Auth::attempt(array(
					'id' => Auth::user()->id,
					'password' => Input::get('old_password'),
					'is_active' => 1,
					'deleted' => 0));
			if($auth) {
				if(Input::get('new_password1') != (Input::get('new_password2'))) {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'New Passwords do not match');
					return View::make('admin.change-password');
				}
				else {
					$user = User::changePassword(Input::get('new_password1'));
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Password successfully updated.');
					return View::make('admin.change-password');
				}
			}
			else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'Old Password do not match');
				return View::make('admin.change-password');
			}
		}
	}
	
}
