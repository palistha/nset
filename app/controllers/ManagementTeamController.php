<?php

class ManagementTeamController extends \BaseController {
	protected $thumb_w;
	protected $thumb_h;
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct(ManagementTeamModel $albums){
		$this->albums = $albums;
		$this->destinationpath = './uploads/managementteams/';
		$this->thumbpath = $this->destinationpath.'thumbs/';
		$this->imagePrefix = 'album_';
		$this->thumb_w = 263;
		$this->thumb_h = 270;
		$this->thumb_sec_w =  1920;
		$this->thumb_sec_h = 608;
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getManagementTeam()
	{
		$managementteamList = ManagementTeamModel::getAllManagementTeam();
		
		//var_dump($testimonialList); die();
		$i = 1;
		$managementteams = '';
		foreach($managementteamList as $managementteam) {


			$managementteams .= '<tr class="odd gradeX">';
			$managementteams .= '<td>'.$i++.'</td>';
			$managementteams .= '<td>'.$managementteam->title.'</td>';
			
			if(file_exists("uploads/managementteams/".$managementteam->images) && $managementteam->images!=''){
				$managementteams .= '<td><img src="'.'../uploads/managementteams/'.$managementteam->images.'" style="width: 200px; height: 120px;"></td>';
			} else {
				$managementteams .= '<td><img src="'.'../uploads/noimage.jpg'.'" style="width: 200px; height: 120px;"></td>';
			}

			$managementteams .= '<td>'.$managementteam->position.'</td>';


			if($managementteam->is_active=='1') { 
				$managementteams .= '<td>'.'<a href="javascript:void(0)" class="unpublish-managementteam" id="'.$managementteam->id.'" >'.
				'<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$managementteams .= '<td>'.'<a href="javascript:void(0)" class="publish-managementteam" id="'.$managementteam->id.'" >'.
				'<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$managementteams .= '<td>'.$managementteam->updated_at.'</td>';
			$managementteams .= '<td>'.
			'<a href="managementteam/edit/'.$managementteam->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
			'<a href="javascript:void(0)" class="delete-managementteam" id="'.$managementteam->id.'" >'.
			'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$managementteams .= '</tr>';
		}

		return View::make('admin.managementteam.list', array('managementteams' => $managementteams));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getAddManagementTeam()
	{
		return View::make('admin.managementteam.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function addManagementTeam()
	{
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.managementteam.add');
		} 
		else {
			$title = Input::get('title');
			$position= Input::get('position');
			
			$imageFile = Input::file('images');
			$destinationPath = 'uploads/managementteams/';
			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
			$maximum_filesize = 1 * 1024 * 1024;

		// dd($imageFile);
			if($imageFile) {
				$extension = strrchr($imageFile->getClientOriginalName(), '.');
				$new_file_name = $this->imagePrefix . time();
				$fullFilename = $new_file_name . $extension;

				$attachment = $imageFile->move($this->destinationpath, $fullFilename);
				$imagePath = $this->destinationpath.$fullFilename;
				$album_attachment= isset($attachment) ? $fullFilename : NULL;

				$img = Image::make($imagePath);

				$img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
					$constraint->upsize();
				});

				if(!file_exists($this->thumbpath)) {
					@mkdir($this->thumbpath);
				}

            // finally we save the image as a new file
				$img->save($this->thumbpath.$fullFilename);
			}
			$logo = isset($attachment) ? $new_file_name  . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
			
			$objectManagementTeam = new ManagementTeamModel();
			$objectManagementTeam->title= Input::get('title');
			$objectManagementTeam->heading= Input::get('heading');
			$objectManagementTeam->position = Input::get('position');

			$objectManagementTeam->is_active = Input::get('is_active');
			$objectManagementTeam->updated_by = Auth::user()->id;
			
			if($logo != '') {
				$objectManagementTeam->images= $logo;
			}
			$objectManagementTeam->save();
			
			if($objectManagementTeam->id) {
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Management Teams successfully added');
				return View::make('admin.managementteam.add');
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.managementteam.add');
			}

		}


	}

	public function getEditManagementTeam($id)
	{
		$managementteamDetail = ManagementTeamModel::find($id);		
		try {
			if($managementteamDetail->deleted == 0) {
				return View::make('admin.managementteam.edit')->with('managementteamDetail',$managementteamDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editManagementTeam()
	{	
		$managementteamId = Input::get('managementteam_id');
		$managementteamDetail = ManagementTeamModel::find($managementteamId);
		// var_dump($managementteamDetail);die;
		$rules = array('title' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.managementteam.edit')->with('managementteamDetail',$managementteamDetail);
		} 
		else
		{
			$managementteamTitle = Input::get('title');

			
			$imageFile = Input::file('images');
			if($imageFile) {
				$oldimage = $this->albums->find($managementteamId)->images;
				$extension = strrchr($imageFile->getClientOriginalName(), '.');
				$new_file_name = $this->imagePrefix . time();
				$fullFilename = $new_file_name . $extension;

				if(!file_exists($this->destinationpath)) {
					@mkdir($this->destinationpath);
				}

				$attachment = $imageFile->move($this->destinationpath, $fullFilename);
				$imagePath = $this->destinationpath.$fullFilename;
				$images = isset($attachment) ? $fullFilename : NULL;

				$img = Image::make($imagePath);


            // add callback functionality to retain maximal original image size
				$img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
					$constraint->upsize();
				});

				if(!file_exists($this->thumbpath)) {
					@mkdir($this->thumbpath);
				}

            // finally we save the image as a new file
				$img->save($this->thumbpath . $fullFilename);
				if($oldimage) {
					if (file_exists($this->destinationpath . $oldimage)) {
						@unlink($this->destinationpath . $oldimage);
					}
					if (file_exists($this->thumbpath . $oldimage)) {
						@unlink($this->thumbpath . $oldimage);
					}
				}
			}

			$albums = $this->albums->update();
			$logo = isset($attachment) ? $new_file_name . $extension : NULL;
			//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;

			$objectManagementTeam = ManagementTeamModel::find($managementteamId);
			$objectManagementTeam->title = Input::get('title');
			$objectManagementTeam->heading = Input::get('heading');
			$objectManagementTeam->position= Input::get('position');

			$objectManagementTeam->is_active = Input::get('is_active');
			$objectManagementTeam->updated_by = Auth::user()->id;

			if($logo != '') {
				$objectManagementTeam->images = $logo;
			}
			$objectManagementTeam->save();

			if($objectManagementTeam->id) {
				$managementteamDetail = ManagementTeamModel::find($managementteamId);		
				Session::flash('class', 'alert alert-success');
				Session::flash('message', 'Management Team successfully updated');
				return View::make('admin.managementteam.edit')->with('managementteamDetail',$managementteamDetail);
			} else {
				Session::flash('class', 'alert alert-error');
				Session::flash('message', 'something error');
				return View::make('admin.managementteam.edit')->with('managementteamDetail',$managementteamDetail);
			}

		}
	}
	public function publishManagementTeam()
	{ 
		$managementteamId = Input::get('managementteam_id');
		$objectManagementTeam = ManagementTeamModel::find($managementteamId);
		$objectManagementTeam->is_active = 1;
		$objectManagementTeam->save();

		if($objectManagementTeam->id) {
			$array = array('message' => 'Management Team is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	public function unpublishManagementTeam()
	{ 
		$managementteamId = Input::get('managementteam_id');
		$objectManagementTeam = ManagementTeamModel::find($managementteamId);
		$objectManagementTeam->is_active = 0;
		$objectManagementTeam->save();

		if($objectManagementTeam->id) {
			$array = array('message' => 'Management Team is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'Something error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}

	public function deleteManagementTeam()
	{ 

		$managementteamId = Input::get('managementteam_id');
		$objectManagementTeam =ManagementTeamModel::find($managementteamId);
		$objectManagementTeam->is_active = 0;
		$objectManagementTeam->deleted = 1;
		$objectManagementTeam->save();

		if($objectManagementTeam->id) {
			print_r(json_encode(['flag' => true, 'message' => 'Management Team is deleted succesfully.']));
		} else {
			print_r(json_encode(['flag' => false, 'message' => 'Something error.']));

		}
	}
}
