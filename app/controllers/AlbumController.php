<?php

class AlbumController extends BaseController {
	protected $thumb_w;
	protected $thumb_h;
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct(AlbumModel $albums){
		$this->albums = $albums;
		$this->destinationpath = './uploads/albums/';
		$this->thumbpath = $this->destinationpath.'thumbs/';
		$this->imagePrefix = 'album_';
		$this->thumb_w =  360;
		$this->thumb_h = 270;
	
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getAlbums()
	{
		$albumList = AlbumModel::getAllAlbums();
		
		//var_dump($albumList); die();
		$i = 1;
		$albums = '';
		foreach($albumList as $album) {
			$albums .= '<tr class="odd gradeX">';
			$albums .= '<td>'.$i++.'</td>';
			$albums .= '<td>'.$album->album_title.'</td>';

			if($album->is_active=='1') { 
				$albums .= '<td>'.'<a href="javascript:void(0)" class="unpublish-album" id="'.$album->id.'" >'.
				'<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$albums .= '<td>'.'<a href="javascript:void(0)" class="publish-album" id="'.$album->id.'" >'.
				'<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$albums .= '<td>'.$album->updated_at.'</td>';
			$albums .= '<td>'.
			'<a href="album/edit/'.$album->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
			'<a href="javascript:void(0)" class="delete-album" id="'.$album->id.'" >'.
			'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$albums .= '<td>'.
			'<a href="album/viewmedia/'.$album->id.'" style="text-decoration:none;">View</a> | '.
			'<a href="album/addmedia/'.$album->id.'" style="text-decoration:none;">Add Image</a>'.
			'</td>';
			$albums .= '</tr>';
		}
		
		return View::make('admin.album.list', array('albums' => $albums));
	}
	
	public function getAddAlbum()
	{
		return View::make('admin.album.add');
	}

	public function addAlbum(){
		// dd(1);
		$imageFile = Input::file('userfile');
		// dd($imageFile);
		if($imageFile) {
			$extension = strrchr($imageFile->getClientOriginalName(), '.');
			$new_file_name = $this->imagePrefix . time();
			$fullFilename = $new_file_name . $extension;

			$attachment = $imageFile->move($this->destinationpath, $fullFilename);
			$imagePath = $this->destinationpath.$fullFilename;
			$album_attachment= isset($attachment) ? $fullFilename : NULL;

			$img = Image::make($imagePath);

			$img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
				$constraint->upsize();
			});

			if(!file_exists($this->thumbpath)) {
				@mkdir($this->thumbpath);
			}

            // finally we save the image as a new file
			$img->save($this->thumbpath.$fullFilename);
		}
		$albumHeading = Input::get('album_title');
		$albumUrl = Input::get('album_url');
		$albumUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $albumUrl);
		$logo = isset($attachment) ? $new_file_name . $extension : NULL;
		$objectAlbum = new AlbumModel();
		$objectAlbum->album_title = Input::get('album_title');
		$objectAlbum->album_url = $albumUrl;
		$objectAlbum->album_description = Input::get('album_description');
		$objectAlbum->is_active = Input::get('is_active');
		$objectAlbum->updated_by = Auth::user()->id;

		if($logo != '') {         
			$objectAlbum->album_attachment = $logo;
		}
		$objectAlbum->save();

		if($objectAlbum->id > 0) {
			Session::flash('class', 'alert alert-success');
			Session::flash('message', 'Album successfully added');
			return Redirect::route('admin.albums.list' );
		} else {
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'something error');
			return View::make('admin.album.add');	

		}
	}
	

	
	// public function addAlbum()
	// {
	// 	$files = Input::file('userfile');
	// 	$count = 0;
	// 	$valid = true;
	// 	$rules = array('album_title' => 'required');
	// 	$validator = Validator::make(Input::all(), $rules);
 //    if($validator->fails()){
	// 		Session::flash('class', 'alert alert-error');
	// 		Session::flash('message', 'Some fields are missing');
	// 		return View::make('admin.album.add');
	// 	} 
	// 	else {
 //      $albumHeading = Input::get('album_title');
 //      $albumUrl = Input::get('album_url');
	// 		$albumUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $albumUrl);

	// 		$albumExist = AlbumModel::checkExist($albumUrl);
	// 		if( count($albumExist)!=0) {
	// 			$message = 'album <b>'.$albumHeading.'</b> with url <b>'.$albumUrl.'</b> is already exist';
	// 			Session::flash('class', 'alert alert-error');
	// 			Session::flash('message', $message);	
	// 			return View::make('admin.album.add');
	// 		}
	// 		else {	
 //        if(count($files) > 10 || count($files) == 0) {
 //          Session::flash('class', 'alert alert-error');
 //          Session::flash('message', 'There is no image to upload or maximum numbers of images exceeds 10.');
 //          return View::make('admin.album.add');
 //        } else {

 //        	$imageFile = Input::file('userfile');
	//         $destinationPath = 'uploads/albums/';
	//         $rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
	//         $maximum_filesize = 1 * 1024 * 1024;

	//         if($imageFile) {
	//           $filename = $imageFile->getClientOriginalName();
	//           $extension = strrchr($filename, '.');
	//           $size = $imageFile->getSize();					
	//           $new_image_name = "album_" . time() . "_" . rand(0,9999);

	//           if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
	//             $attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
	//           } else if (preg_match($rEFileTypes, $extension) == false) {
	//             Session::flash('class', 'alert alert-error');
	//             Session::flash('message', 'Warning : Invalid Image File!');
	//             return View::make('admin.album.add')->withInput();
	//           } else if ($size > $maximum_filesize) {
	//             Session::flash('class', 'alert alert-error');
	//             Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
	//             return View::make('admin.album.add')->withInput();
	//           }				
	//         }
	//         $logo = isset($attachment) ? $new_image_name . $extension : NULL;
 //          $objectAlbum = new AlbumModel();
 //          $objectAlbum->album_title = Input::get('album_title');
 //          $objectAlbum->album_url = $albumUrl;
 //          $objectAlbum->album_description = Input::get('album_description');
 //          $objectAlbum->is_active = Input::get('is_active');
 //          $objectAlbum->updated_by = Auth::user()->id;

 //        	if($logo != '') {         
	//         	$objectAlbum->album_attachment = $logo;
	//     	}
 //          $objectAlbum->save();

 //          if($objectAlbum->id > 0) {
 //            Session::flash('class', 'alert alert-success');
 //            Session::flash('message', 'Album successfully added');
 //            return Redirect::route('admin.albums.list' );
 //          } else {
 //            Session::flash('class', 'alert alert-error');
 //            Session::flash('message', 'something error');
 //            return View::make('admin.album.add');				
 //          }
 //        }
 //      }
 //    }
	// }
	
	public function getEditAlbum($id)
	{
		$albumDetail = AlbumModel::find($id);		
		try {
			if($albumDetail->deleted == 0) {
				return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editAlbum()
	{	$imageFile = Input::file('userfile');

	$albumId = Input::get('album_id');
	$albumDetail = AlbumModel::find($albumId);
	if($imageFile) {
		$oldimage = $this->albums->find($albumId)->album_attachment;
		$extension = strrchr($imageFile->getClientOriginalName(), '.');
		$new_file_name = $this->imagePrefix . time();
		$fullFilename = $new_file_name . $extension;

		if(!file_exists($this->destinationpath)) {
			@mkdir($this->destinationpath);
		}

		$attachment = $imageFile->move($this->destinationpath, $fullFilename);
		$imagePath = $this->destinationpath.$fullFilename;
		$data['album_attachment'] = isset($attachment) ? $fullFilename : NULL;

		$img = Image::make($imagePath);

		// if($img->width() < $this->thumb_w || $img->height() < $this->thumb_h) {
		// 	@unlink($imagePath);
		// 	return redirect()->back()
		// 	->withInput()
		// 	->withWarningMessage("Please upload image with min resolution $this->thumb_w x $this->thumb_h");
		// }

            // add callback functionality to retain maximal original image size
		$img->fit($this->thumb_w, $this->thumb_h, function ($constraint) {
			$constraint->upsize();
		});

		if(!file_exists($this->thumbpath)) {
			@mkdir($this->thumbpath);
		}

            // finally we save the image as a new file
		$img->save($this->thumbpath . $fullFilename);

		if($oldimage) {
			if (file_exists($this->destinationpath . $oldimage)) {
				@unlink($this->destinationpath . $oldimage);
			}
			if (file_exists($this->thumbpath . $oldimage)) {
				@unlink($this->thumbpath . $oldimage);
			}
		}
	}

	$albums = $this->albums->update();
	$logo = isset($attachment) ? $new_file_name . $extension : NULL;
$albumUrl = Input::get('album_url');
			$albumUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $albumUrl);
	$objectAlbum = AlbumModel::find($albumId);
	$objectAlbum->album_title = Input::get('album_title');
	$objectAlbum->album_url = $albumUrl;
	$objectAlbum->album_description = Input::get('album_description');
	$objectAlbum->is_active = Input::get('is_active');
	$objectAlbum->updated_by = Auth::user()->id;

	if($logo != '') {
		if(file_exists('uploads/albums/thumbs/'.$objectAlbum->album_attachment) && $objectAlbum->album_attachment != '') {
			unlink('uploads/albums/thumbs/'.$objectAlbum->album_attachment);
		}
		$objectAlbum->album_attachment = $logo;
	}

	$objectAlbum->save();

	if($objectAlbum->id) {
		$albumDetail = AlbumModel::find($albumId);		
		Session::flash('class', 'alert alert-success');
		Session::flash('message', 'Album successfully updated');
		return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	} else {
		Session::flash('class', 'alert alert-error');
		Session::flash('message', 'something error');
		return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	}

	
}

	// public function editAlbum()
	// {	
	// 	$albumId = Input::get('album_id');
	// 	$albumDetail = AlbumModel::find($albumId);
	// 	$rules = array('album_title' => 'required');
	// 	$validator = Validator::make(Input::all(), $rules);

	// 	if($validator->fails()){
	// 		Session::flash('class', 'alert alert-error');
	// 		Session::flash('message', 'Some fields are missing');
	// 		return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 	} 
	// 	else {
	// 		$albumHeading = Input::get('album_title');
	// 		$albumUrl = Input::get('album_url');
	// 		$albumUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $albumUrl);

	// 		$albumExist = AlbumModel::checkExist($albumUrl);
	// 		if( count($albumExist)!=0 && $albumExist->id != $albumId) {
	// 			$message = 'album <b>'.$albumHeading.'</b> with url <b>'.$albumUrl.'</b> is already exist';
	// 			Session::flash('class', 'alert alert-error');
	// 			Session::flash('message', $message);	
	// 			return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 		}
	// 		else {	
	// 			$imageFile = Input::file('userfile');
	// 			$destinationPath = 'uploads/albums/';
	// 			$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
	// 			$maximum_filesize = 1 * 1024 * 1024;

	// 			if($imageFile) {
	// 				$filename = $imageFile->getClientOriginalName();
	// 				$extension = strrchr($filename, '.');
	// 				$size = $imageFile->getSize();					
	// 				$new_image_name = "album_" . time() . "_" . rand(0,9999);

	// 				if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
	// 					$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
	// 				} else if (preg_match($rEFileTypes, $extension) == false) {
	// 					Session::flash('class', 'alert alert-error');
	// 					Session::flash('message', 'Warning : Invalid Image File!');
	// 					return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 				} else if ($size > $maximum_filesize) {
	// 					Session::flash('class', 'alert alert-error');
	// 					Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
	// 					return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 				}				
	// 			}
	// 			$logo = isset($attachment) ? $new_image_name . $extension : NULL;

	// 			$objectAlbum = AlbumModel::find($albumId);
	// 			$objectAlbum->album_title = Input::get('album_title');
	// 			$objectAlbum->album_url = $albumUrl;
	// 			$objectAlbum->album_description = Input::get('album_description');
	// 			$objectAlbum->is_active = Input::get('is_active');
	// 			$objectAlbum->updated_by = Auth::user()->id;

	// 			if($logo != '') {
	// 				if(file_exists('uploads/albums/'.$objectAlbum->album_attachment) && $objectAlbum->album_attachment != '') {
	// 					unlink('uploads/albums/'.$objectAlbum->album_attachment);
	// 				}
	// 				$objectAlbum->album_attachment = $logo;
	// 			}
	// 			$objectAlbum->save();

	// 			if($objectAlbum->id) {
	// 				$albumDetail = AlbumModel::find($albumId);		
	// 				Session::flash('class', 'alert alert-success');
	// 				Session::flash('message', 'Album successfully updated');
	// 				return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 			} else {
	// 				Session::flash('class', 'alert alert-error');
	// 				Session::flash('message', 'something error');
	// 				return View::make('admin.album.edit')->with('albumDetail',$albumDetail);
	// 			}
	// 		}
	// 	}
	// }

public function publishAlbum()
{ 
	$albumId = Input::get('album_id');
	$objectAlbum = AlbumModel::find($albumId);
	$objectAlbum->is_active = 1;
	$objectAlbum->updated_by = Auth::user()->id;
	$objectAlbum->save();

	if($objectAlbum->id) {
		$array = array('message' => 'Album is published successfully.', 'flag' => true);			
	} else {
		$array = array('message' => 'server error.', 'flag' => false);			
	}
	return $returnValue = json_encode($array);
}

public function unpublishAlbum()
{ 
	$albumId = Input::get('album_id');
	$objectAlbum = AlbumModel::find($albumId);
	$objectAlbum->is_active = 0;
	$objectAlbum->updated_by = Auth::user()->id;
	$objectAlbum->save();

	if($objectAlbum->id) {
		$array = array('message' => 'Album is unpublished successfully.', 'flag' => true);			
	} else {
		$array = array('message' => 'server error.', 'flag' => false);			
	}
	return $returnValue = json_encode($array);
}

public function deleteAlbum()
{ 
	$albumId = Input::get('album_id');
	$objectAlbum = AlbumModel::find($albumId);
	$objectAlbum->updated_by = Auth::user()->id;
	$objectAlbum->is_active = 0;
	$objectAlbum->deleted = 1;		
	$objectAlbum->save();

	if($objectAlbum->id) {
		$array = array('message' => 'Album is deleted successfully.', 'flag' => true);			
	} else {
		$array = array('message' => 'server error.', 'flag' => false);			
	}
	return $returnValue = json_encode($array);
}

}