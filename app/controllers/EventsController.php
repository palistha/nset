<?php

class EventsController extends BaseController {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	public function getEvents()
    {
        $EventsList = EventsModel::getAllEvents();
		
		//var_dump($EventsList); die();
		$i = 1;
		$Eventss = '';
		foreach($EventsList as $Events) {
			$Eventss .= '<tr class="odd gradeX">';
			$Eventss .= '<td>'.$i++.'</td>';
			$Eventss .= '<td>'.$Events->events_heading.'</td>';
			$Eventss .= '<td>'.$Events->events_sdate.'</td>';
			if($Events->is_active=='1') { 
				$Eventss .= '<td>'.'<a href="javascript:void(0)" class="unpublish-events" id="'.$Events->id.'" >'.
						 '<span class="label label-success"><i class="icon-ok"></i></span></a>&nbsp;&nbsp'.'</td>';
			} else {
				$Eventss .= '<td>'.'<a href="javascript:void(0)" class="publish-events" id="'.$Events->id.'" >'.
						 '<span class="label label-warning"><i class="icon-minus-sign"></i></span></a>&nbsp;&nbsp'.'</td>';
			}
			$Eventss .= '<td>'.$Events->updated_at.'</td>';
            $Eventss .= '<td>'.
						'<a href="events/edit/'.$Events->id.'"><i class="icon-pencil"></i> Edit</a>&nbsp;&nbsp;'.
	                    '<a href="javascript:void(0)" class="delete-Events" id="'.$Events->id.'" >'.
						'<i class="icon-trash" ></i> Delete</a>'.'</td>';
			$Eventss .= '</tr>';
		}

        return View::make('admin.events.list', array('Eventss' => $Eventss));
    }
	
	public function getAddEvents()
	{
		return View::make('admin.events.add');
	}
	
	public function addEvents()
	{
		$rules = array('Events_heading' => 'required',
					   'Events_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.events.add');
		} 
		else {
			$EventsHeading = Input::get('Events_heading');
			$EventsUrl = Input::get('Events_url');
			$EventsUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $EventsUrl);
			
			$EventsExist = EventsModel::checkExist($EventsUrl);
			if( count($EventsExist)!=0) {
				$message = 'Events <b>'.$EventsHeading.'</b> with url <b>'.$EventsUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.events.add');
			}
			else {									
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/Events/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "Events" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.events.add');
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.events.add');
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
				
				$objectEvents = new EventsModel();
				$objectEvents->events_heading = Input::get('Events_heading');
				$objectEvents->events_url = $EventsUrl;
				$objectEvents->events_sdate = Input::get('Events_sdate');
				$objectEvents->events_edate = Input::get('Events_edate');
				$objectEvents->events_page_title = Input::get('Events_page_title');
				$objectEvents->events_meta_tags = Input::get('Events_meta_tags');
				$objectEvents->events_meta_description = Input::get('Events_meta_description');
				$objectEvents->events_short_description = Input::get('Events_short_description');
				$objectEvents->events_description = Input::get('Events_description');
				$objectEvents->is_active = Input::get('is_active');
				$objectEvents->updated_by = Auth::user()->id;
				
				if($logo != '')
				{
					$objectEvents->events_attachment = $logo;
				}
				$objectEvents->save();
				
				if($objectEvents->id) {
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Events successfully added');
					return View::make('admin.events.add');
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.events.add');
				}
				
			}			
		}
	}
	
	public function getEditEvents($id)
	{
		$EventsDetail = EventsModel::find($id);		
		try {
			if($EventsDetail->deleted == 0) {
				return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
			} else {
				return View::make('admin.error');
			}			
		} catch(ErrorException $e) {
			return View::make('admin.error');
		}
	}
	
	public function editEvents()
	{	
		$EventsId = Input::get('Events_id');
		$EventsDetail = EventsModel::find($EventsId);
		$rules = array('Events_heading' => 'required',
					   'Events_url' => 'required');
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->fails()){
			Session::flash('class', 'alert alert-error');
			Session::flash('message', 'Some fields are missing');
			return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
		} 
		else {
			$EventsHeading = Input::get('Events_heading');
			$EventsUrl = Input::get('Events_url');
			$EventsUrl = preg_replace('/[^A-Za-z0-9\-]/', '', $EventsUrl);
			$EventsExist = EventsModel::checkExist($EventsUrl);
			if( count($EventsExist)!=0 && $EventsExist->id != $EventsId) {
				$message = 'Events <b>'.$EventsHeading.'</b> with url <b>'.$EventsUrl.'</b> is already exist';
				Session::flash('class', 'alert alert-error');
				Session::flash('message', $message);	
				return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
			}
			else {				
				$imageFile = Input::file('userfile');
				$destinationPath = 'uploads/Events/';
				$rEFileTypes = "/^\.(jpg|jpeg|gif|png){1}$/i";
				$maximum_filesize = 1 * 1024 * 1024;
				
				if($imageFile) {
					$filename = $imageFile->getClientOriginalName();
					$extension = strrchr($filename, '.');
					$size = $imageFile->getSize();					
					$new_image_name = "Events" . "_" . time();
					
					if ($size <= $maximum_filesize && preg_match($rEFileTypes, $extension)) {
						$attachment = $imageFile->move($destinationPath, $new_image_name.$extension);						
					} else if (preg_match($rEFileTypes, $extension) == false) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', 'Warning : Invalid Image File!');
						return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
					} else if ($size > $maximum_filesize) {
						Session::flash('class', 'alert alert-error');
						Session::flash('message', "Warning : The size of the image shouldn't be more than 1MB!");
						return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
					}				
				}
				$logo = isset($attachment) ? $new_image_name . $extension : NULL;
				//echo $logo.'<br>'.$attachment.'<br>'.$new_image_name;
				
				$objectEvents = EventsModel::find($EventsId);
				$objectEvents->events_heading = Input::get('Events_heading');
				$objectEvents->events_url = $EventsUrl;
				$objectEvents->events_sdate = Input::get('Events_sdate');
				$objectEvents->events_edate = Input::get('Events_edate');
				$objectEvents->events_page_title = Input::get('Events_page_title');
				$objectEvents->events_meta_tags = Input::get('Events_meta_tags');
				$objectEvents->events_meta_description = Input::get('Events_meta_description');
				$objectEvents->events_short_description = Input::get('Events_short_description');
				$objectEvents->events_description = Input::get('Events_description');
				$objectEvents->is_active = Input::get('is_active');
				$objectEvents->updated_by = Auth::user()->id;
				
				if($logo != '')
				{
					$objectEvents->Events_attachment = $logo;
				}
				//var_dump($objectEvents);
				$objectEvents->save();
				
				if($objectEvents->id) {
					$EventsDetail = EventsModel::find($EventsId);		
					Session::flash('class', 'alert alert-success');
					Session::flash('message', 'Events successfully updated');
					return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
				} else {
					Session::flash('class', 'alert alert-error');
					Session::flash('message', 'something error');
					return View::make('admin.events.edit')->with('EventsDetail',$EventsDetail);
				}
				
			}			
		}
	}
	
	public function publishEvents()
	{ 
		$EventsId = Input::get('Events_id');
		$objectEvents = EventsModel::find($EventsId);
		$objectEvents->is_active = 1;
		$objectEvents->save();
		
		if($objectEvents->id) {
			$array = array('message' => 'Events is published successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function unpublishEvents()
	{ 
		$EventsId = Input::get('Events_id');
		$objectEvents = EventsModel::find($EventsId);
		$objectEvents->is_active = 0;
		$objectEvents->save();
		
		if($objectEvents->id) {
			$array = array('message' => 'Events is unpublished successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}
	
	public function deleteEvents()
	{ 
		$EventsId = Input::get('Events_id');
		$objectEvents = EventsModel::find($EventsId);
		$objectEvents->is_active = 0;
		$objectEvents->deleted = 1;
		$objectEvents->save();
		
		if($objectEvents->id) {
			$array = array('message' => 'Events is deleted successfully.', 'flag' => true);
			$returnValue = json_encode($array);
		} else {
			$array = array('message' => 'server error.', 'flag' => false);
			$returnValue = json_encode($array);
		}
		return $returnValue;
	}

}
