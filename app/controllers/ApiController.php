<?php

class ApiController extends BaseController {

	public function test(){
		print_r('testsdfsd');exit;
		return Response::json(['status'=>'success', 'message'=>'Invalid data'], 422);
	}

	public function showMenu($limit=100) {
	    $contents = ContentModel::where('is_active', 1)
	    				->limit($limit)
	    				->where('deleted', 0)
	    				->orderby('show_order')
	    				->get();
	    if ($contents->count()>0) {
	    	$json_response = array();
	    	$array = array();
	    	foreach ($contents as $row) { 
	        	$array[] = array("id"=>$row->id,
	                "parent_id"=>$row->parent_id,
	                "content_heading"=>$row->content_heading);
	        }
	        $json_response = \Helper::buildTree($array);
	        return Response::json(['status'=>'success',$json_response], 200);
		} else {
			return Response::json(['status'=>'success','message'=>'Sorry content not found'], 200);
		}
	}

	public function getAllContents(){
		$contents = ContentModel::select('id','parent_id','content_heading','content_url','content_short_description','content_description')
					->where('deleted', 0)
					->where('is_active', 1)
					->orderby('show_order')
                    ->get();
        return Response::json(['status'=>'success','contents'=>$contents], 200);
	}

	public function getAllNews(){
		$news = NewsModel::select('id','news_heading','news_url','news_short_description','news_description', 'news_attachment','news_venue','news_date','news_type')
					->where('deleted', 0)
					->where('is_active', 1)
					->where('news_type' , 'N')
					->orderby('news_date', 'desc')
                    ->get();
        return Response::json(['status'=>'success','news'=>$news], 200);
	}

	public function getAllEvents(){
		$events = NewsModel::select('id','news_heading','news_url','news_short_description','news_description', 'news_attachment','news_venue','news_date','news_type')
					->where('deleted', 0)
					->where('is_active', 1)
					->where('news_type' , 'E')
					->orderby('news_date', 'desc')
                    ->get();
        return Response::json(['status'=>'success','events'=>$events], 200);
	}

	public function getAllGalleries(){
		$albums = AlbumModel::with(array('galleries' => function($query)
		{
		    $query->select('id','album_id','media_title','media_caption','media_description','media_attachment','media_link');
		    $query->where('is_active',1);
		    $query->where('deleted',0);

		}))->select('id','album_title','album_url','album_attachment','album_description')
			->where('is_active', 1)
			->where('deleted', 0)
			->get();
		return Response::json(['status'=>'success','image_path'=>'uploads/albums/', 'albums'=>$albums], 200);
	}

	public function getAllBanners(){
		$banners = BannerModel::select('id','banner_attachment','banner_caption','banner_description','banner_url')
						->where('deleted', 0)
						->where('is_active', 1)
						->where('banner_type', 2)
						->orderby('created_at', 'desc')
						->take(3)
						->get();
		return Response::json(['status'=>'success','image_path'=>'uploads/banners/', 'banners'=>$banners], 200);
	}

	public function getAllNotices(){
		// $notices = NewsModel::getAllActiveAnnouncementForHome();
		$notices = NewsModel::select('id','news_heading','news_url','news_short_description','news_description','news_attachment','news_date','news_venue','news_type')
							->where('is_active', 1)
							->where('deleted', 0)
							->where('news_type', 'A')
							->orderby('created_at', 'desc')
							->take(10)
							->get();
		return Response::json(['status'=>'success','notices'=>$notices], 200);
	}

	public function getAllComingEvents(){
		// $events = NewsModel::getAllActiveNewsForHome();
		$events = NewsModel::select('id','news_heading','news_url','news_short_description','news_description','news_attachment','news_date','news_venue','news_type')
							->where('is_active', 1)
							->where('deleted', 0)
							->where('news_type', 'E')
							->orderby('created_at', 'desc')
							->take(3)
							->get();
		return Response::json(['status'=>'success','events'=>$events], 200);
	}
}
