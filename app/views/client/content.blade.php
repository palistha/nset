@extends('layout.client.container')
@section('content')
    

<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>{{$contentData->content_heading}}</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li>{{$contentData->content_heading}}</li>
                        </ul>
                    </div>
                </div>  
            </div>
                

<div class="content_wrapper">
                <div class="container">  
                 <?php
                    $listLev2Header = ContentModel::headerContentMenus($contentData->id);

                    if(count($listLev2Header) > 0) {

                        echo Helper::GetSubContent($contentData->content_url, $contentData->id, "1");

                    } else {  ?>
                    {{ $contentData->content_description}}
                    <?php } ?>  
                </div>
                </div>
                
                

    
   

@stop
