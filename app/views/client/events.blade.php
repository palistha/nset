@extends('layout.client.container')
@section('content')


    <section class="inner_body_content">

        <div class="container">

            <div class="row">

                <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li class="you">You are here:</li>
                        <li><a href="{{URL::route('home')}}">Home</a></li>
                        <li class="active">Events</li>
                    </ol>
                </div>

                <div class="col-md-12 title">Events</div>

                <div class="col-md-12">
                    <?php
                    if(count($newsList) > 0) {
                    foreach($newsList as $news) { ?>

                    <div class="row">
                        <div class="blogpost clearfix fadeInDown-animation">
                            <div class="col-md-4 col-sm-4 blogpost-image">
                                <?php if(file_exists('uploads/news/thumbs/' . $news->news_attachment) && $news->news_attachment != '') { ?>
                                <img src="{{$sitePath}}uploads/news/thumbs/{{$news->news_attachment}}" class="img-responsive"
                                     alt="{{ $news->news_heading}}"/>
                                <?php }
                                else{?>
                                <img alt="{{ $news->news_heading}}" class="img-responsive"
                                     src="{{$sitePath}}uploads/noimage.jpg"/>
                                <?php } ?>
                            </div>
                            <!--              End Post Image -->
                            <div class="col-md-8 col-sm-8 blogpost-info clearfix">
                                <h3 class="blogpost-title fl">
                                    <span class="post-type-icon"><i class="fa fa-comment"></i></span>
                                    <a href="{{URL::route('news', $news->news_url)}}"
                                       class="ln-tr">{{ $news->news_heading}}</a>
                                </h3>

                                <div class="meta fr">
                                    <div class="date ib">
                                        <span class="icon"><i class="fa fa-clock-o"></i></span>
                                        <span class="text">{{ date('M d, Y', strtotime($news->news_date))}}</span>
                                    </div><!-- date icon -->
                                    <!--              <div class="author ib">
                                                      <span class="icon"><i class="fa fa-user"></i></span>
                                                      <span class="text">By : <a class="ln-tr" href="#">Begha</a></span>
                                                  </div> author icon -->

                                </div><!-- End Meta -->
                                <div class="blogpost-description">
                                    {{ (strlen(strip_tags($news->news_description)) > 620) ? substr(strip_tags($news->news_description),0,620).'...' : strip_tags($news->news_description)

 }}
                                </div>
                                <div class="buttons fr">
                                    <a href="{{URL::route('events', $news->news_url)}}"
                                       class="btn btn-primary grad-btn orange-btn read-btn">Read More</a>
                                </div>
                            </div>

                            <div class="clear"></div>

                        </div>


                    </div>


                    <?php } ?>
                    <div class="text-center">
                        <?php echo $newsList->links(); ?>
                    </div>
                    <?php } else {
                        echo "No Events available";
                    }
                    ?>
                </div>
                <!--inner content end-->
            </div>
        </div>
    </section> <!--body content end-->

    <div class="clearfix"></div>


@stop