@extends('layout.client.container')
@section('content')
<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">{{$album->album_title}}</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">

                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('gallery') }}">Gallery</a></li>
                        <li class="active">{{$album->album_title}}</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">

        <div class="container">

            <div class="row">

                <?php
                foreach($images as $image) {
                if(file_exists("uploads/albums/" . $image->media_attachment) && $image->media_attachment != ''){ ?>


                <a href="{{$sitePath}}uploads/albums/{{$image->media_attachment}}" data-toggle="lightbox"
                   data-gallery="multiimages" data-title="{{$image->media_caption}}" class="col-md-3 col-sm-3 gal_img">
                    <img src="{{$sitePath}}uploads/albums/{{$image->media_attachment}}" class="img-responsive"> </a>

                <?php } }?>


                <div class="clear"></div>
                <!--inner content end-->
            </div>
        </div>
    </section>


@stop
