@extends('layout.client.container')
@section('content')
<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">Contact Us</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">

        <div class="container">

            <div class="row">

               

                <div class="col-xs-12 col-sm-7 col-md-7">
<div class="sub_title">Make Inquiry</div>
  <div class="row">
          <div class="<?php echo $class = Session::get('class');?>">
          <button class="close" data-dismiss="alert"></button>
          <?php echo $message = Session::get('message');?>
       
          </div>
          <form action="{{URL::route('contact')}}" name="inquiry_form" id="inquiry_form" method="post" >
            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="fullname" id="fullname" class="form-control"  placeholder="Full Name" value="{{ Input::old('fullname') }}">
                @if($errors->first('fullname'))
                <label for="fullname" style="color:red">{{$errors->first('fullname')}}</label>
                @endif
              </div>
            </div>
            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="address" id="address" class="form-control" placeholder="Address" value="{{  Input::old('address') }}">
                @if($errors->first('address'))
                <label for="address" style="color:red">{{$errors->first('address')}}</label>
                @endif
              </div>
            </div>
            
            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="phone" id="phone" class="form-control"  placeholder="Phone" value="{{ Input::old('phone') }}">
                @if($errors->first('phone'))
                <label for="phone" style="color:red">{{$errors->first('phone')}}</label>
                @endif
              </div>
            </div>

            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ Input::old('email') }}">
                @if($errors->first('email'))
                <label for="mobile" style="color:red">{{$errors->first('email')}}</label>
                @endif
              </div>
            </div>
            
            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="mobile" id="mobile" class="form-control"  placeholder="Mobile" value="{{ Input::old('mobile') }}">
                @if($errors->first('mobile'))
                <label for="mobile" style="color:red">{{$errors->first('mobile')}}</label>
                @endif
              </div>
            </div>

            <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject" value="{{ Input::old('subject') }}">
                @if($errors->first('mobile'))
                <label for="subject" style="color:red">{{$errors->first('subject')}}</label>
                @endif
              </div>
            </div>
             <div class="col-md-6 col-sm-6 ">
              <div class="form-group">
              <input type="text" class="form-control" name="captcha" placeholder="Captcha" />
              </div>
            </div>
       
             <div class="col-md-3 col-sm-3 ">
              <div class="form-group ">
              <div id="captcha-image" class="refereshrecapcha"><img src="<?php echo $captcha; ?>" /></div>
              </div>
             </div>
            
            <div class="col-md-3 col-sm-3 ">
              <a href="javascript:void(0)"  onclick="refreshCaptcha()">Reload Captcha</a> 
            </div> 
            <div class="clear"></div>
          
            <div class="col-md-12 col-sm-12">
              <textarea name="inquiry" id="inquiry" class="form-control" rows="6" placeholder="Message">{{{Input::old('inquiry')}}}</textarea>
              <label for="inquiry" style="color:red">{{$errors->first('inquiry')}}</label>
            </div>
           
            <div class="col-md-12 col-sm-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          <br/><br/>
          </div>
</div> 
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="sub_title">Location</div>
                  
                            
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14126.320416436101!2d85.311529!3d27.730248!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x29a02394b0cdd313!2sKath+PABSON!5e0!3m2!1sen!2snp!4v1486289148038" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

            </div>
            <!--inner content end-->
        </div>

    </section>

@stop
