@extends('layout.client.container')
@section('content')

<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>{{$newsData->news_heading}}</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li><a href="{{ route('news-list') }}">News & Events</a> </li>
                            <li>{{$newsData->news_heading}}</li>
                        </ul>
                    </div>
                </div>  
            </div>
            
            


   <div class="content_wrapper" >

        <div class="container">

            <div class="row">
              
                

             
                <div class="col-md-12"><?php if(file_exists('uploads/news/'.$newsData->news_attachment) && $newsData->news_attachment != '') { ?><img src="<?php echo $sitePath.'uploads/news/'.$newsData->news_attachment ?>" class="detailnewsimg">
                
        <?php } ?>{{$newsData->news_description}}
                        
                    </div>

                </div>
                
            </div>
        </div>
    </div>
@stop
