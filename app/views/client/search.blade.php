@extends('layout.client.container')
@section('content')

<!--Page Header-->
<section class="page_header ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Search Result</h1>
       
        <div class="page_nav">
      <span>You are here:</span> <a href="{{ route('home') }}">Home</a> <span><i class="fa fa-angle-double-right"></i>Search Result</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


  <section class="padding">

    <div class="container">

      <div class="row">
        
        

        <div class="col-xs-12">

          <script>
  (function() {
    var cx = '011562395463897233843:zi_4e3zsx3k';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

        </div>

        <div class="clear"></div>

      </div>
    </div>
  </section>

@stop
