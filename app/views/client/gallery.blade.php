@extends('layout.client.container-collection')
@section('content')

<div class="container">
		<div class="line"></div>
	</div>

	<!-- BREADCRUMBS -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="bcrumbs">
					<li><a href="<?php echo $sitePath;?>home">Home</a></li>
					<li>Gallery</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- BREADCRUMBS -->
	<div class="margin-top20"></div>
	<!-- PORTFOLIO -->
	<div class="container">
		<div class="col-md-12">
			<div class="portfolio-wrap">
				<div class="row">
					
					<ul id="portfolio">
					<?php foreach($galleries as $gallery) { 
						if(file_exists('uploads/gallery/thumbs/'.$gallery->gallery_attachment) && $gallery->gallery_attachment != '') {
					?>
						<li class="items col-md-3 webdesign">
							<div class="folio-wrap">
								<div class="folio-img">
									<?php /*<img src="<?php echo $sitePath.'uploads/gallery/'.$gallery->gallery_attachment?>" alt="" class="img-responsive">
									<div class="overlay">
										<div class="links">
											<a href="#"><i class="fa fa-expand"></i></a>
										</div>
									</div> */ ?>
									<a class="fancybox" href="<?php echo $sitePath.'uploads/gallery/thumbs/'.$gallery->gallery_attachment?>" data-fancybox-group="gallery" title="<?php echo $gallery->gallery_title ?>"><img src="<?php echo $sitePath.'uploads/gallery/thumbs/'.$gallery->gallery_attachment?>" class="img-responsive" alt="<?php echo $gallery->gallery_title ?>" /></a>
								</div>
								<div class="folio-info">
									<p><?php //echo $gallery->gallery_title ?></p>
								</div>
							</div>
						</li>
						<?php } } ?>
					</ul>
				</div>
			</div>
			<?php echo $galleries->links();?>
		</div>
	</div>
	<!-- PORTFOLIO -->

	<div class="space40"></div>
	<div class="clearfix"></div>
	<div class="space40"></div>
	
@stop