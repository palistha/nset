@extends('layout.client.container')
@section('content')

    <section class="inner_body_content">

        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">

                        <li><a href="{{ route('home') }}">Home</a></li>

                        <li class="active">Download</li>
                    </ol>
                </div>
                <div class="col-md-12 title">Download</div>


                <div class="col-md-12 col-sm-12">


                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th data-field="id">S.No</th>
                            <th data-field="Category">Category</th>
                            <th data-field="name">Description</th>
                            <th data-field="price"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($subcategories) > 0) {
                        $i = 1;
                        foreach($subcategories as $key=>$category){
                        ?>
                        <tr>
                            <td> {{ $i; }} </td>
                            <td>{{ $category->category_heading  }}</td>
                            <td>{{ $category->category_short_description }}</td>
                            <td>
                                <a href="{{ route('download',array($category->category_url,$category->id)) }}">View</a>
                            </td>
                        </tr>
                        <?php
                        $i++;  }  }else { ?>
                        <tr>
                            <td colspan="4" style="text-align: center;"> No downloads found.</td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!--inner content end-->
            </div>
        </div>
    </section>

@stop
