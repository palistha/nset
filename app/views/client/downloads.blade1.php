@extends('layout.client.container')
@section('content')
<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">Download</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">

                        <li><a href="{{ route('home') }}">Home</a></li>

                        <li class="active">Download</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">

        <div class="container">

            <div class="row">
                


                <div class="col-md-12 col-sm-12">


                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th data-field="id">S. No.</th>
                            <th data-field="name">File Name</th>
                            <th data-field="price">Added at</th>
                            <th>Download</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(count($downloads)>0)

                            @foreach($downloads as $key=>$download)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="{{$sitePath}}uploads/downloads/{{$download->download_attachment}}"
                                           target="_blank" class="ln-tr"> {{$download->download_caption}} </td>
                                    <td>{{date('d F, Y', strtotime($download->updated_at))}}</td>
                                    <td><a href="{{$sitePath}}uploads/downloads/{{$download->download_attachment}}"
                                           target="_blank">
                                            <img src="{{ asset('images/download.png') }}" class="download_img">
                                        </a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No download found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!--inner content end-->
            </div>
        </div>
    </section>

@stop
