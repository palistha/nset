@extends('layout.client.container')
@section('content')
    <script type="text/javascript" src="{{ asset('includes/client/js/parsley.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#writetous').parsley().on('field:validated', function () {
            })
        });
    </script>
    <section class="inner_content_wrap pd">

        <div class="inner_title_wrap no_banner">
            <div class="container">
                <div class="inner_title">Application Request</div>
            </div>
        </div>


        <div class="container">

            <div class="row">
                <div class="col l12 m12 s12">
                    <div class="inner_content">


                        <div class="row">

                            <div class="col l2 m2"> &nbsp;</div>
                            <div class="col l8 m8">


                                <form class="form_wrap" method="post" id="writetous" data-parsley-validate="">


                                    <?php if(Session::get('class')) { ?>


                                    <p><?php echo Session::get('message'); ?> </p>
                                    <br>
                                    <?php } ?>
                                    
                                    <div class="row">

<div class="col l6 m6 s12">
                                    <div class="input-field ">
                                        <input id="first_name" name="full_name" class="default_field" type="text"
                                               class="validate" required value="{{  Input::old('full_name') }}"
                                               data-parsley-required-message="Please enter Full name.">
                                        <label for="full_name">Full Name</label>
                                    </div>
                                    </div>
                                    
                                    <div class="col l6 m6 s12">
                                    <div class="input-field ">
                                        <input id="email" type="email" class="default_field validate"
                                               value="{{ Input::old('email') }}" name="email" required
                                               data-parsley-required-message="Please enter email address.">
                                        <label for="email" data-parsley-trigger="change"
                                               data-success="">Email</label>
                                    </div>
                                    </div>

<div class="col l6 m6 s12">
                                    <div class="input-field ">
                                        <input id="contact_no" class="default_field" name="contact_no" type="text"
                                               class="validate" required value="{{ Input::old('contact_no') }}"
                                               data-parsley-required-message="Please enter Contact no."
                                               data-parsley-minlength="7"
                                               data-parsley-minlength-message="Contact no. should be more than 7 digits"
                                               data-parsley-type="digits"
                                               data-parsley-type-message="Please Enter only Number."
                                        >
                                        <label for="contact_no">Contact No</label>
                                    </div>
                                    </div>
                                    <div class="col l6 m6 s12">
                                    <div class="input-field ">
                                        <input id="previous_college"  value="{{ Input::old('previous_college') }}"
                                               name="previous_college" type="text"
                                               class="validate default_field">
                                        <label for="Previous college">Previous college</label>
                                    </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="input-field col s6">

                                        <select name="choose" class="browser-default"required>
                                            <option value="" selected>Faculty option</option>
                                            <option value="BIM">BIM</option>
                                            <option value="CSIT">CSIT</option>
                                            <option value="BSW">BSW</option>
                                        </select>

                                    </div>
                                    <div class="clear"></div>
<div class="col l12 m12 s12">
                                    <div class="input-field ">
                                        <textarea id="known_from" class="materialize-textarea default_field"
                                                  name="known_from" required
                                                  data-parsley-required-message="Please write Known From.">{{ Input::old('known_from') }}</textarea>
                                        <label for="known_from">Known From</label>
                                    </div>
                                    </div>

                                    <div class="clear"></div>


                                    <div class="col l4 m4 s6">
                                        <div class="input-field  ">
                                            <img src="<?php echo $captcha; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col l4 m4 s6">
                                        <div class="input-field  ">
                                            <input id="" type="text" required name="captcha" class="default_field validate"
                                                   data-parsley-required-message="Please enter captcha">
                                            <label for="">captcha</label>
                                        </div>
                                    </div>


                                    <div class="clear"></div>

                                    <div class="input-field col s6">
                                        <button type="submit" class="waves-effect waves-light btn">Submit</button>
                                    </div>
                                    <br/>
                                    
                                    </div>

                                </form>
                            </div>
                            <div class="col l2 m2"> &nbsp;</div>
                        </div> <!--row end-->


                    </div>
                </div> <!--col end-->
            </div>

        </div>
    </section>
@stop