@extends('layout.client.container')
@section('content')
    <section class="inner_body_content">

        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('news-list') }}">News & Events</a></li>
                        <li class="active">{{$newsData->news_heading}}</li>
                    </ol>
                </div>
                <div class="col-md-12 inner-title">{{$newsData->news_heading}}</div>
                <div class="col-md-12">
                    {{--<?php if(file_exists('uploads/news/' . $newsData->news_attachment) && $newsData->news_attachment != '') { ?>--}}
                    {{--<img src="{{$sitePath}}uploads/news/{{$newsData->news_attachment}}" class="event_detail_img"--}}
                         {{--alt="{{ $newsData->news_heading}}" style="max-height: 100%; max-width: 100%;"/>--}}
                    {{--<?php } ?>--}}

                    <div style="text-align: center;">
                        <img src="{{ asset('images/content.jpg') }}">
                    </div>

                    {{$newsData->news_description}}
                </div>
                <!--inner content end-->
            </div>
        </div>
    </section>
@stop
