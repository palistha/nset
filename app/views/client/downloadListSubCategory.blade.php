@extends('layout.client.container')
@section('content')


    <!--Page Header-->
<section class="page_header ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Download</h1>
       
        <div class="page_nav">
      <span>You are here:</span> <a href="{{ route('home') }}">Home</a> <span><i class="fa fa-angle-double-right"></i>Download</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->

    <section class="padding">

        <div class="container">

            <div class="row">
                


                <div class="col-md-12 col-sm-12">


                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th data-field="id" width="8%">S.No</th>
                            <th data-field="Category">Category</th>
                            <th data-field="name">Description</th>
                            <th data-field="price">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($subcategories) > 0) {
                        $i = 1;
                        foreach($subcategories as $key=>$category){
                        ?>
                        <tr>
                            <td> {{ $i; }} </td>
                            <td>{{ $category->category_heading  }}</td>
                            <td>{{ $category->category_short_description }}</td>
                            <td>
                                <a class="btn blue btn-primary"  href="{{ route('download',array($category->category_url,$category->id)) }}">View</a>
                            </td>
                        </tr>
                        <?php
                        $i++;  }  }else { ?>
                        <tr>
                            <td colspan="4" style="text-align: center;"> No downloads found.</td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
              
            </div>
        </div>
    </section>

@stop
