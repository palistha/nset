@extends('layout.client.container')
@section('content')


<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
  <div class="container">
    <div class="pagination-area">
      <h2>{{$subtestimonial->testimonial_auther}}</h2>
      <ul>
        <li><a href="{{ route('home') }}">Home</a> </li>
        <li><a href="{{ route('testimonial-list') }}">Testimonials</a> </li>
        <li>{{$subtestimonial->testimonial_auther}}</li>
      </ul>
    </div>
  </div>  
</div>


<div class="content_wrapper" >
  <div class="container">
    <div class="row">
     
      <div class="single-item">
        <div class="single-item-wrapper">
          <div class="profile-img-wrapper"> <a href="#" class="profile-img"><img class="profile-img-responsive img-circle" style=" width:92px; height: 92px; " src="{{ URL::to('uploads/subtestimonials/'.$subtestimonial->testimonial_attachment) }}" ></a> </div>
          <div class="tlp-tm-content-wrapper">
            <h3 class="item-title"><a href="{{ route('testimonial', $subtestimonial->testimonial_company)}}">{{$subtestimonial->testimonial_auther}}</a></h3>
            <span class="item-designation">{{$subtestimonial->testimonial_remark}}</span>
            <div class="item-content">  
              {{ $subtestimonial->testimonial_description }}  
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>









@stop
