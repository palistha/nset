@extends('layout.client.container')
@section('content')



<section class="inner_body_content">

<div class="container">

<div class="row">
<div class="col-md-12 col-sm-12 ">
        <ol class="breadcrumb">
          <li class="you">You are here: </li>
          <li><a href="{{$sitePath}}home">Home</a></li>
         
          <li class="active">FAQ</li>
        </ol>
      </div>

<div class="col-md-12 title">FAQ</div>

<div class="col-md-12 col-sm-offset-1">

              <ul class="accordion accordion-2">
                  
                  <?php
                  foreach($faqs as $faq) { ?>
                    <li>
                      <div class="title">
                          <h4 class="inline-block mb0">{{$faq->faq_heading}}</h4>
                      </div>
                      <div class="content">
                          <p>
                              {{$faq->faq_description}}
                          </p>
                      </div>
                    </li>
                  <?php } ?>
                  
              </ul>
              <!--end of accordion-->
          </div>

<!--inner content end-->
</div>
</div>
</section> <!--body content end-->

<div class="clearfix"></div>  


@stop