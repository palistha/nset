@extends('layout.client.container')
@section('content')


<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
  <div class="container">
    <div class="pagination-area">
      <h2>Contact Us</h2>
      <ul>
        <li><a href="{{ route('home') }}">Home</a> </li>
        <li>Contact Us</li>
      </ul>
    </div>
  </div>  
</div>

<div class="content_wrapper">
  <div class="container"> 
    <div class="row">
      <div class=" col-md-4 col-sm-4 col-xs-12">
        <h4 class="heading heading_space">Contact Details</h4>
        <div class="contact-us-info2">   
          <ul>
            <li><i class="fa fa-map-marker" aria-hidden="true"></i> Bahiri Gaun, Kirtipur-14,</li>
            <li><i class="fa fa-phone" aria-hidden="true"></i>  + 977-01-4331827 </li>
            <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@pushpasadan.edu.np">info@pushpasadan.edu.np</a></li>
          </ul>
        </div>  
      </div>  
      <div class="col-md-8 col-sm-8 contact-form2" >
        <h4 class="heading heading_space">Write to Us</h4>
        <div class="<?php echo $class = Session::get('class');?>">
          <button class="close" data-dismiss="alert"></button>
          <?php echo $message = Session::get('message');?>
          
        </div>
        <form action="{{URL::route('contact')}}" name="inquiry_form" id="inquiry_form" method="post" >
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="fullname" id="fullname" class="form-control"  placeholder="Full Name" value="{{ Input::old('fullname') }}">
                @if($errors->first('fullname'))
                <label for="fullname" style="color:red">{{$errors->first('fullname')}}</label>
                @endif
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="address" id="address" class="form-control" placeholder="Address" value="{{  Input::old('address') }}">
                @if($errors->first('address'))
                <label for="address" style="color:red">{{$errors->first('address')}}</label>
                @endif
              </div>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="phone" id="phone" class="form-control"  placeholder="Phone" value="{{ Input::old('phone') }}">
                @if($errors->first('phone'))
                <label for="phone" style="color:red">{{$errors->first('phone')}}</label>
                @endif
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ Input::old('email') }}">
                @if($errors->first('email'))
                <label for="mobile" style="color:red">{{$errors->first('email')}}</label>
                @endif
              </div>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="mobile" id="mobile" class="form-control"  placeholder="Mobile" value="{{ Input::old('mobile') }}">
                @if($errors->first('mobile'))
                <label for="mobile" style="color:red">{{$errors->first('mobile')}}</label>
                @endif
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject" value="{{ Input::old('subject') }}">
                @if($errors->first('mobile'))
                <label for="subject" style="color:red">{{$errors->first('subject')}}</label>
                @endif
              </div>
            </div>
            <div class="clear"></div>
            
            <div class="col-md-12 col-sm-12">
              <div class="form-group " style="margin-bottom:0px">
                <textarea name="inquiry" id="inquiry" class="textarea form-control" rows="4" placeholder="Message">{{{Input::old('inquiry')}}}</textarea>
                <label for="inquiry" style="color:red">{{$errors->first('inquiry')}}</label>
              </div>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-6">
              <div class="form-group">
                <input type="text" class="form-control" name="captcha" placeholder="Captcha" />
              </div>
            </div>
            
            <div class="col-md-5 col-sm-4 col-xs-6">
              <div class="form-group " style="margin-bottom:30px;">
                <div id="captcha-image" class="refereshrecapcha"><img src="<?php echo $captcha; ?>" /> &nbsp;  </div><a href="javascript:void(0)"  onclick="refreshCaptcha()" style="font-size:13px;">Reload Captcha</a> 
              
              </div>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-4">
              <button type="submit" class="default-big-btn" style="width:100%; padding:10px 0">Submit</button>
            </div>
            
            
            
            <div class="clear"></div>
            
            
            
            
          </div>
        </form>
        
      </div>
      
      <div class="clearfix"></div>
      <br />
      <div class="col-md-12">
        <h4 class="heading heading_space">Find Us On Map</h4>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14133.27969785605!2d85.2669068835449!3d27.676505262397093!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x997bcf70c7b0b2d9!2sPushpa+Sadan+Boarding+High+School!5e0!3m2!1sen!2snp!4v1511775474515" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div> 
  </div>
</div>







<script>
  function refreshCaptcha(){
    $.ajax({
      url: "{{URL::route('contact.refereshrecapcha')}}",
      type: 'get',
      dataType: 'html',        
      success: function(json) {
        $('.refereshrecapcha').html(json);
      },
      error: function(data) {
        alert('Try Again.');
      }
    });
  }
</script>


@stop
