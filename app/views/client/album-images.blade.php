@extends('layout.client.container')
@section('content')

<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>{{$album->album_title}}</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                             <li><a href="{{ route('gallery') }}">Gallery</a> </li>
                            <li>{{$album->album_title}}</li>
                        </ul>
                    </div>
                </div>  
            </div>





   <div class="content_wrapper" >

        <div class="container">

            <div id="projects" class="row cbp">

                <?php
                foreach($images as $image) {
                if(file_exists("uploads/albums/thumbs/" . $image->media_attachment) && $image->media_attachment != ''){ ?>


               

                <div class="cbp-item tour col-md-3 col-sm-3">
        <img src="{{$sitePath}}uploads/albums/thumbs/{{$image->media_attachment}}" alt="">
        <div class="overlay">
          <div class="centered text-center">
            <a href="{{$sitePath}}uploads/albums/{{$image->media_attachment}}" class="cbp-lightbox opens"> <i class="fa fa-search-plus"></i></a> 
          </div>
        </div>
      </div>
	  
	  <?php } }?>


                <div class="clear"></div>
               
            </div>
        </div>
    </div>


@stop
