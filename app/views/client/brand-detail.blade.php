@extends('layout.client.container')
@section('content')

<div class="container">
  <div class="line"></div>
</div>
<!-- BREADCRUMBS -->

<?php
if(file_exists('uploads/brands/'.$brand->attachment) && $brand->attachment != ''){ ?>
  <section class="page-title page-title-2 image-bg overlay parallax">
      <div class="background-image-holder">
          <img alt="Background Image" class="background-image" src="<?php echo $sitePath.'uploads/brands/'.$brand->attachment; ?>" />
      </div>
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <h2 class="uppercase mb8">{{$brand->name}}</h2>
                  <p class="lead mb0">{{$brand->desc}}</p>
              </div>
              <div class="col-md-6 text-right">
                  <ol class="breadcrumb breadcrumb-2">
                      <li><a href="<?php echo $sitePath;?>home">Home</a></li>
                    <li><a href="{{URL::route('brands')}}">Brands</a></li>
                    <li class="active">{{$brand->name}}</li>
                  </ol>
              </div>
          </div>
          <!--end of row-->
      </div>
      <!--end of container-->
  </section>
<?php
}
else{ ?>
  <section class="page-title page-title-4 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="uppercase mb0">{{$brand->name}}</h3>
            </div>
            <div class="col-md-6 text-right">
                <ol class="breadcrumb breadcrumb-2">
                    <li><a href="<?php echo $sitePath;?>home">Home</a></li>
                    <li><a href="{{URL::route('brands')}}">Brands</a></li>
                    <li>{{$brand->name}}</li>
                </ol>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
  </section>
<?php } ?>


<!-- BREADCRUMBS -->
@include('client/show-productlist')
@stop