<?php
if (isset($download)) {
$files =$sitePath."uploads/downloads/".$download->download_attachment;
header('Content-Description: File Transfer');
header("Content-Type:");
header('Content-Disposition: attachment; filename='.basename($files));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($files));
ob_clean();
flush();
readfile($files);
exit;
}
?>