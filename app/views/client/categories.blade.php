@extends('layout.client.container')
@section('content')

<div class="container">
  <div class="line"></div>
</div>
<!-- BREADCRUMBS -->
<section class="page-title page-title-4 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="uppercase mb0">Product Category</h3>
            </div>
            <div class="col-md-6 text-right">
                <ol class="breadcrumb breadcrumb-2">
                    <li><a href="<?php echo $sitePath;?>home">Home</a></li>
                    <li>Product Category</li>
                </ol>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<!-- BREADCRUMBS -->
<section class="projects">
  <div class="container">
      <div class="masonry-loader">
          <div class="col-sm-12 text-center">
              <div class="spinner"></div>
          </div>
      </div>
      <div class="row masonry masonryFlyIn">
          <?php
          foreach($categories as $category) {
          ?>
            <div class="col-sm-4 masonry-item project" data-filter="People">
              <div class="image-tile hover-tile text-center">
                <?php if(file_exists('uploads/categories/'.$category->thumb_attachment) && $category->thumb_attachment != ''){ ?>
                  <img  class="background-image" alt="<?php echo $category->category_heading;?>" src="<?php echo $sitePath.'uploads/categories/'.$category->thumb_attachment;?>" />

                <?php
                }
                else{ ?>
                  <img alt="<?php echo $category->category_heading;?>" src="uploads/noimage.jpg" />
                <?php } ?>  
                  <div class="hover-state">
                    <a href="{{URL::route('category', $category->category_url)}}">
                          <h3 class="uppercase mb8">{{$category->category_heading}}</h3>
                          <h6 class="uppercase">{{$category->category_short_description}}</h6>
                      </a>
                  </div>
              </div>
              <!--end of hover tile-->
          </div>
          <?php
          }
          ?>                        
      </div>
      <!--end of row-->
  </div>
  <!--end of container-->
</section>
@stop