@extends('layout.client.container')
@section('content')

<div class="container">
  <div class="line"></div>
</div>
<!-- BREADCRUMBS -->
<section class="page-title page-title-4 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="uppercase mb0">Service</h3>
            </div>
            <div class="col-md-6 text-right">
                <ol class="breadcrumb breadcrumb-2">
                    <li><a href="<?php echo $sitePath;?>home">Home</a></li>
                    <li>Service</li>
                </ol>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<!-- BREADCRUMBS -->
	
<!--Services -->
<?php
$count = 1;
foreach($services as $service) {?>
<section class="image-square <?php echo ($count%2)==1?'left':'right'; ?>">
    <div class="col-md-6 image">
        <div class="background-image-holder">
          <?php
            if(file_exists('uploads/services/'.$service->attachment) && $service->attachment != ''){ ?>
              <img class="background-image" alt="<?php echo $service->title?>" src="<?php echo $sitePath.'uploads/services/'.$service->attachment; ?>" />
          <?php }
          else{
          ?>
              <img class="background-image" alt="Pic" src="<?php echo $sitePath; ?>uploads/noimage.jpg" />
          <?php } ?>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-1 content">
        <h3>{{$service->title}}</h3>
        <p class="mb0">
            {{$service->desc}}
        </p>
    </div>
</section>
<?php 
$count++;
} ?>	
@stop