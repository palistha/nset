@extends('layout.client.container')
@section('content')



<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>Downloads</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li>Downloads</li>
                        </ul>
                    </div>
                </div>  
            </div>
            
            


<div class="content_wrapper">
                <div class="container"> 


                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th data-field="id" width="8%">S.No</th>
                                <th data-field="Category">Category</th>
                                <th data-field="name">Description</th>
                                <th data-field="price">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $categories = CategoryModel::where('parent_id', 0)->where('is_active', 1)->get(); ?>

                            <?php
                            if(count($categories) > 0) {
                            $i = 1;
                            foreach($categories as $category){
                            $countSubCategory = CategoryModel::where('parent_id', $category->id)->where('is_active', 1)->get();
                            ?>
                            <tr>
                                <td> {{ $i; }} </td>
                                <td>{{ $category->category_heading  }}</td>
                                <td>{{ $category->category_short_description }}</td>
                                <td>
                                    <?php  if(count($countSubCategory) > 0) { ?>
                                    <a class="btn blue btn-primary"  href="{{ route('sub-downloads',array($category->category_url,$category->id)) }}">View</a>
                                    <?php  } else { ?>
                                    <a class="btn blue btn-primary"  href="{{ route('download',array($category->category_url,$category->id)) }}">View</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;  }  }else { ?>
                            <tr>
                                <td colspan="4" style="text-align: center;"> No downloads found.</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                </div>
                
            </div>


@stop
