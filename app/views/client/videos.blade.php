@extends('layout.client.container')
@section('content')

    <section class="inner_content_wrap pd">

        <div class="inner_title_wrap" style="position: relative; margin-bottom: 20px;">
            <div class="container">
                <div class="inner_title">Videos</div>
            </div>
        </div>


        <div class="container">

            <div class="row">
                <div class="col l12 m12 s12">

                    <?php $latestvideo = VideoModel::getAllVideoFrontend();
                    if(count($latestvideo) > 0) {
                        $i=1;
                    foreach($latestvideo as $video) { ?>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image">
                                <a  href="#modal{{$i}}" class="modal-trigger" id="{{$video->video_url}}" title="{{$video->video_title}}">
                                    <img src="http://img.youtube.com/vi/{{$video->video_url}}/hqdefault.jpg"></a>
                                <span class="card-title">{{$video->video_title}}</span>
                            </div>
                            <div class="card-content">
                                <p>{{$video->video_description}}</p>
                            </div>

                        </div>
                    </div>

                        <div id="modal{{$i}}" class="modal modal-fixed-footer">
                            <div class="modal-content">
                                <h5>{{$video->video_title}}</h5>
                                <iframe title="YouTube video player" width="100%" height="83%"
                                        src="http://www.youtube.com/embed/{{$video->video_url}}?wmode=transparent"
                                        frameborder="0" wmode="Opaque"></iframe>

                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
                            </div>
                        </div>

                    <?php $i++;}


                        echo $latestvideo->links();
                        }


                        else { ?>

                    No Videos found .
                    <?php } ?>

                </div> <!--col end-->
            </div>

        </div>
    </section>


@stop