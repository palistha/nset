@extends('layout.client.container')
@section('content')

<section class="inner_banner">
<img src="<?php echo 'includes/client/images/';?>inner_banner.jpg">

</section> <!--inner banner end-->


<section class="inner_body_content">

<div class="container">

<div class="row">

<div class="col-md-12 title">{{$pageTitle}}</div>

<div class="col-md-12">
   <?php
        if(count($achievements) > 0) {
          foreach($achievements as $achievement) {
//            echo $achievement->title; die();
            if(file_exists('uploads/achievements/'.$achievement->attachment) && $achievement->attachment != '')           {
            ?>
              <div class="col-md-3 col-xs-6">
                <div class="instructor">
                  <div class="avatar">
                    <img src="{{URL('uploads/achievements/'.$achievement->attachment)}}" class="img-responsive" alt="{{$achievement->title}}" />
                  </div><!-- End Avatar -->
                  <div class="instructor-info">
                      <p class="name">{{$achievement->title}}</p>
<!--                      <span class="position">{{$achievement->second_title}}</span>-->
                      
                  </div>
                </div>
              </div>
            <?php
            }
            
          }
        }
        else{
          echo "No Data Available";
        }
        ?>
      </div>
<!--inner content end-->
</div>
</div>
</section> <!--body content end-->

<div class="clearfix"></div>  

@stop