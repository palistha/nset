@extends('layout.client.container')
@section('content')

<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>Gallery</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li>Gallery</li>
                        </ul>
                    </div>
                </div>  
            </div>
            
            

    
    
    <div class="content_wrapper" >

        <div class="container">

            <div class="row">


           

                @foreach($albums as $album) 
<div class="col-md-4 col-sm-6 courses-box1 news_single " >
<div class="single-item-wrapper">
        <div class="courses-img-wrapper hvr-bounce-to-bottom">
          
                            <?php 
                            $image =  DB::table('media')->where('album_id',$album->id)->where('media_attachment','!=','NULL')->where('is_active','1')->where('deleted','0')->first();
                            
                            ?>

                      
                        <img src="{{$sitePath}}uploads/albums/thumbs/{{$album->album_attachment}}" alt=""
                             class="img-responsive"/>
                
                            
                             <a href="{{ route('album', $album->album_url)}}"><i class="fa fa-link" aria-hidden="true"></i></a> 
                        
        </div>
        <div class="courses-content-wrapper" style="background:#f2f2f2">
          <h3 class="item-title"><a href="{{ route('album', $album->album_url)}}">{{$album->album_title}}</a></h3>
           
         
        </div>
        </div>
      </div>
      
      
      
      @endforeach

            </div>
        </div>
    </div>

@stop