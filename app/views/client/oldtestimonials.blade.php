@extends('layout.client.container')
@section('content')

<div class="inner-head">
    <div class="container">
        <h1 class="entry-title">{{$pageTitle}}</h1>
        
        <div class="breadcrumb">
            <ul class="clearfix">
                <li class="ib"><a href="{{$sitePath}}home">Home</a></li>
                <li class="ib current-page"><a >{{$pageTitle}}</a></li>
            </ul>
        </div>
    </div><!-- End container -->
</div><!-- End Inner Page Head -->
<!-- BREADCRUMBS -->
<div class="clearfix"></div>	
<!--Testimonials -->

<section class="full-section latest-courses-section no-slider">
  <div class="section-content latest-courses-content listview fadeInDown-animation">
    <div class="container">
      <div class="row">
        <?php 
        foreach($testimonials as $testimonial) { ?>
        <div class="col-md-12">
          <div class="course clearfix">
            <div class="course-image">
                <?php
                if(file_exists('uploads/testimonials/'.$testimonial->testimonial_attachment) && $testimonial->testimonial_attachment != ''){ ?>
                  <img alt="<?php echo $testimonial->testimonial_auther?>" src="<?php echo $sitePath.'uploads/testimonials/'.$testimonial->testimonial_attachment; ?>" class="img-responsive" />
                <?php }
                else{
                ?>
                    <img alt="Pic" class="img-responsive" src="<?php echo $sitePath; ?>uploads/noimage.jpg" />
                <?php } ?>
            </div><!-- End Course Image -->

              <div class="course-info">
                  <h3 class="course-title"><a class="ln-tr">{{$testimonial->testimonial_auther}}</a></h3>
                  <div class="course-description">
                    {{$testimonial->testimonial_description}}
                    
                  </div>
                  <br />
                  {{$testimonial->testimonial_remark ? "( " . $testimonial->testimonial_remark. " )" : ""}}
                  
              </div>
          </div><!-- End Course -->
        </div><!-- End col-md-12 -->
        <?php } ?>  
      </div><!-- End row -->
    </div><!-- End Container -->
  </div><!-- End Latest-Courses Section Content -->
</section><!-- End Courses Section -->

<div class="clearfix"></div>
               

	
@stop