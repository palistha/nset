@extends('layout.client.container')
@section('content')

<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">Gallery</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">Gallery</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">

        <div class="container">

            <div class="row">


                <?php
                if(count($albums) > 0) {

                foreach($albums as $album) { ?>


                <div class="col-md-3 col-sm-3 gal_img">
                    <div class="gal-img-wrapper">
                     <a href="{{ route('album', $album->album_url)}}">

                        <?php $image = AlbumModel::getSingleImageByAlbumId($album->id);?>

                        <?php if(file_exists("uploads/albums/" . $image->media_attachment) && $image->media_attachment != '')
                        { ?>
                        <img src="{{$sitePath}}uploads/albums/{{$image->media_attachment}}" alt=""
                             class="img-responsive"/>
                        <?php } else { ?>
                        <img src="{{  asset('images/noimage.png') }}"
                             style="height: 205px; width:265px;"/>
                        <?php }  ?>


                    </a>
                    </div>
                    <div class="gal_cat_title">{{$album->album_title}}</div>
                </div>

                <?php } } else { ?>
                No Albums Found ...
                <?php } ?>

            </div>
        </div>
    </section>

@stop
