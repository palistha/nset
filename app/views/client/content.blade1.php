@extends('layout.client.container')
@section('content')
    <div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">{{$contentData->content_heading}}</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">{{$contentData->content_heading}}</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    
    <section class="inner_body_content">

        <div class="container">

            <div class="row">
                
                

                <div class="col-xs-12">

                    <p><?php if(file_exists('uploads/pages/' . $contentData->content_attachment) && $contentData->content_attachment != '') { ?>
                        <img src="{{$sitePath}}uploads/pages/{{$contentData->content_attachment}}" class="detailnewsimg" 
                             alt="{{ $contentData->content_heading}}"/>
                        <?php }  ?>
                    <p>

                    <?php
                    $listLev2Header = ContentModel::headerContentMenus($contentData->id);

                    if(count($listLev2Header) > 0) {

                        echo Helper::GetSubContent($contentData->content_url, $contentData->id, "1");

                    } else {  ?>
                    {{ $contentData->content_description}}
                    <?php } ?>

                </div>

                <div class="clear"></div>
            </div>
        </div>
    </section>

@stop
