@extends('layout.client.container')
@section('content')
<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">Download</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">

                        <li><a href="{{ route('home') }}">Home</a></li>

                        <li class="active">Download</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>

    <section class="inner_body_content">

        <div class="container">

            <div class="row">
       


                <div class="col-md-12 col-sm-12">


                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th data-field="id" width="5%">S.No</th>
                                <th data-field="Category">Category</th>
                                <th data-field="name">Description</th>
                                <th data-field="price"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $categories = CategoryModel::where('parent_id', 0)->where('is_active', 1)->get(); ?>

                            <?php
                            if(count($categories) > 0) {
                            $i = 1;
                            foreach($categories as $category){
                            $countSubCategory = CategoryModel::where('parent_id', $category->id)->where('is_active', 1)->get();
                            ?>
                            <tr>
                                <td> {{ $i; }} </td>
                                <td>{{ $category->category_heading  }}</td>
                                <td>{{ $category->category_short_description }}</td>
                                <td>
                                    <?php  if(count($countSubCategory) > 0) { ?>
                                    <a href="{{ route('sub-downloads',array($category->category_url,$category->id)) }}">View</a>
                                    <?php  } else { ?>
                                    <a href="{{ route('download',array($category->category_url,$category->id)) }}">View</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;  }  }else { ?>
                            <tr>
                                <td colspan="4" style="text-align: center;"> No downloads found.</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                </div>
                <!--inner content end-->
            </div>
        </div>
    </section>

@stop
