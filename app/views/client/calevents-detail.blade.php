@extends('layout.client.container')
@section('content')

<div class="clearfix"></div>
<section class="inner_body_content">

<div class="container">

<div class="row">
<div class="col-md-12 col-sm-12 ">
        <ol class="breadcrumb">
          <li class="you">You are here: </li>
          <li><a href="{{URL::route('home')}}">Home</a></li>
           <li><a href="{{URL::route('calendar-events-list')}}">Events</a></li>
          <li class="active">{{$eventsData->events_heading}}</li>
        </ol>
      </div>

<div class="col-md-12 title">{{$eventsData->events_heading}}</div>
Start Date:      <div class="date ib">
                  <span class="icon"><i class="fa fa-clock-o"></i></span>
                  <span class="text">{{ date('M d, Y', strtotime($eventsData->events_sdate))}}</span>
              </div>

 
<div class="col-md-12">
<?php if(file_exists('uploads/events/'.$eventsData->events_attachment) && $eventsData->events_attachment != '') { ?> <img src="<?php echo $sitePath.'uploads/events/'.$eventsData->events_attachment ?>" class="event_detail_img" style="width:50% !important" alt="{{ $eventsData->events_heading}}"/>   <?php } ?> 
            {{$eventsData->events_description}}
          </div>
<!--inner content end-->
</div>
</div>
</section> <!--body content end-->

<div class="clearfix"></div>
<div class="clearfix"></div>
@stop