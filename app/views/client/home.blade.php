@extends('layout.client.container')
@section('content')

<div class="slider1-area overlay-default">
  <div class="bend niceties preview-1">
    <?php
    $banners = BannerModel::getHomePageBanners('2');
   
    $count = 1;
    if(count($banners) > 0) { ?>
    <div id="ensign-nivoslider-3" class="slides"> 
      <?php 
      $i = 0;
      foreach($banners as $banner) {
        $i++;
        if(file_exists('uploads/banners/' . $banner->banner_attachment) && $banner->banner_attachment != ''){
          ?>
          
          <img src="{{ asset('uploads/banners/' . $banner->banner_attachment) }}"  title="#slider-direction-{{$i}}"/> 
          <?php  } } ?>



        </div>
        <?php 
        $i = 0;
        foreach($banners as $banner) {
         $i++;
         if(file_exists('uploads/banners/' . $banner->banner_attachment) && $banner->banner_attachment != ''){
          ?>
          <div id="slider-direction-{{$i}}" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-{{$i}}">
              <div class="title-container s-tb-c">
                <h1 class="title1">{{$banner->banner_caption}}</h1>

                <div class="slider-btn-area"> <a href="{{$banner->banner_url}}" class="default-big-btn">Read More</a> </div>
              </div>
            </div>
          </div>
          <?php } } } ?>
        </div>
      </div>



      <div class="courses1-area">
        <div class="container">
          <h2 class="title-default-left">News & Events</h2>
        </div>
        <div class="container">
          <div class="rc-carousel"
          data-loop="true"
          data-items="3"
          data-margin="30"
          data-autoplay="true"
          data-autoplay-timeout="10000"
          data-smart-speed="2000"
          data-dots="false"
          data-nav="true"
          data-nav-speed="false"
          data-r-x-small="1"
          data-r-x-small-nav="true"
          data-r-x-small-dots="false"
          data-r-x-medium="2"
          data-r-x-medium-nav="true"
          data-r-x-medium-dots="false"
          data-r-small="2"
          data-r-small-nav="true"
          data-r-small-dots="false"
          data-r-medium="3"
          data-r-medium-nav="true"
          data-r-medium-dots="false"
          data-r-large="3"
          data-r-large-nav="true"
          data-r-large-dots="false">


          <?php $eventLists = NewsModel::getAllActiveEventsForHome();

          if(count($eventLists) > 0){
            foreach($eventLists as $singleEvent){
              ?>         

              <div class="courses-box1">
                <div class="single-item-wrapper">
                  <div class="courses-img-wrapper hvr-bounce-to-bottom">
                    <?php if(file_exists('uploads/news/' . $singleEvent->news_attachment) && $singleEvent->news_attachment != '') { ?>
                    <img src="{{$sitePath}}uploads/news/thumbs/{{$singleEvent->news_attachment}}"
                    alt="{{ $singleEvent->news_heading}}"/>
                    <?php }
                    else{?>
                    <img alt="{{ $singleEvent->news_heading}}"
                    src="{{ asset('includes/client/img/noimage.jpg') }}"
                    />
                    <?php } ?>

                    <a href="{{ route('news', $singleEvent->news_url)}}"><i class="fa fa-link" aria-hidden="true"></i></a> </div>
                    <div class="courses-content-wrapper">
                      <h3 class="item-title"><a href="{{ route('news', $singleEvent->news_url)}}">{{$singleEvent->news_heading}}</a></h3>
                      <p class="item-content">{{$singleEvent->news_short_description}}</p>
                    </div>
                  </div>
                </div>

                <?php } } ?>






              </div>
            </div>
          </div>



          <div class="lecturers-area">
           <div class="container">
            <h2 class="title-default-left">Management Team</h2>
          </div>
          <div class="container">
            <div class="rc-carousel"
            data-loop="true"
            data-items="4"
            data-margin="30"
            data-autoplay="true"
            data-autoplay-timeout="10000"
            data-smart-speed="2000"
            data-dots="false"
            data-nav="true"
            data-nav-speed="false"
            data-r-x-small="1"
            data-r-x-small-nav="true"
            data-r-x-small-dots="false"
            data-r-x-medium="2"
            data-r-x-medium-nav="true"
            data-r-x-medium-dots="false"
            data-r-small="3"
            data-r-small-nav="true"
            data-r-small-dots="false"
            data-r-medium="4"
            data-r-medium-nav="true"
            data-r-medium-dots="false"
            data-r-large="4"
            data-r-large-nav="true"
            data-r-large-dots="false">
            <?php
            $managementteamList  = ManagementTeamModel::getAllManagementTeam();
            ?>
            @foreach( $managementteamList as $managementteam)
            <div class="single-item">
              <div class="lecturers1-item-wrapper">
              <div class="lecturers-img-wrapper"> 
              <a href="{{ route('home')}}">
   
              <img class="img-responsive"   " src="{{ URL::to('uploads/managementteams/thumbs/'.$managementteam->images) }}" ></a> </div>
               <div class="lecturers-content-wrapper">
                <h3 class="item-title"><a href="{{ route('home')}}">{{$managementteam->title}}</a></h3>
                <span class="item-designation">{{$managementteam->position}}</span> 
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>




    <div class="news-event-area">
      <div class="container">
        <h2 class="title-default-left">Photo Gallery</h2>
      </div>
      <div class="container">
        <div class="rc-carousel"
        data-loop="true"
        data-items="3"
        data-margin="30"
        data-autoplay="true"
        data-autoplay-timeout="10000"
        data-smart-speed="2000"
        data-dots="false"
        data-nav="true"
        data-nav-speed="false"
        data-r-x-small="1"
        data-r-x-small-nav="true"
        data-r-x-small-dots="false"
        data-r-x-medium="2"
        data-r-x-medium-nav="true"
        data-r-x-medium-dots="false"
        data-r-small="2"
        data-r-small-nav="true"
        data-r-small-dots="false"
        data-r-medium="3"
        data-r-medium-nav="true"
        data-r-medium-dots="false"
        data-r-large="3"
        data-r-large-nav="true"
        data-r-large-dots="false">


        <?php
        $AlbumList = AlbumModel::getHomePageAlbums();
        if(count($AlbumList) > 0) {
          foreach($AlbumList as $album) { ?>

          <div class="courses-box1">
            <div class="single-item-wrapper">
              <div class="courses-img-wrapper hvr-bounce-to-bottom"> 
               <?php  if(file_exists("uploads/albums/thumbs/" . $album->media_attachment) && $album->media_attachment != ''){ ?>
               <img class="img-responsive border_radius" src="{{$sitePath}}uploads/albums/thumbs/{{$album->media_attachment}}"
               alt="{{$album->album_title}}"/>
               <?php } else { ?>
               <img class="img-responsive border_radius" src="{{ asset('includes/client/img/noimage_gal.jpg') }}" 
               alt="{{$album->album_title}}"/>
               <?php }  ?>

               <a href="{{URL::route('album', $album->album_url)}}"><i class="fa fa-link" aria-hidden="true"></i></a> </div>
               <div class="courses-content-wrapper">
                <h3 class="item-title"><a href="{{URL::route('album', $album->album_url)}}">{{$album->album_title}}</a></h3>
              </div>
            </div>
          </div>

          <?php } } ?>

        </div>
      </div>
    </div>



    <div class="students-say-area">
      <h2 class="title-default-center">What Our Students Say</h2>
      <div class="container">
        <div class="rc-carousel"
        data-loop="true"
        data-items="2"
        data-margin="30"
        data-autoplay="true"
        data-autoplay-timeout="10000"
        data-smart-speed="2000"
        data-dots="true"
        data-nav="false"
        data-nav-speed="false"
        data-r-x-small="1"
        data-r-x-small-nav="false"
        data-r-x-small-dots="true"
        data-r-x-medium="2"
        data-r-x-medium-nav="false"
        data-r-x-medium-dots="true"
        data-r-small="2"
        data-r-small-nav="false"
        data-r-small-dots="true"
        data-r-medium="2"
        data-r-medium-nav="false"
        data-r-medium-dots="true"
        data-r-large="2"
        data-r-large-nav="false"
        data-r-large-dots="true">

        <?php
        $subtestimonialList = SubTestimonialModel::getAllSubTestimonial();
        ?>
        @foreach( $subtestimonialList as $subtestimonial )          

        <div class="single-item">
          <div class="single-item-wrapper">
            <div class="profile-img-wrapper"> <a href="{{ route('testimonial', $subtestimonial->testimonial_company)}}" class="profile-img"><img class="profile-img-responsive img-circle" style=" width:92px; height: 92px; " src="{{ URL::to('uploads/subtestimonials/'.$subtestimonial->testimonial_attachment) }}" ></a> </div>
            <div class="tlp-tm-content-wrapper">
              <h3 class="item-title"><a href="{{ route('testimonial', $subtestimonial->testimonial_company)}}">{{$subtestimonial->testimonial_auther}}</a></h3>
              <span class="item-designation">{{$subtestimonial->testimonial_remark}}</span>
              <div class="item-content">  
                  {{ strip_tags(str_limit($subtestimonial->testimonial_description, 200, '...')) }}  
               
             </div>
           </div>
         </div>
       </div>


       @endforeach

     </div>
   </div>
 </div>








 @endsection