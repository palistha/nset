@extends('layout.client.container')
@section('content')

  <section class="inner_body_content">

    <div class="container">

      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Search</li>
          </ol>
        </div>
        <div class="col-md-12">
          <div class="inner-title">Search</div>
        </div>

        <div class="col-xs-12">

          <script>
  (function() {
    var cx = '011562395463897233843:zi_4e3zsx3k';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

        </div>

        <div class="clear"></div>

      </div>
    </div>
  </section>

@stop
