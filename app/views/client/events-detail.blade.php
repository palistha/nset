 @extends('layout.client.container')
@section('content')

<style type="text/css">
  .my-event{
    margin-top: 40px;

  }
  .my-icon{
    color: #acacac;
    margin-bottom: 15px;
  }
</style>
<div class="clearfix"></div>
<!--inner banner end-->


<section class="inner_body_content">

<div class="container">

<div class="row">

<div class="col-md-12 title">{{$newsData->news_heading}}</div>

<div class="col-md-12">
        <?php if(file_exists('uploads/news/thumbs/'.$newsData->news_attachment) && $newsData->news_attachment != '') { ?>
          <div class="flexslider portfolio-slider featured-image my-event">
            <ul class="slides">
              <li class="slide-item">
                <div class="image">
                  <img src="<?php echo $sitePath.'uploads/news/'.$newsData->news_attachment ?>" class="img-responsive" alt="{{ $newsData->news_heading}}"/>                
                </div>
              </li>

          </ul>
          </div>
        <?php } ?>

        <div class="entry clearfix">
          <!-- <h3 class="single-title fl">
             
              {{ $newsData->news_heading}}
          </h3> --><!-- End Title -->
          <div class="meta fr">
              <div class="date ib my-icon">
                  <span class="icon"><i class="fa fa-clock-o"></i></span>
                  <span class="text">{{ date('M d, Y', strtotime($newsData->news_date))}}</span>
              </div>

              <!-- date icon -->
<!--              <div class="author ib">
                  <span class="icon"><i class="fa fa-user"></i></span>
                  <span class="text">By : <a class="ln-tr" href="#">Begha</a></span>
              </div> author icon -->
          </div><!-- End Meta -->
         <!--  <div class="clearfix"></div> -->
          <div class="content">
            {{$newsData->news_description}}
          </div>
        </div>


      </div>
<!--inner content end-->
</div>
</div>
</section> <!--body content end-->

<div class="clearfix"></div>
<div class="clearfix"></div>
@stop