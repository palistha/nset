@extends('layout.client.container')
@section('content')

<div class="container">
		<div class="line"></div>
	</div>

	<!-- BREADCRUMBS -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="bcrumbs">
					<li><a href="<?php echo $sitePath;?>home">Home</a></li>
					<li>Success</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- BREADCRUMBS -->
	<!-- PORTFOLIO - SINGLE -->
	<div class="portfolio-single">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4>Success</h4>
					<p>Your order has been placed successfully.<br >
					Your order detail has been emailed to us.<br >
					Also a copy of the detail of your order has been emailed to your email address. Please check.</p>
					Regards,<br />
					<?php echo ConstantModel::getDetailByName('site_name')->constant_value;?><br />
					Customer Service<br />
					<div class="clearfix"></div>					
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="space50"></div>
	
@stop