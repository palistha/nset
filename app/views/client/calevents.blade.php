@extends('layout.client.container')
@section('content')
<section class="inner_body_content">

<div class="container">

<div class="row">

<div class="col-md-12 col-sm-12 ">
        <ol class="breadcrumb">
          <li class="you">You are here: </li>
          <li><a href="{{$sitePath}}home">Home</a></li>
         
          <li class="active">Events Calendar</li>
        </ol>
      </div>

<div class="col-md-12 title">Events Calendar</div>

<div class="col-md-12"><div class="row events">
 
  <?php
      if(count($eventsList) > 0) {
        foreach($eventsList as $events) { ?>
<div class="col-md-4 col-sm-4">
<div class="event_title"><a href="{{URL::route('calendar-events', $events->events_url)}}">                    <span class="post-type-icon"><i class="fa fa-comment"></i></span>
{{$events->events_heading}}</a></div>
 <div class="date ib">
                      <span class="icon"><i class="fa fa-clock-o"></i></span>
                      <span class="text">{{ date('M d, Y', strtotime($events->events_sdate))}}</span>
                  </div>
<div class="event_img"><a href="{{URL::route('calendar-events', $events->events_url)}}">
 <?php if(file_exists('uploads/events/'.$events->events_attachment) && $events->events_attachment != '') { ?>
  <img src="{{$sitePath}}uploads/events/{{$events->events_attachment}}"  alt="{{ $events->events_heading}}"/>
                <?php }
                else{?> 
                  <img alt="{{ $events->events_heading}}" class="img-responsive" src="{{$sitePath}}uploads/noimage.jpg" />
                <?php } ?>
</a></div>
<div class="event_brief"> {{$events->events_short_description}}</div>

</div>
        

          <?php }
      }
      else{
        echo "No events available";
      }
      ?>
      </div></div>
<!--inner content end-->
</div>
</div>
</section>

<div class="clearfix"></div>  

	
@stop