@extends('layout.client.container')
@section('content')

<!-- ERROR INFO -->
	<section class="error-wrap">
		<div class="container">
			<div class="col-md-12">
				<div class="e-inner">
					<h4>Error 404</h4>
					<h5>Oops, This Page was lost in time</h5>
					<p>the page, post or content in general you are looking for no longer exists. perhaps you can return back to the site's 
					<a href="{{ $sitePath }}">homepage</a> and see if you can find what you are looking for. 
						<br>
						<br>
						Or, you can try finding it with the information below with another search:
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- ERROR INFO -->
	
@stop