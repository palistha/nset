@extends('layout.client.container')
@section('content')

<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2>News & Events</h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li>News & Events</li>
                        </ul>
                    </div>
                </div>  
            </div>
            
            



    <div class="content_wrapper" >
        <div class="container">
            <div class="row">
           

                <?php
                if(count($newsList) > 0) {
                foreach($newsList as $news) { ?>


<div class="col-md-4 col-sm-6 courses-box1 news_single " >
<div class="single-item-wrapper">
        <div class="courses-img-wrapper hvr-bounce-to-bottom">
          
                            <?php if(file_exists('uploads/news/thumbs/' . $news->news_attachment) && $news->news_attachment != '') { ?>
                            <img src="{{$sitePath}}uploads/news/thumbs/{{$news->news_attachment}}"
                                />
                            <?php }
                            else{?>
                            <img src="{{ asset('includes/client/img/noimage.jpg') }}" />
                            <?php } ?>
                            
                             <a href="{{route('news', $news->news_url)}}"><i class="fa fa-link" aria-hidden="true"></i></a> 
                        
        </div>
        <div class="courses-content-wrapper" style="background:#f2f2f2">
          <h3 class="item-title"><a href="{{route('news', $news->news_url)}}">{{ $news->news_heading}}</a></h3>
          
          <p class="item-content">{{ (strlen(strip_tags($news->news_description)) > 70) ? substr(strip_tags($news->news_description),0,70).'...' : strip_tags($news->news_description)}}</p>
          
          
          
          
         
        </div>
        </div>
      </div>



                
                
                
                
                

                <?php }
                echo '<div class="clearfix"></div><br>' . $newsList->links();
                }else { ?>
                No News Found.
                <?php } ?>


            </div>
        </div>
    </div>

@stop
