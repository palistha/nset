@extends('layout.client.container')
@section('content')


<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
  <div class="container">
    <div class="pagination-area">
      <h2>Testimonial</h2>
      <ul>
        <li><a href="{{ route('home') }}">Home</a> 
        </li>
        <li>Testimonial</li>
      </ul>
    </div>
  </div>  
</div>


<div class="content_wrapper" >
  <div class="container">
    <div class="row">


      <div class="col-sm-12">
        <div class="about_featured">
          <div class="panel-group" id="accordion">
            <?php $firstItem = true; ?>
            @foreach($subtestimonials as $special)
            <div class="panel panel-default wow fadInLeft">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a aria-expanded="<?php echo ($firstItem ? 'true' : 'false');?>" data-toggle="collapse" data-parent="#accordion" href="#{{$special->id}}">
                    <span class=""></span>{{ $special->testimonial_auther}}
                  </a>
                </h4>
              </div>
              <div id="{{$special->id}}" class="panel-collapse collapse<?php echo ($firstItem ? ' in' : '');?>">
                <div class="panel-body">
                 {{ $special->testimonial_description}}
               </div>
             </div>
           </div>
           <?php $firstItem = false; ?>
           @endforeach()
         </div>
       </div>
     </div>

   </div>
 </div>
</div>

</div>
</div>
</div>


@stop
