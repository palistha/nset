@extends('layout.client.container')
@section('content')
<div class="inner-page-banner-area" style="background-image: url('{{ asset('includes/client/img/banner/5.jpg') }}');">
                <div class="container">
                    <div class="pagination-area">
                        <h2><td>{{ $category->category_heading  }}</td></h2>
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a> </li>
                            <li><a href="{{ route('downloads') }}">Downloads</a></li>
                            <li>{{ $category->category_heading  }}</li>
                        </ul>
                    </div>
                </div>  
            </div>

<div class="content_wrapper">
                <div class="container"> 


                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th data-field="id" width="8%">S. No.</th>
                            <th data-field="name">File Name</th>
                            <th data-field="price">Added at</th>
                            <th>Download</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if(count($downloads)>0)

                            @foreach($downloads as $key=>$download)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="{{$sitePath}}uploads/downloads/{{$download->download_attachment}}"
                                           target="_blank" class="ln-tr"> {{$download->download_caption}} </td>
                                    <td>{{date('d F, Y', strtotime($download->updated_at))}}</td>
                                    <td><a class="btn blue btn-primary" href="{{$sitePath}}uploads/downloads/{{$download->download_attachment}}"
                                           target="_blank">
                                            Download
                                        </a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No download found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                
            </div>


@stop
