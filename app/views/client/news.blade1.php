@extends('layout.client.container')
@section('content')
<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">News & Events</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">News & Events</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">
        <div class="container">
            <div class="row">
           

                <?php
                if(count($newsList) > 0) {
                foreach($newsList as $news) { ?>


                <div class="col-md-4 col-sm-4 col-xs-6 innernews-fix">
                    <div class="event_img"><a href="{{route('news', $news->news_url)}}">
                            <?php if(file_exists('uploads/news/' . $news->news_attachment) && $news->news_attachment != '') { ?>
                            <img src="{{$sitePath}}uploads/news/{{$news->news_attachment}}"
                                />
                            <?php }
                            else{?>
                            <img src="{{ asset('images/noimage.png') }}" height="200" width="200"
                                 style="max-height: 200px; max-height: 200px;"/>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="event_title">
                       
                        <a href="{{route('news', $news->news_url)}}">{{ $news->news_heading}}</a></div>
                         <div class="date">- {{ date("M", strtotime($news->news_date)); }}
                            {{ date("D", strtotime($news->news_date)); }},
                            {{date("Y", strtotime($news->news_date));}}
                        </div>
                    <div class="clear"></div>
                    <div class="event_brief">{{ (strlen(strip_tags($news->news_description)) > 300) ? substr(strip_tags($news->news_description),0,300).'...' : strip_tags($news->news_description)}}</div>
                </div>

                <?php }
                echo '<div class="clear"></div>' . $newsList->links();
                }else { ?>
                No News Found.
                <?php } ?>


            </div>
        </div>
    </section>

@stop
