@extends('layout.client.container')
@section('content')

<div class="innercover">
      <div class="container">
        <div class="row">
         <div class="col-md-12">
             <div class="inner-title">{{$newsData->news_heading}}</div>
         </div> 
      </div>
      </div>
      <div class="breadcrumbinner">
     <div class="container">
       <div class="row">
         <div class="col-md-12 col-sm-12 ">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('news-list') }}">News & Events</a></li>
                        <li class="active">{{$newsData->news_heading}}</li>
                    </ol>
                </div>
       </div>
     </div>
    </div>
    </div>
    <section class="inner_body_content">

        <div class="container">

            <div class="row">
              
                

             
                <div class="col-md-12"><?php if(file_exists('uploads/news/'.$newsData->news_attachment) && $newsData->news_attachment != '') { ?><img src="<?php echo $sitePath.'uploads/news/'.$newsData->news_attachment ?>" class="detailnewsimg">
                <div class="press-date col-md-12 col-sm-12">Published date: {{ date("M", strtotime($newsData->news_date)); }}
                                    {{ date("d", strtotime($newsData->news_date)); }},
                                    {{date("Y", strtotime($newsData->news_date));}}
                                </div>
        <?php } ?>{{$newsData->news_description}}
                        
                    </div>

                </div>
                <!--inner content end-->
            </div>
        </div>
    </section>
@stop
