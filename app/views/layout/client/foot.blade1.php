



<script src="{{ asset('includes/client/js/jquery-2.2.3.js') }}"></script>
<script src="{{ asset('includes/client/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('includes/client/js/bootsnav.js') }}"></script>
<script src="{{ asset('includes/client/js/jquery.appear.js') }}"></script>

<script src="{{ asset('includes/client/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('includes/client/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('includes/client/js/jquery.cubeportfolio.min.js') }}"></script>
<script src="{{ asset('includes/client/js/jquery.tools.min.js') }}"></script>
<script src="{{ asset('includes/client/js/jquery.revolution.min.js') }}"></script>
<script src="{{ asset('includes/client/js/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('includes/client/js/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('includes/client/js/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('includes/client/js/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('includes/client/js/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset('includes/client/js/wow.min.js') }}"></script>
<script src="{{ asset('includes/client/js/functions.js') }}"></script>


