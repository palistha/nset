<?php
$firstPara = trim(Request::segment(1));
$secondPara = trim(Request::segment(2));
$thirdPara = trim(Request::segment(3));
?>


<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--Loader-->
<div class="loader">
  <div class="bouncybox">
      <div class="bouncy"></div>
    </div>
</div>


<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
        <span class="info"><i class="icon-map-pin"></i>Maitighar, Kathmandu, Nepal</span>
        <span class="info"><i class="icon-phone2"></i>4262426, 9851024388</span>
        <span class="info"><i class="icon-mail"></i><a href="mailto:manju_sakya@hotmail.com">manju_sakya@hotmail.com</a></span>
        </div>
        <ul class="social_top pull-right">
          <li><a href="#."><i class="fa fa-facebook"></i></a></li>
          <li><a href="#."><i class="icon-twitter4"></i></a></li>
          <li><a href="#."><i class="icon-youtube"></i></a></li>
          <li><a href="#."><i class="icon-google"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>



<!--Header-->
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container"> 
    
    <div class="flag"><img src="{{ asset('includes/client/images/flag.png') }}"  ></div>
       
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('includes/client/images/logo-white.png') }}"  class="logo logo-display">
        <img src="{{ asset('includes/client/images/logo.png') }}" class="logo logo-scrolled" >
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
      <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
                                <li><a href="{{ route('home') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'home' || trim(Request::segment(1)) === '') {
                                           echo 'active';
                                       } ?>">Home</a></li>

                                <?php
                                $listHeaderMobileContents = ContentModel::headerContentMenus(0);
                                if (count($listHeaderMobileContents) > 0) {
                                $i = 10;
                                foreach ($listHeaderMobileContents as $listHeaderContentMob) {
                                $secondLevelMobs = ContentModel::headerContentMenus($listHeaderContentMob->id); ?>


                                <li <?php if(count($secondLevelMobs) > 0){ ?>class="dropdown" <?php } ?> ><a
                                            href="{{ $sitePath.$listHeaderContentMob->content_url }}"
                                            class="<?php if (trim(Request::segment(1)) === $listHeaderContentMob->content_url) {
                                                echo 'active';
                                            } ?>"
                                            <?php if(count($secondLevelMobs) > 0){ ?> data-toggle="dropdown"
                                            class="dropdown-toggle" <?php } ?> >{{ strtoupper($listHeaderContentMob->content_heading) }}
                                        <?php if(count($secondLevelMobs) > 0) { ?><b class="caret"></b><?php  } ?>
                                    </a>
                                    <?php if(count($secondLevelMobs) > 0) { ?>
                                    <ul class="dropdown-menu" role="menu">


                                        @foreach($secondLevelMobs as $secondlevelMob)
                                            <li>
                                                <a href="{{ $sitePath.$secondlevelMob->content_url }}"
                                                   class="<?php if (trim(Request::segment(1)) === $secondlevelMob->content_url) {
                                                       echo 'active';
                                                   } ?>">{{ strtoupper($secondlevelMob->content_heading) }}</a>
                                            </li>
                                        @endforeach


                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php $i++; } } ?>


                                <li><a href="{{ route('gallery') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'gallery' || trim(Request::segment(1)) === 'album') {
                                           echo 'active';
                                       } ?>">Gallery</a></li>
                                <li><a href="{{ route('news-list') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'news') {
                                           echo 'active';
                                       } ?>">News & Events</a></li>
                                
                               
                                <li><a href="{{ route('contact') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'contact') {
                                           echo 'active';
                                       } ?>">Contact</a></li>
                            </ul>
                            
                            
                            
       
      </div>
    </div>   
  </nav>
</header>


<!--Search-->
<div id="search">

  <button type="button" class="close">×</button>
  <form id="form1" name="form1" method="get" action="{{URL::route('search')}}"
                              target="_self">
                                  
    <input name="q" type="search" value="" placeholder="Keywords"  required/>
    <!--<input name="q" type="text" class="searchform search_input search_field" value="Search"
                                       onBlur="if(this.value=='')this.value='Search'"
                                       onFocus="if(this.value=='Search')this.value=''"/>-->
                                       
    <button type="submit" class="btn btn_common blue" name="sitesearch">Search</button>
  </form>
</div>
