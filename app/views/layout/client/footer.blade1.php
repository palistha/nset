<footer class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 footer_panel bottom45">
         <h3 class="heading bottom25">Video<span class="divider-left"></span></h3>
     

       
       <iframe width="100%" height="220" src="http://www.npfca.org.np/includes/client/video/vdo.mp4"  frameborder="0" autoplay="false"   allowfullscreen></iframe>
       
       <!-- <embed  width="100%" height="220" src="http://localhost/npfca/includes/client/video/vdo.mp4"  autoplay="false"  >
        </embed>-->
       
         
       
       
        </div>
      <div class="col-md-4 col-sm-4 footer_panel bottom45">
        <h3 class="heading bottom25">Quick Links<span class="divider-left"></span></h3>
        <ul class="links">

          
           <?php
                        $listFooterContents = ContentModel::footerContentMenus(0, 11);
                        ?>
                        @if(count($listFooterContents)>0)
                            @foreach($listFooterContents as $footerContent)

                                <li>
                                    <a href="{{ $sitePath.$footerContent->content_url }}"><i class="icon-chevron-small-right"></i> {{ ucfirst($footerContent->content_heading) }}</a>
                                </li>
                            @endforeach
                        @endif
                        
        </ul>
      </div>
      
      <div class="col-md-4 col-sm-4 footer_panel bottom45">
        <h3 class="heading bottom25">Contact Details <span class="divider-left"></span></h3>
        <p class=" address"><i class="icon-map-pin"></i>Maitighar, Kathmandu, Nepal  </p>
        <p class=" address"><i class="icon-phone"></i>4262426, 9851024388</p>
        <p class=" address"><i class="icon-mail"></i><a href="mailto:manju_sakya@hotmail.com">manju_sakya@hotmail.com</a></p>
         
        <ul class="social_icon top25">
          <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
          <li><a href="#." class="youtube"><i class="icon-youtube"></i></a></li>
          <li><a href="#." class="googleplus"><i class="icon-google"></i></a></li>
          
        </ul>
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <p>Copyright &copy; <?php echo date("Y"); ?>  NPFCA. Powered by <a href="http://peacenepal.com" target="_blank">Peace Nepal DOT Com</a></p>
      </div>
    </div>
  </div>
</div>
<!--FOOTER ends-->