

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<meta name="keywords" content="{{ isset($metaKeywords) ? $metaKeywords : '' }}"/>
<meta name="description" content="{{ isset($metaDescription) ? $metaDescription : '' }}">
<meta name="copyright" content="© {{ date('Y').' , ' . SITE_NAME}}"/>
<meta name="author" content="Peace Nepal"/>
<meta name="email" content="raman.maharjan@peacenepal.com"/>
<meta name="Distribution" content="Global"/>

<title>{{ isset($pageTitle) ? $pageTitle : '' }} :: {{ SITE_NAME }}</title>
<link rel="shortcut icon" href="{{URL('includes/client/img/favicon.png')}}"/>



<!-- Normalize CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>normalize.css">

<!-- Main CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>main.css">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>bootstrap.min.css">

<!-- Animate CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>animate.min.css">

<!-- Font-awesome CSS-->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>font-awesome.min.css">

<!-- Owl Caousel CSS -->


<link rel="stylesheet" href="{{URL('includes/client/vendor/OwlCarousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL('includes/client/vendor/OwlCarousel/owl.theme.default.min.css')}}">

<!-- Main Menu CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>meanmenu.min.css">

<!-- nivo slider CSS -->
<link rel="stylesheet" href="{{URL('includes/client/vendor/slider/css/nivo-slider.css')}}"  />
<link rel="stylesheet" href="{{URL('includes/client/vendor/slider/css/preview.css')}}"  />

<!-- Switch Style CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>hover-min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo CSS_PATH; ?>style.css">

<link rel="stylesheet" href="<?php echo CSS_PATH; ?>cubeportfolio.min.css">




