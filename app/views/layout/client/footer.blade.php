<footer>
    <div class="footer-area-top">
      <div class="container">
        <div class="row">
          <div class=" col-md-4 col-sm-4 col-xs-12">
            <div class="footer-box">
              <h3>Follow Us On Facebook</h3>
              <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fpushpasadan%2F&tabs=timeline&width=320&height=210&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=109802699076236" width="100%" height="210" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
          </div>
          <div class=" col-md-4 col-sm-4 col-xs-12">
            <div class="footer-box">
              <h3>Featured Links</h3>
              <ul class="featured-links">
                 <?php
                        $listFooterContents = ContentModel::footerContentMenus(0, 11);
                        ?>
                        @if(count($listFooterContents)>0)
                            @foreach($listFooterContents as $footerContent)

                                <li>
                                    <a href="{{ $sitePath.$footerContent->content_url }}"></i> {{ ucfirst($footerContent->content_heading) }}</a>
                                </li>
                            @endforeach
                        @endif
              </ul>
            </div>
          </div>
          <div class=" col-md-4 col-sm-4 col-xs-12">
            <div class="footer-box">
              <h3>Contact Details</h3>
              <ul class="corporate-address">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i>Bahiri Gaun, Kirtipur-14,</li>
                <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+977-01-4331827"> + 977-01-4331827 </a></li>
                <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@pushpasadan.edu.np">info@pushpasadan.edu.np</a></li>
              </ul>
              <ul class="footer-social">
                <li><a href="https://www.facebook.com/pushpasadan/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <p>&copy; <?php echo date("Y"); ?> Pushpasadan Boarding School. </p>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p>Powered By: <a href="https://theconnectplus.com/" target="_blank" >The Connect Plus - TCP</a> </p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
