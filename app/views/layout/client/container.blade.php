<?php
define('SITE_PATH', ConstantModel::getDetailByName('site_path')->constant_value);
define('SITE_NAME', ConstantModel::getDetailByName('site_name')->constant_value);
define('CSS_PATH', SITE_PATH . 'includes/client/css/');
define('FONT_PATH', SITE_PATH . 'includes/client/font-awesome/');
define('JS_PATH', SITE_PATH . 'includes/client/js/');
define('IMAGE_PATH', SITE_PATH . 'includes/client/images/');
define('UPLOAD_IMAGE_PATH', SITE_PATH . 'uploads/');
define('CALENDAR', SITE_PATH . 'includes/client/calendar-master/');
define('MENU_PROGRAM_ID', '166');
?>
        <!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<head>
    @include('layout.client.head')
    <script>
        var sitePath = '<?php echo SITE_PATH?>';
    </script>
</head>

<body>
@include('layout.client.header')
@yield('content')
{{--<!-- FOOTER -->--}}
@include('layout.client.footer')

@include('layout.client.foot')
</body>
</html>
