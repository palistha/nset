

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<meta name="keywords" content="{{ isset($metaKeywords) ? $metaKeywords : '' }}"/>
<meta name="description" content="{{ isset($metaDescription) ? $metaDescription : '' }}">
<meta name="copyright" content="© {{ date('Y').' , ' . SITE_NAME}}"/>
<meta name="author" content="Peace Nepal"/>
<meta name="email" content="raman.maharjan@peacenepal.com"/>
<meta name="Distribution" content="Global"/>

<title>{{ isset($pageTitle) ? $pageTitle : '' }} :: {{ SITE_NAME }}</title>
<link rel="shortcut icon" href="{{URL('includes/client/images/favicon.png')}}"/>

<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>animate.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>owl.transitions.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>bootsnav.css">
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH; ?>style.css">



