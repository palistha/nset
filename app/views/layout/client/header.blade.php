<?php
$firstPara = trim(Request::segment(1));
$secondPara = trim(Request::segment(2));
$thirdPara = trim(Request::segment(3));
?>


<header>
    <div id="header2" class="header4-area">
      <div class="header-top-area">
        <div class="container">
          <div class="row">
            <div class=" col-md-5 col-sm-3 col-xs-12">
              <div class="header-top-left">
                <div class="logo-area"> <a href="{{ route('home') }}"><img class="img-responsive" src="{{ asset('includes/client/img/logo.png') }}" alt="logo"></a> </div>
              </div>
            </div>
            <div class=" col-md-7 col-sm-9 col-xs-12">
              <div class="header-top-right">
                <ul>
                  <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+977-01-4331827"> + 977-01-4331827 </a></li>
                  <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@pushpasadan.edu.np">info@pushpasadan.edu.np</a></li>
                  <li><a href="{{ route('contact') }}" class="apply-now-btn2">Contact Us</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="main-menu-area bg-primary" id="sticker">
        <div class="container">
          <nav>
            <ul>
               <li><a href="{{ route('home') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'home' || trim(Request::segment(1)) === '') {
                                           echo 'active';
                                       } ?>">Home</a></li>

                                <?php
                                $listHeaderMobileContents = ContentModel::headerContentMenus(0);
                                if (count($listHeaderMobileContents) > 0) {
                                $i = 10;
                                foreach ($listHeaderMobileContents as $listHeaderContentMob) {
                                $secondLevelMobs = ContentModel::headerContentMenus($listHeaderContentMob->id); ?>


                                <li <?php if(count($secondLevelMobs) > 0){ ?>class="" <?php } ?> ><a
                                            href="{{ $sitePath.$listHeaderContentMob->content_url }}"
                                            class="<?php if (trim(Request::segment(1)) === $listHeaderContentMob->content_url) {
                                                echo 'active';
                                            } ?>"
                                            <?php if(count($secondLevelMobs) > 0){ ?> data-toggle="dropdown"
                                            class="dropdown-toggle" <?php } ?> >{{ strtoupper($listHeaderContentMob->content_heading) }}
                                        <?php if(count($secondLevelMobs) > 0) { ?><b class="caret"></b><?php  } ?>
                                    </a>
                                    <?php if(count($secondLevelMobs) > 0) { ?>
                                    <ul >


                                        @foreach($secondLevelMobs as $secondlevelMob)
                                            <li>
                                                <a href="{{ $sitePath.$secondlevelMob->content_url }}"
                                                   class="<?php if (trim(Request::segment(1)) === $secondlevelMob->content_url) {
                                                       echo 'active';
                                                   } ?>">{{ strtoupper($secondlevelMob->content_heading) }}</a>
                                            </li>
                                        @endforeach


                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php $i++; } } ?>


                                <li><a href="{{ route('gallery') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'gallery' || trim(Request::segment(1)) === 'album') {
                                           echo 'active';
                                       } ?>">Gallery</a></li>
                                <li><a href="{{ route('news-list') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'news') {
                                           echo 'active';
                                       } ?>">News & Events</a></li>
                                
                               
                                
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <!-- Mobile Menu Area Start -->
    <div class="mobile-menu-area" style="display:none">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="mobile-menu">
              <nav id="dropdown">
                <ul>
               <li><a href="{{ route('home') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'home' || trim(Request::segment(1)) === '') {
                                           echo 'active';
                                       } ?>">Home</a></li>

                                <?php
                                $listHeaderMobileContents = ContentModel::headerContentMenus(0);
                                if (count($listHeaderMobileContents) > 0) {
                                $i = 10;
                                foreach ($listHeaderMobileContents as $listHeaderContentMob) {
                                $secondLevelMobs = ContentModel::headerContentMenus($listHeaderContentMob->id); ?>


                                <li <?php if(count($secondLevelMobs) > 0){ ?>class="" <?php } ?> ><a
                                            href="{{ $sitePath.$listHeaderContentMob->content_url }}"
                                            class="<?php if (trim(Request::segment(1)) === $listHeaderContentMob->content_url) {
                                                echo 'active';
                                            } ?>"
                                            <?php if(count($secondLevelMobs) > 0){ ?> data-toggle="dropdown"
                                            class="dropdown-toggle" <?php } ?> >{{ strtoupper($listHeaderContentMob->content_heading) }}
                                        <?php if(count($secondLevelMobs) > 0) { ?> <?php  } ?>
                                    </a>
                                    <?php if(count($secondLevelMobs) > 0) { ?>
                                    <ul >


                                        @foreach($secondLevelMobs as $secondlevelMob)
                                            <li>
                                                <a href="{{ $sitePath.$secondlevelMob->content_url }}"
                                                   class="<?php if (trim(Request::segment(1)) === $secondlevelMob->content_url) {
                                                       echo 'active';
                                                   } ?>">{{ strtoupper($secondlevelMob->content_heading) }}</a>
                                            </li>
                                        @endforeach


                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php $i++; } } ?>


                                <li><a href="{{ route('gallery') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'gallery' || trim(Request::segment(1)) === 'album') {
                                           echo 'active';
                                       } ?>">Gallery</a></li>
                                <li><a href="{{ route('news-list') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'news') {
                                           echo 'active';
                                       } ?>">News & Events</a></li>
                                
                               
                                <li><a href="{{ route('contact') }}"
                                       class="<?php if (trim(Request::segment(1)) === 'contact') {
                                           echo 'active';
                                       } ?>">Contact</a></li>
            </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Mobile Menu Area End --> 
  </header>