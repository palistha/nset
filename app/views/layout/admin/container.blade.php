
<?php
	define('SITE_PATH', ConstantModel::getDetailByName('site_path')->constant_value);
	define('SITE_ADMIN_PATH', SITE_PATH.'admin/');
	define('ADMIN_CSS_PATH', SITE_PATH.'includes/admin/css/');
	define('ADMIN_JS_PATH', SITE_PATH.'includes/admin/js/');
   
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>         
   @include('layout.admin.head')
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="fixed-top">
<!-- BEGIN HEADER -->
	 @include('layout.admin.header')
<!-- END HEADER -->
<!-- BEGIN SIDEBAR -->
	@include('layout.admin.sidebar')
<!-- END SIDEBAR -->

    @yield('content')
	
	@include('layout.admin.footer') 
    
</body>
<!-- END BODY -->
</html>
