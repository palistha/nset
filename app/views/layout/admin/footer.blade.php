<div class="footer">
&nbsp; &nbsp;&nbsp;
&nbsp; &nbsp;&nbsp;
		{{ date('Y');}} &copy; peacenepal dot com
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<script src="<?php echo ADMIN_JS_PATH ?>app.js"></script>				
	<script>
		jQuery(document).ready(function() {		
			App.setPage("index");  // set current page
			App.init(); // init the rest of plugins and elements
   });
        
	</script>
	<!-- END JAVASCRIPTS -->