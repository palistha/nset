<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <ul>
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li>
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            &nbsp;
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start active">
            <a href="<?php echo SITE_ADMIN_PATH?>dashboard">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>


      <?php /*  <li class="videos" id="videos">
            <a href="<?php echo SITE_ADMIN_PATH?>videos">
                <i class="icon-home"></i>
                <span class="title">Videos Management</span>
                <span class="selected"></span>
            </a>
        </li> */ ?>


        <li id="albums" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>albums">
                <i class="icon-camera"></i>
                <span class="title">Album Management</span>
                <span class="selected"></span>
            </a>
        </li>
        <li id="banners" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>banners">
                <i class="icon-camera"></i>
                <span class="title">Banner Management</span>
                <span class="selected"></span>
            </a>
        </li>


        <li id="contents" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>contents">
                <i class="icon-bookmark-empty"></i>
                <span class="title">Content Management</span>
                <span class="selected"></span>
            </a>
        </li>


        <li id="news" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>news">
                <i class="icon-bookmark-empty"></i>
                <span class="title">News / Events Management</span>
                <span class="selected"></span>
            </a>
        </li>


        

<?php /*<li id="online" class="">
            <a href="{{ route('admin.application.list') }}">
                <i class="icon-bookmark-empty"></i>
                <span class="title">Application Request</span>
                <span class="selected"></span>
            </a>
        </li> */ ?>


        <li id="downloads" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>downloads">
                <i class="icon-download"></i>
                <span class="title">Download Management</span>
                <span class="selected"></span>
            </a>
        </li>

        <li id="categories" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>categories">
                <i class="icon-bookmark-empty"></i>
                <span class="title">Categories  Management</span>
                <span class="selected"></span>
            </a>
        </li>

        <li id="seos" class="">
            <a href="<?php echo SITE_ADMIN_PATH?>seos">
                <i class="icon-bookmark-empty"></i>
                <span class="title">SEO Management</span>
                <span class="selected"></span>
            </a>
        </li>
   <li id="managementteam" class="">
            <a href="{{URL::to('/admin/managementteam')}}">
                <i class="icon-bookmark-empty"></i>
                <span class="title"> Management Team</span>
                <span class="selected"></span>
            </a>
        </li>
          <li id="subtestimonial" class="">
            <a href="{{URL::to('/admin/subtestimonial')}}">
                <i class="icon-bookmark-empty"></i>
                <span class="title"> Testimonial</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="">
            <a href="<?php echo SITE_ADMIN_PATH?>logout">
                <i class="icon-off"></i>
                <span class="title">Log Out</span>
            </a>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
