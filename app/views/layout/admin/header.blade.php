
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="<?php echo SITE_ADMIN_PATH?>dashboard">
				<img style="padding-top: 8px; width:100px;" src="<?php echo SITE_PATH?>includes/admin/img/logo.png" alt="logo" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php echo SITE_PATH?>includes/admin/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<!-- BEGIN TOP NAVIGATION MENU -->					
				<ul class="nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->	

					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<?php 
							$userId = Auth::user()->id;
							$userDetail = user::getdetail($userId);
						?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">					
						<span class="username"><?php echo $userDetail->user_first_name.' '.$userDetail->user_last_name;?></span>
						<i class="icon-angle-down"></i>
						</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo SITE_ADMIN_PATH?>change-password"><i class="icon-key"></i> Change Password</a></li>
							<li><a href="<?php echo SITE_PATH?>" target="_blank"><i class="icon-plane"></i> Visit Site</a></li>

                            <li><a href="<?php echo SITE_ADMIN_PATH?>logout"><i class="icon-off"></i> Log Out</a></li>

						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
