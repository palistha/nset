@extends('layout.admin.container')

@section('content')
<!-- BEGIN CONTAINER -->

		<div class="page-container row-fluid">

		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							Content
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../contents"><?php echo ucwords('Content Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit content : '. $contentDetail->content_heading;?></li>
						</ul>
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
				<div class="row-fluid">
					<div class="span12">
						<div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>
                                Content Management</h4>
								<div class="actions">
									<a href="../../content/add" class="btn blue"><i class="icon-pencil"></i> Add New</a>
								</div>
							</div>


                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="contentForm" name="contentForm" method="post" class="form-horizontal" enctype="multipart/form-data">

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Parent: </label>
                                             <div class="controls">
											 {{ ContentModel::content_list_for_contentEntry(0,0,$contentDetail->parent_id) }}
                                                 </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Heading<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="content_heading" id="content_heading" class="m-wrap span12"
                                                value="{{{ $contentDetail->content_heading}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Content Url<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="content_url" id="content_url" class="m-wrap span12"
                                                value="{{{ $contentDetail->content_url}}}">
                                             <p class="general-note"> Note: valid characters are alpha numeric and -, _(underscore)</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Short Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="content_short_description" >{{{ $contentDetail->content_short_description}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12 ckeditor" rows="20" cols="20" name="content_description">
                                                {{{ $contentDetail->content_description}}}
												</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Page Title</label>
                                             <div class="controls">
                                                <input type="text" name="content_page_title" id="content_page_title" class="m-wrap span12"
                                                value="{{{ $contentDetail->content_page_title}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Tags</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="content_meta_tags" >{{{ $contentDetail->content_meta_tags}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="content_meta_description" >{{{ $contentDetail->content_meta_description}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachment <small>Image size: 720px/480px</small></label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                                        <span id="contentImage"><?php if(file_exists("uploads/pages/".$contentDetail->content_attachment) && $contentDetail->content_attachment!=''){ ?>
                                                        <img src="{{{'../../../uploads/pages/'.$contentDetail->content_attachment}}}" alt="" />
                                                        <?php }  ?>

                                                        </span>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>

                                                        <?php if(file_exists("uploads/pages/".$contentDetail->content_attachment) && $contentDetail->content_attachment!=''){ ?>&nbsp;&nbsp;
                                                        <span  class="remove-image btn" id="remove-image">Remove Image </span>
                                                        <div id="success-message" class="success-message"></div>
                                                        <?php } ?>
                                                    </div>
                                                 </div>

                                              </div>
                                           </div>
                                        </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Show In</label>
                                             <div class="controls">
                                             <?php
       $globalArrayMenuPosition = array('Header' => '1', 'Footer' => '2');
         $arrayPrevSelected = explode(",",$contentDetail->show_in);
         $welcomecheck = ($contentDetail->show_in_welcome == '1')?'checked="checked"':'';
                                        			foreach($globalArrayMenuPosition as $menuTitle => $singleMenuPosition) {
                                        				$selectedText = in_array($singleMenuPosition,$arrayPrevSelected)?'checked="checked"':'';
                                        			?>
             <input type="checkbox" name="show_in[]" value="<?php echo $singleMenuPosition?>" <?php echo $selectedText;?> />&nbsp;<?php echo $menuTitle?>&nbsp;&nbsp;
                                                    <?php } ?>

                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Show Order</label>
                                             <div class="controls">
                                                <input type="text" name="show_order" id="show_order" class="m-wrap span12"
                                                value="{{{ $contentDetail->show_order}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($contentDetail->is_active == '1' || $contentDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($contentDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Update';?></button>
                                       <input type="hidden" name="content_id" value="{{{ $contentDetail->id}}}">
                                    </div>
                                 </form>
                                 </div>
                                </div>

                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
<script>
$('documnet').ready(function(){
	$('.page-sidebar ul li').removeClass('active');
	$('#contents').addClass('active');
});
CKEDITOR.replace( 'content_description',{
	filebrowserBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Flash',
	filebrowserUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	filebrowserImageUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	filebrowserFlashUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
	filebrowserWindowWidth : '1000',
 	filebrowserWindowHeight : '700'
});
</script>
<script>
    $(document).ready(function () {

        $('.remove-image').bind("click", function () {
            if (!confirm("Are you sure to delete Image?"))
                return false;
            $object = $(this);
            var content_id = '{{ $contentDetail->id }}';
            $.ajax({
                type: "POST",
                url: "{{ route('admin.content.image.delete') }}",
                data: "content_id="+content_id,
                success: function(response){
                    var $jsondata = JSON.parse(response);
                    var flag = $jsondata.flag;
                    if(flag === true){
                        $("#success-message").html('<br><div class="span6"><div class="alert alert-success">'+$jsondata.message+'</div></div>');
                        $('#remove-image').remove();
                        $('#contentImage').remove();
                    } else {
                        $("#success-message").html('<br><div class="span6"><div class="alert alert-error">'+$jsondata.message+'</div></div>');
                    }
                },
                error: function(e){
                    $("#success-message").html('<br><div class="span6"><div class="alert alert-error">Server Error:</div></div>');
                }
            });



        });
    });

</script>


@stop