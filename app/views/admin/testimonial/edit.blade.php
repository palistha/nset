@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Opinion				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../testimonials"><?php echo ucwords('Opinion Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit Opinion : '. $testimonialDetail->testimonial_auther;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
<div class="row-fluid">
               <div class="span12">                     
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>
                                Opinion Management</h4>
								<div class="actions">
									<a href="../../testimonial/add" class="btn blue"><i class="icon-pencil"></i> Add New</a>									
								</div>
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="testimonialForm" name="testimonialForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Author<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="testimonial_auther" id="testimonial_auther" class="m-wrap span12" 
                                                value="{{{ $testimonialDetail->testimonial_auther}}}">
                                             </div>
                                          </div>
                                       </div>	
                                      <div>
                                      <div class="row-fluid">  
                                        
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Opinion Type</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="type">
                                                   <option <?php echo (($testimonialDetail->type == 'SO')? 'selected="selected"' : '') ?> value="SO">Students Opinion</option>
                                                   <option <?php echo (($testimonialDetail->type == 'G')? 'selected="selected"' : '') ?> value="G">UC Geniuses</option>
                                                     <option <?php echo (($testimonialDetail->type == 'P')? 'selected="selected"' : '') ?> value="P">Principal</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="10" cols="20" name="testimonial_description">{{{ $testimonialDetail->testimonial_description}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                   <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Extra Remark</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="10" cols="10" name="testimonial_remark">{{{ $testimonialDetail->testimonial_remark}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                                                                                       
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachment <small>Image size: 80px /80px</small></label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if(file_exists("uploads/testimonials/".$testimonialDetail->testimonial_attachment) && $testimonialDetail->testimonial_attachment!=''){ ?>
                                                        <img src="{{{'../../../uploads/testimonials/'.$testimonialDetail->testimonial_attachment}}}" alt="" />
                                                        <?php } else { ?>
                                                       <img src="<?php echo '../../../uploads/noimage.jpg' ?>" alt="" />
                                                       <?php }  ?>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
                                                 
                                              </div>
                                           </div>
                                        </div>
										
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($testimonialDetail->is_active == '1' || $testimonialDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($testimonialDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>                                       
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Update';?></button>
                                       <input type="hidden" name="testimonial_id" value="{{{ $testimonialDetail->id}}}">
                                    </div>
                                 </form>
                                 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#testimonials').addClass('active');
	});	
	</script>
@stop