@extends('layout.admin.container')

@section('content')
<!-- BEGIN CONTAINER -->
		
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							User				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../users"><?php echo ucwords('User Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Add new user';?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
<div class="row-fluid">
               <div class="span12">                       
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i><?php echo 'Add new user';?></h4>                                 
                              </div>
                        
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="userForm" name="userForm" method="post" class="form-horizontal">
                                    
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">First Name<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="user_first_name" class="m-wrap span12" value="" >                                                
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Last Name<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text"  name="user_last_name" class="m-wrap span12" value="" >                                                
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Email Address<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="username" class="m-wrap span12" value="" >
                                                
                                             </div>
                                          </div>
                                       </div>
                                       
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">User Type</label>
                                             <div class="controls">
                                                <select class="span12 chosen_category" name="user_type">
													<option value="0">Select User Type</option>
													<?php  
													$userTypes = UserTypeModel::getAllUserTypes();
													foreach($userTypes as $userType) { 
														echo '<option value="'.$userType->id.'">'.$userType->user_type_name.'</option>';
													} 
													?>                                                   
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                     </div>
                                     
                                     <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Password<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="password" id="password" name="password" class="m-wrap span12" >
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Confirm Password<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="password" id="confirm_password" name="confirm_password" class="m-wrap span12" >                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <!--/row-->
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Company</label>
                                             <div class="controls">
                                                <input type="text" name="user_company" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Gender</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="gender">
                                                   <option value="">Select</option>
                                                   <option value="M">Male</option>
												   <option value="F">Female</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
									
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Phone Number</label>
                                             <div class="controls">
                                                <input type="text" name="user_phone_number" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Mobile Number</label>
                                             <div class="controls">
                                                <input type="text" name="user_mobile_number" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Address Line 1</label>
                                             <div class="controls">
                                                <input type="text" name="user_address_line_1" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Address Line 2</label>
                                             <div class="controls">
                                                <input type="text" name="user_address_line_2" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									<div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">City</label>
                                             <div class="controls">
                                                <input type="text" name="user_city" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">State</label>
                                             <div class="controls">
                                                <input type="text" name="user_state" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>	
									
									<div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Postal Code</label>
                                             <div class="controls">
                                                <input type="text" name="user_zip_code" class="m-wrap span12" value="">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Country</label>
                                             <div class="controls">
                                                <select class="span12 chosen_category" name="country_id">
													<option value="0">Select Country</option>
													<?php  
													$countries = FunctionModel::getAllCountries();
													foreach($countries as $country) { 
														echo '<option value="'.$country->country_id.'">'.$country->country_name.'</option>';
													} 
													?>													
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Status</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="is_active">
                                                   <option value="1" >Yes</option>
                                                   <option value="0" >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Add';?></button>
                                       <input type="hidden" name="user_id" value="0">
                                    </div>
                                 </form>
                                 
								 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#users').addClass('active');
		
		$('#userForm input').hover(function(){
				$(this).popover('toggle')
			});
			
			$("#userForm").validate({			 
				rules:{
				    user_first_name:"required",
                    user_last_name:"required",
					username:{
						required:true,
							email: true
					},
			         password: {
				        required: true,
				        minlength: 5
			         },
			         confirm_password: {
				        required: true,
				        minlength: 5,
				        equalTo: "#password"
			         },
				},
				messages:{
				    user_first_name:"Please enter first name.",
                    user_last_name:"Please enter last name.",
					username: {
				        required: "Please enter an email address",
				        email: "Email should be valid email address."
                    },
			         password: {
				        required: "Please provide a password",
				        minlength: "Your password must be at least 5 characters long"
			         },
			         confirm_password: {
				        required: "Please provide a password",
				        equalTo: "Please enter the same password as above"
			         },
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});		
	});	
	</script>
	
	@stop
	
