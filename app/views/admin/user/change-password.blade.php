@extends('layout.admin.container')
@section('content')

<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							User				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
									<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../users"><?php echo ucwords('User Management');?></a></li>
							<span class="icon-angle-right"></span>
							<li><?php echo 'Change Password : '.$user->username;?></li>                                                      
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>	
				<div class="row-fluid">
					<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
					
					<form action="" id="changePassword" name="changePassword" method="post" class="form-horizontal">
												   
						<div class="row-fluid">
							<div class="span6">
								<div class="control-group">
								<label class="control-label">New Password</label>
								<div class="controls">
								 <input type="password" name="password" class="m-wrap span12" value="">                                             
								</div>
								</div>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span6">
								<div class="control-group">
								<label class="control-label">Re-type New Password</label>
								<div class="controls">
								 <input type="password" name="confirm_password" class="m-wrap span12" value="">                                             
								</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="id" value="<?php echo $user->id;?>">	   
						<div class="form-actions">
						<button type="submit" name="changePassword" class="btn blue"><i class="icon-ok"></i>Change Password</button>
						</div>
					</form>
							   
					</div>
				</div>
			</div>
            </div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->		
	</div>
	<!-- END PAGE -->
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#users').addClass('active');
	});	
</script>
	
	@stop