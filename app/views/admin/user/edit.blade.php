@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
		
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							User				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../users"><?php echo ucwords('User Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit user : ' . $userDetail->user_first_name . $userDetail->user_last_name;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
				<div class="row-fluid">
					<div class="span12">                       
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
                                <h4><i class="icon-edit"></i><?php echo 'Edit user';?></h4> 
								<div class="actions">
									<a href="../../user/add" class="btn blue"><i class="icon-pencil"></i>Add New</a>									
								</div> 
                              </div>
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="userForm" name="userForm" method="post" class="form-horizontal">
                                    
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">First Name<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="user_first_name" class="m-wrap span12" value="{{{ $userDetail->user_first_name}}}" >                                                
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Last Name<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text"  name="user_last_name" class="m-wrap span12" value="{{{ $userDetail->user_last_name}}}" >                                                
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Email Address<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="username" class="m-wrap span12" value="{{{ $userDetail->username}}}" >
                                                
                                             </div>
                                          </div>
                                       </div>
                                       
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">User Type</label>
                                             <div class="controls">
                                                <select class="span12 chosen_category" name="user_type">
                                                <option value="0">Select User Type</option>
												<?php  
												$userTypes = UserTypeModel::getAllUserTypes();
												foreach($userTypes as $userType) { 
													$selected = '';
													if($userType->id == $userDetail->user_type)
													$selected = 'selected = "selected"';
													echo '<option value="'.$userType->id.'"'.$selected.'>'.$userType->user_type_name.'</option>';
												} 
												?>  
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                     </div>
                                                                       
                                    <!--/row-->
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Company</label>
                                             <div class="controls">
                                                <input type="text" name="user_company" class="m-wrap span12" value="{{{ $userDetail->user_company}}}">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Gender</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="gender">
                                                   <option value="" <?php echo ($userDetail->gender == '')? 'selected="selected"' : '' ?>>Select</option>
                                                   <option value="M" <?php echo (($userDetail->gender == 'M')? 'selected="selected"' : '') ?>>Male</option>
												   <option value="F" <?php echo ($userDetail->gender == 'F')? 'selected="selected"' : '' ?> >Female</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
									
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Phone Number</label>
                                             <div class="controls">
                                                <input type="text" name="user_phone_number" class="m-wrap span12" value="{{{ $userDetail->user_phone_number}}}">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Mobile Number</label>
                                             <div class="controls">
                                                <input type="text" name="user_mobile_number" class="m-wrap span12" value="{{{ $userDetail->user_mobile_number}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Address Line 1</label>
                                             <div class="controls">
                                                <input type="text" name="user_address_line_1" class="m-wrap span12" value="{{{ $userDetail->user_address_line_1}}}">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Address Line 2</label>
                                             <div class="controls">
                                                <input type="text" name="user_address_line_2" class="m-wrap span12" value="{{{ $userDetail->user_address_line_2}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									<div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">City</label>
                                             <div class="controls">
                                                <input type="text" name="user_city" class="m-wrap span12" value="{{{ $userDetail->user_city}}}">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">State</label>
                                             <div class="controls">
                                                <input type="text" name="user_state" class="m-wrap span12" value="{{{ $userDetail->user_state}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>	
									
									<div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Postal Code</label>
                                             <div class="controls">
                                                <input type="text" name="user_zip_code" class="m-wrap span12" value="{{{ $userDetail->user_zip_code}}}">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Country</label>
                                             <div class="controls">
                                                <select class="span12 chosen_category" name="country_id">
													<option value="0">Select Country</option>
													<?php  
													$countries = FunctionModel::getAllCountries();
													foreach($countries as $country) { 
														$selected = '';
														if($country->country_id == $userDetail->country_id)
														$selected = 'selected = "selected"';
													
														echo '<option value="'.$country->country_id.'"'.$selected.'>'.$country->country_name.'</option>';
													} 
													?>                                                   
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($userDetail->is_active == '1' || $userDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($userDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Update';?></button>
                                       <input type="hidden" name="user_id" value="{{{ $userDetail->id}}}">
                                    </div>
                                 </form>
                                 
								 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#users').addClass('active');
	});	
	</script>
	
	@stop
	
