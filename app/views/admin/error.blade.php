@extends('layout.admin.container')
@section('content')

<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Dashboard				
							<small>statistics and more</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="#">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><?php echo ucwords('Error');?></li>                                                      
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				
				<div class="portlet-body">
					<div class="alert alert-error">
					<button class="close" data-dismiss="alert"></button>
					Access Denied
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12 ">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>Error</h4>
								
							</div>
							<div class="portlet-body">
								<div class="" data-height="600px">
									<p>An error has occurred while processing your request. This may occured because there was an attemt to manipulate this software. Users are prohibited from taking unauthorized actions to intentionally modify the system.</p>
                                    <?php /*<h4>Regards,</h4>
                                    <h4>Peace Nepal DOT Com P. Ltd.</h4> */?>
								</div>
							</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>
				</div>
				
			</div>
            </div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->		
	</div>
	<!-- END PAGE -->
</div>
	
	@stop