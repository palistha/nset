@extends('layout.admin.container')

@section('content')

    <style type="text/css">
        .admin-span {
            margin-left: 5px !important;
            margin-bottom: 10px !important;
        }

        .admin-img {
            text-align: center;

        }

        .admin-img img {
            width: 80px;
            height: 80px;
            border-radius: 50% !important;
        }

        .personal-block {
            padding: 10px 5px;
            background-color: #eee;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #418bb0;
            color: white;
        }

        .academic-tab {
            width: 48%;
            float: left;
            padding: 0% 1%;
        }

        .admin-span h5 {
            color: #333;
        }
    </style>
    <!-- BEGIN CONTAINER -->

    <div class="page-container row-fluid">

        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>Widget Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="span12">

                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Application Request
                        <small>Management</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="dashboard">Home</a>
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="{{ route('admin.application.list') }}"><?php echo ucwords('Application Request Management');?></a>
                        </li>
                        <span class="icon-angle-right"></span>
                        <li>{{ $applicantInfo->full_name }}'s Info</li>
                    </ul>
                </div>
                <!-- END PAGE HEADER-->
                <div id="dashboard">
                    <div id="success-message"></div>
                    <div class="row-fluid">
                        <div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box grey">
                                <div class="portlet-title">
                                    <h4><i class="icon-bookmark-empty"></i>
                                        {{ $applicantInfo->full_name }}'s Info</h4>

                                </div>
                                <div class="portlet-body">


                                    <div class="span3 responsive" data-tablet="span12 fix-offset" data-desktop="span6">

                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="personal-block">
                                        <div class="span2 responsive admin-span" data-tablet="span12 fix-offset">
                                            <h5><strong>Full Name:</strong></h5>
                                            <p>{{ $applicantInfo->full_name }}</p>
                                        </div>
                                        <div class="span3 responsive admin-span" data-tablet="span12 fix-offset">
                                            <h5><strong>Email:</strong></h5>
                                            <p>{{ $applicantInfo->email }}</p>
                                        </div>
                                        <div class="span2 responsive admin-span" data-tablet="span12 fix-offset">
                                            <h5><strong>Applying For:</strong></h5>
                                            <p> {{ $applicantInfo->choose }}</p>
                                        </div>
                                        <div class="span2 responsive admin-span" data-tablet="span12 fix-offset">
                                            <h5><strong>Contact No.</strong></h5>
                                            <p>{{ $applicantInfo->contact_no }}</p>
                                        </div>
                                        <div class="span2 responsive admin-span" data-tablet="span12 fix-offset">
                                            <h5><strong>Previous College :</strong></h5>
                                            <p> {{ $applicantInfo->previous_college	 }}</p>
                                        </div>


                                        <div class="clearfix"></div>
                                    </div>

                                    <br/>


                                    <table class="table table-striped table-bordered table-hover" id="">

                                        <tbody>


                                        <tr>
                                            <td><strong>Known From </strong></td>
                                            <td> {{ $applicantInfo->known_from }}</td>
                                        </tr>


                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
    <script type="text/javascript">
        $('documnet').ready(function () {
            $('.page-sidebar ul li').removeClass('active');
            $('#online').addClass('active');
        });
    </script>
@stop
