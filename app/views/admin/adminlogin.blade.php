<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="../includes/admin/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../includes/admin/css/metro.css" rel="stylesheet" />
  <link href="../includes/admin/css/font-awesome.css" rel="stylesheet" />
  <link href="../includes/admin/css/style.css" rel="stylesheet" />
  <link href="../includes/admin/css/style_responsive.css" rel="stylesheet" />
  <link href="../includes/admin/css/style_default.css" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="../includes/admin/css/uniform.default.css" />
  <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
    
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form  method="post" action="">
    <?php if(Session::get('class')) { ?>
	<h3 class="form-title"><?php echo Session::get('message')?></h3>
	<?php } else { ?>
      <h3 class="form-title">Login to your account</h3>
      <?php } ?>
        <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="password"/>
          </div>
        </div>
      </div>
      <div class="form-actions">
            <button type="submit" class="btn green pull-right" name="btnsubmit">
        Login <i class="m-icon-swapright m-icon-white"></i>
        </button>            
      </div>
     
    </form>
      
  </div>
  <!-- END LOGIN -->
  
  <!-- BEGIN JAVASCRIPTS -->
  <script src="../includes/admin/js/jquery-1.8.3.min.js"></script>
  <script src="../includes/admin/js/bootstrap.min.js"></script>  
  <script src="../includes/admin/js/jquery.uniform.min.js"></script> 
  <script src="../includes/admin/js/jquery.blockui.js"></script>
  <script type="text/javascript" src="../includes/admin/js/jquery.validate.min.js"></script>
  <script src="../includes/admin/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
      App.initLogin();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>