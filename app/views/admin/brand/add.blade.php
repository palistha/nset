@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Brand				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../brands"><?php echo ucwords('Brand Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Add new brand';?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
				<div class="row-fluid">
					<div class="span12">                       
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i><?php echo 'Add new brand';?></h4>                                 
                              </div>                
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="brandForm" name="brandForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Heading<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="name" id="name" class="m-wrap span12" 
                                                value="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Brand Url<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="url" id="url" class="m-wrap span12" 
                                                value="">
                                             <p class="general-note"> Note: valid characters are alpha numeric and -, _(underscore)</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Short Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="short_desc" ></textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12 ckeditor" rows="20" cols="20" name="desc">
                                                </textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                    
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Tags</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="meta_tags" ></textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="meta_desc" ></textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                     <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Feature Attachment</label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?php echo '../../uploads/noimage.jpg'?>" alt="" />
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
                                                 
                                              </div>
                                           </div>
                                        </div>
                                   
                                   <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Thumb Attachment</label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?php echo '../../uploads/noimage.jpg'?>" alt="" />
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="thumb_image" id="thumb_image" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
                                                 
                                              </div>
                                           </div>
                                        </div>
										<?php /*
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Show In</label>
                                             <div class="controls">
                                             <?php $globalArrayMenuPosition = array('Header' => '1', 'Footer' => '2');
                                        			foreach($globalArrayMenuPosition as $menuTitle=>$singleMenuPosition){
                                        				
                                        			?>
                                                    <input type="checkbox" name="show_in[]" value="<?php echo $singleMenuPosition?>" />&nbsp;<?php echo $menuTitle?>&nbsp;&nbsp;
                                                    <?php } ?>                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									*/ ?>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="is_active">
                                                   <option value="1">Yes</option>
                                                   <option value="0">No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Add';?></button>
                                       <input type="hidden" name="brand_id" value="0">
                                    </div>
                                 </form>
                                 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
<script>
$('documnet').ready(function(){
	$('.page-sidebar ul li').removeClass('active');
	$('#brands').addClass('active');
});
CKEDITOR.replace( 'desc',{
	filebrowserBrowseUrl : sitePath + 'includes/client/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : sitePath + 'includes/client/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : sitePath + 'includes/client/ckfinder/ckfinder.html?type=Flash',
	filebrowserUploadUrl : sitePath + 'includes/client/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	filebrowserImageUploadUrl : sitePath + 'includes/client/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	filebrowserFlashUploadUrl : sitePath + 'includes/client/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
	filebrowserWindowWidth : '1000',
 	filebrowserWindowHeight : '700'
});
</script>

@stop