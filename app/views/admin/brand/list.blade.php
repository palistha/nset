@extends('layout.admin.container')

@section('content')
	
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
	<!-- BEGIN PAGE -->
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div id="portlet-config" class="modal hide">
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button"></button>
				<h3>Widget Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here will be a configuration form</p>
			</div>
		</div>
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE CONTAINER-->
		<div class="container-fluid">
			<!-- BEGIN PAGE HEADER-->
			<div class="span12">				
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Brand
					<small>Management</small>
				</h3>
				<ul class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="dashboard">Home</a>
						<i class="icon-angle-right"></i>
					</li>
					<li><?php echo ucwords('Brand Management');?></li>
						<span class="icon-angle-right"></span>
					<li><?php echo ucwords('All Categories');?></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->			
			<div id="dashboard">
				<div id="success-message"></div>
				<div class="row-fluid">
					<div class="span12 responsive" data-tablet="span12 fix-offset" data-desktop="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>
                                Brand Management</h4>
								<div class="actions">
									<a href="brand/add" class="btn blue"><i class="icon-pencil"></i> Add New</a>									
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<?php /*	<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th> */ ?>
											<th>S N</th>
                                            <th>Brand Name</th>
											<th class="hidden-120">Published</th>
											<th class="hidden-120">Updated On</th>											
											<th class="hidden-80">Options</th>
										</tr>
									</thead>
									<tbody>
                                    {{ $brands }}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->		
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
	
<script type="text/javascript">
$('documnet').ready(function(){
	$('.page-sidebar ul li').removeClass('active');
	$('#brands').addClass('active');
});
</script>
	
	@stop