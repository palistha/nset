@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
		
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Media				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../albums"><?php echo 'Album Management';?></a></li>
                            <span class="icon-angle-right"></span>
							<li><a href="../../album/viewmedia/<?php echo $album_id;?>"><?php echo ucwords($album_title);?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Add new images';?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
			<div class="row-fluid">
               <div class="span12">                       
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i><?php echo 'Add new images';?></h4>                                 
                              </div>
                        
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="bannerForm" name="bannerForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachments</label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                       <img src="../../../uploads/noimage.jpg" alt="" />
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select images</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile[]" id="userfile" class="default" multiple />
                                                       </span>                                                     
                                                    </div>
                                                 </div>
												 <p class="general-note"> Note: valid image files are jpg, jpeg, png and gif, 800 X 600 looks better .<br />
													Maximum allowed files are 10 and each filesize upto 1 MB.</p>
													<?php /*<p class="general-note"> Note: Image Size : 800*356 for home page big banner and 370*530 for small*/ ?>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid">										
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="is_active">
                                                   <option value="1">Yes</option>
                                                   <option value="0">No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i> ADD </button>
                                       <input type="hidden" name="media_id" value="0">
									   <input type="hidden" name="album_id" value="{{{$album_id}}}">
                                    </div>
                                 </form>
                                
								</div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
		$('documnet').ready(function(){
			$('.page-sidebar ul li').removeClass('active');
			$('#albums').addClass('active');
		});
	</script>

	@stop
	
