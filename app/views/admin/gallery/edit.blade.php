@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Gallery				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../gallery"><?php echo ucwords('Gallery Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit gallery : ' . $galleryDetail->gallery_title;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
<div class="row-fluid">
               <div class="span12">                     
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>Edit Gallery</h4>
								<div class="actions">
									<a href="../../gallery/add" class="btn blue"><i class="icon-pencil"></i>Add New</a>									
								</div>
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="galleryForm" name="galleryForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Title<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="gallery_title" class="m-wrap span12" value="{{{$galleryDetail->gallery_title}}}">                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div> 
									<?php /*
									<div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Caption</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="5" cols="10" name="gallery_caption">{{{ $galleryDetail->gallery_caption}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									*/ ?>
									<input type="hidden" name="gallery_caption">
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachment <small>Image size: 285px 220px;</small></label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                       <?php if(file_exists("uploads/gallery/".$galleryDetail->gallery_attachment) && $galleryDetail->gallery_attachment!=''){ ?>
                                                        <img src="{{{'../../../uploads/gallery/'.$galleryDetail->gallery_attachment}}}" alt="" />
                                                        <?php } else { ?>
                                                       <img src="<?php echo '../../../uploads/noimage.jpg' ?>" alt="" />
                                                       <?php }  ?>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
													<?php /*<p class="general-note"> Note: Image Size : 800*356 for home page big gallery and 370*530 for small gallery</p>
                                              */ ?></div>											  
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($galleryDetail->is_active == '1' || $galleryDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($galleryDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i>UPDATE</button>
                                       <input type="hidden" name="gallery_id" value="{{{$galleryDetail->id}}}">
                                    </div>
                                 </form>
                                 
								 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
		$('documnet').ready(function(){
			$('.page-sidebar ul li').removeClass('active');
			$('#gallerys').addClass('active');
		});
	</script>

@stop