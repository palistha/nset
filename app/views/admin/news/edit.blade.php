@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							News / Events				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../news"><?php echo ucwords('News / Events Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit News / Events : '.$newsDetail->news_heading;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
				
				<div class="row-fluid">
					<div class="span12">                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>
                                News / Events Management</h4>
								<div class="actions">
									<a href="../../news/add" class="btn blue"><i class="icon-pencil"></i> Add New</a>									
								</div>
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="newsForm" name="newsForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                     
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Heading<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="news_heading" id="news_heading" class="m-wrap span12" 
                                                value="{{{ $newsDetail->news_heading}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">News / Events Url<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="news_url" id="news_url" class="m-wrap span12" 
                                                value="{{{ $newsDetail->news_url}}}">
                                             <p class="general-note"> Note: valid characters are alpha numeric and -, _(underscore)</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Short Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="news_short_description" >{{{ $newsDetail->news_short_description}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12 ckeditor" rows="20" cols="20" name="news_description">
                                                {{{ $newsDetail->news_description}}}
												</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Page Title</label>
                                             <div class="controls">
                                                <input type="text" name="news_page_title" id="news_page_title" class="m-wrap span12" 
                                                value="{{{ $newsDetail->news_page_title}}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Tags</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="news_meta_tags" >{{{ $newsDetail->news_meta_tags}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="news_meta_description" >{{{ $newsDetail->news_meta_description}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                                                     
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachment <small>Image size: 720px/480px</small></label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if(file_exists("uploads/news/".$newsDetail->news_attachment) && $newsDetail->news_attachment!=''){ ?>
                                                        <img src="{{{'../../../uploads/news/'.$newsDetail->news_attachment}}}" alt="" />
                                                        <?php } else { ?>
                                                       <img src="<?php echo '../../../uploads/noimage.jpg' ?>" alt="" />
                                                       <?php }  ?>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
                                                 <p class="general-note"> Note: Image Size : 800 X 600 looks better </p>
                                              </div>
                                           </div>
                                        </div>
                                   
                                    <div class="row-fluid">
                                        <div class="span6">
                                          <div class="control-group">
                                             <label class="control-label">News / Events Date</label>
												<div class="controls">
													<input type="text" class="m-wrap span12 date-picker" name="news_date" value="<?php echo $newsDetail->news_date;?>" />                                                                                                 
												</div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Type</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="news_type">
                                                   <option value="N" <?php echo (($newsDetail->news_type == 'N' || $newsDetail->is_active == '')? 'selected="selected"' : '') ?> >Latest Notice</option>
                                                   <option value="E" <?php echo ($newsDetail->news_type == 'E')? 'selected="selected"' : '' ?> >News & Events</option>
                                                   <option value="A" <?php echo ($newsDetail->news_type == 'A')? 'selected="selected"' : '' ?> >Events</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>                                       
                                    </div>
                                    
                                   <div class="row-fluid">
                                       <div class="span6 ">
                                           <div class="control-group">
                                               <label class="control-label">Show in HomePage ?</label>
                                               <div class="controls">
                                                   <select class="m-wrap span12" name="show_in_home">
                                                       <option value="1" <?php echo (($newsDetail->show_in_home == '1' || $newsDetail->show_in_home == '')? 'selected="selected"' : '') ?>>Yes</option>
                                                       <option value="0" <?php echo ($newsDetail->show_in_home == '0')? 'selected="selected"' : '' ?>>No</option>
                                                   </select>
                                               </div>
                                           </div>
                                       </div>

                                    <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($newsDetail->is_active == '1' || $newsDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($newsDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>                                       
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Update';?></button>
                                       <input type="hidden" name="news_id" value="{{{ $newsDetail->id}}}">
                                    </div>
                                 </form>
                                 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
<script>
$('document').ready(function(){
	$('.page-sidebar ul li').removeClass('active');
	$('#news').addClass('active');
});
CKEDITOR.replace( 'news_description',{
	filebrowserBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Flash',
	filebrowserUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	filebrowserImageUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	filebrowserFlashUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
	filebrowserWindowWidth : '1000',
 	filebrowserWindowHeight : '700'
});
</script>

@stop
