@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Achievement Type				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../achievements"><?php echo ucwords('Achievement Type Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit achievement : ' . $achievementDetail->achievement_type_title ;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
			<div class="row-fluid">
				<div class="span12">                     
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>Edit Achievement Type</h4>
								<div class="actions">
									<a href="../../achievementtype/add/" class="btn blue"><i class="icon-pencil"></i>Add New</a>									
								</div>
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="achievementForm" name="achievementForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Title<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="achievement_type_title" class="m-wrap span12" value="{{{$achievementDetail->achievement_type_title}}}">                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                   <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Achievement Type URL<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" id="achievement_url" name="achievement_url" class="m-wrap span12" value="{{{$achievementDetail->achievement_url}}}" />                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="5" cols="10" name="achievement_description">{{{ $achievementDetail->achievement_description}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                                                      
                                    
									
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($achievementDetail->is_active == '1' || $achievementDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($achievementDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i>UPDATE</button>
                                       <input type="hidden" name="achievement_id" value="{{{$achievementDetail->id}}}">
                                    </div>
                                 </form>
                                 
								 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#achievements').addClass('active');
	});	
	</script>
	
@stop