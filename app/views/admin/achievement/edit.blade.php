@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Achievement				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../../achievements/{{{ $achievementDetail->type}}}"><?php echo 'Achievement Management';?></a></li>							
                            <span class="icon-angle-right"></span>
                            <li><?php echo 'Edit Achievement : '. $achievementDetail->title;?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
			<div class="row-fluid">
				<div class="span12">                     
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>Edit Achievement</h4>
								
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="achievementForm" name="achievementForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
<!--                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Title<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="achievement_title" class="m-wrap span12" value="{{{$achievementDetail->achievement_title}}}">                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>-->
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Title<span class="required">*</span></label>
                                             <div class="controls">
                                               <input type="text" name="title" class="m-wrap span12" value="{{{ $achievementDetail->title}}}" />
                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Secondary Title</label>
                                             <div class="controls">
                                               <input type="text" name="second_title" class="m-wrap span12" value="{{{ $achievementDetail->second_title}}}" />                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Note</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="5" cols="10" name="note">{{{ $achievementDetail->note}}}</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                                                      
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Achievement Attachment</label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                       <?php if(file_exists("uploads/achievements/".$achievementDetail->attachment) && $achievementDetail->attachment!=''){ 
                                                         ?>
                                                        <img src="{{URL('uploads/achievements/'.$achievementDetail->attachment)}}" alt="" />
                                                        <?php } else { ?>
                                                       <img src="<?php echo '../../../uploads/noimage.jpg' ?>" alt="" />
                                                       <?php }  ?>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
												 <?php /*<p class="general-note"> Note: Image Size : 100x100 looks better</p> */ ?>
                                              </div>
                                        </div>
									</div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
												<select class="m-wrap span12" name="is_active">
                                                   <option value="1" <?php echo (($achievementDetail->is_active == '1' || $achievementDetail->is_active == '')? 'selected="selected"' : '') ?> >Yes</option>
                                                   <option value="0" <?php echo ($achievementDetail->is_active == '0')? 'selected="selected"' : '' ?> >No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i>Update</button>
                                       <input type="hidden" name="achievement_id" value="{{{$achievementDetail->id}}}">
                                       <input type="hidden" name="achievement_type" value="{{{$type}}}">
                                    </div>
                                 </form>
                                 
								 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#<?php echo $divId; ?>').addClass('active');
	});	
	</script>
	
@stop