@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->
	
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							SEO				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../../seos"><?php echo ucwords('Seo Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo ucwords('Edit');?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
<div class="row-fluid">
               <div class="span12">                     
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
								<h4><i class="icon-bookmark-empty"></i>
                                Seo Management</h4>
								<div class="actions">
									<?php /*<a href="../../seo/add" class="btn blue"><i class="icon-pencil"></i> Add New</a> */ ?>								
								</div>
							</div>
							                         
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="seoForm" name="seoForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Page<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="seo_heading" id="seo_heading" class="m-wrap span12" readonly="readonly"
                                                value="{{{ $seoDetail->seo_heading }}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Page Title</label>
                                             <div class="controls">
                                                <input type="text" name="page_title" id="page_title" class="m-wrap span12" 
                                                value="{{{ $seoDetail->page_title }}}">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="desc" >{{{ $seoDetail->desc}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Tags</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="page_meta_tags" >{{{ $seoDetail->page_meta_tags}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="page_meta_description" >{{{ $seoDetail->page_meta_description}}}</textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Update';?></button>
                                       <input type="hidden" name="seo_id" value="{{{ $seoDetail->id}}}">
                                    </div>
                                 </form>
                                 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#seos').addClass('active');
	});	
  
  CKEDITOR.replace( 'desc',{
    filebrowserBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
    filebrowserWindowWidth : '1000',
    filebrowserWindowHeight : '700'
  });
	</script>
@stop