@extends('layout.admin.container')

@section('content')
<!-- BEGIN CONTAINER -->
		
		<div class="page-container row-fluid">
	
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="span12">
							
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Project				
							<small>Management</small>
						</h3>
                        <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="../dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="../projects"><?php echo ucwords('Project Management');?></a></li>
                            <span class="icon-angle-right"></span>
                            <li><?php echo ucwords('Add');?></li>                            
						</ul> 
                </div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				<?php if(Session::get('class')) { ?>
				<div class="portlet-body">
					<div class="<?php echo $class = Session::get('class');?>">
					<button class="close" data-dismiss="alert"></button>
					<?php echo $message = Session::get('message');?>
					</div>
				</div>
				<?php } ?>
<div class="row-fluid">
               <div class="span12">                       
                     
                        <div class="portlet box grey">
                              <div class="portlet-title">
                                 <h4><i class="icon-edit"></i><?php echo ucwords('Add new Project');?></h4>                                 
                              </div>
                        
                        
                                 <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 <form action="" id="projectForm" name="projectForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Title<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="title" id="title" class="m-wrap span12" 
                                                value="">
                                             </div>
                                          </div>
                                       </div>
                                      
                                      
<!--                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Type</label>
                                             <div class="controls">
                                                <input type="text" name="type" id="type" class="m-wrap span12" 
                                                value="">
                                             </div>
                                          </div>
                                       </div>-->
                                    </div>
                                    
                                                
                                   <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">URL<span class="required">*</span></label>
                                             <div class="controls">
                                                <input type="text" name="url" id="url" class="m-wrap span12" 
                                                value="" />
                                                <p class="general-note"> Note: valid characters are alpha numeric and -, _(underscore)</p>
                                             </div>
                                          </div>
                                       </div>
                                   </div>
                                   <div class="row-fluid">
                                        
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Type</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="type">
                                                   <option value="C">Current</option>
                                                   <option value="P">Past</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Short Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="10" cols="20" name="short_desc"></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                   <div class="row-fluid">
                                        <div class="span12 ">
                                          <div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="m-wrap span12" rows="10" cols="20" name="desc"></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                   <div class="row-fluid">
                                        <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Page Title</label>
                                             <div class="controls">
                                                <input type="text" name="page_title" id="page_title" class="m-wrap span12" 
                                                value="">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                   <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Tags</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="meta_tags" ></textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span9 ">
                                          <div class="control-group">
                                             <label class="control-label">Meta Description</label>
                                             <div class="controls">
                                             <textarea class="m-wrap span12" name="meta_description" ></textarea> 
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                    <div class="row-fluid">
                                        <div class="control-group">
                                              <label class="control-label">Image Attachment <small>Image size: </small></label>
                                              <div class="controls">
                                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?php echo '../../uploads/noimage.jpg'?>" alt="" />
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile" class="default" />
                                                       </span>                                                       
                                                    </div>
                                                 </div>
                                                 
                                              </div>
                                           </div>
                                        </div>
									
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label">Published</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="is_active">
                                                   <option value="1">Yes</option>
                                                   <option value="0">No</option>
                                                </select>
                                               </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i><?php echo 'Add';?></button>
                                       <input type="hidden" name="project_id" value="0">
                                    </div>
                                 </form>
                                 </div>                                 
                                </div>
                                
                            </div>
                        </div>
    </div>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sidebar ul li').removeClass('active');
		$('#projects').addClass('active');
    
    CKEDITOR.replace( 'desc',{
    filebrowserBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : sitePath + 'includes/admin/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : sitePath + 'includes/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
    filebrowserWindowWidth : '1000',
    filebrowserWindowHeight : '700'
  });
	});	
	</script>
	
	@stop
	
