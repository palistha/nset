@extends('layout.admin.container')

@section('content')<!-- BEGIN CONTAINER -->

<div class="page-container row-fluid">

    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>Widget Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here will be a configuration form</p>
            </div>
        </div>
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="span12">

                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Download
                    <small>Management</small>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="../../dashboard">Home</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li><a href="../../downloads"><?php echo ucwords('Download Management');?></a></li>
                    <span class="icon-angle-right"></span>
                    <li><?php echo 'Edit download : ' . $downloadDetail->download_caption;?></li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <div id="dashboard">
                <?php if(Session::get('class')) { ?>
                <div class="portlet-body">
                    <div class="<?php echo $class = Session::get('class');?>">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $message = Session::get('message');?>
                    </div>
                </div>
                <?php } ?>
                <div class="row-fluid">
                    <div class="span12">

                        <div class="portlet box grey">
                            <div class="portlet-title">
                                <h4><i class="icon-bookmark-empty"></i>Edit Download</h4>
                                <div class="actions">
                                    <a href="../../download/add" class="btn blue"><i class="icon-pencil"></i>Add New</a>
                                </div>
                            </div>


                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form action="" id="downloadForm" name="downloadForm" method="post"
                                      class="form-horizontal" enctype="multipart/form-data">

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Parent </label>
                                                <div class="controls">
                                                    {{ CategoryModel::category_list_for_DownloadEntry(0,0,$downloadDetail->category_id) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Caption<span
                                                            class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="download_caption" class="m-wrap span12"
                                                           value="{{{$downloadDetail->download_caption}}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="control-group">
                                            <label class="control-label">File Attachment</label>
                                            <div class="controls">
                                                <div class="fileupload fileupload-new" data-provides="fileupload">

                                                    <?php if(file_exists("uploads/downloads/" . $downloadDetail->download_attachment) && $downloadDetail->download_attachment != ''){ ?>
                                                    <a href="{{'../../../uploads/downloads/'.$downloadDetail->download_attachment}}"
                                                       target="_blank">{{$downloadDetail->download_caption}}</a>

                                                    <?php } else { ?>
                                                       
                                                       <?php }  ?>

                                                    <div class="fileupload-preview fileupload-exists thumbnail"
                                                         style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
                                                    <div>
                                                       <span class="btn btn-file">
                                                            <span class="fileupload-new">Select file</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="userfile" id="userfile"
                                                                   class="default"/>
                                                       </span>
                                                    </div>
                                                </div>
                                                <!--													<p class="general-note"> Note: Image Size : 800*356 for home page big download and 370*530 for small download</p>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">

                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Published</label>
                                                <div class="controls">
                                                    <select class="m-wrap span12" name="is_active">
                                                        <option value="1" <?php echo(($downloadDetail->is_active == '1' || $downloadDetail->is_active == '') ? 'selected="selected"' : '') ?> >
                                                            Yes
                                                        </option>
                                                        <option value="0" <?php echo ($downloadDetail->is_active == '0') ? 'selected="selected"' : '' ?> >
                                                            No
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" name="btnSubmit" class="btn blue"><i class="icon-ok"></i>UPDATE
                                        </button>
                                        <input type="hidden" name="download_id" value="{{{$downloadDetail->id}}}">
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<script type="text/javascript">
    $('documnet').ready(function () {
        $('.page-sidebar ul li').removeClass('active');
        $('#downloads').addClass('active');
    });
</script>

@stop