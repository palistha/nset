<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => array(
		'domain' => 'sandbox33a928e0aa5547f6a060bf48133c2e89.mailgun.org',
		'secret' => 'key-a38d7dd83f34ee36233deda55f6619c2',
	),

	'mandrill' => array(
		'secret' => 'SxLPDMUz9NRoop7t0hXQAg',
	),

	'stripe' => array(
		'model'  => 'User',
		'secret' => '',
	),

);
