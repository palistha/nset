<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Setting global constants
	|-------------------------------------------------------------------------- 
	|
	*/
	
	'ADMIN_EMAIL' => 'raman.maharjan@peacenepal.com',
	'SITE_NAME' => 'Pushpasadan Boarding High School',
	'FROM_NAME' => 'Pushpasadan Boarding High School',
	'NO_REPLY_EMAIL' => 'raman.maharjan@peacenepal.com',
	'SITE_PATH' => 'http://project.peacenepal.com/pushpasadan/',
	'HOME_PAGE_PRODUCT' => '4',
	'PER_PAGE_PRODUCT' => '12',
	'PER_PAGE_RELATED_PRODUCT' => '4',
	'PER_PAGE_NEWS' => '10',
	'SIDEBAR_PRODUCT' => '9',
);
