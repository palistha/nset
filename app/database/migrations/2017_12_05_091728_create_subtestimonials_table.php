<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtestimonialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('subtestimonials', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('name');
		
			$table->string('images');            
			$table->text('description');            
			$table->string('is_active');
			
			$table->string('updated_by');
			$table->string('deleted');

			$table->rememberToken();
			$table->timestamps();
	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		  Schema::dropIfExists('subtestimonials');
	}

}
