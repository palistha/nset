<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagementteamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('managementteams', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('position');	
	
			$table->string('images');            
			$table->string('is_active');
			
			$table->string('updated_by');

			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	  Schema::dropIfExists('managementteams');
	}

}
