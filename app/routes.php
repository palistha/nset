<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('404',function()
{
	$seoDetail	= SeoModel::getDetailBypage('404');
	$sitePath	= ConstantModel::getDetailByName('site_path')->constant_value;
	return View::make('client.404', 
						array('pageTitle' => $seoDetail->page_title,
								'metaDescription' => $seoDetail->page_meta_tags,
								'metaKeywords' => $seoDetail->page_meta_description,
							)
						)->with('sitePath', $sitePath);
});

Route::group(array('prefix' => 'admin'), function()
{
	Route::get('', 'LoginController@getAdminBase');
	Route::get('/', 'LoginController@getAdminBase');
	Route::get('login', 'LoginController@getLogin');
	Route::post('login', 'LoginController@postLogin');
});	

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{	
	Route::get('dashboard', 'LoginController@dashboard');
	Route::get('logout', 'LoginController@logout');
	Route::get('change-password', 'LoginController@getChangePassword');
	Route::post('change-password', 'LoginController@changePassword');

	/*
	 * Admin route for online application
	 */
	Route::get('application', array('as'=>'admin.application.list','uses'=>'OnlineApplicationController@getApplications'));
	Route::get('application/view/{id}', array('as'=>'admin.application.detail','uses'=>'OnlineApplicationController@getApplicationDetail'));
	Route::post('application/delete',array('as'=>'admin.application.delete', 'uses'=>'OnlineApplicationController@deleteApplication'));
	/*
	 * Admin Online application ends
	 */

	
	Route::get('albums', array('as'=>'admin.albums.list',
             'uses'=>'AlbumController@getAlbums'));
	Route::get('album/add', 'AlbumController@getAddAlbum');
	Route::post('album/add', 'AlbumController@addAlbum');
	Route::post('album/publish', 'AlbumController@publishAlbum');
	Route::post('album/unpublish', 'AlbumController@unpublishAlbum');
	Route::post('album/delete', 'AlbumController@deleteAlbum');
	Route::get('album/edit/{id}', 'AlbumController@getEditAlbum'); 
	Route::post('album/edit/{id}', 'AlbumController@editAlbum'); 
	

	Route::get('managementteam', array('as'=>'admin.managementteams.list',
             'uses'=>'ManagementTeamController@getManagementTeam'));
	Route::get('managementteam/add', 'ManagementTeamController@getAddManagementTeam');
	Route::post('managementteam/add', 'ManagementTeamController@addManagementTeam');
	Route::post('managementteam/publish', 'ManagementTeamController@publishManagementTeam');
	Route::post('managementteam/unpublish', 'ManagementTeamController@unpublishManagementTeam');
	Route::post('managementteam/delete', 'ManagementTeamController@deleteManagementTeam');
	Route::get('managementteam/edit/{id}', 'ManagementTeamController@getEditManagementTeam'); 
	Route::post('managementteam/edit/{id}', 'ManagementTeamController@editManagementTeam'); 


	Route::get('subtestimonial', array('as'=>'admin.subtestimonials.list',
             'uses'=>'SubtestimonialController@getSubTestimonial'));
	Route::get('subtestimonial/add', 'SubtestimonialController@getAddSubTestimonial');
	Route::post('subtestimonial/add', 'SubtestimonialController@addSubTestimonial');
	Route::post('subtestimonial/publish', 'SubtestimonialController@publishSubTestimonial');
	Route::post('subtestimonial/unpublish', 'SubtestimonialController@unpublishSubTestimonial');
	Route::post('subtestimonial/delete', 'SubtestimonialController@deleteSubTestimonial');
	Route::get('subtestimonial/edit/{id}', 'SubtestimonialController@getEditSubTestimonial'); 
	Route::post('subtestimonial/edit/{id}', 'SubtestimonialController@editSubTestimonial'); 

	Route::get('videos', array('as'=>'admin.videos.list',
             'uses'=>'VideoController@getVideos'));
	Route::get('video/add', 'VideoController@getAddVideo');
	Route::post('video/add', 'VideoController@addVideo');
	Route::post('video/publish', 'VideoController@publishVideo');
	Route::post('video/unpublish', 'VideoController@unpublishVideo');
	Route::post('video/delete', 'VideoController@deleteVideo');
	Route::get('video/edit/{id}', 'VideoController@getEditVideo'); 
	Route::post('video/edit/{id}', 'VideoController@editVideo'); 
	
	Route::get('album/viewmedia/{id}', 'MediaController@getMedias');
	Route::get('album/addmedia/{id}', 'MediaController@getAddMedia');
	Route::post('album/addmedia/{id}', 'MediaController@addMedia');
	Route::post('media/publish', 'MediaController@publishMedia');
	Route::post('media/unpublish', 'MediaController@unpublishMedia');
	Route::post('media/delete', 'MediaController@deleteMedia');
	Route::get('media/edit/{id}', 'MediaController@getEditMedia');
	Route::post('media/edit/{id}', 'MediaController@editMedia');
	
	Route::get('banners', 'BannerController@getBanners');
	Route::get('banner/add', 'BannerController@getAddBanner');
	Route::post('banner/add', 'BannerController@addBanner');
	Route::post('banner/publish', 'BannerController@publishBanner');
	Route::post('banner/unpublish', 'BannerController@unpublishBanner');
	Route::post('banner/delete', 'BannerController@deleteBanner');
	Route::get('banner/edit/{id}', 'BannerController@getEditBanner'); 
	Route::post('banner/edit/{id}', 'BannerController@editBanner');
  
  Route::get('downloads', 'DownloadController@getDownloads');
	Route::get('download/add', 'DownloadController@getAddDownload');
	Route::post('download/add', 'DownloadController@addDownload');
	Route::post('download/publish', 'DownloadController@publishDownload');
	Route::post('download/unpublish', 'DownloadController@unpublishDownload');
	Route::post('download/delete', 'DownloadController@deleteDownload');
	Route::get('download/edit/{id}', 'DownloadController@getEditDownload'); 
	Route::post('download/edit/{id}', 'DownloadController@editDownload');
	
	Route::get('gallery', 'GalleryController@getGalleries');
	Route::get('gallery/add', 'GalleryController@getAddGallery');
	Route::post('gallery/add', 'GalleryController@addGallery');
	Route::post('gallery/publish', 'GalleryController@publishGallery');
	Route::post('gallery/unpublish', 'GalleryController@unpublishGallery');
	Route::post('gallery/delete', 'GalleryController@deleteGallery');
	Route::get('gallery/edit/{id}', 'GalleryController@getEditGallery'); 
	Route::post('gallery/edit/{id}', 'GalleryController@editGallery');
	
	Route::get('categories', 'CategoryController@getCategories');
	Route::get('category/add', 'CategoryController@getAddCategory');
	Route::post('category/add', 'CategoryController@addCategory');
	Route::post('category/publish', 'CategoryController@publishCategory');
	Route::post('category/unpublish', 'CategoryController@unpublishCategory');
	Route::post('category/delete', 'CategoryController@deleteCategory');
	Route::get('category/edit/{id}', 'CategoryController@getEditCategory'); 
	Route::post('category/edit/{id}', 'CategoryController@editCategory');
  
  Route::get('brands', 'BrandController@getBrands');
	Route::get('brand/add', 'BrandController@getAddBrand');
	Route::post('brand/add', 'BrandController@addBrand');
	Route::post('brand/publish', 'BrandController@publishBrand');
	Route::post('brand/unpublish', 'BrandController@unpublishBrand');
	Route::post('brand/delete', 'BrandController@deleteBrand');
	Route::get('brand/edit/{id}', 'BrandController@getEditBrand'); 
	Route::post('brand/edit/{id}', 'BrandController@editBrand');
	
	Route::get('contents', 'ContentController@getContents');
	Route::get('content/add', 'ContentController@getAddContent');
	Route::post('content/add', 'ContentController@addContent');
	Route::post('content/publish', 'ContentController@publishContent');
	Route::post('content/unpublish', 'ContentController@unpublishContent');
	Route::post('content/delete', 'ContentController@deleteContent');
	Route::get('content/edit/{id}', 'ContentController@getEditContent'); 
	Route::post('content/edit/{id}', 'ContentController@editContent');
	Route::post('content/removeImage',array('as'=>'admin.content.image.delete','uses'=> 'ContentController@removeContentImage'));

	Route::get('faqs', 'FaqController@getFaqs');
	Route::get('faq/add', 'FaqController@getAddFaq');
	Route::post('faq/add', 'FaqController@addFaq');
	Route::post('faq/publish', 'FaqController@publishFaq');
	Route::post('faq/unpublish', 'FaqController@unpublishFaq');
	Route::post('faq/delete', 'FaqController@deleteFaq');
	Route::get('faq/edit/{id}', 'FaqController@getEditFaq'); 
	Route::post('faq/edit/{id}', 'FaqController@editFaq');
	
	Route::get('news', 'NewsController@getNews');
	Route::get('news/add', 'NewsController@getAddNews');
	Route::post('news/add', 'NewsController@addNews');
	Route::post('news/publish', 'NewsController@publishNews');
	Route::post('news/unpublish', 'NewsController@unpublishNews');
	Route::post('news/delete', 'NewsController@deleteNews');
	Route::get('news/edit/{id}', 'NewsController@getEditNews'); 
	Route::post('news/edit/{id}', 'NewsController@editNews');

		Route::get('events', 'EventsController@getEvents');
	Route::get('events/add', 'EventsController@getAddEvents');
	Route::post('events/add', 'EventsController@addEvents');
	Route::post('events/publish', 'EventsController@publishEvents');
	Route::post('events/unpublish', 'EventsController@unpublishEvents');
	Route::post('events/delete', 'EventsController@deleteEvents');
	Route::get('events/edit/{id}', 'EventsController@getEditEvents'); 
	Route::post('events/edit/{id}', 'EventsController@editEvents');

	Route::get('products', 'ProductController@getProducts');
	Route::get('product/add', 'ProductController@getAddProduct');
	Route::post('product/add', 'ProductController@addProduct');
	Route::post('product/publish', 'ProductController@publishProduct');
	Route::post('product/unpublish', 'ProductController@unpublishProduct');
	Route::post('product/delete', 'ProductController@deleteProduct');
	Route::get('product/edit/{id}', 'ProductController@getEditProduct'); 
	Route::post('product/edit/{id}', 'ProductController@editProduct');	
	
	Route::get('users', 'UserController@getUsers');
	Route::get('user/add', 'UserController@getAddUser');
	Route::post('user/add', 'UserController@addUser');
	Route::post('user/publish', 'UserController@publishUser');
	Route::post('user/unpublish', 'UserController@unpublishUser');
	Route::post('user/delete', 'UserController@deleteUser');
	Route::get('user/edit/{id}', 'UserController@getEditUser'); 
	Route::post('user/edit/{id}', 'UserController@editUser');
	Route::get('user/change-password/{id}', 'UserController@getUserChangePassword');
	Route::post('user/change-password/{id}', 'UserController@passwordChangeUser');
	Route::get('user/showprofile/{id}', 'UserController@showProfile');
	
	Route::get('testimonials', 'TestimonialController@getTestimonials');
	Route::get('testimonial/add', 'TestimonialController@getAddTestimonial');
	Route::post('testimonial/add', 'TestimonialController@addTestimonial');
	Route::post('testimonial/publish', 'TestimonialController@publishTestimonial');
	Route::post('testimonial/unpublish', 'TestimonialController@unpublishTestimonial');
	Route::post('testimonial/delete', 'TestimonialController@deleteTestimonial');
	Route::get('testimonial/edit/{id}', 'TestimonialController@getEditTestimonial'); 
	Route::post('testimonial/edit/{id}', 'TestimonialController@editTestimonial');
  
  
  Route::get('whyuss', 'WhyusController@getWhyUs');
	Route::get('whyus/add', 'WhyusController@getAddWhyUs');
	Route::post('whyus/add', 'WhyusController@addWhyUs');
	Route::post('whyus/publish', 'WhyusController@publishWhyUs');
	Route::post('whyus/unpublish', 'WhyusController@unpublishWhyUs');
	Route::post('whyus/delete', 'WhyusController@deleteWhyUs');
	Route::get('whyus/edit/{id}', 'WhyusController@getEditWhyUs'); 
	Route::post('whyus/edit/{id}', 'WhyusController@editWhyUs');
	
	Route::get('orders', 'OrderController@getOrders');
	Route::get('order-detail/{id}', 'OrderController@getOrderDetail');
	Route::post('order/change_status', 'OrderController@orderChangeStatus');
	
	Route::get('seos', 'SeoController@getSeos');
	Route::get('seo/add', 'SeoController@getAddSeo');
	Route::post('seo/add', 'SeoController@addSeo');
	Route::get('seo/edit/{id}', 'SeoController@getEditSeo'); 
	Route::post('seo/edit/{id}', 'SeoController@editSeo');
	
	Route::get('constants', 'ConstantController@getConstants');
	Route::get('constant/add', 'ConstantController@getAddConstant');
	Route::post('constant/add', 'ConstantController@addConstant');
	Route::get('constant/edit/{id}', 'ConstantController@getEditConstant'); 
	Route::post('constant/edit/{id}', 'ConstantController@editConstant');

	Route::get('cities', 'CityController@getCities');
	Route::get('city/add', 'CityController@getAddCity');
	Route::post('city/add', 'CityController@addCity');
	Route::get('city/edit/{id}', 'CityController@getEditCity'); 
	Route::post('city/edit/{id}', 'CityController@editCity');
	Route::post('city/publish', 'CityController@publishCity');
	Route::post('city/unpublish', 'CityController@unpublishCity');
	Route::post('city/delete', 'CityController@deleteCity');
	
	Route::get('retailers', 'RetailerController@getRetailers');
	Route::get('retailer/add', 'RetailerController@getAddRetailer');
	Route::post('retailer/add', 'RetailerController@addRetailer');
	Route::get('retailer/edit/{id}', 'RetailerController@getEditRetailer'); 
	Route::post('retailer/edit/{id}', 'RetailerController@editRetailer');
	Route::post('retailer/publish', 'RetailerController@publishRetailer');
	Route::post('retailer/unpublish', 'RetailerController@unpublishRetailer');
	Route::post('retailer/delete', 'RetailerController@deleteRetailer');	
  
  Route::get('services', 'ServiceController@getServices');
	Route::get('service/add', 'ServiceController@getAddService');
	Route::post('service/add', 'ServiceController@addService');
	Route::post('service/publish', 'ServiceController@publishService');
	Route::post('service/unpublish', 'ServiceController@unpublishService');
	Route::post('service/delete', 'ServiceController@deleteService');
	Route::get('service/edit/{id}', 'ServiceController@getEditService'); 
	Route::post('service/edit/{id}', 'ServiceController@editService');
  
  Route::get('profiles', 'ProfileController@getProfiles');
	Route::get('profile/add', 'ProfileController@getAddProfile');
	Route::post('profile/add', 'ProfileController@addProfile');
	Route::post('profile/publish', 'ProfileController@publishProfile');
	Route::post('profile/unpublish', 'ProfileController@unpublishProfile');
	Route::post('profile/delete', 'ProfileController@deleteProfile');
	Route::get('profile/edit/{id}', 'ProfileController@getEditProfile'); 
	Route::post('profile/edit/{id}', 'ProfileController@editProfile');
  
  Route::get('portfolios', 'PortfolioController@getPortfolios');
	Route::get('portfolio/add', 'PortfolioController@getAddPortfolio');
	Route::post('portfolio/add', 'PortfolioController@addPortfolio');
	Route::post('portfolio/publish', 'PortfolioController@publishPortfolio');
	Route::post('portfolio/unpublish', 'PortfolioController@unpublishPortfolio');
	Route::post('portfolio/delete', 'PortfolioController@deletePortfolio');
	Route::get('portfolio/edit/{id}', 'PortfolioController@getEditPortfolio'); 
	Route::post('portfolio/edit/{id}', 'PortfolioController@editPortfolio');
  
  Route::get('projects', 'ProjectController@getProjects');
	Route::get('project/add', 'ProjectController@getAddProject');
	Route::post('project/add', 'ProjectController@addProject');
	Route::post('project/publish', 'ProjectController@publishProject');
	Route::post('project/unpublish', 'ProjectController@unpublishProject');
	Route::post('project/delete', 'ProjectController@deleteProject');
	Route::get('project/edit/{id}', 'ProjectController@getEditProject'); 
	Route::post('project/edit/{id}', 'ProjectController@editProject');
  
  Route::get('slcstars', array('as'=>'admin.slcstar.list',
             'uses'=>'SlcstarController@getSlcstars'));
	Route::get('slcstar/add', 'SlcstarController@getAddSlcstar');
	Route::post('slcstar/add', 'SlcstarController@addSlcstar');
	Route::post('slcstar/publish', 'SlcstarController@publishSlcstar');
	Route::post('slcstar/unpublish', 'SlcstarController@unpublishSlcstar');
	Route::post('slcstar/delete', 'SlcstarController@deleteSlcstar');
	Route::get('slcstar/edit/{id}', 'SlcstarController@getEditSlcstar'); 
	Route::post('slcstar/edit/{id}', 'SlcstarController@editSlcstar');
  
  Route::get('achievements/{type}', array('as'=>'admin.achievement.list',
             'uses'=>'AchievementController@getAchievements'));
	Route::get('achievement/add/{type}', 'AchievementController@getAddAchievement');
	Route::post('achievement/add/{type}', 'AchievementController@addAchievement');
	Route::post('achievement/publish', 'AchievementController@publishAchievement');
	Route::post('achievement/unpublish', 'AchievementController@unpublishAchievement');
	Route::post('achievement/delete', 'AchievementController@deleteAchievement');
	Route::get('achievement/edit/{id}/{type}', 'AchievementController@getEditAchievement'); 
	Route::post('achievement/edit/{id}/{type}', 'AchievementController@editAchievement');

});

Route::get('/', array('as' => 'home',
      'uses'=>'FrontendController@getHome'));


Route::group(array('prefix' => 'api'), function()
{	

	Route::get('test', ['as'=>'api.test','uses'=>'ApiController@test']);
	Route::get('showMenu', ['as'=>'api.showMenu','uses'=>'ApiController@showMenu']);
	Route::get('contents', ['as'=>'api.contents','uses'=>'ApiController@getAllContents']);
	Route::get('news', ['as'=>'api.news','uses'=>'ApiController@getAllNews']);
	Route::get('events', ['as'=>'api.events','uses'=>'ApiController@getAllEvents']);
	Route::get('gallery', ['as'=>'api.gallery','uses'=>'ApiController@getAllGalleries']);
	Route::get('banners', ['as'=>'api.banners','uses'=>'ApiController@getAllBanners']);
	Route::get('notices', ['as'=>'api.notices','uses'=>'ApiController@getAllNotices']);
	Route::get('upcoming-events', ['as'=>'api.upcoming.events','uses'=>'ApiController@getAllComingEvents']);

});

Route::get('testimonial/{testimonial_company}', array('as' => 'testimonial',
      'uses'=> 'FrontendController@getTestimonialDetail'));
Route::get('testimonial', array('as' => 'testimonial-list',
      'uses'=>'FrontendController@getTestimonialList'));
Route::get('testimonial/{slug}', ['as'=>'testimonial.details', 'uses'=>'FrontendController@getTestimonailDetails']);
Route::get('collection', 'FrontendController@getAllProducts');
Route::get('collection/{url}', 'FrontendController@getProductsCategoryWise');

Route::get('gallery', 'FrontendController@getGallery');

Route::get('services1', array('as' => 'services1',
      'uses'=>'FrontendController@getServices'));
Route::get('profiles/{url}', array('as' => 'profile',
      'uses'=>'FrontendController@getProfileDetail'));
Route::get('profiles', array('as' => 'profiles',
      'uses'=>'FrontendController@getProfiles'));

Route::get('brands/{url}', array('as' => 'brand',
      'uses'=>'FrontendController@getBrandDetail'));
Route::get('brands', array('as' => 'brands',
      'uses'=>'FrontendController@getBrands'));

Route::get('categories/{url}', array('as' => 'category',
      'uses'=>'FrontendController@getCategoryDetail'));
Route::get('categories', array('as' => 'categories',
      'uses'=>'FrontendController@getCategories'));

Route::get('product/{url}', array('as' => 'product',
      'uses'=>'FrontendController@getProductDetail'));
Route::post('product/{url}', array('as' => 'product',
      'uses'=>'FrontendController@postProductDetail'));

Route::get('products', array('as' => 'products',
      'uses'=>'FrontendController@getProducts'));

Route::get('news', array('as' => 'news-list',
      'uses'=>'FrontendController@getNewsList'));
Route::get('news/{url}', array('as' => 'news',
      'uses'=> 'FrontendController@getNewsDetail'));

Route::get('events', array('as' => 'events-list',
      'uses'=>'FrontendController@getEventsList'));
Route::get('events/{url}', array('as' => 'events',
      'uses'=> 'FrontendController@getEventsDetail'));

Route::get('faq', array('as' => 'faq',
      'uses'=>'FrontendController@getFAQ'));
Route::get('portfolios', array('as' => 'portfolios',
      'uses'=>'FrontendController@getPortfolios'));

Route::get('contact', array('as' => 'contact',
      'uses'=>'FrontendController@getContact'));

Route::get('refereshrecapcha', array('as'=>'contact.refereshrecapcha', 'uses'=>'FrontendController@reloadCaptcha'));

Route::post('contact', array('as' => 'contact',
      'uses'=>'FrontendController@postContact'));

Route::get('projects/{type}', array('as' => 'projects',
      'uses'=>'FrontendController@getProjects'));
Route::get('project/{url}', array('as' => 'project',
      'uses'=>'FrontendController@getProjectDetail'));

Route::get('student-opinions', array('as' => 'student-opinions',
      'uses'=>'FrontendController@getStudentOpinions'));

Route::get('uc-geniuses', array('as' => 'uc-geniuses',
      'uses'=>'FrontendController@getGeniuses'));

Route::get('slc-stars-admitted', array('as' => 'slc-stars-admitted',
      'uses'=>'FrontendController@getSlcStars'));

Route::get('downloads', array('as' => 'downloads',
      'uses'=>'FrontendController@getDownloadlist'));
Route::get('sub-downloads/{category}/{id}', array('as' => 'sub-downloads',
	'uses'=>'FrontendController@getDownloadSubCategory'));
Route::get('download/{url}/{id}', array('as' => 'download',
      'uses'=>'FrontendController@getDownloads'));

Route::get('gallery', array('as' => 'gallery',
      'uses'=>'FrontendController@getAlbums'));
Route::get('album/{url}', array('as' => 'album',
      'uses'=>'FrontendController@getAlbumImages'));
Route::get('videos', array('as' => 'videos',
      'uses'=>'FrontendController@getVideos'));

Route::get('mbbs', array('as' => 'mbbs',
      'uses'=>'FrontendController@getMbbsAchievements'));
Route::get('engineering', array('as' => 'engineering',
      'uses'=>'FrontendController@getEnggAchievements'));
Route::get('outstanding-performance', array('as' => 'outstanding-performance',
      'uses'=>'FrontendController@getOutstandingPerformances'));
Route::get('achievements-in-hseb', array('as' => 'achievements-in-hseb',
      'uses'=>'FrontendController@getHsebAchievements'));
Route::get('uc-plus-2-toppers', array('as' => 'uc-plus-2-toppers',
      'uses'=>'FrontendController@getPlusTwoToppers'));

Route::get('write-to-us', array('as' => 'onlineapplication',
      'uses'=>'FrontendController@getOnlineApplication'));
Route::post('write-to-us', array('as' => 'onlineapplication',
      'uses'=>'FrontendController@postOnlineApplication'));

Route::get('cevents', array('as' => 'calendar-events-list',
      'uses'=>'FrontendController@getCalEventsList'));
Route::get('cevents/{url}', array('as' => 'calendar-events',
      'uses'=> 'FrontendController@getCalEventsDetail'));

Route::get('login', 'FrontendController@getClientLogin');
Route::post('login', 'FrontendController@postClientLogin');
Route::get('logout', 'FrontendController@getClientLogout');
Route::get('change-password', 'FrontendController@getChangePassword');
Route::post('change-password', 'FrontendController@changePassword');
Route::post('add-to-cart', 'FrontendController@addCart');
Route::post('delete-from-cart', 'FrontendController@deleteCart');
Route::post('get-cart-total', 'FrontendController@getCartTotal');
Route::get('view-cart', 'FrontendController@viewCart');
Route::get('checkout', 'FrontendController@getCheckout');
Route::get('confirm-order', 'FrontendController@ConfirmOrder');

Route::get('calendarevents', array('as' => 'calendarevents',
      'uses'=>'FrontendController@calendarEvents'));

Route::get('search', array('as' => 'search',
      'uses'=>'FrontendController@getGoogleSearch'));
Route::get('success', 'FrontendController@getSuccess');
Route::get('{url}', ['as'=>'content.detail','uses'=>'FrontendController@getContentDetail']);
// Route::get('{url}', 'FrontendController@getContentDetail');
Route::get('{parenturl}/{childurl}', 'FrontendController@getContentParentDetail');




App::missing(function()
{
	return Redirect::to('404');
});